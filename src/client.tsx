import React from "react"
import ReactDOM from "react-dom"
import { runWebPCheck } from "./common/dom/helpers/webp"
import { assignStores } from "./modules/core/helpers/globalStores"
import { getSetting } from "./modules/settings/helpers/getSetting"
import { instantiateConsumers } from "./modules/realtime/consumers"
import { createManager } from "./common/state/manager"
import { managerContext } from "./common/state/contexts/managerContext"
import { spawnDiscontinuedModal } from "./modules/core/actions/spawnDiscontinuedModal"
import { hydrateStores } from "./common/state/helpers/hydrateStores"

const manager = createManager()
const debugSetting = getSetting(manager, "general", "debug")

export async function render() {
  const { App } = await import("./modules/core/components/App")
  const element = document.querySelector("#app")!

  const wrapInContext = (element: React.ReactNode) => {
    return (
      <managerContext.Provider value={manager}>
        {element}
      </managerContext.Provider>
    )
  }

  if (false /*element.hasChildNodes() Hydration is really broken*/) {
    // ReactDOM.hydrate(wrapInContext(<App />), element)
  } else {
    ReactDOM.render(wrapInContext(<App />), element)
  }
}

export async function start() {
  instantiateConsumers(manager)
  hydrateStores(manager)

  await manager.init()
  await runWebPCheck()
  await render()

  if (module.hot) {
    module.hot.accept("modules/core/components/App", render)
  }

  if (!localStorage.getItem("dismissed-discontinuation")) {
    spawnDiscontinuedModal(manager)
  }

  debugSetting.setHandler((enabled) => {
    if (enabled) {
      assignStores(manager, window)
    } else {
      delete (window as any).stores
    }
  })
}

start()
