import "babel-polyfill"
import "pepjs"
import "url-search-params-polyfill"

Object.fromEntries =
  Object.fromEntries ||
  function fromEntries(iterable: any[]) {
    return [...iterable].reduce(
      (obj, { 0: key, 1: val }) => Object.assign(obj, { [key]: val }),
      {},
    )
  }
