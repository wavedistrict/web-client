import "regenerator-runtime/runtime"

import "dotenv/config"

import Koa from "koa"
import Router from "koa-router"

import React from "react"
import { renderToNodeStream } from "react-dom/server"

import serve from "koa-static"
import conditional from "koa-conditional-get"
import isBot from "isbot"

import {
  BUILD_PUBLIC_FOLDER,
  SERVER_SUPPORTED_ENCODINGS,
} from "./modules/core/constants"
import { Head } from "./modules/core/components/Head"
import { App } from "./modules/core/components/App"
import { createManager } from "./common/state/manager"

import { getHTML } from "./common/state/helpers/getHTML"
import { createStateSettler } from "./common/state/helpers/createStateSettler"
import { promisifyPipe } from "./common/state/helpers/promisifyPipe"
import { Theme } from "./modules/theme/components/Theme"
import { managerContext } from "./common/state/contexts/managerContext"

const app = new Koa()
const router = new Router()

router.use(conditional())

router.use(
  serve(BUILD_PUBLIC_FOLDER, {
    index: false,
  }),
)

app.use(async (context, next) => {
  try {
    await next()
  } catch (error) {
    console.error(error)

    context.status = 500
    context.body = {
      message: "Internal server error",
    }
  }
})

router.get("*", async (context) => {
  context.set("Content-Type", "text/html")

  const userAgent = context.get("User-Agent")
  const html = getHTML(isBot(userAgent))

  const encoding = context.acceptsEncodings(
    Object.keys(SERVER_SUPPORTED_ENCODINGS),
  ) as keyof typeof SERVER_SUPPORTED_ENCODINGS | false

  if (!encoding) return context.throw(406)
  if (encoding !== "identity") context.set("Content-Encoding", encoding)

  const stream = SERVER_SUPPORTED_ENCODINGS[encoding]()

  const manager = createManager()
  const { routingStore } = manager.stores

  await manager.init()

  routingStore.location = {
    state: undefined,
    pathname: context.path,
    search: context.search,
    hash: "",
  }

  const wrapInContext = (element: React.ReactNode) => {
    return (
      <managerContext.Provider value={manager}>
        <Theme>{element}</Theme>
      </managerContext.Provider>
    )
  }

  const app = wrapInContext(<App />)
  const safeRender = createStateSettler(manager)

  // Wait for async
  await safeRender(app)

  const newPathname = routingStore.location.pathname

  if (newPathname !== context.path) {
    return context.redirect(newPathname)
  }

  stream.write(html.start)
  await promisifyPipe(renderToNodeStream(wrapInContext(<Head />)), stream)
  stream.write(html.bundles)
  await promisifyPipe(renderToNodeStream(app), stream)
  stream.write(html.app)

  stream.end()

  context.status = routingStore.status
  context.body = stream

  manager.reset()
})

app.use(router.middleware())

const port = Number(process.env.PORT) || 9000
const server = app.listen(port)

server.on("listening", () => {
  console.log(`Listening on ${port}`)
})
