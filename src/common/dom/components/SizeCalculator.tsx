import React, { useEffect, useRef, useState } from "react"
import { MaybeRecord } from "../../lang/object/types"
import { IS_SERVER } from "../../../modules/core/constants"

export interface Dimensions {
  width: number
  height: number
}

export interface SizeCalculatorProps {
  id?: string
  absolute?: boolean
  children: (size: Dimensions) => JSX.Element | null
}

const sizeCache: MaybeRecord<Dimensions> = {}

export function SizeCalculator(props: SizeCalculatorProps) {
  const { id, absolute, children } = props
  const ref = useRef<HTMLDivElement>(null)

  const [hasSize, setHasSize] = useState(false)

  const [width, setWidth] = useState(IS_SERVER ? 0 : window.innerWidth)
  const [size, setSize] = useState<Dimensions | undefined>(undefined)

  useEffect(() => {
    calculateSize()

    const invalidate = () => {
      if (width !== window.innerWidth) {
        setHasSize(false)
      }
    }

    window.addEventListener("resize", invalidate)

    return () => {
      window.removeEventListener("resize", invalidate)
    }
  })

  const calculateSize = () => {
    const { current } = ref

    if (current) {
      const width = current.offsetWidth
      const height = current.offsetHeight

      if (id) {
        setSize((sizeCache[id] = { width, height }))
      } else {
        setSize({ width, height })
      }

      setWidth(window.innerWidth)
      setHasSize(true)
    }
  }

  const renderSize = (id && sizeCache[id]) || size
  if (hasSize && renderSize) return children(renderSize)

  return (
    <div
      ref={ref}
      data-size={JSON.stringify(size || {})}
      style={{
        width: "100%",
        height: "100%",
        position: absolute ? "absolute" : "relative",
      }}
    >
      {children({
        height: 0,
        width: 0,
      })}
    </div>
  )
}
