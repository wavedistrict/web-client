import { MemoryStorage } from "./MemoryStorage";
import { StoredValue } from "./StoredValue";

const setup = () => {
  const storageKey = "test"
  const data = { hello: { world: 123 } }
  const memoryStorage = new MemoryStorage()
  const storage = new StoredValue<typeof data | undefined>(
    storageKey,
    undefined,
    memoryStorage,
  )
  return { storage, data, storageKey, memoryStorage }
}

test("save - only saves if restored first, should save stringified data", () => {
  const { storage, data, storageKey, memoryStorage } = setup()

  storage.save(data)
  expect(memoryStorage.getItem(storageKey)).toBeNull()

  storage.restore()
  storage.save(data)
  expect(memoryStorage.getItem(storageKey)).toBe(JSON.stringify(data))
})

test("restore", () => {
  const { storage, data, storageKey, memoryStorage } = setup()

  memoryStorage.setItem(storageKey, JSON.stringify(data))
  expect(storage.restore()).toEqual(data)
})

test("delete", () => {
  const { storage, data, storageKey, memoryStorage } = setup()

  memoryStorage.setItem(storageKey, JSON.stringify(data))
  storage.delete()
  expect(memoryStorage.getItem(storageKey)).toBeNull()
})
