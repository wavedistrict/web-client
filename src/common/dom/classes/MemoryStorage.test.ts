import { MemoryStorage } from "./MemoryStorage"

describe("MemoryStorage", () => {
  it("has a correct length", () => {
    const storage = new MemoryStorage()

    expect(storage.length).toBe(0)

    storage.setItem("ur", "123")
    storage.setItem("mom", "123")
    storage.setItem("lol", "123")

    expect(storage.length).toBe(3)
  })

  it("clears items", () => {
    const storage = new MemoryStorage()

    storage.setItem("test", "123")

    expect(storage.length).toBe(1)

    storage.clear()

    expect(storage.length).toBe(0)
  })

  it("sets and gets stored items", () => {
    const storage = new MemoryStorage()
    storage.setItem("test", "123")

    expect(storage.getItem("test")).toBe("123")
  })

  it("removes items", () => {
    const storage = new MemoryStorage()
    storage.setItem("test", "123")

    expect(storage.getItem("test")).toBe("123")

    storage.removeItem("test")

    expect(storage.getItem("test")).toBeNull()
  })

  it("returns null if the item doesn't exist", () => {
    const storage = new MemoryStorage()
    expect(storage.getItem("doesn't exist")).toBeNull()
  })

  it("can get an item by index", () => {
    const storage = new MemoryStorage()
    storage.setItem("test", "123")

    expect(storage.key(0)).toBe("123")
  })
})
