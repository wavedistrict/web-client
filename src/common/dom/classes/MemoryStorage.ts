export class MemoryStorage implements Storage {
  private items = new Map<string, string>()

  public get length() {
    return this.items.size
  }

  public clear(): void {
    this.items.clear()
  }

  public getItem(key: string): string | null {
    const value = this.items.get(key)
    return value != undefined ? value : null
  }

  public removeItem(key: string): void {
    this.items.delete(key)
  }

  public setItem(key: string, value: string): void {
    this.items.set(key, value)
  }

  public key(index: number): string | null {
    const entries = [...this.items.entries()]
    const value = entries[index][1]
    return value != undefined ? value : null
  }
}
