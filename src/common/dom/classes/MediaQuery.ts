import { observable } from "mobx"
import { IS_SERVER } from "../../../modules/core/constants"

export class MediaQuery {
  private media?: MediaQueryList
  @observable public matches: boolean

  constructor(query: string) {
    if (IS_SERVER) {
      this.matches = false
      return
    }

    this.media = window.matchMedia(query)
    this.matches = this.media.matches

    this.media.addListener(() => {
      this.matches = this.media!.matches
    })
  }
}
