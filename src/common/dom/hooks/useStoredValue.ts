import { useState } from "react"
import { StoredValue } from "../classes/StoredValue"
import { useWindowEvent } from "../../ui/react/hooks/useWindowEvent"

export const useStoredValue = <T>(key: string, defaultValue: T) => {
  const [storage] = useState(() => new StoredValue(key, defaultValue))
  const [value, setValue] = useState(() => storage.restore())

  useWindowEvent("storage", (change) => {
    if (change.key === key) {
      setValue(change.newValue as any)
    }
  })

  const set = (newValue: T) => {
    storage.save(newValue)
    setValue(newValue)
  }

  return [value, set] as const
}
