export const getFixedParent = (element: Element | null): Element | null => {
  if (!element) return null

  if (getComputedStyle(element).position === "fixed") {
    return element
  } else {
    return getFixedParent(element.parentElement)
  }
}
