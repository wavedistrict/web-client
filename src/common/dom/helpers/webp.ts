// sources:
// https://stackoverflow.com/a/5573422/1332403
// https://github.com/Modernizr/Modernizr/blob/a9369b031d05b90231b572e2796afba0334b21e6/feature-detects/img/webp.js#L43

const testImageUrl =
  "data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA="

let checkResult: boolean | undefined

export const canUseWebP = (): boolean => checkResult || false

export const runWebPCheck = (): Promise<boolean> => {
  if (checkResult !== undefined) {
    return Promise.resolve(checkResult)
  }

  return new Promise((resolve, reject) => {
    const image = new Image()
    image.onload = () => resolve((checkResult = true))
    image.onerror = () => resolve((checkResult = false))
    image.src = testImageUrl
  })
}
