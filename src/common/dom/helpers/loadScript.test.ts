import { ConsoleMock } from '../../testing/classes/ConsoleMock';
import { loadScript } from "./loadScript";

const consoleMock = new ConsoleMock(["log", "error"])

beforeAll(() => {
  consoleMock.apply()
})

afterAll(() => {
  consoleMock.restore()
})

describe("loadScript", () => {
  it("resolves on load success", async () => {
    const result = await loadScript("data:text/javascript,console.log('hi')")
    expect(result).toBeUndefined()
    expect(consoleMock.mocks.log).toBeCalled()
  })

  it("errors on load failure", async () => {
    let caught = undefined

    try {
      await loadScript("oops")
    } catch (error) {
      caught = error
    }

    expect(caught).not.toBeUndefined()
  })
})
