export const getScrollableParent = (
  element: Element | null,
): Window | Element => {
  if (!element) return window

  if (element.hasAttribute("data-scrollable")) {
    return element
  } else {
    return getScrollableParent(element.parentElement)
  }
}
