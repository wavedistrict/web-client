import { loadScript } from "./loadScript"

const recaptchaScriptUrl =
  "https://www.google.com/recaptcha/api.js?render=explicit&onload=onCaptchaLoadCallback"

export const loadRecaptcha = (): Promise<Recaptcha> => {
  return new Promise((resolve) => {
    if (window.grecaptcha) {
      return resolve(window.grecaptcha)
    }

    window.onCaptchaLoadCallback = () => resolve(window.grecaptcha)
    loadScript(recaptchaScriptUrl)
  })
}
