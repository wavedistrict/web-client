import { clamp } from "./clamp"

test("clamp", () => {
  expect(clamp(100, -100, 200)).toBe(100)

  expect(clamp(100, -100, 50)).toBe(50)

  expect(clamp(-200, -100, 50)).toBe(-100)

  expect(clamp(200, 300, 500)).toBe(300)
})
