import { AnyFunction } from "../../lang/function/types"

type ConsoleKey = keyof Console

export interface ConsoleMockOptions {
  silent?: boolean
}

/**
 * A utility class used to mock and check console calls in tests
 *
 * @template TMethod A union of all the mocked console methods, e.g. "log" | "warn" | "error"
 */
export class ConsoleMock<TMethod extends ConsoleKey> {
  private original = {} as Record<TMethod, AnyFunction>
  public mocks = {} as Record<TMethod, jest.Mock>

  constructor(mockedMethodNames: TMethod[], options: ConsoleMockOptions = {}) {
    const { silent = true } = options

    for (const methodName of mockedMethodNames) {
      // keep the original console method for restoring later
      this.original[methodName] = console[methodName]

      // create a mock function of the given console method
      // if we pass options.silent, the mock will not log to the console
      this.mocks[methodName] = silent ? jest.fn() : jest.fn(console[methodName])
    }
  }

  public apply() {
    Object.assign(global.console, this.mocks)
    return this
  }

  public restore() {
    Object.assign(global.console, this.original)
    return this
  }
}
