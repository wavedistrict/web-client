import React from "react"
import { render } from "react-testing-library"
import { Theme } from "../../../modules/theme/components/Theme"

// Use this function when a component is using a theme
export const renderWithTheme = (element: JSX.Element) =>
  render(<Theme>{element}</Theme>)
