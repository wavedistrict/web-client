import { bind } from '../../lang/function/helpers/bind';
import { Emitter } from '../../state/classes/Emitter';

/** Makes communicating with a specific service on the worker more trivial */
export class WorkerChannel<
  EventMap extends object,
  SendMap extends object
> extends Emitter<EventMap> {
  private worker: Worker
  private serviceName: string

  constructor(worker: Worker, serviceName: string) {
    super()

    this.worker = worker
    this.serviceName = serviceName

    this.worker.addEventListener("message", this.handleMessage)
  }

  @bind
  private handleMessage(message: any) {
    const { type, serviceName, data } = message

    if (serviceName === this.serviceName) {
      this.emit(type, data)
    }
  }

  @bind
  public send<E extends keyof SendMap>(type: E, data: SendMap[E]) {
    this.worker.postMessage({ type, data, serviceName: this.serviceName })
  }
}
