import React from "react"
import { bind } from "../../lang/function/helpers/bind"

export interface PlaceholderOptions<Props> {
  className: string
  renderElement: (props: Props) => React.ReactNode
}

export class Placeholder<Props extends object> {
  private cache: Record<string | number, React.ReactNode> = {}

  private className: PlaceholderOptions<Props>["className"]
  private renderElement: PlaceholderOptions<Props>["renderElement"]

  constructor(options: PlaceholderOptions<Props>) {
    const { className, renderElement } = options

    this.className = className
    this.renderElement = renderElement
  }

  private getElement(props: Props, key: string | number) {
    const existing = this.cache[key]
    if (existing) return existing

    const element = this.renderElement(props)
    this.cache[key] = element

    return element
  }

  @bind
  public render({
    props,
    key,
    ref,
  }: {
    props: Props
    key: string | number
    ref?: React.Ref<any>
  }): React.ReactNode {
    const element = this.getElement(props, key)

    return React.createElement(
      "div",
      {
        className: this.className,
        key,
        ref,
      },
      element,
    )
  }
}
