import { useState, useEffect, PropsWithChildren } from "react"
import { USER_FRIENDLY_DELAY } from "../../../modules/core/constants"
import React from "react"

export function DelayedRenderer(props: PropsWithChildren<{}>) {
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    const timeout = setTimeout(() => {
      setVisible(true)
    }, USER_FRIENDLY_DELAY)

    return () => clearTimeout(timeout)
  }, [])

  return <>{visible ? props.children : null}</>
}
