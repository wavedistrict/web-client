import { Manager } from "../../state/types/Manager"

export const debug = (manager: Manager, message: string) => {
  const { configStore, toastStore } = manager.stores

  if (configStore.debug)
    toastStore.addToast({
      content: `DEBUG: ${message}`,
      variant: "debug",
      duration: 10000,
    })
}
