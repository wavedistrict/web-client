import { ThemedCSS } from "../../../../modules/theme/types/ThemedCSS"

export type ThemedButton<T extends string> = ThemedCSS<Record<T, boolean>>
