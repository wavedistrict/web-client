import { ButtonProps } from "../components/Button"

export interface ButtonTypeBase<Variant extends string> {
  variants?: (Variant | "")[]
}

export type ButtonType<Variant extends string> = ButtonTypeBase<Variant> &
  Omit<ButtonProps, "className" | "variants">
