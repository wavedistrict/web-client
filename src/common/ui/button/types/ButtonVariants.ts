export type ButtonVariants =
  | "small"
  | "flat"
  | "stretch"
  | "active"
  | "disabled"
