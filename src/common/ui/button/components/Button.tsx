import React, { ComponentPropsWithoutRef } from "react"
import { IconType } from "../../icon"
import { Icon } from "../../icon/components/Icon"
import { styled } from "../../../../modules/theme/themes"

export type ButtonVariants = {
  stretch?: boolean
}

export type ButtonProps = ComponentPropsWithoutRef<"button"> &
  ButtonVariants & {
    className?: string
    icon?: IconType
    label?: string
    onClick: () => void
  }

const Root = styled.button<ButtonVariants>`
  display: inline-block;
  pointer-events: none;

  ${(props) =>
    props.stretch &&
    `
    width: 100%;
  `}
`

const Container = styled.span<ButtonVariants>`
  display: flex;
  width: 100%;

  pointer-events: all;
  user-select: none;

  ${(props) =>
    props.stretch &&
    `
    width: 100%;
  `}
`

function _Button(props: ButtonProps, ref: React.Ref<HTMLButtonElement>) {
  const { icon, stretch = false, label, className, children, ...rest } = props

  const renderIcon = () => {
    if (!icon) return null

    return <Icon className="icon" name={icon} />
  }

  const renderLabel = () => {
    if (!label) return null

    return <div className="label">{label}</div>
  }

  return (
    <Root type="button" stretch={stretch} ref={ref} {...rest}>
      <Container className={className} stretch={stretch}>
        {renderIcon()}
        {renderLabel()}
        {children}
      </Container>
    </Root>
  )
}

export const Button = React.forwardRef(_Button)
