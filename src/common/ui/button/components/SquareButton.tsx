import { size } from "polished"
import { styled } from "../../../../modules/theme/themes"
import { Button } from "./Button"
import { getDuration } from "../../../../modules/theme/helpers"

export const SquareButton = styled<typeof Button, { size: number }>(Button)`
  ${(props) => size(props.size)}

  justify-content: center;
  align-items: center;

  outline: none;

  transition: ${getDuration("normal")} ease opacity;
  opacity: 0.5;

  > .icon {
    ${size(24)}
  }

  &:hover {
    opacity: 1;
  }
`
