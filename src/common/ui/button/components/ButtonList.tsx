import { styled } from "../../../../modules/theme/themes"
import { css } from "@emotion/core"

/** Consistent spacing of buttons */
export const ButtonList = styled.div<{
  horizontal?: boolean
  compact?: boolean
}>`
  ${(props) => {
    const { horizontal = false, compact = false } = props
    const { spacings } = props.theme

    const verticalStyle = css`
      display: flex;
      flex-direction: column;

      > * + * {
        margin-top: ${compact ? spacings[3] : spacings[4]}px;
      }
    `
    const horizontalStyle = css`
      display: flex;

      > * + * {
        margin-left: ${compact ? spacings[3] : spacings[4]}px;
      }
    `
    return horizontal ? horizontalStyle : verticalStyle
  }}
`
