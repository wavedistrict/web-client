import { styled } from "../../../../modules/theme/themes"
import { PrimaryButton } from "./PrimaryButton"

export const SecondaryButton = styled(PrimaryButton)`
  background: transparent;

  ${({ theme, active }) =>
    active &&
    `
    background: ${theme.fontColors.normal};
    color: ${theme.colors.primary};

    > .icon {
      fill: ${theme.colors.primary};
    }
  `}
`
