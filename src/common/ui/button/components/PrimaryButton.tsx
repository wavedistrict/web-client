import { size } from "polished"
import { styled } from "../../../../modules/theme/themes"
import { Button } from "./Button"
import {
  getSpacing,
  getFontSize,
  getFontWeight,
  getTransparency,
  getDuration,
  getFontColor,
  getColor,
} from "../../../../modules/theme/helpers"
import css from "@emotion/css"

export type PrimaryButtonVariants = {
  active?: boolean
  outlined?: boolean
  noLabel?: boolean
  compact?: boolean
  disabled?: boolean
  flat?: boolean
}

export const PrimaryButton = styled<typeof Button, PrimaryButtonVariants>(
  Button,
)`
  align-items: center;
  justify-content: center;

  padding: ${getSpacing("3")}px ${getSpacing("4")}px;

  font-size: ${getFontSize("1")}px;
  font-weight: ${getFontWeight("bold")};
  text-transform: capitalize;

  border-radius: 50px;
  border: 2px solid transparent;

  color: ${getFontColor("normal")};
  background: ${getColor("accent")};

  transition: ${getDuration("normal")} ease;
  transition-property: background color;

  &:focus {
    outline: none;
    border-color: ${getTransparency("strongPositive")};
  }

  > .icon {
    ${size(16)}

    margin-right: ${getSpacing("3")}px;
    transition: ${getDuration("normal")} ease fill;

    &:last-child {
      margin-right: 0px;
    }
  }

  ${({ theme, active }) =>
    !active &&
    css`
      &:hover {
        background: ${theme.transparencies.weakPositive};
      }
    `}

  &:active {
    background: ${getTransparency("strongNegative")};
  }

  ${({ theme, compact }) =>
    compact &&
    css`
      padding: ${theme.spacings[2]}px ${theme.spacings[3]}px;
      font-size: ${theme.fontSizes[1]}px;

      > .icon {
        ${size(13)}
      }
    `}

  ${({ theme, noLabel, compact }) =>
    noLabel &&
    css`
      padding: ${compact ? theme.spacings[3] - 2 : theme.spacings[3]}px;

      > .label {
        display: none;
      }

      > .icon {
        ${compact ? size(13) : size(20)}
        margin-right: 0px;
      }
    `}

  ${(props) =>
    props.outlined &&
    css`
      border-radius: 3px;
      border: solid 1px ${props.theme.transparencies.weakPositive};
    `}

  ${({ theme, active }) =>
    active &&
    `
    background: ${theme.fontColors.normal};
    color: ${theme.colors.primary};

    > .icon {
      fill: ${theme.colors.primary};
    }
  `}

  ${({ disabled }) =>
    disabled &&
    css`
      opacity: 0.5;
      pointer-events: none;
    `}

  ${({ flat }) =>
    flat &&
    css`
      padding: 0px;
      border: none;

      opacity: 0.6;

      > .icon {
        ${size(16)}
      }

      &:hover {
        opacity: 1;
        background: transparent;
      }

      &:active {
        background: transparent;
      }
    `}
`
