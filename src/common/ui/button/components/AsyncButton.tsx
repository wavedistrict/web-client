import { observable } from "mobx"
import { observer } from "mobx-react"
import React from "react"
import { bind } from "../../../lang/function/helpers/bind"
import { ButtonProps } from "./Button"
import { PrimaryButton, PrimaryButtonVariants } from "./PrimaryButton"
import { SecondaryButton } from "./SecondaryButton"

export type AsyncButtonProps = ButtonProps &
  PrimaryButtonVariants & {
    onClick: () => Promise<any>
    kind: "primary" | "secondary"
  }

@observer
export class AsyncButton extends React.Component<AsyncButtonProps> {
  @observable private pending = false

  @bind
  private async onClick() {
    const { onClick } = this.props

    this.pending = true
    try {
      await onClick()
    } catch {}
    this.pending = false
  }

  public render() {
    const { kind, ...props } = this.props

    const Component = {
      primary: PrimaryButton,
      secondary: SecondaryButton,
    }[kind]

    return (
      <Component {...props} disabled={this.pending} onClick={this.onClick} />
    )
  }
}
