import { size } from "polished"
import React from "react"
import { PopoverTrigger } from "../../../../modules/popover/components/PopoverTrigger"
import {
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { IconType } from "../../icon"
import { ButtonType } from "../types/ButtonType"
import { Button } from "./Button"
import { PrimaryButton, PrimaryButtonVariants } from "./PrimaryButton"
import { SecondaryButton } from "./SecondaryButton"

export type MoreButtonProps = Omit<
  ButtonType<any>,
  "onClick" | "icon" | "title"
> &
  PrimaryButtonVariants & {
    kind: "primary" | "secondary"
    items: MoreButtonItem[]
  }

export interface MoreButtonItem {
  label: string
  icon: IconType
  onClick: () => void
}

const Popover = styled.div`
  display: flex;
  flex-direction: column;
`

const Item = styled(Button)`
  display: flex;
  align-items: center;

  padding: 12px 16px;

  min-width: 150px;
  transition: ${getDuration("normal")} ease all;

  border: 2px solid transparent;

  > .icon {
    ${size(18)};
    margin-right: ${getSpacing("4")}px;
  }

  > .label {
    flex-grow: 1;

    font-weight: ${getFontWeight("bold")};
    font-size: ${getFontSize("1")}px;
    text-transform: uppercase;
  }

  &:hover {
    background: ${getTransparency("weakPositive")};
  }

  &:active {
    background: ${getTransparency("weakNegative")};
  }

  &:focus {
    border-color: ${getTransparency("strongPositive")};
  }
`

export function MoreButton(props: MoreButtonProps) {
  const { kind, items, ...rest } = props

  if (items.length === 0) return null

  const Component = {
    primary: PrimaryButton,
    secondary: SecondaryButton,
  }[kind]

  return (
    <PopoverTrigger placement="top-end">
      {({ isActive, toggle, ref }) => (
        <Component
          ref={ref}
          icon="more"
          title="More"
          active={isActive}
          onClick={toggle}
          {...rest}
        />
      )}
      {(props) => {
        const list = items.map((item, index) => {
          const onClick = () => {
            item.onClick()
            props.close()
          }

          return <Item key={index} {...{ ...item, onClick }} />
        })

        return <Popover>{list}</Popover>
      }}
    </PopoverTrigger>
  )
}
