import { Validator } from "../../classes/FieldController"

export const validateStringReserved = (
  reserved: string[],
): Validator<string> => (value) => {
  if (reserved.includes(value)) {
    return Promise.resolve({ isValid: false, errorMessage: "Reserved word" })
  }

  return Promise.resolve({ isValid: true, errorMessage: "" })
}
