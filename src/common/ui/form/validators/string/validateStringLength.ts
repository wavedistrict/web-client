import { Validator } from "../../classes/FieldController"

interface Options {
  min?: number
  max?: number
}

export const validateStringLength = (options: Options): Validator<string> => (
  value,
) => {
  const { min, max } = options

  if (min && value.length < min)
    return Promise.resolve({
      isValid: false,
      errorMessage: `Field must be ${min} characters`,
    })

  if (max && value.length > max)
    return Promise.resolve({
      isValid: false,
      errorMessage: `Field cannot be longer than ${max} characters`,
    })

  return Promise.resolve({ isValid: true, errorMessage: "" })
}
