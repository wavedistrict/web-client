import { apiServer } from "../../../../network"
import { Validator } from "../../classes/FieldController"

interface Options {
  endpoint: string
  validator: string
  message: string
}

export const validateStringFromEndpoint = (
  options: Options,
): Validator<string> => async (value) => {
  const { endpoint, validator, message } = options

  const { data: result } = await apiServer.get<{ valid: boolean }>(endpoint, {
    params: { validator, value },
  })

  if (result.valid)
    return {
      isValid: true,
      errorMessage: "",
    }

  return {
    isValid: false,
    errorMessage: message,
  }
}
