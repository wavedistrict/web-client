import { Validator } from "../../classes/FieldController"

interface Options {
  regex: string
  warningText?: string
}

export const validateStringRegex = (options: Options): Validator<string> => (
  value,
) => {
  const { regex, warningText } = options

  const expression = new RegExp(regex)
  const matches = expression.test(value)

  if (!matches)
    return Promise.resolve({
      isValid: false,
      errorMessage: warningText || `Value does not match ${regex}`,
    })

  return Promise.resolve({ isValid: true, errorMessage: "" })
}
