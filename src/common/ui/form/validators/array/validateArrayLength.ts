import { Validator } from "../../classes/FieldController"

interface Options {
  min?: number
  max?: number
}

export const validateArrayLength = (options: Options): Validator<any[]> => (
  value,
) => {
  const { min, max } = options

  if (min && value.length < min)
    return Promise.resolve({
      isValid: false,
      errorMessage: `Add at least ${min}`,
    })

  if (max && value.length > max)
    return Promise.resolve({
      isValid: false,
      errorMessage: `Add less than ${max}`,
    })

  return Promise.resolve({ isValid: true, errorMessage: "" })
}
