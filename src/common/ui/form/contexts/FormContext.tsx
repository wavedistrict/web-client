import { createContext } from "react"
import { FormManager } from "../classes/FormManager"

interface FormContextState {
  form: FormManager<any>
  onSubmit?: () => void
}

export const FormContext = createContext<FormContextState | undefined>(
  undefined,
)
