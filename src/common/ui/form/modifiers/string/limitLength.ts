export const limitLength = (length: number) => (text: string) => {
  return text.slice(0, length)
}
