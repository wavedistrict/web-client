import { getValueFromMap } from "../../../../lang/object/helpers/getValueFromMap"

export const setCase = (type: string) => (value: string) => {
  const resultMap = {
    lower: value.toLowerCase(),
    upper: value.toUpperCase(),
  }
  return getValueFromMap(resultMap, type, value)
}
