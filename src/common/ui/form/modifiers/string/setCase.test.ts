import { setCase } from "./setCase"

describe("setCase", () => {
  it("sets lowercase", () => {
    const result = setCase("lower")("FoO")
    expect(result).toBe("foo")
  })

  it("sets uppercase", () => {
    const result = setCase("upper")("foo")
    expect(result).toBe("FOO")
  })
})
