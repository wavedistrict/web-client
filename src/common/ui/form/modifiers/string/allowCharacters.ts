export const allowCharacters = (characters: string) => (value: string) => {
  const notAllowed = new RegExp(`[^${characters}]`, "g")
  return value.replace(notAllowed, "")
}
