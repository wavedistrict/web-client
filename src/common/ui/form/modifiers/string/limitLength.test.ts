import { limitLength } from "./limitLength"

describe("limitLength", () => {
  it("limits length", () => {
    const result = limitLength(3)("fooooooo")
    expect(result).toBe("foo")
  })
})
