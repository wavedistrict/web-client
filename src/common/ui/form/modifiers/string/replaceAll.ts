export const replaceAll = (char: string, repl: string) => (value: string) => {
  return value.replace(new RegExp(`[${char}]+`, "g"), repl)
}
