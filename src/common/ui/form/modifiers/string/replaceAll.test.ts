import { replaceAll } from "./replaceAll"

describe("replaceAll", () => {
  it("replaces characters", () => {
    const result = replaceAll("abc", "-")("abc a b")
    expect(result).toBe("- - -")
  })
})
