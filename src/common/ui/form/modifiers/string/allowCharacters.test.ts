import { allowCharacters } from "./allowCharacters"

describe("allowCharacters", () => {
  it("allows whitelisted characters", () => {
    const result = allowCharacters("abc")("abcfoo")
    expect(result).toBe("abc")
  })
})
