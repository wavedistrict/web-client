import { DropdownOption } from "../../input/components/DropdownInput"

export const getDropdownOptions = (
  list: Record<string, any>[],
  key: string,
  label: string,
): DropdownOption[] =>
  list
    .sort((a, b) => a[label].localeCompare(b[label]))
    .map((type) => ({ key: type[key], label: type[label] }))
