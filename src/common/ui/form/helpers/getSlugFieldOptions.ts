import { slug } from "../expressions"
import { allowCharacters } from "../modifiers/string/allowCharacters"
import { replaceAll } from "../modifiers/string/replaceAll"
import { setCase } from "../modifiers/string/setCase"
import { validateStringLength } from "../validators/string/validateStringLength"
import { validateStringRegex } from "../validators/string/validateStringRegex"

export const getSlugFieldOptions = () => ({
  validators: [
    validateStringLength({ min: 3, max: 64 }),
    validateStringRegex({
      regex: slug,
      warningText:
        "Slug can only contain alphanumeric and dashes. Must have at least one alphabetical character.",
    }),
  ],
  modifiers: [
    setCase("lower"),
    replaceAll(" ", "-"),
    allowCharacters("a-z0-9-"),
  ],
})
