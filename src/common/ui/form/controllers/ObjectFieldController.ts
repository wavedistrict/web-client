import { FieldController } from "../classes/FieldController"

export class ObjectFieldController extends FieldController<object> {
  protected hasValue(value: object): boolean {
    return Object.keys(value).length > 0
  }
}
