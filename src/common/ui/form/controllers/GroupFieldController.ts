import { computed } from "mobx"
import {
  FieldController,
  FieldControllerOptions,
} from "../classes/FieldController"

export interface GroupFieldControllerOptions<
  O extends object,
  C extends Record<E, FieldController<O[E]>>,
  E extends keyof O
> extends FieldControllerOptions<O> {
  controllers: C
}

export class GroupFieldController<
  O extends object,
  C extends Record<E, FieldController<O[E]>>,
  E extends keyof O
> extends FieldController<O> {
  public controllers: C

  constructor(options: GroupFieldControllerOptions<O, C, E>) {
    super(options)
    this.controllers = options.controllers

    if (options.value) {
      const data = options.value as Record<string, any>

      for (const [key, controller] of Object.entries<C[E]>(this
        .controllers as any)) {
        controller.setValue(data[key])
      }
    }
  }

  protected hasValue() {
    return this.value !== undefined
  }

  @computed
  public get serializedValue(): O {
    const result: any = {}

    for (const [key, controller] of Object.entries<FieldController<any>>(
      this.controllers,
    )) {
      result[key] = controller.serializedValue
    }

    return result
  }
}
