import { wait } from "../../../lang/promise/helpers/wait"
import { FieldControllerStatus, Validator } from "../classes/FieldController"
import { validateStringLength } from "../validators/string/validateStringLength"
import { TextFieldController } from "./TextFieldController"

const asyncValidator: Validator<string> = async () => {
  await wait(15)

  return {
    isValid: true,
    errorMessage: "",
  }
}

describe("TextFieldController", () => {
  it("checks required", async () => {
    const field = new TextFieldController({
      isRequired: true,
    })

    await field.validate()
    expect(field.status).toBe(FieldControllerStatus.Invalid)

    field.setValue("foo")

    await field.validate()
    expect(field.status).toBe(FieldControllerStatus.Valid)
  })

  it("checks validity", async () => {
    const field = new TextFieldController({
      value: "f",
      validators: [
        validateStringLength({
          max: 5,
          min: 2,
        }),
      ],
    })

    await field.validate()
    expect(field.status).toBe(FieldControllerStatus.Invalid)

    field.setValue("foo")

    await field.validate()
    expect(field.status).toBe(FieldControllerStatus.Valid)
  })

  it("checks async validity", async () => {
    const field = new TextFieldController({
      value: "f",
      validators: [
        asyncValidator,
        validateStringLength({
          max: 5,
          min: 2,
        }),
      ],
    })

    await field.validate()
    expect(field.status).toBe(FieldControllerStatus.Invalid)

    field.setValue("foo")

    await field.validate()
    expect(field.status).toBe(FieldControllerStatus.Valid)

    // Check rapidly changing values such as a user typing
    field.setValue("f")
    await wait(5)
    field.setValue("fo")
    await wait(5)
    field.setValue("foo")

    const promise = field.validate()

    expect(field.isValidating).toBe(true)
    await promise

    expect(field.isValidating).toBe(false)
    expect(field.isValid).toBe(true)
  })

  it("lazily validates", () => {
    const field = new TextFieldController({
      value: "f",
      validators: [
        asyncValidator,
        validateStringLength({
          max: 5,
          min: 2,
        }),
      ],
    })

    const firstPromise = field.validate()
    const secondPromise = field.validate()

    expect(firstPromise).toStrictEqual(secondPromise)
  })
})
