import { FieldController } from "../classes/FieldController"

export class CaptchaFieldController extends FieldController<string> {
  public value = ""

  protected hasValue(): boolean {
    return this.value !== ""
  }
}
