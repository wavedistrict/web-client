import {
  FieldController,
  FieldControllerOptions,
} from "../classes/FieldController"

export class BooleanFieldController extends FieldController<boolean> {
  constructor(options: FieldControllerOptions<boolean>) {
    super({ ...options, value: options.value || false })
  }

  protected hasValue() {
    return this.value !== undefined
  }
}
