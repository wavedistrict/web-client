import { DropdownOption } from "../../input/components/DropdownInput"
import {
  FieldController,
  FieldControllerOptions,
} from "../classes/FieldController"

type ValueType = string | number | null

export interface SelectFieldControllerOptions
  extends FieldControllerOptions<ValueType> {
  options: DropdownOption[] // move the DropdownOption to a more fitting location
}

export class SelectFieldController extends FieldController<ValueType> {
  public options: DropdownOption[] = []

  protected hasValue() {
    return this.value !== undefined
  }

  constructor(options: SelectFieldControllerOptions) {
    super(options)

    this.options = options.options // lol
  }
}
