import { observable } from "mobx"
import { ProcessingMedia } from "../../../media/models/ProcessingMedia"
import { AudioReferenceMetadata } from "../../../media/types/AudioReferenceData"
import { FileUploadResult } from "../../../network/classes/FileUpload"
import { FieldControllerOptions } from "../classes/FieldController"
import { FileFieldController } from "./FileFieldController"
import { Manager } from "../../../state/types/Manager"

export interface AudioFieldControllerOptions
  extends FieldControllerOptions<File> {}

export class AudioFieldController extends FileFieldController {
  @observable public processingAudio?: ProcessingMedia<
    unknown,
    AudioReferenceMetadata
  >

  constructor(private manager: Manager, options: AudioFieldControllerOptions) {
    super({
      ...options,
      type: "track",
      kind: "audio",
    })
  }

  protected handleComplete(result: FileUploadResult) {
    const { processingAudioStore } = this.manager.stores

    // CHANGE THIS ONCE MEDIA SERVER RETURNS METADATA
    const metadata: any = {}

    this.processingAudio = processingAudioStore.add({ ...result, metadata })
    this.result = result
  }
}
