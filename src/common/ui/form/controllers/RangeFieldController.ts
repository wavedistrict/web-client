import {
  FieldController,
  FieldControllerOptions,
} from "../classes/FieldController"

export interface RangeFieldControllerOptions
  extends FieldControllerOptions<number> {
  max: number
  min?: number
  steps?: boolean | number
}

export class RangeFieldController extends FieldController<number> {
  public max: number
  public min?: number
  public steps?: boolean | number

  protected hasValue() {
    return typeof this.value === "number"
  }

  constructor(options: RangeFieldControllerOptions) {
    super(options)

    this.max = options.max
    this.min = options.min
    this.steps = options.steps
  }
}
