import { computed } from "mobx"
import {
  FieldController,
  FieldControllerOptions,
} from "../classes/FieldController"

export class TextFieldController extends FieldController<
  string,
  string | undefined | null
> {
  constructor(options: FieldControllerOptions<string>) {
    super({ ...options, value: options.value || "" })
  }

  protected hasValue(value: string): boolean {
    return value !== ""
  }

  @computed
  public get serializedValue() {
    if (this.value === "") return null
    return this.value!
  }
}
