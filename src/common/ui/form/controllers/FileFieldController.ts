import { action, computed, observable, when } from "mobx"
import {
  FileUpload,
  FileUploadError,
  FileUploadResult,
  FileUploadStatus,
} from "../../../network/classes/FileUpload"
import { QueryParams } from "../../../network/types"
import {
  FieldController,
  FieldControllerOptions,
  FieldControllerStatus,
} from "../classes/FieldController"

function noop() {}

export interface FileFieldControllerOptions
  extends FieldControllerOptions<File> {
  type: string
  kind: string
  params?: QueryParams
}

export class FileFieldController extends FieldController<
  File,
  number | undefined | null
> {
  protected uploader?: FileUpload
  protected isRemoved = false

  @observable public progress = 0
  @observable public error?: FileUploadError
  @observable public result?: FileUploadResult
  @observable public uploadStatus = FileUploadStatus.Checking

  constructor(private options: FileFieldControllerOptions) {
    super(options)
  }

  private upload() {
    this.dispose()

    const { value } = this
    const { type, kind } = this.options

    if (!value) {
      throw new Error(
        "Attempt to call FileFieldController#upload() without setting a file first",
      )
    }

    const uploader = (this.uploader = new FileUpload(value, kind, type))

    uploader.on("progress", (x) => (this.progress = x))
    uploader.on("error", (x) => (this.error = x))
    uploader.on("complete", (x) => this.handleComplete(x))
    uploader.on("status", (x) => (this.uploadStatus = x))

    uploader.start()
  }

  // Overridable
  protected handleComplete(result: FileUploadResult) {
    this.result = result
  }

  protected hasValue(): boolean {
    return this.value !== null
  }

  @action
  public setValue(value: File) {
    if (value === this.value) return

    this.value = value
    this.isRemoved = false

    when(
      () => this.status !== FieldControllerStatus.Validating,
      () => {
        if (this.status === FieldControllerStatus.Valid) this.upload()
      },
    )

    this.validate()
  }

  @action
  public remove() {
    this.isRemoved = true
    this.value = undefined
    this.dispose()
  }

  @action
  public dispose() {
    if (this.uploader) {
      this.uploader.cancel()
      this.uploader.removeAllListeners()
    }
  }

  @computed
  get isReady() {
    if (this.result) return true
    if (this.value instanceof File) return false

    return true
  }

  @computed
  get serializedValue() {
    if (this.isRemoved) return null
    if (this.result) return this.result.id

    return undefined
  }
}
