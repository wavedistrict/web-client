import { action, computed, observable } from "mobx"
import { ImageModel } from "../../image/models/ImageModel"
import { FieldControllerOptions } from "../classes/FieldController"
import { FileFieldController } from "./FileFieldController"

type ImageType = "avatar" | "artwork" | "coverArtwork" | "coverAvatar"

export interface ImageFieldControllerOptions
  extends FieldControllerOptions<File> {
  type: ImageType
  reference?: ImageModel
}

export class ImageFieldController extends FileFieldController {
  private reader: FileReader = new FileReader()

  @observable public imageURL?: string
  @observable public reference?: ImageModel

  constructor(options: ImageFieldControllerOptions) {
    super({
      ...(options as FieldControllerOptions<File>),
      type: options.type,
      kind: "image",
    })

    this.reference = options.reference
    this.reader.onload = this.handleLoad
  }

  @action.bound
  private handleLoad() {
    const { result } = this.reader

    if (typeof result === "string") {
      this.imageURL = result
    }
  }

  private read() {
    if (!this.value) return
    this.reader.readAsDataURL(this.value)
  }

  @computed
  public get hasImage() {
    return !!(this.value || this.imageURL || this.reference)
  }

  @action
  public setValue(value: File) {
    super.setValue(value)
    this.read()
  }

  @action
  public remove() {
    super.remove()
    this.imageURL = undefined
    this.reference = undefined
  }
}
