import { computed, toJS } from "mobx"
import {
  FieldController,
  FieldControllerOptions,
} from "../classes/FieldController"

export interface ArrayFieldControllerOptions<T>
  extends FieldControllerOptions<T[]> {
  value?: T[]
}

export class ArrayFieldController<T> extends FieldController<T[]> {
  public value: T[]

  constructor(options: ArrayFieldControllerOptions<T>) {
    super(options)
    this.value = options.value || []
  }

  protected hasValue(): boolean {
    return this.value.length > 0
  }

  @computed
  public get serializedValue() {
    return toJS<T[]>(this.value)
  }
}
