import React from "react"
import { getSpacing } from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { useFormContext } from "../hooks/useFormContext"

export type GridCellVariants = "full" | "half"

export interface Props {
  children: React.ReactNode
}

const mq = "@media (max-width: 810px)"

const Container = styled.form`
  display: flex;
  flex-wrap: wrap;

  ${mq} {
    flex-direction: column;
  }
`

export function FormLayout(props: Props) {
  const { form, onSubmit } = useFormContext()

  function handleSubmit(event: React.FormEvent<unknown>) {
    event.preventDefault()
    if (onSubmit) onSubmit()
  }

  return (
    <Container onSubmit={handleSubmit}>
      {props.children}
      <input type="submit" style={{ display: "none" }} />
      {/** This ensures the form can be submitted by pressing enter **/}
    </Container>
  )
}

const GridContainer = styled.div`
  width: 0px;
  flex: 1;

  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: ${getSpacing("4")}px;
  align-content: flex-start;

  ${mq} {
    width: 100%;
  }
`

FormLayout.Grid = function Grid(props: Props) {
  return <GridContainer>{props.children}</GridContainer>
}

const GridCellContainer = styled.div<{ size: GridCellVariants }>`
  ${(props) => {
    if (props.size === "full")
      return `
      grid-column: span 2;
    `

    if (props.size === "half")
      return `
      grid-column: span 1;
    `
  }}
`

FormLayout.GridCell = function GridCell(
  props: Props & { size: GridCellVariants },
) {
  return (
    <GridCellContainer size={props.size}>{props.children}</GridCellContainer>
  )
}

const HeaderContainer = styled.div`
  width: 100%;
  margin-bottom: ${getSpacing("4")}px;
`

FormLayout.Header = function Header(props: Props) {
  return <HeaderContainer>{props.children}</HeaderContainer>
}

const SidebarContainer = styled.div`
  width: 180px;
  flex-shrink: 0;
  margin-right: ${getSpacing("5")}px;

  ${mq} {
    width: 100%;
  }
`

FormLayout.Sidebar = function Sidebar(props: Props) {
  return <SidebarContainer>{props.children}</SidebarContainer>
}

const ListContainer = styled.div`
  flex: 1;
`

FormLayout.List = function List(props: Props) {
  return <ListContainer>{props.children}</ListContainer>
}
