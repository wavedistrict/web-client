import { observer } from "mobx-react"
import React from "react"
import {
  getColor,
  getDuration,
  getFontSize,
  getFontWeight,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { FormManager, FormManagerStatus } from "../classes/FormManager"

export interface FormStatusProps {
  form: FormManager<any>
  submitting?: string
  error?: string
}

const Container = styled.div<{
  status: boolean
  error: boolean
}>`
  font-size: ${getFontSize("2")}px;
  font-weight: ${getFontWeight("bold")};

  opacity: 0;
  flex: 1;

  transition: ${getDuration("normal")} ease all;

  ${(props) =>
    props.status &&
    `
    opacity: 0.8;
  `}

  ${(props) =>
    props.error &&
    `
    color: ${getColor("invalid")};
    opacity: 1;
  `}
`

export const FormStatus = observer(function FormStatus(props: FormStatusProps) {
  const { form, submitting, error } = props

  const message = {
    "0": "",
    "1": "Validating...",
    "2": submitting || "Submitting...",
    "3": error || "An error occurred.",
  }[form.status]

  return (
    <Container
      error={form.status === FormManagerStatus.Error}
      status={message !== ""}
    >
      {message}
    </Container>
  )
})
