import { observer } from "mobx-react"
import React from "react"
import {
  getColor,
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { FieldController } from "../classes/FieldController"
import { FieldError } from "./FieldError"

type FormFieldVariants = "inline" | ""

interface FormFieldProps<T extends FieldController<any>> {
  label: string
  controller: T
  renderInput: (controller: T) => React.ReactNode
  variants?: FormFieldVariants[]
}

const Container = styled.div<{
  inline: boolean
  required: boolean
}>`
  > .label {
    margin-bottom: ${getSpacing("3")}px;
    font-weight: ${getFontWeight("bold")};

    font-size: ${getFontSize("1")}px;
    text-transform: uppercase;
    letter-spacing: 0.3px;

    color: ${getFontColor("normal")};
  }

  > .label > .required {
    display: none;

    margin-left: ${getSpacing("2")}px;
    color: ${getColor("invalid")};

    font-weight: ${getFontWeight("normal")};
  }

  ${(props) =>
    props.inline &&
    `
    display: flex;
    flex-wrap: wrap;
    align-items: center;
  
    > .label {
      flex-grow: 1;
      max-width: 50%;

      text-transform: unset;
      margin-bottom: 0;
    }

    > .input {
      flex-grow: 1;
      max-width: 50%;

      width: 200px;
      display: flex;
      justify-content: flex-end;
    }

    > .error {
      width: 100%;
    }

    & + & {
      border-top: solid 1px ${props.theme.colors.divider};
      padding-top: ${props.theme.spacings[4]}px;
    }
  `}

  ${(props) =>
    props.required &&
    `
    > .label > .required {
      display: inline;
    }
  `}
`

export const FormField = observer(function FormField<
  T extends FieldController<any>
>(props: FormFieldProps<T>) {
  const { controller, label, renderInput, variants = [] } = props

  const containerProps = {
    required: controller.isRequired,
    inline: variants.includes("inline"),
  }

  return (
    <Container {...containerProps}>
      <div className="label">
        {label}
        <div title="This field is required" className="required">
          *
        </div>
      </div>
      <div className="input">{renderInput(controller)}</div>
      <div className="error">
        <FieldError controller={controller} />
      </div>
    </Container>
  )
})
