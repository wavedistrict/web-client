import { observer } from "mobx-react"
import React, { useEffect, useState } from "react"
import {
  getColor,
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { Icon } from "../../icon/components/Icon"
import {
  FieldController,
  FieldControllerStatus,
} from "../classes/FieldController"
import { size } from "polished"

interface FieldErrorProps {
  controller: FieldController<any>
}

const Container = styled.div<{
  error: boolean
}>`
  height: 0px;
  display: flex;
  margin-top: ${getSpacing("4")}px;
  align-items: center;
  pointer-events: none;

  color: ${getColor("invalid")};
  opacity: 0;

  font-size: ${getFontSize("1")}px;
  font-weight: ${getFontWeight("bold")};
  font-style: italic;

  transition: ${getDuration("normal")} ease all;

  > .icon {
    ${size(16)}

    margin-right: ${getSpacing("3")}px;
    fill: ${getColor("invalid")};
  }

  ${(props) =>
    props.error &&
    `
    height: 12px;
    opacity: 1;
  `}
`

export const FieldError = observer(function FieldError(props: FieldErrorProps) {
  const { controller } = props
  const [message, setMessage] = useState("")

  useEffect(() => {
    if (controller.validationError !== "") {
      setMessage(controller.validationError)
    }
  }, [controller.validationError])

  const hasError = controller.status === FieldControllerStatus.Invalid

  return (
    <Container error={hasError}>
      <Icon className="icon" name="error" />
      {message}
    </Container>
  )
})
