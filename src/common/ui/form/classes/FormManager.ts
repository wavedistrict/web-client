import { computed, observable, runInAction, when } from "mobx"
import { AnyObject } from "../../../lang/object/types"
import { wait } from "../../../lang/promise/helpers/wait"
import { apiServer } from "../../../network"
import { ARTIFICIAL_UX_DELAY } from "../../constants"
import { FieldController, FieldControllerStatus } from "./FieldController"

type SubmitMethod = "post" | "patch"

export interface FieldsType {
  [field: string]: FieldController<any>
}

export enum FormManagerStatus {
  Idling,
  Validating,
  Submitting,
  Error,
}

export class FormManager<TFields extends FieldsType> {
  public fields: TFields

  @observable public status = FormManagerStatus.Idling

  constructor(fields: TFields) {
    this.fields = fields
  }

  /** Attempt to submit the form */
  public async submit<T>(url: string, method: SubmitMethod) {
    if (!this.hasValidated) {
      this.status = FormManagerStatus.Validating
      await this.validate()
    }

    await when(() => this.ready)

    if (!this.valid) {
      this.status = FormManagerStatus.Idling
      throw new Error("Invalid form")
    }

    this.status = FormManagerStatus.Submitting

    try {
      await wait(ARTIFICIAL_UX_DELAY)
      const result = await apiServer[method]<T>(url, this.serialized)

      this.status = FormManagerStatus.Idling
      return result
    } catch (error) {
      runInAction(() => {
        this.status = FormManagerStatus.Error
      })

      throw error
    }
  }

  /** Validates fields that need to be validated */
  public async validate() {
    const unvalidatedFields = Object.values(this.fields).filter((field) => {
      return field.status === FieldControllerStatus.Idle
    })

    for (const field of unvalidatedFields) await field.validate()
  }

  /** A helper for setting the value of a field if it exists */
  public setFieldValue<T>(name: keyof TFields, value: T): void {
    const field = this.fields[name]
    field.setValue(value)
  }

  /** Returns serialized data to submit */
  private get serialized(): AnyObject {
    const result: AnyObject = {}

    for (const [key, field] of Object.entries(this.fields)) {
      result[key] = field.serializedValue
    }

    return result
  }

  /** Resets the form */
  public reset() {
    for (const field of Object.values(this.fields)) {
      field.reset()
    }
  }

  /** Disposes the form */
  public dispose() {
    for (const field of Object.values(this.fields)) {
      field.dispose()
    }
  }

  /** The form can be submitted to the server */
  @computed
  public get ready() {
    return Object.values(this.fields).every((field) => field.isReady)
  }

  @computed
  public get valid() {
    return Object.values(this.fields).every(
      (field) => field.status === FieldControllerStatus.Valid,
    )
  }

  @computed
  public get hasValidated() {
    return Object.values(this.fields).every(
      (field) => field.status !== FieldControllerStatus.Idle,
    )
  }
}
