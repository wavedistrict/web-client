import { action, computed, observable } from "mobx"

export type Validator<V> = (value: V) => Promise<ValidatorResult>

export interface ValidatorResult {
  isValid: boolean
  errorMessage: string
}

export type Modifier<V> = (value: V) => V

export interface FieldControllerOptions<V> {
  value?: V
  isRequired?: boolean
  validators?: Validator<V>[]
  modifiers?: Modifier<V>[]
  requiredWarning?: string
}

export enum FieldControllerStatus {
  Idle,
  Validating,
  Valid,
  Invalid,
  Error,
}

export abstract class FieldController<V, TSerialized = V> {
  // FIXME: figure out a way to make `value` non-nullable
  // ideas:
  // - make a new `getInitialValue` function
  // - make a new `initialValue` abstract property
  // - make `defaultValue` abstract, requiring subclasses to implement it
  @observable public value?: V
  protected defaultValue?: V

  private requiredWarning?: string
  public isRequired = false

  @observable public status = FieldControllerStatus.Idle
  @observable public validationError = ""

  @observable private validatedValue?: V
  @observable private validatingPromise?: Promise<void>

  private validators: Validator<V>[] = []
  private modifiers: Modifier<V>[] = []

  constructor(options: FieldControllerOptions<V>) {
    const {
      value,
      isRequired,
      requiredWarning,
      validators,
      modifiers,
    } = options

    this.defaultValue = value
    this.value = value

    this.validators = validators || []
    this.modifiers = modifiers || []

    this.isRequired = isRequired || false
    this.requiredWarning = requiredWarning
  }

  /** Returns whether or not the field has a value */
  protected abstract hasValue(value?: V): boolean

  @action
  public setValue(value: V) {
    this.value = this.applyModifiers(value)
    this.validateSafely(value)
  }

  private applyModifiers(value: V): V {
    return this.modifiers.reduce((value, modifier) => modifier(value), value)
  }

  public async validate() {
    return this.validateSafely()
  }

  private async validateSafely(
    value: V | undefined = this.value,
  ): Promise<void> {
    if (this.isValidating) {
      await this.validatingPromise

      if (!this.isFresh) {
        return this.validateSafely(this.value!)
      }
    }

    await (this.validatingPromise = this.validateAll(value))

    this.validatedValue = this.value
    this.validatingPromise = undefined
  }

  /** Validates the field */
  public async validateAll(value?: V) {
    this.status = FieldControllerStatus.Validating

    const hasValue = this.hasValue(value)

    if (this.isRequired && !hasValue) {
      this.status = FieldControllerStatus.Invalid
      this.validationError = this.requiredWarning || "This field is required."
      return
    }

    if (!hasValue) {
      this.status = FieldControllerStatus.Valid
      this.validationError = ""
      return
    }

    return this.runValidators(value!)
  }

  /** Validates the value against validators */
  private async runValidators(value: V) {
    const { validators } = this

    const promises = validators.map((validate) => validate(value!))
    const results = await Promise.all(promises)

    const firstInvalidResult = results.find(
      (result) => result.isValid === false,
    )

    if (!firstInvalidResult) {
      this.status = FieldControllerStatus.Valid
      this.validationError = ""
      return
    }

    this.status = FieldControllerStatus.Invalid
    this.validationError = firstInvalidResult.errorMessage
  }

  @action
  public reset() {
    this.value = this.defaultValue
    this.status = FieldControllerStatus.Idle
  }

  @action
  public dispose() {}

  @computed
  get isReady() {
    return true
  }

  @computed
  get serializedValue(): TSerialized {
    // @ts-ignore: see comment on this.value
    return this.value
  }

  @computed
  get rawValue(): V {
    // @ts-ignore: see comment on this.value
    return this.value
  }

  @computed get isValidating() {
    return (
      this.validatingPromise !== undefined &&
      this.status === FieldControllerStatus.Validating
    )
  }

  @computed get isValid() {
    return this.isFresh && this.status === FieldControllerStatus.Valid
  }

  @computed get isFresh() {
    return this.value === this.validatedValue
  }
}
