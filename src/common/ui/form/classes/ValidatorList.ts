import { Validator } from "./FieldController"
import { observable, computed } from "mobx"

/** Validates a list of validators safely */
export class ValidatorList<T> {
  @observable private currentValue: T

  @observable private validatedValue?: T
  @observable private validatingPromise?: Promise<void>
  @observable private validationError?: string

  constructor(private validators: Validator<T>[], value: T) {
    this.currentValue = value
  }

  public async validate(value: T): Promise<void> {
    this.currentValue = value

    if (this.isValidating) {
      await this.validatingPromise

      if (!this.isFresh) {
        return this.validate(this.currentValue)
      }
    }

    await (this.validatingPromise = this.runValidators(value))
    this.validatingPromise = undefined
  }

  private async runValidators(value: T) {
    const results = await Promise.all(this.validators.map((v) => v(value)))
    const invalidResult = results.find((r) => r.isValid === false)

    this.validatedValue = value

    if (invalidResult) {
      this.validationError = invalidResult.errorMessage
      return
    }

    this.validationError = undefined
  }

  @computed
  public get isFresh() {
    return this.currentValue === this.validatedValue
  }

  @computed
  public get isValidating() {
    return this.validatingPromise !== undefined
  }

  @computed
  public get isValid() {
    return this.validationError === undefined && this.isFresh
  }

  @computed
  public get error() {
    return this.validationError
  }
}
