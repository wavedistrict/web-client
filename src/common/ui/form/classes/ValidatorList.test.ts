import { wait } from "../../../lang/promise/helpers/wait"
import { validateStringLength } from "../validators/string/validateStringLength"
import { Validator } from "./FieldController"
import { ValidatorList } from "./ValidatorList"

const asyncValidator: Validator<string> = async () => {
  await wait(15)

  return {
    isValid: true,
    errorMessage: "",
  }
}

const getList = () =>
  new ValidatorList<string>(
    [
      validateStringLength({
        max: 5,
        min: 2,
      }),
    ],
    "",
  )

const getAsyncList = () => new ValidatorList<string>([asyncValidator], "")

describe("ValidatorList", () => {
  it("has correct initial state", () => {
    const list = getList()

    expect(list.isFresh).toBe(false)
    expect(list.isValid).toBe(false)
    expect(list.isValidating).toBe(false)
  })

  it("is invalid on invalid input", async () => {
    const list = getList()
    await list.validate("")

    expect(list.isValid).toBe(false)
    expect(list.isFresh).toBe(true)
    expect(list.error).toBe("Field must be 2 characters")
  })

  it("is valid on valid input", async () => {
    const list = getList()
    await list.validate("foo")

    expect(list.isValid).toBe(true)
    expect(list.isFresh).toBe(true)
  })

  it("lazily validates", async () => {
    const list = getAsyncList()

    const firstPromise = list.validate("foo")
    const secondPromise = list.validate("f")

    expect(firstPromise).toStrictEqual(secondPromise)
  })

  it("handles async race conditions", async () => {
    const list = getAsyncList()

    const firstPromise = list.validate("foo")
    await wait(1)
    const secondPromise = list.validate("fooo")
    await wait(1)

    expect(firstPromise).toStrictEqual(secondPromise)

    const promise = list.validate("fooooooooo")

    expect(promise).toStrictEqual(firstPromise)
    expect(list.isValidating).toBe(true)

    await promise
    expect(list.isValidating).toBe(false)
  })
})
