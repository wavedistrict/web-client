import React from "react"
import { LightboxModal } from "../components/LightboxModal"
import { Manager } from "../../../state/types/Manager"

export const spawnLightboxModal = (manager: Manager, source: string) => {
  const { modalStore } = manager.stores

  modalStore.spawn({
    key: source,
    dismissOnClickout: true,
    render: () => React.createElement(LightboxModal, { source }),
  })
}
