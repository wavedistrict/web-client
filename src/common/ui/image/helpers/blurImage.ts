import stackblur from "stackblur"
import { ImageProcessingFunction } from "../stores/imageStore"

export const blurImage = (blur: number): ImageProcessingFunction => (
  pixels,
  width,
  height,
) => {
  stackblur(pixels, width, height, blur)
  return pixels
}
