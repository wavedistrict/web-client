import { observer } from "mobx-react"
import React from "react"
import { MediaModel } from "../../../media/models/Media"
import {
  ImageReferenceData,
  ImageSource,
} from "../../../media/types/ImageReferenceData"
import { IconType } from "../../icon"
import { ImageReferenceRenderer } from "../components/ImageReferenceRenderer"
import { ImageReferenceStatic } from "../components/ImageReferenceStatic"
import { IS_SERVER } from "../../../../modules/core/constants"

export interface ImageComponentProps {
  id: string
  alt?: string
  title?: string
  lightbox?: boolean
  absolute?: boolean
}

export interface GetImageComponentProps {
  id: string
  reference?: MediaModel<ImageReferenceData, ImageSource>
  placeholder: {
    hash: number
    icon: IconType
  }
}

export const createImageComponent = <P extends object>(
  getProps: (
    props: Readonly<ImageComponentProps & P>,
  ) => GetImageComponentProps,
) => {
  @observer
  class ImageComponent extends React.Component<ImageComponentProps & P> {
    public render() {
      const { id, alt, title, absolute, lightbox } = this.props
      const { reference, placeholder } = getProps(this.props)

      if (IS_SERVER)
        return (
          <ImageReferenceStatic {...{ reference, alt: alt || title, title }} />
        )

      return (
        <ImageReferenceRenderer
          {...{
            id,
            reference,
            absolute,
            placeholder,
            lightbox,
          }}
        />
      )
    }
  }

  return ImageComponent as React.ComponentType<ImageComponentProps & P>
}
