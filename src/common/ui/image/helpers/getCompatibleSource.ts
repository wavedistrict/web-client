import { canUseWebP } from "../../../dom/helpers/webp"
import { getLastItem } from "../../../lang/array/helpers/getLastItem"
import { ImageModel } from "../models/ImageModel"

/**
 * Gets a compatible source based on what the browser can render.
 * Receives a width and height, and finds the first source with greater dimensions
 */
export const getCompatibleSource = (
  reference: ImageModel,
  width: number,
  height: number,
  preferred?: string,
) => {
  const mime = canUseWebP() ? "image/webp" : "image/jpeg"

  const filteredSources = reference.sources.filter(
    ({ data }) => data.mime === mime,
  )

  const source = filteredSources.find(({ data }) => {
    const sourceArea = data.width * data.height
    const containerArea = width * height

    return data.name === preferred || sourceArea >= containerArea
  })

  return source || getLastItem(filteredSources)
}
