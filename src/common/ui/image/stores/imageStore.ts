import {
  InitializableStore,
  createStoreFactory,
} from "../../../state/classes/InitializableStore"

type RenderedImageString = string

export type ImageProcessingFunction = (
  pixels: Uint8ClampedArray,
  width: number,
  height: number,
) => Uint8ClampedArray

export class ImageStore extends InitializableStore {
  private images = new Map<string, string>()

  public async init() {}

  private processImageData(
    imageData: ImageData,
    processors: ImageProcessingFunction[],
  ) {
    const { data, width, height } = imageData

    const processedPixels = processors.reduce(
      (pixels, process) => process(pixels, width, height),
      data,
    )

    imageData.data.set(processedPixels)
    return imageData
  }

  private renderImage(image: HTMLImageElement) {
    const { width, height } = image

    const canvas = document.createElement("canvas")
    canvas.width = width
    canvas.height = height

    const context = canvas.getContext("2d")!
    context.drawImage(image, 0, 0, width, height)

    return { context, canvas }
  }

  private getPathFromCanvas(canvas: HTMLCanvasElement) {
    return new Promise<string>((resolve, reject) => {
      if (canvas.toBlob) {
        return canvas.toBlob((blob) => {
          resolve(URL.createObjectURL(blob))
        })
      }

      if (canvas.toDataURL) return resolve(canvas.toDataURL())

      reject(new Error("Failed to create image url from canvas"))
    })
  }

  private storeImage(fingerprint: string, image: RenderedImageString) {
    this.images.set(fingerprint, image)
  }

  public getImage(fingerprint: string) {
    return this.images.get(fingerprint)
  }

  public loadImage(
    path: string,
    processors: ImageProcessingFunction[],
    fingerprint: string,
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      const existingPath = this.getImage(fingerprint)
      if (existingPath) return resolve(existingPath)

      const image = document.createElement("img")

      const handleLoad = () => {
        const { canvas, context } = this.renderImage(image)
        const imageData = context.getImageData(0, 0, image.width, image.height)
        const processedImageData = this.processImageData(imageData, processors)

        context.putImageData(processedImageData, 0, 0)

        this.getPathFromCanvas(canvas).then(handleGetRenderedPath)
      }

      const handleGetRenderedPath = (renderedPath: string) => {
        this.storeImage(fingerprint, renderedPath)
        resolve(renderedPath)
      }

      image.onload = handleLoad
      image.crossOrigin = "anonymous"
      image.src = path
    })
  }
}

export const imageStore = createStoreFactory(ImageStore)
