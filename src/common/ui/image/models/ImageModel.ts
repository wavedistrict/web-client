import { MediaModel } from "../../../media/models/Media"
import {
  ImageReferenceData,
  ImageSource,
} from "../../../media/types/ImageReferenceData"

export type ImageModel = MediaModel<ImageReferenceData, ImageSource>
