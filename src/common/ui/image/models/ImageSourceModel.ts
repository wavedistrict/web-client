import { SourceModel } from "../../../media/models/Source"
import { ImageSource } from "../../../media/types/ImageReferenceData"

export declare class ImageSourceModel extends SourceModel<ImageSource> {}
exports.ImageSourceModel = SourceModel
