import React from "react"
import { getFontColor } from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { IconType } from "../../icon"
import { Icon } from "../../icon/components/Icon"

export interface ImagePlaceholderProps {
  hash: number
  icon?: IconType
  isAbsolute?: boolean
}

const variations = [
  ["#d4cf35", "#d22d2d"],
  ["#2ac869", "#2e75bd"],
  ["#ae5fb3", "#2e49bb"],
  ["#d2c52d", "#d22dc9"],
  ["#2dced2", "#cfb72c"],
]

const Container = styled.div<{ variation: number; absolute: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;

  position: ${(props) => (props.absolute ? "absolute" : "relative")};

  width: 100%;
  height: 100%;

  background: ${(props) => {
    const colors = variations[props.variation]

    if (colors.length === 1) return colors[0]

    return `linear-gradient(135deg, ${colors.join(",")})`
  }};

  > .icon {
    opacity: 0.7;

    height: 50%;
    width: 50%;

    svg {
      fill: ${getFontColor("normal")};
    }
  }
`

export function ImagePlaceholder(props: ImagePlaceholderProps) {
  const { hash, icon, isAbsolute } = props

  const variation = hash % variations.length

  return (
    <Container variation={variation} absolute={isAbsolute || false}>
      {icon && (
        <div className="icon">
          <Icon name={icon} />
        </div>
      )}
    </Container>
  )
}
