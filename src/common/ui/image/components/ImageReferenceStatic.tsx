import React from "react"
import { MediaModel } from "../../../media/models/Media"
import {
  ImageReferenceData,
  ImageSource,
} from "../../../media/types/ImageReferenceData"

export interface ImageReferenceStaticProps {
  reference?: MediaModel<ImageReferenceData, ImageSource>
  alt?: string
  title?: string
}

export class ImageReferenceStatic extends React.Component<
  ImageReferenceStaticProps
> {
  public render() {
    const { reference, alt, title } = this.props
    if (!reference) return <img {...{ alt, title }} />

    const source = reference.sources
      .filter(({ data }) => data.mime === "image/jpeg" && data.streamable)
      .sort(({ data: a }, { data: b }) => {
        const aSum = a.width + a.height
        const bSum = b.width + b.height

        return aSum < bSum ? 1 : -1
      })[0]

    if (!source) return null

    return (
      <img
        src={source.path}
        crossOrigin="anonymous"
        {...{ alt, title }}
        style={{
          width: "100%",
          height: "100%",
        }}
      />
    )
  }
}
