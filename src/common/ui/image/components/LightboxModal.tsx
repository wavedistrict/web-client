import React from "react"
import { observer } from "mobx-react"
import { observable, action } from "mobx"
import { CSSTransition } from "react-transition-group"

interface LightboxModalProps {
  source: string
}

@observer
export class LightboxModal extends React.Component<LightboxModalProps> {
  @observable private isLoaded = false

  @action.bound
  private handleLoad() {
    this.isLoaded = true
  }

  public render() {
    const { source } = this.props

    const style: React.CSSProperties = {
      maxWidth: "calc(100vw - 30px)",
      maxHeight: "calc(100vh - 30px)",
      display: this.isLoaded ? undefined : "none",
    }

    return (
      <CSSTransition
        appear
        classNames="fade-grow"
        timeout={200}
        in={this.isLoaded}
      >
        <img src={source} style={style} onLoad={this.handleLoad} />
      </CSSTransition>
    )
  }
}
