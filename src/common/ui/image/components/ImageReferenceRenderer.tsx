import React from "react"
import {
  Dimensions,
  SizeCalculator,
} from "../../../dom/components/SizeCalculator"
import { IconType } from "../../icon"
import { spawnLightboxModal } from "../actions/spawnLightboxModal"
import { getCompatibleSource } from "../helpers/getCompatibleSource"
import { ImageModel } from "../models/ImageModel"
import { ImagePlaceholder } from "./ImagePlaceholder"
import { ImageRenderer } from "./ImageRenderer"
import { styled } from "../../../../modules/theme/themes"
import { useObserver } from "mobx-react-lite"
import { cover } from "polished"
import { useManager } from "../../../state/hooks/useManager"

export interface ImageReferenceRendererProps {
  reference?: ImageModel
  id?: string
  lightbox?: boolean
  absolute?: boolean
  placeholder?: {
    hash: number
    icon: IconType
  }
}

const Container = styled.div<{ hasLightbox?: boolean }>`
  ${cover()}

  ${(props) =>
    props.hasLightbox &&
    `
    cursor: pointer;
  `}
`

const Layer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
`

export function ImageReferenceRenderer(props: ImageReferenceRendererProps) {
  const { reference, lightbox, absolute, placeholder } = props
  const manager = useManager()

  const spawnLightbox = (reference: ImageModel) => {
    const source = getCompatibleSource(reference, 1000, 1000)

    if (source) {
      spawnLightboxModal(manager, source.path)
    }
  }

  const renderImage = (size: Dimensions, reference: ImageModel) => {
    const { width, height } = size
    const source = getCompatibleSource(reference, width, height)

    if (source) return <ImageRenderer source={source.path} />
  }

  const renderPreload = (reference: ImageModel) => {
    const source = reference.getSource("preload")

    if (source) return <ImageRenderer source={source.path} blur={10} />
  }

  const renderPlaceholder = () => {
    if (placeholder)
      return (
        <ImagePlaceholder
          hash={placeholder.hash}
          icon={placeholder.icon}
          isAbsolute={absolute}
        />
      )

    return null
  }

  const renderLayer = (render: () => React.ReactNode) => {
    const element = render()

    if (element) return <Layer>{element}</Layer>
  }

  const render = (dimensions: Dimensions) => {
    if (!reference) return renderPlaceholder()

    const { width, height } = dimensions

    const style: React.CSSProperties = {
      width,
      height,
      position: absolute ? "absolute" : "relative",
    }

    const handleClick = () => {
      if (lightbox) spawnLightbox(reference)
    }

    return (
      <Container style={style} hasLightbox={lightbox} onClick={handleClick}>
        {renderLayer(() => renderPreload(reference))}
        {renderLayer(() => renderImage(dimensions, reference))}
      </Container>
    )
  }

  return useObserver(() => (
    <SizeCalculator absolute={absolute}>{render}</SizeCalculator>
  ))
}
