import { darken } from "polished"
import React, { useCallback, useEffect, useState, useRef } from "react"
import { useSetting } from "../../../../modules/settings/hooks/useSetting"
import { getDuration } from "../../../../modules/theme/helpers"
import { styled, useTheme } from "../../../../modules/theme/themes"
import { blurImage } from "../helpers/blurImage"
import { useMounted } from "../../react/hooks/useMounted"
import { useStores } from "../../../state/hooks/useStores"

interface ImageRendererProps {
  source: string
  blur?: number
  blend?: string
}

const Container = styled.div`
  display: block;

  width: 100%;
  height: 100%;

  overflow: hidden;
  position: relative;
`

const ImageContainer = styled.div<{ rendered: boolean; blend?: string }>`
  width: 100%;
  height: 100%;

  background-size: cover;
  background-position: center;

  transition: ${getDuration("normal")} ease;
  transition-property: opacity, transform;

  opacity: 0;
  transform: scale(1.1);

  ${(props) =>
    props.rendered &&
    `
    opacity: 1;
    transform: scale(1);
  `}

  ${(props) =>
    props.blend &&
    `
    background-color: ${darken(0.3, props.blend)};
    background-blend-mode: hard-light;
    filter: saturate(1.5);
  `}
`

const getFingerprint = (props: object) => {
  return Object.entries(props)
    .filter(([_, value]) => value)
    .map(([key, value]) => `${key}:${value}`)
    .join("-")
}

export function ImageRenderer(props: ImageRendererProps) {
  const isMounted = useMounted()
  const shouldBlur = useSetting("appearance", "blur")

  const { durations } = useTheme()

  const { source, blur, blend } = props
  const { imageStore } = useStores()

  const fingerprint = getFingerprint({
    blur: shouldBlur && blur,
    source,
    blend,
  })

  const [path, setPath] = useState(() => imageStore.getImage(fingerprint) || "")
  const [renderedFingerprint, setRenderedFingerprint] = useState(
    path ? fingerprint : "",
  )

  const load = useCallback(async () => {
    const processors = []

    if (shouldBlur && blur) {
      processors.push(blurImage(blur))
    }

    const newPath = await imageStore.loadImage(source, processors, fingerprint)

    if (isMounted()) {
      setPath(newPath)
      setRenderedFingerprint(fingerprint)
    }
  }, [blur, fingerprint, imageStore, shouldBlur, source, isMounted])

  useEffect(() => {
    if (fingerprint === renderedFingerprint) return
    setRenderedFingerprint("")

    const timeout = setTimeout(load, parseInt(durations.normal))

    return () => {
      clearTimeout(timeout)
    }
  }, [durations.normal, fingerprint, load, renderedFingerprint])

  const style: React.CSSProperties = {
    backgroundImage: path ? `url(${path})` : "",
  }

  return (
    <Container>
      <ImageContainer
        style={style}
        rendered={!!renderedFingerprint}
        blend={blend}
      />
    </Container>
  )
}
