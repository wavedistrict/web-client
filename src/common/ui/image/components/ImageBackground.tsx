import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { getDuration, getOverlay } from "../../../../modules/theme/helpers"
import { styled, useTheme } from "../../../../modules/theme/themes"
import { canUseWebP } from "../../../dom/helpers/webp"
import { ImageModel } from "../models/ImageModel"
import { ImageSourceModel } from "../models/ImageSourceModel"
import { ImagePlaceholder } from "./ImagePlaceholder"
import { ImageRenderer } from "./ImageRenderer"

interface ImageBackgroundProps {
  reference?: ImageModel
  getSource?: (sources: ImageSourceModel[]) => ImageSourceModel | undefined
  hash: number
  blur?: number
  isClear?: boolean
  hasGradient?: boolean
}

const defaultGetSource = (sources: ImageSourceModel[]) =>
  sources.find(({ data }) => {
    const format = canUseWebP() ? "webp" : "jpeg"

    return data.name === `${format}-small` || data.name === `${format}-half`
  })

const Container = styled.div`
  ${size("100%")};
`

const ImageContainer = styled.div`
  ${size("100%")};
`

const Filter = styled.div<{
  isClear?: boolean
  isBlended?: boolean
}>`
  ${size("100%")};

  position: absolute;
  top: 0px;

  background: ${getOverlay("background")};
  transition: ${getDuration("normal")} ease all;
  opacity: ${({ isClear, isBlended, theme }) => {
    const { opacity } = theme.overlay

    if (isBlended) return opacity - 0.4
    if (isClear) return opacity - 0.3

    return opacity
  }};
`

export const ImageBackground = observer(function ImageBackground(
  props: ImageBackgroundProps,
) {
  const { hash, reference, blur, isClear, getSource = defaultGetSource } = props
  const theme = useTheme()

  const renderPlaceholder = () => {
    return (
      <ImageContainer>
        <ImagePlaceholder hash={hash} />
        <Filter isClear={isClear} />
      </ImageContainer>
    )
  }

  const renderContent = () => {
    if (!reference) return renderPlaceholder()

    const source = getSource(reference.sources)
    if (!source) return renderPlaceholder()

    const blend =
      blur && theme.overlay.blend ? theme.overlay.background : undefined

    return (
      <ImageContainer>
        <ImageRenderer source={source.path} blur={blur} blend={blend} />
        <Filter isClear={isClear} isBlended={!!blend} />
      </ImageContainer>
    )
  }

  return <Container>{renderContent()}</Container>
})
