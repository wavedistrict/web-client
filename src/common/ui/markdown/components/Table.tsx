import { styled } from "../../../../modules/theme/themes"
import { PropsWithChildren } from "react"
import React from "react"
import {
  getColor,
  getSpacing,
  getFontWeight,
  getFontSize,
} from "../../../../modules/theme/helpers"

const Container = styled.table`
  width: 100%;
  text-align: left;
`

export function Table(props: PropsWithChildren<{}>) {
  const { children } = props

  return <Container>{children}</Container>
}

Table.Head = styled.thead`
  border-bottom: solid 1px ${getColor("divider")};
`

Table.Body = styled.tbody``

Table.Row = styled.tr``

Table.Heading = styled.th`
  color: ${getColor("accent")};

  font-weight: ${getFontWeight("bold")};
  font-size: ${getFontSize("1")}px;

  padding-bottom: ${getSpacing("3")}px;
  text-transform: uppercase;

  & + & {
    padding-left: ${getSpacing("4")}px;
  }
`

Table.Data = styled.td`
  padding: ${getSpacing("3")}px 0px;

  & + td {
    padding-left: ${getSpacing("4")}px;
  }
`
