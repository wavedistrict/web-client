import React from "react"
import { sanitizeHtml } from "../../../lang/string/helpers/sanitizeHtml"

interface MarkdownProps extends React.HTMLAttributes<HTMLDivElement> {
  text: string
}

export class Markdown extends React.Component<MarkdownProps> {
  public renderRichText(text: string) {
    return sanitizeHtml(text)
      .replace(/\n/g, "<br/>")
      .replace(/\*([^\*]+)\*/g, "<strong>$1</strong>")
      .replace(/_([^_]+)_/g, "<em>$1</em>")
  }

  public render() {
    const { text, className, ...otherProps } = this.props

    const renderedContent = this.renderRichText(text)

    return (
      <div
        {...otherProps}
        dangerouslySetInnerHTML={{ __html: renderedContent }}
      />
    )
  }
}
