import React from "react"
import { styled } from "../../../../modules/theme/themes"
import { PropsWithChildren } from "react"
import {
  getFontColor,
  getFontSize,
  getSpacing,
} from "../../../../modules/theme/helpers"

const Container = styled.p`
  color: ${getFontColor("normal")};
  font-size: ${getFontSize("2")}px;
  margin: ${getSpacing("3")}px 0px;
`

export function Paragraph(props: PropsWithChildren<{}>) {
  return <Container>{props.children}</Container>
}
