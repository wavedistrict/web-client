import { Ref } from "react"

export const mergeRefs = <T>(...refs: (Ref<T> | undefined)[]) => (ref: T) => {
  for (const resolvableRef of refs) {
    if (typeof resolvableRef === "function") {
      resolvableRef(ref)
    } else if (!resolvableRef) {
    } else {
      (resolvableRef as any).current = ref
    }
  }
}
