/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect } from "react"
import { Dimensions } from "react-virtualized"
import { IS_SERVER } from "../../../../modules/core/constants"

export const useViewport = () => {
  if (IS_SERVER) {
    return {
      height: 720,
      width: 1280,
    }
  }

  const [dimensions, setDimensions] = useState<Dimensions>({
    height: window.innerHeight,
    width: window.innerWidth,
  })

  const handleResize = () =>
    setDimensions({
      height: window.innerHeight,
      width: window.innerWidth,
    })

  useEffect(() => {
    window.addEventListener("resize", handleResize, { passive: true })

    return () => {
      window.removeEventListener("resize", handleResize)
    }
  })

  return dimensions
}
