import { useEffect, useRef } from "react"

export const useAnimation = (frame: () => void, enabled = true) => {
  const frameRef = useRef<number>()

  useEffect(() => {
    const frameHandler = () => {
      frame()
      frameRef.current = requestAnimationFrame(frameHandler)
    }

    if (enabled) {
      frameHandler()
    }

    return () => {
      cancelAnimationFrame(frameRef.current as number)
    }
  }, [enabled, frame])
}
