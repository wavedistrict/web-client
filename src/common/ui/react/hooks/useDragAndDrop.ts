import { useState, useRef } from "react"
import { useWindowEvent } from "./useWindowEvent"

const checkIfFile = (event: DragEvent) => {
  const { dataTransfer } = event

  if (dataTransfer) {
    return [...dataTransfer.items].some((item) => item.kind === "file")
  }

  return false
}

export const useDragAndDrop = (callback: (files: FileList) => void) => {
  const [dragCount, setDragCount] = useState(0)

  useWindowEvent("dragenter", (event) => {
    event.preventDefault()

    if (checkIfFile(event)) setDragCount((prev) => prev + 1)
  })

  useWindowEvent("dragleave", (event) => {
    event.preventDefault()

    if (checkIfFile(event)) setDragCount((prev) => prev - 1)
  })

  useWindowEvent("drop", (event) => {
    event.preventDefault()
    setDragCount(0)

    const { dataTransfer } = event

    if (dataTransfer) {
      callback(dataTransfer.files)
    }
  })

  return dragCount > 0
}
