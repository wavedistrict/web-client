export const NAVIGATION_HEADER_HEIGHT = 56
export const NAVIGATION_BODY_WIDTH = 350
export const NAVIGATION_MQ = `(min-width: ${NAVIGATION_BODY_WIDTH * 2}px)`
