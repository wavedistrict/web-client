import { useContext } from "react"
import {
  ActivityContext,
  ActivityContextState,
} from "../components/ActivityTabs/ActivityTabs"

export const useActivity = <T extends string | number>() => {
  const context = useContext(ActivityContext)
  if (!context) throw new Error("Cannot useActivity outside ActivityTabs")

  return context as ActivityContextState<T>
}
