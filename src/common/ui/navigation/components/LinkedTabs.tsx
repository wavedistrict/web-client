import { observer } from "mobx-react"
import React from "react"
import UrlPattern from "url-pattern"
import { IconType } from "../../icon"
import { Tabs, TabVariants } from "./Tabs"
import { useStores } from "../../../state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

export interface LinkedTabItem {
  label: string
  icon?: IconType
  to: string
}

export interface LinkedTabsProps {
  items: LinkedTabItem[]
  variants?: TabVariants[]
}

export function LinkedTabs(props: LinkedTabsProps) {
  const { routingStore } = useStores()
  const { items, variants } = props

  return useObserver(() => {
    const mappedItems = items.map((item) => {
      const pattern = new UrlPattern(item.to)
      const isActive = !!pattern.match(routingStore.location.pathname)

      const onClick = () => routingStore.push(item.to)

      return {
        ...item,
        key: item.to,
        onClick,
        isActive,
      }
    })

    return <Tabs variants={variants} items={mappedItems} />
  })
}
