import React, { AnchorHTMLAttributes } from "react"
import { Link } from "./Link"

export type ExternalLinkProps = Omit<
  AnchorHTMLAttributes<any>,
  "target" | "rel"
>

export function ExternalLink(props: ExternalLinkProps) {
  return <Link target="_blank" rel="noopener noreferrer" {...props} />
}
