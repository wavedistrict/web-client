import { getColor, getDuration } from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { css } from "@emotion/core"
import { Theme } from "../../../../modules/theme/types/Theme"

export const linkStyle = (theme: Theme) => css`
  color: ${getColor("accent")({ theme })};
  display: inline-block;
  border-bottom: 1px solid transparent;
  cursor: pointer;

  transition: ${getDuration("normal")({ theme })} ease all;

  &:hover {
    border-bottom-color: ${getColor("accent")({ theme })};
  }
`

export const Link = styled.a`
  ${(props) => linkStyle(props.theme)}
`
