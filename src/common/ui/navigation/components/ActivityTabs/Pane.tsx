import { cover } from "polished"
import React from "react"
import { PRIMARY_MODAL_MQ } from "../../../../../modules/modal/components/PrimaryModal"
import {
  getColor,
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../../../modules/theme/helpers"
import { styled } from "../../../../../modules/theme/themes"
import {
  EmptyState,
  EmptyStateProps,
} from "../../../../state/components/EmptyState"
import { SquareButton } from "../../../button/components/SquareButton"
import { useViewport } from "../../../react/hooks/useViewport"
import {
  NAVIGATION_BODY_WIDTH,
  NAVIGATION_HEADER_HEIGHT,
  NAVIGATION_MQ,
} from "../../constants"
import { useActivity } from "../../hooks/useActivity"

export interface PaneProps {
  children: React.ReactNode
  empty?: EmptyStateProps
}

const defaultEmpty: EmptyStateProps = {
  icon: "search",
  message: "Select a category",
}

export interface ContainerProps {
  open: boolean
  viewportHeight: number
}

const Container = styled.div<ContainerProps>`
  ${cover()}

  display: flex;
  flex-direction: column;

  height: ${(props) => props.viewportHeight}px;
  max-width: 100vw;

  background: ${getColor("primary")};
  transition: ${getDuration("normal")} ease transform;

  transform: translateX(100vw);

  > .empty {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
  }

  > .body {
    flex: 1;
  }

  ${(props) =>
    props.open &&
    `
    transform: translateX(0%);
    `}

  @media ${NAVIGATION_MQ} {
    width: ${NAVIGATION_BODY_WIDTH * 2}px;

    position: relative;
    border-left: solid 1px ${getColor("divider")};
    transform: translateX(0%);
  }

  ${PRIMARY_MODAL_MQ} {
    height: ${NAVIGATION_BODY_WIDTH * 2}px;
  }
`

export function Pane(props: PaneProps) {
  const { children, empty = defaultEmpty } = props
  const activity = useActivity()
  const viewport = useViewport()

  const renderContent = () => {
    if (!activity.current)
      return (
        <div className="empty">
          <EmptyState {...empty} />
        </div>
      )
    return children
  }

  return (
    <Container viewportHeight={viewport.height} open={activity.open}>
      {renderContent()}
    </Container>
  )
}

export interface HeaderProps {
  titleMap?: Record<string | number, string>
  children?: React.ReactNode
}

const HeaderContainer = styled.div`
  height: ${NAVIGATION_HEADER_HEIGHT}px;
  background: ${getColor("primary")};

  border-bottom: solid 1px ${getColor("divider")};

  display: flex;
  align-items: center;
  flex-shrink: 0;

  > .title {
    margin-left: ${getSpacing("4")}px;

    font-size: ${getFontSize("3")}px;
    font-weight: ${getFontWeight("bold")};

    flex: 1;
  }

  @media ${NAVIGATION_MQ} {
    > .close {
      display: none;
    }
  }
`

Pane.Header = function Header(props: HeaderProps) {
  const { current, close } = useActivity()
  if (!current) throw new Error("Current cannot be undefined")

  const title = props.titleMap ? props.titleMap[current] : "Header"

  return (
    <HeaderContainer>
      <div className="close">
        <SquareButton
          size={NAVIGATION_HEADER_HEIGHT}
          icon="back"
          onClick={close}
        />
      </div>
      {props.children ? props.children : <h1 className="title">{title}</h1>}
    </HeaderContainer>
  )
}

export interface BodyProps<T> {
  children: (value: T) => React.ReactNode
}

const BodyContainer = styled.div`
  flex: 1;
  height: calc(100% - ${NAVIGATION_HEADER_HEIGHT}px);
`

Pane.Body = function Body<T>(props: BodyProps<T>) {
  const { children } = props
  const { current } = useActivity()

  if (!current) throw new Error("Current cannot be undefined")

  return <BodyContainer>{children(current)}</BodyContainer>
}
