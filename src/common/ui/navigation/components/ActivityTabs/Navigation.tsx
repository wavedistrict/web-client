import { size } from "polished"
import React from "react"
import {
  getColor,
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
} from "../../../../../modules/theme/helpers"
import { styled } from "../../../../../modules/theme/themes"
import { IconType } from "../../../icon"
import { Icon } from "../../../icon/components/Icon"
import {
  NAVIGATION_BODY_WIDTH,
  NAVIGATION_HEADER_HEIGHT,
  NAVIGATION_MQ,
} from "../../constants"
import { useActivity } from "../../hooks/useActivity"

export interface NavigationProps {
  children: React.ReactNode
}

const Container = styled.div`
  max-width: 100vw;
  flex: 1;

  @media ${NAVIGATION_MQ} {
    min-width: ${NAVIGATION_BODY_WIDTH}px;
  }
`

export function Navigation(props: NavigationProps) {
  return <Container>{props.children}</Container>
}

export interface HeaderProps {
  title: string
  children?: React.ReactNode
}

const HeaderContainer = styled.div`
  height: ${NAVIGATION_HEADER_HEIGHT}px;

  background: ${getColor("primary")};
  border-bottom: solid 1px ${getColor("divider")};

  display: flex;
  align-items: center;

  > h1 {
    margin-left: ${getSpacing("4")}px;
    font-size: ${getFontSize("3")}px;

    font-weight: ${getFontWeight("bold")};
    flex: 1;
  }
`

Navigation.Header = function Header(props: HeaderProps) {
  return (
    <HeaderContainer>
      <h1>{props.title}</h1>
      <div className="actions">{props.children}</div>
    </HeaderContainer>
  )
}

const BodyContainer = styled.div`
  max-width: 100vw;
  height: calc(100% - ${NAVIGATION_HEADER_HEIGHT}px);
`

export interface BodyProps {
  children: React.ReactNode
}

Navigation.Body = function Body(props: BodyProps) {
  return <BodyContainer>{props.children}</BodyContainer>
}

const ItemContainer = styled.div<{
  active: boolean
}>`
  padding: ${getSpacing("4")}px;

  display: flex;
  align-items: center;

  transition: ${getDuration("normal")} ease all;

  border-right: solid 3px transparent;

  > .icon {
    ${size(24)};

    svg {
      fill: ${getColor("accent")};
    }
  }

  > .label {
    margin-left: ${getSpacing("4")}px;
    font-weight: ${getFontWeight("bold")};
  }

  &:hover {
    cursor: pointer;
    background: ${getTransparency("weakNegative")};
  }

  ${(props) =>
    props.active &&
    `
    border-color: ${props.theme.colors.accent};
  `}
`

export interface ItemProps<T> {
  value: T
  label: string
  icon: IconType
}

Navigation.Item = function Item<T extends string | number>(
  props: ItemProps<T>,
) {
  const { value, icon, label } = props
  const activity = useActivity<T>()

  const isActive = value === activity.current && activity.open

  const handleChange = () => {
    activity.setCurrent(value)
  }

  return (
    <ItemContainer onClick={handleChange} active={isActive}>
      <div className="icon">
        <Icon name={icon} />
      </div>
      <div className="label">{label}</div>
    </ItemContainer>
  )
}
