import { cover } from "polished"
import React, { useState } from "react"
import { styled } from "../../../../../modules/theme/themes"
import { NAVIGATION_MQ } from "../../constants"

export interface ActivityTabsProps<T extends string | number> {
  initial?: T
  initiallyOpen?: boolean
  children: React.ReactNode
}

export interface ActivityContextState<T extends string | number> {
  current?: T
  open: boolean
  close: () => void
  setCurrent: (v: T) => void
}

export const ActivityContext = React.createContext<
  ActivityContextState<any> | undefined
>(undefined)

const Container = styled.div`
  position: relative;
  display: flex;

  ${cover()}

  @media ${NAVIGATION_MQ} {
    position: relative;
  }
`

export function ActivityTabs<T extends string | number = string>(
  props: ActivityTabsProps<T>,
) {
  const [current, setCurrent] = useState<T | undefined>(props.initial)
  const [open, setOpen] = useState(props.initiallyOpen || false)

  const state: ActivityContextState<T> = {
    current,
    open,
    close: () => setOpen(false),
    setCurrent: (v) => {
      setCurrent(v)
      setOpen(true)
    },
  }

  return (
    <ActivityContext.Provider value={state}>
      <Container>{props.children}</Container>
    </ActivityContext.Provider>
  )
}
