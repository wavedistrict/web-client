import css from "@emotion/css"
import { size } from "polished"
import React from "react"
import { PLAYER_CONTROLS_HEIGHT } from "../../../../modules/player/constants"
import { getDuration, getSpacing } from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { IconType } from "../../icon"
import { Icon } from "../../icon/components/Icon"

export type TabVariants = "borderless" | "icon-only" | "overlay"

export interface TabItem {
  key: string
  label?: string
  icon?: IconType
  isActive?: boolean
  onClick?: () => void
}

export interface TabsProps {
  items: TabItem[]
  variants?: TabVariants[]
}

const mq = "@media (max-width: 600px)"

const List = styled.ul<{ hasBorder: boolean }>`
  display: flex;
  ${({ hasBorder, theme }) =>
    hasBorder
      ? `border-bottom: solid 2px ${theme.transparencies.weakPositive};`
      : "border: none;"}
`

const Item = styled.li<{
  hasBorder: boolean
  isActive: boolean
  iconOnly: boolean
}>`
  display: flex;
  align-items: center;
  margin-bottom: ${({ hasBorder }) => (hasBorder ? "-2px" : "0px")};

  text-transform: uppercase;
  font-weight: 600;

  padding-bottom: ${getSpacing("4")}px;

  border-bottom: solid 2px;
  border-bottom-color: ${({ isActive, theme }) =>
    isActive ? theme.colors.accent : "transparent"};

  transition: ${getDuration("normal")} ease all;
  opacity: ${({ isActive }) => (isActive ? "1" : "0.5")};

  > .icon {
    ${size(16)};

    margin-right: ${getSpacing("3")}px;
    margin-top: ${getSpacing("1")}px;
  }

  &:not(:last-child) {
    margin-right: ${({ iconOnly, theme }) =>
      iconOnly ? "0" : theme.spacings[6]}px;
  }

  &:hover {
    opacity: 1;
    cursor: pointer;
  }

  ${({ iconOnly }) =>
    !iconOnly &&
    `
    ${mq} {
      > .icon {
        display: none;
      }
    }
  `}

  ${({ iconOnly }) =>
    iconOnly &&
    css`
      justify-content: center;
      align-items: center;
      padding: 0;
      margin-right: 0;

      ${size(PLAYER_CONTROLS_HEIGHT)};

      > .label {
        display: none;
      }

      > .icon {
        ${size(24)};
        margin: 0;
      }
    `}
`

export function Tabs(props: TabsProps) {
  const { items, variants = [] } = props

  const objectVariants = {
    hasBorder: !variants.includes("borderless"),
    iconOnly: variants.includes("icon-only"),
  }

  return (
    <List {...objectVariants}>
      {items.map((item) => {
        const { key, isActive = false, label, icon, onClick } = item

        const onClickSafe = () => {
          if (onClick) onClick()
        }

        const renderedLabel = label && <h1 className="label">{label}</h1>
        const renderedIcon = icon && (
          <div className="icon">
            <Icon name={icon} />
          </div>
        )

        return (
          <Item
            isActive={isActive}
            key={key}
            onClick={onClickSafe}
            {...objectVariants}
          >
            {renderedIcon}
            {renderedLabel}
          </Item>
        )
      })}
    </List>
  )
}
