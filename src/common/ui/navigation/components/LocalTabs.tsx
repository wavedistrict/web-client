import css from "@emotion/css"
import { observer } from "mobx-react"
import React, { useState } from "react"
import { Theme } from "../../../../modules/theme/types/Theme"
import { TabItem, Tabs, TabVariants } from "./Tabs"

export interface LocalTabItem
  extends Omit<TabItem, "isActive" | "onClick" | "key"> {
  renderContent: () => React.ReactElement<any>
}

export interface LocalTabsRenderProps {
  tabs: React.ReactElement<any>
  content: React.ReactElement<any>
}

export interface LocalTabsProps {
  tabs: Record<string, LocalTabItem>
  variants?: TabVariants[]
  children?: (props: LocalTabsRenderProps) => React.ReactElement<any>
  onChange?: (tab: string) => void
}

const defaultChildren = (props: LocalTabsRenderProps) => (
  <>
    <div className="LocalTabs">
      <div
        className="tabs"
        css={(theme: Theme) => css`
          margin-bottom: ${theme.spacings["6"]}px;
        `}
      >
        {props.tabs}
      </div>
      <div className="content">{props.content}</div>
    </div>
  </>
)

export const LocalTabs = observer(function LocalTabs(props: LocalTabsProps) {
  const { children = defaultChildren, tabs, variants, onChange } = props

  const [selected, setSelected] = useState(Object.keys(props.tabs)[0])

  const tabItems = Object.entries(tabs).map(([key, value]) => {
    const onClick = () => {
      setSelected(key)
      if (onChange) onChange(key)
    }

    return {
      ...value,
      isActive: key === selected,
      onClick,
      key,
    }
  })

  const renderedTabs = <Tabs variants={variants} items={tabItems} />
  const renderedContent = tabs[selected].renderContent()

  return children({
    tabs: renderedTabs,
    content: renderedContent,
  })
})
