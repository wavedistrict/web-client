import { observable } from "mobx"
import { observer } from "mobx-react"
import React from "react"
import ReactDOM from "react-dom"
import { ScrollInfo } from "../types/ScrollInfo"
import { ScrollLock } from "./ScrollLock"
import { IS_SERVER } from "../../../../modules/core/constants"

export interface ScrollerContextRenderProps {
  height: number
  scrollTop: number
  isScrolling: boolean
}

export interface ScrollerContextProps {
  children: (props: ScrollerContextRenderProps) => React.ReactNode
  onScroll?: (info: ScrollInfo) => void
}

@observer
export class ScrollerContext extends React.Component<ScrollerContextProps> {
  @observable private offset = 0
  @observable private scrollY = 0
  @observable private isScrolling = false

  private timer: any

  public componentDidMount() {
    window.addEventListener("scroll", this.handleScroll, { passive: true })
    this.offset = this.getOffset()
  }

  public componentDidUpdate() {
    this.emitScroll()
  }

  public componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll)
  }

  private handleScroll = () => {
    clearTimeout(this.timer)
    this.isScrolling = true

    if (window.scrollY !== this.scrollY) {
      this.scrollY = window.scrollY
      this.emitScroll()
    }

    this.timer = setTimeout(() => {
      this.isScrolling = false
    }, 150)
  }

  private emitScroll() {
    const { onScroll } = this.props

    if (onScroll) {
      onScroll({
        scrollTop: this.scrollY,
        scrollHeight: document.documentElement.scrollHeight,
        clientHeight: window.innerHeight,
      })
    }
  }

  private getOffset() {
    const element = ReactDOM.findDOMNode(this)

    if (element instanceof Element) {
      const { top } = element.getBoundingClientRect()
      return top + window.scrollY
    }

    return 0
  }

  public render() {
    const { children } = this.props
    const { offset, scrollY, isScrolling } = this

    if (IS_SERVER) {
      return children({
        height: 720,
        isScrolling: false,
        scrollTop: 0,
      })
    }

    return (
      <ScrollLock.Consumer>
        {([locked, cachedScrollY]) => {
          const scrollTop = locked ? cachedScrollY - offset : scrollY - offset

          return children({
            height: window.innerHeight,
            isScrolling,
            scrollTop,
          })
        }}
      </ScrollLock.Consumer>
    )
  }
}
