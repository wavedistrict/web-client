import { cover } from "polished"
import React, { useImperativeHandle, useRef } from "react"
import { styled } from "../../../../modules/theme/themes"
import { ScrollInfo } from "../types/ScrollInfo"
import { ScrollRefMethods } from "../types/ScrollRefMethods"

export interface InternalNativeScrollContainerProps {
  onScroll?: (info: ScrollInfo) => void
  children?: React.ReactNode
  forwardedRef: React.Ref<ScrollRefMethods>
  autoHeight?: boolean
  autoHeightMax?: number | string
}

const Container = styled.div`
  height: 100%;
  width: 100%;

  position: relative;
  overflow: hidden;

  display: flex;
  flex-direction: column;
  flex: 1;
`

const View = styled.div`
  ${cover()};
  overflow: auto;
  overflow-x: hidden;

  transform: translateZ(0); /** Don't remove this, it's for perf **/
`

export function InternalNativeScrollContainer(
  props: InternalNativeScrollContainerProps,
) {
  const {
    onScroll,
    children,
    forwardedRef,
    autoHeight,
    autoHeightMax = 10,
  } = props
  const ref = useRef<HTMLDivElement>(null)

  useImperativeHandle(forwardedRef, () => ({
    getValues,
    scrollTop,
    scrollToTop,
    scrollToBottom,
  }))

  const getValues = (): ScrollInfo => {
    const { current: element } = ref

    if (!element)
      return {
        clientHeight: 0,
        scrollHeight: 0,
        scrollTop: 0,
      }

    return {
      clientHeight: element.clientHeight,
      scrollHeight: element.scrollHeight,
      scrollTop: element.scrollTop,
    }
  }

  const scrollTop = (top: number) => {
    const { current: element } = ref
    if (element) element.scrollTop = top
  }

  const scrollToTop = () => {
    scrollTop(0)
  }

  const scrollToBottom = () => {
    const { current: element } = ref
    if (element) scrollTop(element.scrollHeight)
  }

  const handleScroll = (event: React.UIEvent<HTMLDivElement>) => {
    const { currentTarget: element } = event

    const info: ScrollInfo = {
      clientHeight: element.clientHeight,
      scrollHeight: element.scrollHeight,
      scrollTop: element.scrollTop,
    }

    if (onScroll) onScroll(info)
  }

  const containerStyle = autoHeight
    ? {
        maxHeight: autoHeightMax,
        minHeight: 0,
        height: "auto",
      }
    : undefined

  const viewStyle = autoHeight
    ? ({
        maxHeight: autoHeightMax,
        position: "relative",
      } as const)
    : undefined

  return (
    <Container style={containerStyle}>
      <View style={viewStyle} ref={ref} onScroll={handleScroll}>
        {children}
      </View>
    </Container>
  )
}

export type NativeScrollContainerProps = Omit<
  InternalNativeScrollContainerProps,
  "forwardedRef"
>

export const NativeScrollContainer = React.forwardRef<
  ScrollRefMethods,
  NativeScrollContainerProps
>((props, ref) => {
  return <InternalNativeScrollContainer {...props} forwardedRef={ref} />
})
