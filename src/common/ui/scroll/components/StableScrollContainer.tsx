import React, { useLayoutEffect, useRef } from "react"
import { ScrollbarProps } from "react-custom-scrollbars"
import { useWindowEvent } from "../../react/hooks/useWindowEvent"
import { ScrollInfo } from "../types/ScrollInfo"
import { ScrollRefMethods } from "../types/ScrollRefMethods"
import { ScrollContainer } from "./ScrollContainer"

export interface StableScrollContainerProps
  extends Omit<ScrollbarProps, "ref" | "children" | "onScroll"> {
  onScroll?: (info: ScrollInfo) => void
  children: React.ReactNode
}

const KEYBOARD_HEIGHT_PERCENTAGE = 0.5

export function StableScrollContainer(props: StableScrollContainerProps) {
  const { children, onScroll, ...rest } = props

  const scrollRef = useRef<ScrollRefMethods>(null)
  const infoRef = useRef<ScrollInfo>({
    clientHeight: 0,
    scrollHeight: 0,
    scrollTop: 0,
  })

  const withinKeyboardReach = (info: ScrollInfo) => {
    const bottom = info.scrollHeight - info.clientHeight
    const distanceFromBottom = Math.abs(info.scrollTop - bottom)
    const threshold = window.innerHeight * KEYBOARD_HEIGHT_PERCENTAGE

    return distanceFromBottom < threshold
  }

  useLayoutEffect(() => {
    const { current: scrollInfo } = infoRef
    const { current: scrollbars } = scrollRef

    if (!scrollbars) return

    const { scrollHeight } = scrollbars.getValues()
    const difference = Math.abs(scrollInfo.scrollHeight - scrollHeight)

    /** If scrolled to the bottom, keep it at the bottom */
    if (withinKeyboardReach(scrollInfo)) {
      return scrollbars.scrollToBottom()
    }

    /** If scrolled to top, scroll down difference in content */
    if (scrollInfo.scrollTop === 0) {
      return scrollbars.scrollTop(difference)
    }
  }, [children])

  useWindowEvent("resize", () => {
    const { current: scrollbars } = scrollRef

    if (withinKeyboardReach(infoRef.current) && scrollbars) {
      scrollbars.scrollToBottom()
    }
  })

  const handleScroll = (info: ScrollInfo) => {
    infoRef.current = info
    if (onScroll) onScroll(info)
  }

  return (
    <ScrollContainer ref={scrollRef} onScroll={handleScroll} {...rest}>
      {children}
    </ScrollContainer>
  )
}
