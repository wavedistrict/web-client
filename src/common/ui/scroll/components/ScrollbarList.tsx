import React from "react"
import Scrollbars from "react-custom-scrollbars"
import { List, ListProps } from "react-virtualized"
import { ScrollInfo } from "../types/ScrollInfo"
import { ScrollContainer } from "./ScrollContainer"

export class ScrollbarList extends React.Component<ListProps> {
  private listRef = React.createRef<List>()
  private scrollbarsRef = React.createRef<Scrollbars>()

  public componentDidUpdate() {
    this.emitScroll()
  }

  public get list() {
    return this.listRef.current!
  }

  public get grid() {
    return this.list.Grid!
  }

  public get scrollbars() {
    return this.scrollbarsRef.current!
  }

  private handleScroll = (info: ScrollInfo) => {
    const { scrollTop } = info

    if (this.grid) {
      this.grid.handleScrollEvent({ scrollTop, scrollLeft: 0 })
      this.emitScroll()
    }
  }

  private emitScroll() {
    const { onScroll } = this.props

    if (onScroll) {
      onScroll(this.scrollbars.getValues())
    }
  }

  public componentDidMount() {
    const { scrollToIndex } = this.props

    if (scrollToIndex) this.scrollToRow(scrollToIndex)
  }

  /** This method needed to be reimplemented */
  public scrollToRow(index: number) {
    const { scrollToAlignment = "start" } = this.props

    const offset = this.list.getOffsetForRow({
      alignment: scrollToAlignment,
      index,
    })

    this.list.scrollToRow(index)
    this.scrollbars.scrollTop(offset)
  }

  public render() {
    const { width, height } = this.props
    const { scrollToIndex, ...safeProps } = this.props

    return (
      <ScrollContainer
        style={{ width, height }}
        ref={this.scrollbarsRef}
        onScroll={this.handleScroll}
      >
        <List
          {...safeProps}
          ref={this.listRef}
          style={{
            outline: "none",
            overflowX: "visible",
            overflowY: "visible",
          }}
        />
      </ScrollContainer>
    )
  }
}
