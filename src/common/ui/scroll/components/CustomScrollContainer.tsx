import { cover } from "polished"
import React, { useImperativeHandle, useRef } from "react"
import Scrollbars, { ScrollbarProps } from "react-custom-scrollbars"
import {
  getColor,
  getDuration,
  getTransparency,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { mergeRefs } from "../../react/helpers/mergeRefs"
import { ScrollInfo } from "../types/ScrollInfo"
import { ScrollRefMethods } from "../types/ScrollRefMethods"

type InternalCustomScrollContainerProps = Omit<ScrollbarProps, "onScroll"> & {
  onScroll?: (info: ScrollInfo) => void
  forwardedRef: React.Ref<ScrollRefMethods>
}

const renderElement = (className: string) => (props: any) => {
  const { width, height, ...style } = props.style
  return <div {...{ className, style }} />
}

const VERTICAL_WIDTH = 18
const VERTICAL_PADDING = 7

const StyledScrollbars = styled(Scrollbars)`
  > .verticalTrack {
    width: ${VERTICAL_WIDTH}px;

    right: 0px;
    top: 0px;
    bottom: 0px;

    &:before {
      display: block;
      content: "";

      position: absolute;
      top: 0px;
      bottom: 0px;
      left: 0px;
      right: 0px;

      background: ${getTransparency("weakNegative")};
      margin: ${VERTICAL_PADDING}px;
      border-radius: 3px;

      transition: ${getDuration("normal")} ease all;
    }

    &:hover::before {
      background: ${getTransparency("strongNegative")};
      cursor: pointer;
    }
  }

  > .verticalTrack > .thumb {
    width: 100%;
    cursor: pointer;

    &:before {
      display: block;
      content: "";

      ${cover()};

      border-radius: 3px;
      background: ${getColor("accent")};
      margin: ${VERTICAL_PADDING}px;
    }
  }
`

export const InternalCustomScrollContainer = function InternalCustomScrollContainer(
  props: InternalCustomScrollContainerProps,
) {
  const { onScroll, forwardedRef, ...safeProps } = props

  const scrollbarRef = useRef<Scrollbars>(null)
  const safeRef = mergeRefs(scrollbarRef, forwardedRef)

  useImperativeHandle(safeRef, () => scrollbarRef.current as any)

  const renderView = (props: any) => {
    const { ...safeStyle } = props.style

    const style = {
      ...safeStyle,
      transform: `translateZ(0)`,
    }

    return <div style={style} className="view" />
  }

  const handleScroll = () => {
    const { current: scrollbars } = scrollbarRef

    if (!scrollbars || !onScroll) return

    const { clientHeight, scrollHeight, scrollTop } = scrollbars.getValues()
    onScroll({ clientHeight, scrollHeight, scrollTop })
  }

  const renderProps: Pick<
    ScrollbarProps,
    | "renderTrackVertical"
    | "renderTrackHorizontal"
    | "renderThumbVertical"
    | "renderThumbHorizontal"
  > = {
    renderTrackHorizontal: renderElement("horizontalTrack"),
    renderTrackVertical: renderElement("verticalTrack"),
    renderThumbHorizontal: renderElement("thumb"),
    renderThumbVertical: renderElement("thumb"),
  }

  const fixedProps = {
    onScroll: handleScroll,
    ref: safeRef as any,
  }

  const finalProps = {
    hideTracksWhenNotNeeded: true,
    autoHide: true,
    renderView: renderView,
    ...safeProps,
    ...fixedProps,
    ...renderProps,
  }

  return <StyledScrollbars {...finalProps}>{props.children}</StyledScrollbars>
}

export type CustomScrollContainerProps = Omit<
  InternalCustomScrollContainerProps,
  "forwardedRef"
>

export const CustomScrollContainer = React.forwardRef<
  ScrollRefMethods,
  CustomScrollContainerProps
>((props, ref) => {
  return <InternalCustomScrollContainer {...props} forwardedRef={ref} />
})
