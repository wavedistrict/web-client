import React, { useImperativeHandle, useRef } from "react"
import { useSetting } from "../../../../modules/settings/hooks/useSetting"
import { ScrollInfo } from "../types/ScrollInfo"
import { ScrollRefMethods } from "../types/ScrollRefMethods"
import {
  CustomScrollContainer,
  CustomScrollContainerProps,
} from "./CustomScrollContainer"
import {
  NativeScrollContainer,
  NativeScrollContainerProps,
} from "./NativeScrollContainer"

export type MergedScrollContainerProps = Omit<
  CustomScrollContainerProps & NativeScrollContainerProps,
  "ref"
>

export interface InternalScrollContainerProps
  extends MergedScrollContainerProps {
  onScroll?: (info: ScrollInfo) => void
  children?: React.ReactNode
  forwardedRef: React.Ref<ScrollRefMethods>
}

export function InternalScrollContainer(props: InternalScrollContainerProps) {
  const { forwardedRef, ...rest } = props
  const custom = useSetting("appearance", "customScrollbars")
  const ref = useRef<ScrollRefMethods>(null)

  useImperativeHandle(forwardedRef, () => ref.current as ScrollRefMethods)

  if (custom) {
    return <CustomScrollContainer ref={ref} {...rest} />
  } else {
    return <NativeScrollContainer ref={ref} {...rest} />
  }
}

export const ScrollContainer = React.forwardRef<
  ScrollRefMethods,
  Omit<InternalScrollContainerProps, "forwardedRef">
>((props, ref) => {
  return <InternalScrollContainer {...props} forwardedRef={ref} />
})
