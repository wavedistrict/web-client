import React, { CSSProperties } from "react"

interface ScrollLockProps {
  locked: boolean
  children: (style?: CSSProperties) => React.ReactNode
}

export const ScrollLockContext = React.createContext<[boolean, number]>([
  false,
  0,
])

const { Provider, Consumer } = ScrollLockContext

export class ScrollLock extends React.Component<ScrollLockProps> {
  public static Consumer = Consumer
  private scrollY = 0

  public shouldComponentUpdate() {
    if (!this.props.locked) {
      this.scrollY = window.scrollY
    }

    return true
  }

  public componentDidUpdate() {
    if (!this.props.locked) {
      window.scrollTo(0, this.scrollY)
    }
  }

  public render() {
    const { locked, children } = this.props

    const style: CSSProperties | undefined = locked
      ? {
          position: "fixed",
          left: "0px",
          right: "0px",
          top: `-${this.scrollY}px`,
        }
      : undefined

    return <Provider value={[locked, this.scrollY]}>{children(style)}</Provider>
  }
}
