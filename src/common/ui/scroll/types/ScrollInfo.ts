export interface ScrollInfo {
  clientHeight: number
  scrollHeight: number
  scrollTop: number
}
