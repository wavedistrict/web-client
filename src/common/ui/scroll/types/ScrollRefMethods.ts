import { ScrollInfo } from "./ScrollInfo"

export interface ScrollRefMethods {
  scrollTop(top: number): void
  scrollToTop(): void
  scrollToBottom(): void
  getValues(): ScrollInfo
}
