import Scrollbars from "react-custom-scrollbars"

class ScrollStore {
  private scroller?: Scrollbars

  public setScroller(scroller: Scrollbars) {
    this.scroller = scroller
  }

  public restoreScroll(scrollY: number) {
    const { scroller } = this
    if (scroller) {
      requestAnimationFrame(() => scroller.scrollTop(scrollY))
    }
  }

  public getScroll() {
    const result = { y: 0 }

    if (this.scroller) {
      result.y = this.scroller.getScrollTop()
    }

    return result
  }
}

export const scrollStore = new ScrollStore()
