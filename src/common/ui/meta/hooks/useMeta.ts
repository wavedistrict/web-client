import { MetaInformation } from "../stores/metaStore"
import { useStores } from "../../../state/hooks/useStores"
import { useUniversalEffect } from "../../react/hooks/useUniversalEffect"

export const useMeta = (newMeta: Partial<MetaInformation>) => {
  const { metaStore } = useStores()

  useUniversalEffect(() => {
    metaStore.setValue(newMeta)

    return () => {
      metaStore.setValue({})
    }
  }, [newMeta])
}
