import { observable, computed } from "mobx"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../state/classes/InitializableStore"

export interface MetaInformation {
  type: string
  title: string
  description: string
  image?: string
}

const DEFAULT: MetaInformation = {
  type: "website",
  title: "WaveDistrict",
  description:
    "Upload your audio or listen to other people's audio on WaveDistrict.",
}

export class MetaStore extends InitializableStore {
  @observable public value = DEFAULT

  public init() {}

  public reset() {
    this.value = DEFAULT
  }

  public setValue(newValue: Partial<MetaInformation>) {
    this.value = { ...DEFAULT, ...newValue }
  }

  @computed
  public get documentTitle() {
    const { playerStore, activityStore } = this.manager.stores

    const { track, isPlaying } = playerStore
    const { unreadAll } = activityStore

    let result = this.value.title

    if (track && isPlaying) {
      result = `${track.data.title}`
    }

    if (unreadAll > 0) {
      result = `(${unreadAll}) ${result}`
    }

    if (result !== DEFAULT.title) {
      result = `${result} | WaveDistrict`
    }

    return result
  }
}

export const metaStore = createStoreFactory(MetaStore)
