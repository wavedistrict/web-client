const MAX_LENGTH = 300

export const getSafeString = (str = "") => {
  if (!str) return ""
  if (str.length > MAX_LENGTH) return str.slice(0, MAX_LENGTH).trim() + "..."

  return str
}
