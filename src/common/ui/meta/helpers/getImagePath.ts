import { MediaModel } from "../../../media/models/Media"
import { ImageReferenceData } from "../../../media/types/ImageReferenceData"

export const getImagePath = (model?: MediaModel<ImageReferenceData>) => {
  if (!model) return ""

  const source = model.getSource("jpeg-medium")
  if (!source) return ""

  return source.path
}
