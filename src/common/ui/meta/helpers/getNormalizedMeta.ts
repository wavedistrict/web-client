import { CANONICAL_HOST } from "../../../../modules/core/constants"

const normalMap: Record<string, string[]> = {
  description: ["description", "og:description", "twitter:description"],
  title: ["og:title", "twitter:title"],
  url: ["og:url"],
  image: ["og:image", "twitter:image"],
  audio: ["og:audio"],
  card: ["twitter:card"],
  site: ["twitter:site"],
  type: ["og:type"],
  siteName: ["og:site_name"],
  color: ["theme-color"],
}

export interface MetaObj {
  type: string
  url: string
  [key: string]: string
}

export const getNormalizedMeta = (obj: MetaObj) => {
  const result: any = {}

  obj.url = CANONICAL_HOST + obj.url

  for (const [key, value] of Object.entries(obj)) {
    const normalized = normalMap[key]

    if (normalized) {
      for (const key of normalized) {
        result[key] = value
      }
    } else {
      result[key] = value
    }
  }

  return result as Record<string, string>
}
