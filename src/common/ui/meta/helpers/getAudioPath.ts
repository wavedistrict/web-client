import { getPathSafely } from "../../../media/helpers/getPathSafely"
import { MediaModel } from "../../../media/models/Media"
import { AudioReferenceData } from "../../../media/types/AudioReferenceData"

export const getAudioPath = (model?: MediaModel<AudioReferenceData>) =>
  getPathSafely(model, "mp3-low")
