import { cloneElement } from "react"
import { icons, IconType } from ".."

export interface IconProps {
  name: IconType
  className?: string
}

export function Icon({ name, className }: IconProps) {
  const icon = cloneElement(icons[name], {
    className,
    width: "100%",
    height: "100%",
  })

  return icon
}
