import React from "react"
import {
  FetcherList,
  ListAddPosition,
} from "../../../state/classes/FetcherList"
import { Model } from "../../../state/classes/Model"
import { EmptyStateProps } from "../../../state/components/EmptyState"
import { ConsumeListButton } from "../components/ConsumeListButton"
import {
  FetcherListCursor,
  FetcherListCursorRenderProps,
} from "../components/FetcherListCursor"
import { FetcherListEmpty } from "../components/FetcherListEmpty"
import { FetcherListTail } from "../components/FetcherListTail"

export interface RenderWindowListOptions<T extends Model> {
  list: FetcherList<T>
  state?: EmptyStateProps
  pending?: ListAddPosition | null
  render: (props: FetcherListCursorRenderProps<T>) => React.ReactNode
}

/** Automatically renders a window scrolling fetcher list */
export const renderWindowList = <T extends Model>(
  options: RenderWindowListOptions<T>,
) => {
  const { list, render, pending = "first", state } = options

  const renderConsume = (position: ListAddPosition | null) => {
    if (position === pending && position !== null && list.pendingCount > 0) {
      return <ConsumeListButton list={list} position={position} />
    }
  }

  return (
    <FetcherListCursor window tail="bottom" list={list}>
      {(props) => (
        <>
          {renderConsume("first")}
          {render(props)}
          {renderConsume("last")}
          <FetcherListEmpty state={state} list={list} />
          <FetcherListTail list={list} />
        </>
      )}
    </FetcherListCursor>
  )
}
