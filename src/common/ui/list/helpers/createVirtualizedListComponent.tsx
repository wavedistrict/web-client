import React, { useEffect } from "react"
import { SizeCalculator } from "../../../dom/components/SizeCalculator"
import { ScrollbarList } from "../../scroll/components/ScrollbarList"
import { ListComponentOptions } from "../types/ListComponentOptions"
import { ListComponentProps } from "../types/ListComponentProps"

export function createVirtualizedListComponent<T>(
  options: ListComponentOptions<T>,
) {
  const { rowHeight, renderItem, cache } = options

  function VirtualizedListComponent(
    props: ListComponentProps<T> & { _ref?: React.Ref<ScrollbarList> },
  ) {
    const { items, _ref, ...listProps } = props

    useEffect(() => {
      return () => {
        if (cache) {
          cache.clearAll()
        }
      }
    }, [])

    return (
      <SizeCalculator>
        {({ width, height }) => (
          <ScrollbarList
            ref={_ref}
            style={{
              outline: "none",
              overflowY: "hidden",
              overflowX: "hidden",
            }}
            rowRenderer={(props) => renderItem(items, props)}
            rowCount={items.length}
            {...{
              rowHeight,
              height,
              cache,
              width,
              ...listProps,
            }}
          />
        )}
      </SizeCalculator>
    )
  }

  return React.forwardRef<ScrollbarList, ListComponentProps<T>>(
    (props, ref) => <VirtualizedListComponent _ref={ref} {...props} />,
  ) as React.ComponentType<ListComponentProps<T>>
}
