import React from "react"
import { CellMeasurer, CellMeasurerCache } from "react-virtualized"
import { ListComponentOptions } from "../types/ListComponentOptions"

export interface RenderItemProps<T> {
  style: React.CSSProperties
  measure: () => void
  index: number
  item: T
}

/** Wraps a row in a cell measurer */
export function wrapDynamicHeightRow<T>(
  render: (props: RenderItemProps<T>) => React.ReactNode,
  cache: CellMeasurerCache,
): ListComponentOptions<T>["renderItem"] {
  return (items, { style, index, key, parent }) => {
    const item = items[index]

    return (
      <CellMeasurer {...{ key, parent, cache, rowIndex: index }}>
        {({ measure }) =>
          render({
            style,
            measure,
            index,
            item,
          })
        }
      </CellMeasurer>
    )
  }
}
