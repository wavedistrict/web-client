import React, { useEffect } from "react"
import { List } from "react-virtualized"
import { SizeCalculator } from "../../../dom/components/SizeCalculator"
import { ScrollerContext } from "../../scroll/components/ScrollerContext"
import { ListComponentOptions } from "../types/ListComponentOptions"
import { ListComponentProps } from "../types/ListComponentProps"

export function createWindowListComponent<T>(options: ListComponentOptions<T>) {
  const { rowHeight, renderItem, cache } = options

  function WindowListComponent(
    props: ListComponentProps<T> & { _ref?: React.Ref<List> },
  ) {
    const { items, _ref } = props

    useEffect(() => {
      return () => {
        if (cache) {
          cache.clearAll()
        }
      }
    }, [])

    return (
      <SizeCalculator>
        {({ width }) => (
          <ScrollerContext onScroll={props.onScroll}>
            {({ height, isScrolling, scrollTop }) => (
              <List
                ref={_ref}
                autoHeight
                style={{ outline: "none" }}
                rowRenderer={(props) => renderItem(items, props)}
                rowCount={items.length}
                {...{
                  isScrolling,
                  scrollTop,
                  rowHeight,
                  height,
                  cache,
                  width,
                }}
              />
            )}
          </ScrollerContext>
        )}
      </SizeCalculator>
    )
  }

  return React.forwardRef<List, ListComponentProps<T>>((props, ref) => (
    <WindowListComponent _ref={ref} {...props} />
  )) as React.ComponentType<ListComponentProps<T>>

  //return WindowListComponent as React.ComponentType<ListComponentProps<T>>
}
