import React from "react"
import { FetcherList } from "../../../state/classes/FetcherList"
import { Model } from "../../../state/classes/Model"
import { EmptyStateProps } from "../../../state/components/EmptyState"
import {
  FetcherListCursor,
  FetcherListCursorRenderProps,
} from "../components/FetcherListCursor"
import { FetcherListEmpty } from "../components/FetcherListEmpty"
import { FetcherListTail } from "../components/FetcherListTail"
import { ListTail } from "../types/ListTail"

export interface RenderContainerListOptions<T extends Model> {
  tail?: ListTail
  state?: EmptyStateProps
  list: FetcherList<T>
  render: (props: FetcherListCursorRenderProps<T>) => React.ReactNode
}

const STYLE: React.CSSProperties = {
  width: "100%",
  height: "100%",
  position: "relative",
}

/** Automatically renders a container scrolling fetcher list */
export const renderContainerList = <T extends Model>(
  options: RenderContainerListOptions<T>,
) => {
  const { list, tail = "bottom", render, state } = options

  return (
    <FetcherListCursor tail={tail} list={list}>
      {(props) => (
        <div style={STYLE}>
          {render(props)}
          <FetcherListEmpty absolute={!!tail} state={state} list={list} />
          <FetcherListTail absolute={tail} list={list} />
        </div>
      )}
    </FetcherListCursor>
  )
}
