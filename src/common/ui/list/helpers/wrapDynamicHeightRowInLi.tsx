import React from "react"
import { CellMeasurer, CellMeasurerCache } from "react-virtualized"
import { ListComponentOptions } from "../types/ListComponentOptions"

export function wrapDynamicHeightRowInLi<T>(
  render: (item: T, index: number, measure: () => void) => React.ReactNode,
  cache: CellMeasurerCache,
): ListComponentOptions<T>["renderItem"] {
  return (items, { style, index, key, parent }) => {
    const item = items[index]

    return (
      <CellMeasurer {...{ key, parent, cache, rowIndex: index }}>
        {({ measure }) => (
          <li key={key} style={style}>
            {render(item, index, measure)}
          </li>
        )}
      </CellMeasurer>
    )
  }
}
