import React from "react"
import { ListComponentOptions } from "../types/ListComponentOptions"

/** Wraps the row renderer in a li with the style added */
export function wrapRowInLi<T>(
  render: (item: T, index: number) => React.ReactNode,
): ListComponentOptions<T>["renderItem"] {
  return (items, { style, index, key }) => {
    const item = items[index]

    return (
      <li key={key} style={style}>
        {render(item, index)}
      </li>
    )
  }
}
