import css from "@emotion/css"
import { observer } from "mobx-react"
import React from "react"
import { pluralize } from "../../../lang/string/helpers/pluralize"
import {
  FetcherList,
  ListAddPosition,
} from "../../../state/classes/FetcherList"
import { styled } from "../../../../modules/theme/themes"
import { PrimaryButton } from "../../button/components/PrimaryButton"

const Container = styled<typeof PrimaryButton, { bottom: boolean }>(
  PrimaryButton,
)`
  ${(props) => {
    const { theme, bottom } = props

    if (bottom) {
      return css`
        margin-top: ${theme.spacings[4]}px;
      `
    }

    return css`
      margin-bottom: ${theme.spacings[4]}px;
    `
  }}
`

export interface ConsumeListButtonProps {
  list: FetcherList<any>
  position: ListAddPosition
}

export const ConsumeListButton = observer(function ConsumeListButton(
  props: ConsumeListButtonProps,
) {
  const { list, position } = props

  return (
    <Container
      label={`Show ${pluralize("new item", list.pendingCount)}`}
      onClick={() => list.consumePending(position)}
      bottom={position === "last"}
    />
  )
})
