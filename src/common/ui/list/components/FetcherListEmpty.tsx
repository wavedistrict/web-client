import { observer } from "mobx-react"
import { cover } from "polished"
import React from "react"
import { styled } from "../../../../modules/theme/themes"
import { FetcherList } from "../../../state/classes/FetcherList"
import { Model } from "../../../state/classes/Model"
import {
  EmptyState,
  EmptyStateProps,
} from "../../../state/components/EmptyState"

export interface FetcherListEmptyProps<T extends Model> {
  list: FetcherList<T>
  state?: EmptyStateProps
  absolute?: boolean
}

const DEFAULT_STATE: EmptyStateProps = {
  icon: "error",
  message: "List is empty",
}

const Container = styled.div<{ absolute?: boolean }>`
  ${(props) => props.absolute && cover()}
`

@observer
export class FetcherListEmpty<T extends Model> extends React.Component<
  FetcherListEmptyProps<T>
> {
  public render() {
    const { list, state = DEFAULT_STATE, absolute } = this.props

    if (list.hasFetched && list.models.length === 0) {
      return (
        <Container absolute={absolute}>
          <EmptyState {...state} />
        </Container>
      )
    }

    return null
  }
}
