import { observer } from "mobx-react"
import React, { useContext, useEffect, useRef } from "react"
import {
  FetcherList,
  FetcherListStatus,
} from "../../../state/classes/FetcherList"
import { Model } from "../../../state/classes/Model"
import { ScrollLockContext } from "../../scroll/components/ScrollLock"
import { ScrollInfo } from "../../scroll/types/ScrollInfo"
import { useStores } from "../../../state/hooks/useStores"
import { useUniversalEffect } from "../../react/hooks/useUniversalEffect"

export interface FetcherListCursorRenderProps<T extends Model> {
  onScroll: (info: ScrollInfo) => void
  items: T[]
}

export interface FetcherListCursorProps<T extends Model> {
  children: (props: FetcherListCursorRenderProps<T>) => React.ReactNode
  window?: boolean
  padding?: number
  list: FetcherList<T>
  tail: "top" | "bottom"
}

const SCROLL_PADDING = 450

export const FetcherListCursor = observer(function FetcherListCursor<
  T extends Model
>(props: FetcherListCursorProps<T>) {
  const { ssrStore } = useStores()

  const { list, window } = props

  const [locked] = useContext(ScrollLockContext)
  const infoRef = useRef<ScrollInfo | undefined>(undefined)

  const handleScroll = (info: ScrollInfo) => {
    infoRef.current = info
    fetchIfNecessary()
  }

  const fetchIfNecessary = () => {
    const { current: info } = infoRef

    if (!info) return
    if (window && locked) return

    const { clientHeight, scrollTop, scrollHeight } = info
    const { tail, padding = SCROLL_PADDING } = props

    const height = scrollHeight - clientHeight

    if (tail === "top" && scrollTop < padding) {
      safelyFetch()
    }

    if (tail === "bottom" && scrollTop >= height - padding) {
      safelyFetch()
    }
  }

  const safelyFetch = async () => {
    const { list } = props

    if (list.status === FetcherListStatus.Idle && list.hasNext) {
      await list.fetch()
      fetchIfNecessary()
    }
  }

  useUniversalEffect(() => {
    if (!list.hasFetched) ssrStore.register(safelyFetch())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [list.hasFetched])

  useEffect(() => {
    if (!list.valid && list.status !== FetcherListStatus.Fetching) {
      list.refresh()
    }
  }, [list, list.valid])

  return props.children({
    items: list.models,
    onScroll: handleScroll,
  }) as any
})
