import { observer } from "mobx-react"
import React from "react"
import {
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import {
  FetcherList,
  FetcherListStatus,
} from "../../../state/classes/FetcherList"
import { Model } from "../../../state/classes/Model"
import { LoadingIndicator } from "../../../state/components/LoadingIndicator"
import { PrimaryButton } from "../../button/components/PrimaryButton"

export type AbsoluteDirection = "top" | "bottom"

export interface FetcherListTailProps<T extends Model> {
  absolute?: AbsoluteDirection
  list: FetcherList<T>
}

const Absolute = styled.div<{
  position: AbsoluteDirection
  fetching: boolean
}>`
  position: absolute;
  left: 0px;
  right: 0px;
  opacity: 0;

  transition: ${getDuration("extended")} ease opacity;

  ${(props) => props.position}: 0px;

  ${(props) =>
    props.fetching &&
    `
    opacity: 1;
  `}
`

const Relative = styled.div<{
  fetching: boolean
}>`
  display: none;
  flex-direction: column;

  justify-content: center;
  align-items: center;

  height: 250px;

  ${(props) =>
    props.fetching &&
    `
    display: flex;
  `}
`

const Error = styled.div<{
  error: boolean
}>`
  display: none;
  flex-direction: column;

  justify-content: center;
  align-items: center;

  height: 250px;

  > .message {
    font-size: ${getFontSize("2")}px;
    font-weight: ${getFontWeight("bold")};

    margin-bottom: ${getSpacing("4")}px;
  }

  ${(props) =>
    props.error &&
    `
    pointer-events: all;
    display: flex;
    opacity: 1;
  `}
`

export const FetcherListTail = observer(function FetcherListTail<
  T extends Model
>(props: FetcherListTailProps<T>) {
  const { absolute, list } = props

  const hasError = list.status === FetcherListStatus.Error
  const isFetching = list.status === FetcherListStatus.Fetching

  const renderIndicator = () => {
    if (absolute) {
      return (
        <Absolute position={absolute} fetching={isFetching}>
          <LoadingIndicator bar />
        </Absolute>
      )
    }

    return (
      <Relative fetching={isFetching}>
        <LoadingIndicator />
      </Relative>
    )
  }

  return (
    <div>
      {renderIndicator()}
      <Error error={hasError} className="error">
        <h1 className="message">{"Oops, there was an error loading items"}</h1>
        <PrimaryButton onClick={() => list.fetch()} label="Retry" />
      </Error>
    </div>
  )
})
