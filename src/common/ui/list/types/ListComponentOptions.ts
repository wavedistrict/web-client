import { CellMeasurerCache, ListRowProps, Index } from "react-virtualized"

export interface ListComponentOptions<T> {
  renderItem: (items: T[], props: ListRowProps) => React.ReactNode
  rowHeight: number | ((info: Index) => number)
  cache?: CellMeasurerCache
}
