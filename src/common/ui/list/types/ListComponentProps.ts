import { ListProps } from "react-virtualized"

export interface ListComponentProps<T>
  extends Omit<
    ListProps,
    "rowRenderer" | "rowCount" | "rowHeight" | "height" | "width"
  > {
  items: T[]
}
