import { observer } from "mobx-react"
import { size } from "polished"
import React, { useEffect, useState } from "react"
import {
  getDuration,
  getFontColor,
  getTransparency,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"

export interface SwitchInputProps {
  active?: boolean
  onChange?: (newState: boolean) => void
}

const SWITCH_KNOB_SIZE = 20
const SWITCH_KNOB_PADDING = 5

const Container = styled.div<{
  active: boolean
}>`
  transition: ${getDuration("normal")} ease all;
  width: ${SWITCH_KNOB_SIZE * 2 + SWITCH_KNOB_PADDING * 2}px;
  height: ${SWITCH_KNOB_SIZE + SWITCH_KNOB_PADDING * 2}px;

  background: ${getTransparency("weakNegative")};
  border-radius: 50px;

  padding: ${SWITCH_KNOB_PADDING}px;

  > .knob {
    ${size(SWITCH_KNOB_SIZE)};

    background: ${getFontColor("normal")};
    border-radius: ${SWITCH_KNOB_SIZE}px;
    transition: ${getDuration("normal")} ease all;
  }

  &:hover {
    background: ${getTransparency("strongPositive")};
    cursor: pointer;
  }

  &:active > .knob {
    transform: scale(0.8);
  }

  ${(props) =>
    props.active &&
    `
    > .knob {
      background: ${props.theme.colors.accent};
      transform: translateX(${SWITCH_KNOB_SIZE}px);
    }

    &:active > .knob {
      transform: translateX(${SWITCH_KNOB_SIZE}px) scale(0.8);
    }
  `}
`

export const SwitchInput = observer(function SwitchInput(
  props: SwitchInputProps,
) {
  const { onChange } = props
  const [isActive, setIsActive] = useState(props.active || false)

  useEffect(() => {
    if (props.active !== undefined) {
      setIsActive(props.active)
    }
  }, [props.active])

  const onClick = () => {
    if (props.active !== undefined) {
      setIsActive(!isActive)
    }

    if (onChange) onChange(!isActive)
  }

  return (
    <Container onClick={onClick} active={isActive}>
      <div className="knob" />
    </Container>
  )
})
