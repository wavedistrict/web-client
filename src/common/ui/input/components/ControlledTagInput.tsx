import { observer } from "mobx-react"
import React from "react"
import { TagFieldController } from "../../form/controllers/TagFieldController"
import { TagInput, TagInputProps } from "./TagInput"

export interface ControlledTagInputProps extends TagInputProps {
  controller: TagFieldController
}

@observer
export class ControlledTagInput extends React.Component<
  ControlledTagInputProps
> {
  public render() {
    const { controller, ...otherProps } = this.props

    const handleChange = (values: string[]) => {
      controller.setValue(values)
    }

    return (
      <TagInput
        values={controller.value}
        onChange={handleChange}
        {...otherProps}
      />
    )
  }
}

/**
 * Helper for creating a render prop to render a {@link ControlledTagInput},
 * to be used with {@link FormField}
 *
 * @example
 * <FormField
 *   label="..."
 *   controller={someController}
 *   renderInput={renderControlledTagInput({ ... })}
 * />
 */
export const renderControlledTagInput = (
  props: Omit<ControlledTagInputProps, "controller"> = {},
) => {
  return (controller: TagFieldController) => {
    return <ControlledTagInput {...props} controller={controller} />
  }
}
