import { css } from "@emotion/core"
import React, {
  useCallback,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react"
import {
  getColor,
  getDuration,
  getFontColor,
  getSpacing,
  getTransparency,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { clamp } from "../../../math/helpers/clamp"

export interface SliderInputProps {
  fillValue?: number
  placeholderValue?: number
  steps?: number
  onGrab?: SliderEventHandler
  onRelease?: SliderEventHandler
  onMove?: SliderEventHandler
  onChange?: SliderEventHandler
  disabled?: boolean
  accented?: boolean
}

export type SliderEventHandler = (value: number) => void

type SetValues = (
  fillValue?: number | undefined,
  placeholderValue?: number | undefined,
  silent?: boolean,
) => void

export type SliderInputRef = {
  setValues: SetValues
  isGrabbing: boolean
}

function _SliderInput(props: SliderInputProps, ref: React.Ref<SliderInputRef>) {
  const { onChange, onGrab, onMove, onRelease, steps } = props

  const areaRef = useRef<HTMLDivElement>(null)
  const fillRef = useRef<HTMLDivElement>(null)
  const thumbRef = useRef<HTMLDivElement>(null)
  const placeholderRef = useRef<HTMLDivElement>(null)

  const [isGrabbing, setIsGrabbing] = useState(false)

  const fillValueRef = useRef(props.fillValue || 0)
  const placeholderValueRef = useRef(props.placeholderValue || 0)

  const setValues = useCallback<SetValues>(
    (fillValue, placeholderValue, silent = false) => {
      const fill = fillRef.current
      const thumb = thumbRef.current
      const placeholder = placeholderRef.current

      if (!fill || !placeholder || !thumb) {
        throw `"setValues" may not be called when a SliderInput has not been mounted.`
      }

      if (fillValue !== undefined) {
        const safeFillValue = steps
          ? Math.round(fillValue * steps) / steps
          : fillValue

        fill.style.transform = `scaleX(${safeFillValue})`
        thumb.style.transform = `translateX(-${100 - 100 * safeFillValue}%)`

        fillValueRef.current = safeFillValue
        if (onChange && !silent) onChange(safeFillValue)
      }

      if (placeholderValue !== undefined) {
        placeholder.style.transform = `scaleX(${placeholderValue})`
        placeholderValueRef.current = placeholderValue
      }
    },
    [onChange, steps],
  )

  useImperativeHandle(ref, () => ({
    setValues,
    isGrabbing,
  }))

  useEffect(() => {
    setValues(fillValueRef.current, placeholderValueRef.current)
  }, [setValues])

  useEffect(() => {
    const area = areaRef.current
    if (!area) return

    const handleGrab = (event: PointerEvent) => {
      event.preventDefault()

      setIsGrabbing(true)

      const position = calculatePosition(event)

      addMoveListeners()
      addReleaseListeners()
      setValues(position)

      if (onGrab) onGrab(position)
    }

    const handleRelease = (event: PointerEvent) => {
      event.preventDefault()
      setIsGrabbing(false)

      removeMoveListeners()
      removeReleaseListeners()

      if (onRelease) onRelease(fillValueRef.current)
    }

    const handleMove = (event: PointerEvent) => {
      event.preventDefault()

      const position = calculatePosition(event)

      setValues(position)
      if (onMove) onMove(position)
    }

    const addMoveListeners = () =>
      window.addEventListener("pointermove", handleMove)
    const removeMoveListeners = () =>
      window.removeEventListener("pointermove", handleMove)

    const addReleaseListeners = () =>
      window.addEventListener("pointerup", handleRelease)
    const removeReleaseListeners = () =>
      window.removeEventListener("pointerup", handleRelease)

    area.addEventListener("pointerdown", handleGrab)
    return () => area.removeEventListener("pointerdown", handleGrab)
  }, [setValues, onGrab, onMove, onRelease])

  const calculatePosition = (event: PointerEvent) => {
    const area = areaRef.current
    if (!area) return 0

    const width = area.offsetWidth
    const boundingRect = area.getBoundingClientRect()

    const elementLeftOffset = boundingRect.left + document.body.scrollLeft

    const calculatedPosition = (event.pageX - elementLeftOffset) / width
    return clamp(calculatedPosition, 0, 1)
  }

  const { disabled, accented } = props

  const containerProps = {
    isGrabbing,
    disabled,
    accented,
  }

  return (
    <Container {...containerProps}>
      <div ref={areaRef} className="area" style={{ touchAction: "none" }}>
        <div className="rail">
          <div ref={fillRef} className="fill" />
          <div ref={placeholderRef} className="placeholder" />
          <div ref={thumbRef} className="thumb" />
        </div>
      </div>
    </Container>
  )
}

export const SliderInput = React.forwardRef(_SliderInput)

const Container = styled.div<{
  isGrabbing: boolean
  disabled?: boolean
  accented?: boolean
}>`
  width: 100%;

  > .area {
    padding: ${getSpacing("3")}px 0px;
    cursor: pointer;
  }

  > .area > .rail {
    position: relative;
    height: 2px;

    background: ${getTransparency("weakPositive")};
  }

  > .area > .rail > .fill,
  .placeholder {
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;

    transform-origin: 0;
    transform: scaleX(0);
  }

  > .area > .rail > .fill {
    height: 2px;
    background: ${(props) =>
      props.accented ? getColor("accent") : getFontColor("normal")};
  }

  > .area > .rail > .thumb:after {
    display: block;
    content: "";

    height: 8px;
    width: 8px;

    float: right;

    background: ${getFontColor("normal")};

    margin-right: -4px;
    margin-top: -3px;

    border-radius: 10px;
    transition: ${getDuration("normal")} ease all;

    transform: scale(0);
  }

  > .area > .rail > .placeholder {
    height: 2px;

    background: ${getTransparency("weakPositive")};
  }

  > .area:hover > .rail > .thumb:after {
    transform: scale(${(props) => (props.isGrabbing ? 1.3 : 1)});
  }

  ${(props) => props.disabled && disabledStyle};
`

const disabledStyle = css`
  pointer-events: none;
  opacity: 0.5;
`
