import React from "react"
import { fireEvent } from "react-testing-library"
import { renderWithTheme } from "../../../testing/helpers/renderWithTheme"
import { TagInput } from "./TagInput"

describe.skip("TagInput", () => {
  it("adds values", () => {
    const changeCallback = jest.fn()

    const { container } = renderWithTheme(
      <TagInput values={[]} onChange={changeCallback} />,
    )

    const input = container.querySelector("input")!

    fireEvent.change(input, {
      target: {
        value: "helloworld",
      },
    })

    expect(changeCallback).toHaveBeenCalledWith(["helloworld"])
  })

  it("does not add empty values", () => {
    const changeCallback = jest.fn()

    const { container } = renderWithTheme(
      <TagInput values={[]} onChange={changeCallback} />,
    )

    const input = container.querySelector("input")!

    fireEvent.change(input, {
      target: {
        value: "  ",
      },
    })

    expect(changeCallback).toHaveBeenCalledWith([])
  })

  it("does not add duplicate values", () => {
    const changeCallback = jest.fn()
    const { container } = renderWithTheme(
      <TagInput values={[]} onChange={changeCallback} />,
    )

    const input = container.querySelector("input")!

    fireEvent.change(input, {
      target: {
        value: "dicks",
      },
    })

    expect(changeCallback).toHaveBeenCalledWith(["dicks"])
  })
})
