import React from "react"
import { ConsoleMock } from "../../../testing/classes/ConsoleMock"
import { renderWithTheme } from "../../../testing/helpers/renderWithTheme"
import { TextInput } from "./TextInput"

describe.skip("TextInput", () => {
  test("forwards input props", () => {
    const { container } = renderWithTheme(
      <TextInput type="password" placeholder="dicks" />,
    )
    const input = container.querySelector("input") as HTMLInputElement

    expect(input.type).toBe("password")
    expect(input.placeholder).toBe("dicks")
  })

  test("rendering a textarea with multiline prop", () => {
    const { container } = renderWithTheme(<TextInput multiline />)

    expect(container.querySelector("input")).toBeNull()
    expect(container.querySelector("textarea")).not.toBeNull()
  })

  test("unknown props warning", () => {
    const consoleMock = new ConsoleMock(["error"]).apply()

    renderWithTheme(<TextInput onEnter={() => {}} />)

    expect(consoleMock.mocks.error).not.toBeCalled()

    consoleMock.restore()
  })
})
