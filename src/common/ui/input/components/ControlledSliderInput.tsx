import { observer } from "mobx-react"
import React from "react"
import { RangeFieldController } from "../../form/controllers/RangeFieldController"
import { SliderInput, SliderInputProps } from "./SliderInput"

export interface ControlledSliderInputProps extends SliderInputProps {
  controller: RangeFieldController
}

export const ControlledSliderInput = observer(function ControlledSliderInput(
  props: ControlledSliderInputProps,
) {
  const { controller, ...otherProps } = props
  const { max, min = 0, steps } = controller

  const safeSteps = typeof steps === "boolean" ? max - min : steps
  const rawValue = (controller.value! - min) / (max - min)

  const handleChange = (value: number) => {
    const safeValue = value * (max - min) + min
    controller.setValue(safeValue)
  }

  return (
    <SliderInput
      {...otherProps}
      fillValue={rawValue}
      onChange={handleChange}
      steps={safeSteps}
    />
  )
})
