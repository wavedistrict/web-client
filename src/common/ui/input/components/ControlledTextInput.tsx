import { observer } from "mobx-react"
import React from "react"
import { FieldControllerStatus } from "../../form/classes/FieldController"
import { TextFieldController } from "../../form/controllers/TextFieldController"
import { TextInput, TextInputProps } from "./TextInput"

export interface ControlledTextInputProps extends TextInputProps {
  controller: TextFieldController
}

@observer
export class ControlledTextInput extends React.Component<
  ControlledTextInputProps
> {
  public render() {
    const { controller, ...otherProps } = this.props

    const handleChange = (value: string) => {
      controller.setValue(value)
    }

    return (
      <TextInput
        {...otherProps}
        value={controller.value}
        onChange={handleChange}
        isInvalid={controller.status === FieldControllerStatus.Invalid}
      />
    )
  }
}

/**
 * Helper for creating a render prop to render a {@link ControlledTextInput},
 * to be used with {@link FormField}
 *
 * @example
 * <FormField
 *   label="..."
 *   controller={someController}
 *   renderInput={renderControlledTextInput({ ... })}
 * />
 */
export const renderControlledTextInput = (
  props: Omit<ControlledTextInputProps, "controller"> = {},
) => {
  return (controller: TextFieldController) => {
    return <ControlledTextInput {...props} controller={controller} />
  }
}
