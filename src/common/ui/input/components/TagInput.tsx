import { observer } from "mobx-react"
import React, { useMemo, useState } from "react"
import { Tag } from "../../../../modules/core/components/Tag"
import { getDuration, getSpacing } from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { removeDuplicates } from "../../../lang/array/helpers/removeDuplicates"
import { TextInput } from "./TextInput"

export interface TagInputProps {
  values?: string[]
  onChange?: (values: string[]) => void
}

const Container = styled.div`
  > .items {
    margin-top: ${getSpacing("4")}px;
  }

  > .items > .item {
    opacity: 1;
    display: inline-flex;

    margin-right: ${getSpacing("3")}px;
    margin-bottom: ${getSpacing("3")}px;

    transition: ${getDuration("normal")} ease all;
  }
`

const parseTagsSafely = (str: string) => {
  return removeDuplicates(
    str
      .split(",")
      .map((x) => x.trim().replace(/\s+/, " "))
      .filter((x) => x),
  )
}

export const TagInput = observer(function TagInput(props: TagInputProps) {
  const { values = [], onChange } = props
  const [inputValue, setInputValue] = useState(values.join(","))

  const tags = useMemo(() => parseTagsSafely(inputValue), [inputValue])

  const handleChange = (value: string) => {
    setInputValue(value)
    if (onChange) {
      onChange(parseTagsSafely(value))
    }
  }

  const renderItems = () => {
    return tags.map((value, i) => {
      return (
        <div key={value + i} className="item">
          <Tag name={value} />
        </div>
      )
    })
  }

  return (
    <Container>
      <div className="input">
        <TextInput
          placeholder="Separate tags with a comma"
          value={inputValue}
          onChange={handleChange}
        />
      </div>
      <div className="items">{renderItems()}</div>
    </Container>
  )
})
