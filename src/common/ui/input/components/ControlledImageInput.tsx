import { observer } from "mobx-react"
import { cover, transparentize } from "polished"
import React from "react"
import { Overlay } from "../../../../modules/theme/components/Overlay"
import {
  getDuration,
  getFontColor,
  getFontWeight,
  getTransparency,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { SQUARE_DIMENSIONS, WIDE_DIMENSIONS } from "../../../media/constants"
import { FileUploadStatus } from "../../../network/classes/FileUpload"
import { ProgressBar } from "../../../state/components/ProgressBar"
import { SecondaryButton } from "../../button/components/SecondaryButton"
import { ImageFieldController } from "../../form/controllers/ImageFieldController"
import { ImageReferenceRenderer } from "../../image/components/ImageReferenceRenderer"
import { ImageRenderer } from "../../image/components/ImageRenderer"

interface ControlledImageInputProps {
  controller: ImageFieldController
  isSquare?: boolean
  dimensions?: {
    width: number
    height: number
  }
}

const allowedImageTypes = ["image/jpeg", "image/png", "image/gif", "image/webp"]

const Container = styled.div`
  > .remove {
    width: 100%;
    margin-top: 8px;
  }
`

const Area = styled.label<{
  image: boolean
  uploading: boolean
}>`
  position: relative;
  display: block;

  height: 180px;
  width: 100%;

  background-color: ${getTransparency("weakNegative")};
  border-radius: 3px;
  overflow: hidden;
  transition: ${getDuration("normal")} ease all;

  > .container {
    ${cover()}
  }

  > .container > .image {
    width: 100%;
    height: 100%;
  }

  > input {
    display: none;
  }

  > .progress {
    position: absolute;

    bottom: 0px;
    left: 0px;
    right: 0px;

    transition: 200ms ease opacity;
    opacity: 0;
  }

  &:hover {
    cursor: pointer;
    background: ${getTransparency("weakPositive")};
  }

  ${(props) =>
    props.image &&
    `
      &:hover > .overlay {
        opacity: 1;
      }
    `}

  ${(props) =>
    props.uploading &&
    `
    > .progress {
      opacity: 1;
    }
  `}
`

const ImageOverlay = styled.div<{
  image: boolean
}>`
  ${cover()}

  color: ${getFontColor("normal")};
  transition: ${getDuration("normal")} ease all;

  display: flex;
  align-items: center;
  justify-content: center;

  font-weight: ${getFontWeight("bold")};

  ${(props) =>
    props.image &&
    `
    opacity: 0;
    background: ${transparentize(0.4, props.theme.overlay.background)};
  `}
`

export const ControlledImageInput = observer(function ControlledImageInput(
  props: ControlledImageInputProps,
) {
  const { controller, dimensions, isSquare } = props

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { files } = event.currentTarget
    if (!files) return

    const file = files[0]
    if (!file || !allowedImageTypes.includes(file.type)) return

    controller.setValue(file)
    event.target.value = ""
  }

  const renderImage = () => {
    const { imageURL, reference } = controller

    if (imageURL) return <ImageRenderer source={imageURL} />
    if (reference) return <ImageReferenceRenderer reference={reference} />
  }

  const renderRemoveButton = () => {
    if (!controller.hasImage) return null

    return (
      <div className="remove">
        <SecondaryButton
          stretch
          outlined
          label="Remove"
          onClick={() => {
            controller.remove()
          }}
        />
      </div>
    )
  }

  const wrapInOverlay = (element: JSX.Element) => {
    if (!controller.hasImage) return element

    return <Overlay>{element}</Overlay>
  }

  const { height, width } =
    dimensions || isSquare ? SQUARE_DIMENSIONS : WIDE_DIMENSIONS

  return (
    <Container>
      <Area
        uploading={controller.uploadStatus === FileUploadStatus.Uploading}
        image={!!controller.hasImage}
      >
        <div className="container">
          <div className="image">{renderImage()}</div>
        </div>
        {wrapInOverlay(
          <>
            <ImageOverlay className="overlay" image={!!controller.hasImage}>
              <div className="help">{`${width} x ${height}`}</div>
            </ImageOverlay>
            <div className="progress">
              <ProgressBar value={controller.progress} />
            </div>
          </>,
        )}
        <input type="file" onChange={handleChange} />
      </Area>
      <div className="remove">{renderRemoveButton()}</div>
    </Container>
  )
})

/**
 * Helper for creating a render prop to render a {@link ControlledImageInput},
 * to be used with {@link FormField}
 *
 * @example
 * <FormField
 *   label="..."
 *   controller={someController}
 *   renderInput={renderControlledImageInput({ ... })}
 * />
 */
export const renderControlledImageInput = (
  props: Omit<ControlledImageInputProps, "controller"> = {},
) => {
  return (controller: ImageFieldController) => {
    return <ControlledImageInput {...props} controller={controller} />
  }
}
