import styled from "@emotion/styled"
import { observer } from "mobx-react"
import React, { useState } from "react"
import {
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../../modules/theme/helpers"
import { PrimaryButton } from "../../button/components/PrimaryButton"
import { ObjectFieldController } from "../../form/controllers/ObjectFieldController"
import { TextInput, TextInputProps } from "./TextInput"

export interface ControlledJSONInputProps extends TextInputProps {
  controller: ObjectFieldController
}

const TAB = "    "

const format = (v: any) => JSON.stringify(v, null, 4)

const Container = styled.div`
  width: 100%;

  > .actions {
    margin-top: ${getSpacing("4")}px;

    display: flex;
    align-items: center;
  }

  > .actions > .status {
    margin-left: ${getSpacing("4")}px;

    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("bold")};
  }
`

export const ControlledJSONInput = observer(function ControlledJSONInput(
  props: ControlledJSONInputProps,
) {
  const { controller, ...otherProps } = props
  const formatted = format(controller.value)

  const [valid, setValid] = useState(controller.isValid)
  const [text, setText] = useState(formatted)
  const [parsed, setParsed] = useState(controller.value)

  const handleChange = (value: string) => {
    setText(value)

    try {
      const parsed = JSON.parse(value)

      setValid(true)
      setParsed(parsed)
    } catch {
      setValid(false)
    }
  }

  const handleSave = () => {
    if (valid) {
      controller.setValue(parsed!)
      setText(format(parsed))
    }
  }

  const handleKeyDown = (event: React.KeyboardEvent) => {
    if (event.keyCode === 9) {
      event.preventDefault()
      document.execCommand("insertText", false, TAB)
    }
  }

  const renderStatus = () => {
    if (!valid) return "Invalid JSON"

    if (text === formatted) return "Saved"

    return "Click save to apply changes"
  }

  return (
    <Container>
      <TextInput
        {...otherProps}
        value={text}
        multiline={true}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        isInvalid={!valid || !controller.isValid}
        style={{ fontFamily: "monospace", minHeight: 210 }}
      />
      <div className="actions">
        <PrimaryButton label="Save" disabled={!valid} onClick={handleSave} />
        <div className="status">{renderStatus()}</div>
      </div>
    </Container>
  )
})
