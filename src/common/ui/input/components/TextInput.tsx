import { observer } from "mobx-react"
import React, { useEffect, useRef, useState } from "react"
import {
  getDuration,
  getFontColor,
  getTransparency,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"

// specify that we want to accept both input and textarea props
// then remove the onChange prop so we can override it appropriately
// also remove ref otherwise react gets scared and confused when using this anywhere
type ReactInputProps = JSX.IntrinsicElements["input"]
type AcceptedProps = Omit<ReactInputProps, "onChange" | "ref" | "onEnter">

export interface TextInputProps extends AcceptedProps {
  multiline?: boolean
  isDisabled?: boolean
  isInvalid?: boolean
  onChange?: (value: string) => void
  onEnter?: (value: string) => void
  onFocus?: () => void
  onBlur?: () => void
  onKeyDown?: (event: React.KeyboardEvent<any>) => void
}

const Container = styled.div<{
  focused: boolean
  invalid: boolean
}>`
  width: 100%;

  overflow: hidden;
  border-radius: 3px;

  border-bottom: solid 3px ${getTransparency("weakPositive")};
  background: ${getTransparency("weakNegative")};
  transition: ${getDuration("normal")} ease all;

  font-family: inherit;

  input::placeholder,
  textarea::placeholder {
    color: ${getFontColor("muted")};
  }

  > .input > * {
    padding: 10px;
    width: 100%;

    box-sizing: border-box;
    transition: ${getDuration("normal")} ease border;
    resize: vertical;

    max-height: 300px;

    font-size: 14px;
    font-family: inherit;
  }

  ${(props) =>
    props.focused &&
    `
    border-color: ${props.theme.colors.accent};
  `}

  ${(props) =>
    props.invalid &&
    `
    border-color: ${props.theme.colors.invalid};
  `}
`

export const TextInput = observer(function TextInput(props: TextInputProps) {
  const {
    isInvalid = false,
    autoFocus,
    onChange,
    onEnter,
    onBlur,
    onFocus,
    onKeyDown,
  } = props

  const inputRef = useRef<HTMLInputElement>()
  const [isFocused, setIsFocused] = useState(false)

  useEffect(() => {
    const { current: input } = inputRef

    if (input && autoFocus) {
      input.focus()
    }
  }, [autoFocus])

  const handleInput = (event: React.ChangeEvent<{ value: string }>) => {
    const { value } = event.currentTarget
    if (onChange) onChange(value)
  }

  const handleKeyDown = (event: React.KeyboardEvent<{ value: string }>) => {
    if (event.key === "Enter" && onEnter) {
      event.preventDefault()
      onEnter(event.currentTarget.value)

      return
    }

    if (onKeyDown) {
      onKeyDown(event)
    }
  }

  const handleBlur = () => {
    setIsFocused(false)
    if (onBlur) onBlur()
  }

  const handleFocus = () => {
    setIsFocused(true)
    if (onFocus) onFocus()
  }

  const renderInput = () => {
    const { multiline, isInvalid, autoFocus, onEnter, ...otherProps } = props

    const tagProps = {
      spellCheck: false,
      ref: inputRef,
      onChange: handleInput,
      onFocus: handleFocus,
      onBlur: handleBlur,
      onKeyDown: handleKeyDown,
    }

    const Tag = multiline ? "textarea" : "input"
    return React.createElement(Tag, { ...otherProps, ...tagProps })
  }

  return (
    <Container focused={isFocused} invalid={isInvalid}>
      <div className="input">{renderInput()}</div>
    </Container>
  )
})
