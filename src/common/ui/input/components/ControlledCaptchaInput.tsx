import { observer } from "mobx-react"
import React from "react"
import { loadRecaptcha } from "../../../dom/helpers/recaptcha"
import { bind } from "../../../lang/function/helpers/bind"
import { CaptchaFieldController } from "../../form/controllers/CaptchaFieldController"

export interface ControlledCaptchaInputProps {
  controller: CaptchaFieldController
}

@observer
export class ControlledCaptchaInput extends React.Component<
  ControlledCaptchaInputProps
> {
  private captchaRef = React.createRef<HTMLDivElement>()

  public async componentDidMount() {
    if (!this.captchaRef.current) return

    const recaptcha = await loadRecaptcha()

    recaptcha.render(this.captchaRef.current, {
      sitekey: "6LdICysUAAAAAEQn2ekzTotfH82OU5RmiGFiQlHl",
      callback: this.handleVerify,
      "expired-callback": this.handleExpired,
      theme: "white",
    })
  }

  public componentWillUnmount() {
    if (!window.grecaptcha) return

    // the reset() function can throw, so this try/catch is required
    // fuck google
    try {
      window.grecaptcha.reset()
    } catch (error) {
      console.warn("Recaptcha reset error:", error)
    }
  }

  @bind
  public handleVerify(data: any) {
    this.props.controller.setValue(data)
  }

  @bind
  public handleExpired() {
    this.props.controller.reset()
  }

  public render() {
    return (
      <div className="CaptchaInput">
        <div id="g-captcha" ref={this.captchaRef} />
      </div>
    )
  }
}

type CaptchaInputRenderProps = Omit<ControlledCaptchaInputProps, "controller">

/**
 * Helper for creating a render prop to render a {@link ControlledCaptchaInput},
 * to be used with {@link FormField}
 *
 * @example
 * <FormField
 *   label="..."
 *   controller={someController}
 *   renderInput={renderControlledCaptchaInput({ ... })}
 * />
 */
export const renderControlledCaptchaInput = (
  props: CaptchaInputRenderProps = {},
) => {
  return (controller: CaptchaFieldController) => {
    return <ControlledCaptchaInput {...props} controller={controller} />
  }
}
