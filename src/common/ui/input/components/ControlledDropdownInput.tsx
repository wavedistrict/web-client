import { observer } from "mobx-react"
import React from "react"
import { SelectFieldController } from "../../form/controllers/SelectFieldController"
import { DropdownInput } from "./DropdownInput"

export interface ControlledDropdownInputProps {
  controller: SelectFieldController
}

@observer
export class ControlledDropdownInput extends React.Component<
  ControlledDropdownInputProps
> {
  public render() {
    const { controller } = this.props

    const handleChange = (value: any) => {
      controller.setValue(value)
    }

    const props = {
      value: controller.value,
      options: controller.options,
      onChange: handleChange,
    }

    return <DropdownInput {...props} />
  }
}

/**
 * Helper for creating a render prop to render a {@link ControlledDropdownInput},
 * to be used with {@link FormField}
 *
 * @example
 * <FormField
 *   label="..."
 *   controller={someController}
 *   renderInput={renderControlledDropdownInput({ ... })}
 * />
 */
export const renderControlledDropdownInput = (
  props: Omit<ControlledDropdownInputProps, "controller"> = {},
) => {
  return (controller: SelectFieldController) => {
    return <ControlledDropdownInput {...props} controller={controller} />
  }
}
