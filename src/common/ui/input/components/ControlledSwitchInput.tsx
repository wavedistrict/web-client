import { observer } from "mobx-react"
import React from "react"
import { BooleanFieldController } from "../../form/controllers/BooleanFieldController"
import { SwitchInput, SwitchInputProps } from "./SwitchInput"

export interface ControlledSwitchInputProps extends SwitchInputProps {
  controller: BooleanFieldController
}

@observer
export class ControlledSwitchInput extends React.Component<
  ControlledSwitchInputProps
> {
  public render() {
    const { controller, ...otherProps } = this.props

    const handleChange = (value: boolean) => {
      controller.setValue(value)
    }

    return (
      <SwitchInput
        {...otherProps}
        active={controller.value}
        onChange={handleChange}
      />
    )
  }
}

export const renderControlledSwitchInput = (
  props: Omit<ControlledSwitchInputProps, "controller"> = {},
) => (controller: BooleanFieldController) => (
  <ControlledSwitchInput {...props} controller={controller} />
)
