import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { PopoverTrigger } from "../../../../modules/popover/components/PopoverTrigger"
import {
  getColor,
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
} from "../../../../modules/theme/helpers"
import { styled } from "../../../../modules/theme/themes"
import { IconType } from "../../icon"
import { Icon } from "../../icon/components/Icon"

type ValueType = string | number | null

export interface DropdownOption<T = ValueType> {
  key: T
  label: string
  icon?: IconType
}

interface DropdownInputProps {
  value?: ValueType
  placeholder?: string
  options: DropdownOption[]
  onChange?: (value: ValueType) => void
}

const Container = styled.div<{
  open: boolean
}>`
  width: 100%;
  position: relative;

  > .handle {
    padding: 10px;
    height: 38px;

    background: ${getTransparency("weakNegative")};
    transition: ${getDuration("normal")} ease all;
    border-radius: 3px;

    font-weight: ${getFontWeight("bold")};
    font-size: ${getFontSize("2")}px;

    display: flex;
    align-items: center;

    svg {
      transition: ${getDuration("normal")} ease all;
    }
  }

  > .handle > .icon {
    ${size(24)};

    margin-top: 3px;
    margin-right: ${getSpacing("3")}px;
  }

  > .handle:hover {
    cursor: pointer;
    background: ${getTransparency("strongPositive")};
  }

  ${(props) =>
    props.open &&
    `
    > .handle {
      color: ${props.theme.colors.accent};

      svg {
        fill: ${props.theme.colors.accent};
      }
    }
  `}
`

const Option = styled.div<{
  active: boolean
}>`
  padding: 0px ${getSpacing("4")}px;
  height: 38px;

  font-weight: ${getFontWeight("bold")};
  font-size: ${getFontSize("2")}px;

  transition: ${getDuration("normal")} ease all;

  border-bottom: solid 1px ${getColor("divider")};
  display: flex;
  align-items: center;

  &:hover {
    cursor: pointer;
    background: ${getTransparency("weakNegative")};
  }

  ${(props) =>
    props.active &&
    `
    color: ${props.theme.colors.accent};
  `}
`

export const DropdownInput = observer(function DropdownInput(
  props: DropdownInputProps,
) {
  const { options, value, placeholder, onChange } = props

  const selected = options.find((o) => o.key === value)

  const renderOptions = (close: () => void) => {
    return options.map((option) => {
      const key = String(option.key)
      const isActive = option === selected

      const onClick = () => {
        if (onChange) onChange(option.key)
        close()
      }

      return (
        <Option active={isActive} key={key} onClick={onClick}>
          <div className="label">{option.label}</div>
        </Option>
      )
    })
  }

  return (
    <PopoverTrigger useAnchorWidth placement="bottom-start">
      {(props) => (
        <Container open={props.isActive} ref={props.ref}>
          <div className="handle" onClick={props.toggle}>
            <div className="icon">
              <Icon name="chevronDown" />
            </div>
            <div className="label">
              {selected ? selected.label : placeholder || "Select an option"}
            </div>
          </div>
        </Container>
      )}
      {(props) => <div className="options">{renderOptions(props.close)}</div>}
    </PopoverTrigger>
  )
})
