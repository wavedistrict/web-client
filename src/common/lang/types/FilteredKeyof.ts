import { Values } from "./Values"

export type FilteredKeyof<O, T> = Values<
  { [K in keyof O]: O[K] extends T ? K : never }
>
