export type Values<O> = O[keyof O]
