export const getSmoothedAverage = (
  oldValue: number,
  newValue: number,
  constant: number,
) => {
  return oldValue * constant + newValue * (1 - constant)
}
