export interface AnyObject {
  [key: string]: any
}

/**
 * Like record, but with optional values.
 * Slightly nicer than `Record<K, V | undefined>`
 *
 * Also defaults key to `string` and puts it last because that's the common case
 */
export type MaybeRecord<V, K extends keyof any = string> = { [_ in K]?: V }
