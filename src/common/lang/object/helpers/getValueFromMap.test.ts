import { getValueFromMap } from "./getValueFromMap"

describe("getValueFromMap", () => {
  it("should return the correct value", () => {
    const inputObject = { foo: "bar" }
    const inputKey = "foo"
    const defaultValue = undefined
    const output = inputObject[inputKey]

    expect(getValueFromMap(inputObject, inputKey, defaultValue)).toBe(output)
  })

  it("should return the default value", () => {
    const inputObject = { foo: "bar" }
    const inputKey = "test"
    const defaultValue = undefined
    const output = defaultValue

    expect(getValueFromMap(inputObject, inputKey, defaultValue)).toBe(output)
  })
})
