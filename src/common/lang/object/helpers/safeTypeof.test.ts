import { safeTypeof } from "./safeTypeof"

test("safeTypeof", () => {
  expect(safeTypeof([])).toBe("array")
  expect(safeTypeof({})).toBe("object")
  expect(safeTypeof("")).toBe("string")
  expect(safeTypeof(null)).toBe("null")
})
