import { safeTypeof } from "./safeTypeof"

export const safeMerge = <T extends object>(
  obj: T,
  partial: Record<string, any>,
): T => {
  const result = { ...obj } as Record<string, any>

  for (const [key, value] of Object.entries(obj)) {
    const newValue = partial[key]

    const type = safeTypeof(newValue)
    const isSameType = safeTypeof(value) === type

    if (isSameType && newValue) {
      if (type === "array" && newValue.length === value.length) {
        result[key] = newValue
      }

      if (type === "object") {
        result[key] = safeMerge(value, newValue)
      }

      if (type !== "object" && type !== "array") {
        result[key] = newValue
      }
    }
  }

  return result as T
}
