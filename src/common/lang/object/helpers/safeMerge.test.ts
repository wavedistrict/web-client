import { safeMerge } from "./safeMerge"

describe("safeMerge", () => {
  it("merges objects safely", () => {
    const oldObj = {
      foo: 3,
      bar: "A string",
    }

    const newObj = {
      foo: "3",
      bar: "Another string",
    }

    const result = safeMerge(oldObj, newObj)

    expect(result).toMatchObject({ foo: 3, bar: "Another string" })
  })

  it("supports nesting", () => {
    const oldObj = {
      colors: {
        one: "red",
        two: "blue",
      },
    }

    const newObj = {
      foo: 4,
      colors: {
        one: null,
        two: "green",
      },
    }

    const result = safeMerge(oldObj, newObj)

    expect(result).toMatchObject({
      colors: {
        one: "red",
        two: "green",
      },
    })
  })
})
