export const safeTypeof = (value: any) => {
  const type = typeof value

  if (value === null) return "null"

  if (type === "object") {
    return Array.isArray(value) ? "array" : "object"
  }

  return type
}
