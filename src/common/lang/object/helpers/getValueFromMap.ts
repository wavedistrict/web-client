/** Helper function for safely getting a key from a map-style object */
export function getValueFromMap<O extends object, D>(
  obj: O,
  key: string | number,
  defaultValue: D,
): O[keyof O] | D {
  return key in obj ? obj[key as keyof O] : defaultValue
}
