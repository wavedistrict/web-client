/**
 * Represents any kind of foriegn error we might encounter in the app
 */
export interface UnknownError {
  data?: {
    message?: string
  }
  message?: string
}
