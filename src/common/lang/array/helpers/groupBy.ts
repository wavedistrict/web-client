export const groupBy = <T>(items: T[], separate: (item: T) => any) => {
  const groups: T[][] = []

  let currentGroup: T[] = []
  let previousSeparator: any

  for (const item of items) {
    const separator = separate(item)

    if (separator !== previousSeparator) {
      groups.push(currentGroup)
      currentGroup = []
    }

    currentGroup.push(item)

    previousSeparator = separator
  }

  groups.shift()
  groups.push(currentGroup)

  return groups
}
