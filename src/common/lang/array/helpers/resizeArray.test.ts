import { resizeArray } from "./resizeArray"

test("resizeArray", () => {
  const input = [1, 2, 3, 4, 5, 6]
  const output = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6]

  expect(resizeArray(input, input.length * 2)).toEqual(output)
})
