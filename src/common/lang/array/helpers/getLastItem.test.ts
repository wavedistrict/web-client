import { getLastItem } from "./getLastItem"

describe("getLastItem", () => {
  it("should return the last item", () => {
    expect(getLastItem([1, 2, 3])).toBe(3)
  })

  it("should return undefined, since there is no last item", () => {
    expect(getLastItem([])).toBe(undefined)
  })
})
