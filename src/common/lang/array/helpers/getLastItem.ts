/**
 * Gets the last item from an array.
 * Looks cleaner than items[items.length - 1] and is less error-prone
 */
export const getLastItem = <T>(items: T[]): T | undefined =>
  items[items.length - 1]
