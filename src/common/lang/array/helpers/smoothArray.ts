type EasingFunction = (
  time: number,
  beginning: number,
  change: number,
  duration: number,
) => number

const easeInOutQuad: EasingFunction = (time, beginning, change, duration) => {
  time /= duration / 2

  if (time < 1) {
    return (change / 2) * time * time + beginning
  } else {
    return (-change / 2) * ((time -= 1) * (time - 2) - 1) + beginning
  }
}

const applyEasing = (func: EasingFunction, part: number, value: number) => {
  return func(1000 * part, 0, value, 1000) || 0
}

export const smoothArray = (array: number[], parts: number) => {
  array = [...array]

  for (const index of Array(parts).keys()) {
    const value = array[index]
    array[index] = applyEasing(easeInOutQuad, index / parts, value)

    /** Do it on the other side of the array, too */
    const reversedIndex = array.length - 1 - index
    const reversedValue = array[reversedIndex]

    array[reversedIndex] = applyEasing(
      easeInOutQuad,
      index / parts,
      reversedValue,
    )
  }

  return array
}
