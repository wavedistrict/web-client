export const range = (from: number, to?: number) => {
  if (to === undefined) {
    to = from
    from = 0
  }

  const result: number[] = []

  for (let i = from; i < to; i++) {
    result.push(i)
  }

  return result
}
