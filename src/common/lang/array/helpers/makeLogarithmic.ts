export const makeLogarithmic = (array: ArrayLike<number>): number[] => {
  const result = []

  for (let i = 0; i < array.length; i++) {
    const index = Math.pow(array.length, (i - 1) / (array.length - 1))

    const low = Math.floor(index)
    const high = Math.ceil(index)

    const lv = array[low]
    const hv = array[high]

    const w = (index - low) / (high - low)
    const v = low === high ? lv : lv + (hv - lv) * w

    result.push(v)
  }

  return result
}
