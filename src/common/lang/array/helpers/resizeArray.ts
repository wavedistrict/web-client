export const resizeArray = (
  array: ArrayLike<number>,
  size: number,
): number[] => {
  const length = array.length
  const factor = length / size

  return Array(size)
    .fill(0)
    .map((_, i) => array[Math.floor(i * factor)])
}
