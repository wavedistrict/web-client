/** Get item safely from mobx array */
export const getItem = <T>(arr: T[], index: number, defaultValue: T) => {
  return arr.length - 1 >= index ? arr[index] : defaultValue
}
