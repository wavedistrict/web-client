/**
 * Throttles a function, making sure it's only called once within a recurring timeframe of the
 * specified duration
 */
export const createThrottledFunction = <T>(
  func: (...args: any[]) => void,
  time: number,
) => {
  let lastFunc: number
  let lastRan: number
  return function(this: T) {
    const context = this
    const args = arguments
    if (!lastRan) {
      func.call(context, ...args)
      lastRan = Date.now()
    } else {
      window.clearTimeout(lastFunc)
      lastFunc = window.setTimeout(function() {
        if (Date.now() - lastRan >= time) {
          func.call(context, ...args)
          lastRan = Date.now()
        }
      }, time - (Date.now() - lastRan))
    }
  }
}

/**
 * Decorator for throttling a class method.
 * @see createThrottledFunction
 */
export const throttle = (time: number) => (
  target: Object,
  key: string,
  descriptor: PropertyDescriptor,
) => {
  descriptor.value = createThrottledFunction(descriptor.value, time)
}
