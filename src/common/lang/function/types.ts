/**
 * Represents a function which can have any signature. Use this over `Function`,
 * or, if possible, use a better signature which properly reflects the usage of the function
 */
export type AnyFunction = (...args: any[]) => any
