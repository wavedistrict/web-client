/** Creates a positive hash from a string */
export const hashString = (str: string) => {
  let hash = 0

  if (str.length === 0) return 0

  for (let i = 0; i < str.length; i++) {
    const id = str.charCodeAt(i)

    hash = (hash << 5) - hash + id
    hash |= 0
  }

  return Math.abs(hash)
}
