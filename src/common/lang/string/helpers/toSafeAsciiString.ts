/** Returns a safe ASCII string */
export const toSafeAsciiString = (string: string) =>
  string.replace(/[^\x00-\x7F]/g, "").replace(/\s/g, "")
