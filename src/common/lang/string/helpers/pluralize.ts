export const pluralize = (unit: string, count: number) => {
  const canBePluralized = !unit.toLowerCase().endsWith("s")

  if (count === 0) {
    return `${unit}s`
  } else if (canBePluralized && count !== 1) {
    return `${count} ${unit}s`
  } else {
    return `${count} ${unit}`
  }
}
