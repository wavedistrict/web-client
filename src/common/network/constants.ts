import { IS_PROD } from "../../modules/core/constants"

export const API_URL = IS_PROD
  ? "https://api.wavedistrict.com"
  : "http://localhost:3070"

export const MEDIA_URL = IS_PROD
  ? "https://media.wavedistrict.com"
  : "http://localhost:6020"

export const SOCKET_URL = IS_PROD
  ? "wss://socket.wavedistrict.com"
  : "ws://localhost:8030"

export const TUS_RESUMABLE = "1.0.0"
export const TUS_CHUNK_SIZE = 10 * 1024 * 1024 // 10mib
