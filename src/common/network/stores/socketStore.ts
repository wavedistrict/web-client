import { SocketMessage } from "../types"
import { getSetting } from "../../../modules/settings/helpers/getSetting"
import { Emitter } from "../../state/classes/Emitter"
import { SOCKET_URL } from "../constants"
import { observable } from "mobx"
import {
  InitializableStore,
  createStoreFactory,
} from "../../state/classes/InitializableStore"
import { IS_SERVER } from "../../../modules/core/constants"

export interface SocketConnectionEvents {
  message: SocketMessage
  connected: void
  disconnected: void
}

export enum SocketStatus {
  Idle,
  Connecting,
  Connected,
  Reconnecting,
  Disconnected,
}

const CAN_USE_SOCKET = typeof globalThis.WebSocket !== undefined

export class SocketStore extends InitializableStore {
  private emitter = new Emitter<SocketConnectionEvents>()
  private socket?: WebSocket

  @observable public status = SocketStatus.Idle

  public init() {
    if (IS_SERVER) return

    const offlineMode = getSetting(this.manager, "general", "offlineMode")

    offlineMode.setHandler((offline) => {
      if (offline) {
        this.disconnect()
      } else {
        this.connect()
      }
    })
  }

  private connect = () => {
    if (!CAN_USE_SOCKET) return
    this.status = SocketStatus.Connecting

    const socket = (this.socket = new WebSocket(SOCKET_URL))

    socket.onopen = this.handleConnected
    socket.onmessage = this.handleMessage
    socket.onerror = this.handleClosed
    socket.onclose = this.handleClosed
  }

  private disconnect = () => {
    const { socket } = this
    if (!socket) return

    socket.close()
    socket.onopen = null
    socket.onmessage = null
    socket.onerror = null
    socket.onclose = null

    this.socket = undefined
  }

  private reconnect = () => {
    this.status = SocketStatus.Reconnecting

    this.disconnect()
    this.connect()
  }

  private handleConnected = () => {
    this.status = SocketStatus.Connected
    this.emit("connected", undefined)
  }

  private handleClosed = () => {
    this.reconnect()
    this.emit("disconnected", undefined)
  }

  private handleMessage = (message: MessageEvent) => {
    const data = JSON.parse(message.data)
    this.emit("message", data)
  }

  public get on() {
    return this.emitter.on
  }

  public get off() {
    return this.emitter.off
  }

  private get emit() {
    return this.emitter.emit
  }
}

export const socketStore = createStoreFactory(SocketStore)
