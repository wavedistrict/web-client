// HTTP stuff
export type RequestMethod =
  | "GET"
  | "POST"
  | "PUT"
  | "DELETE"
  | "PATCH"
  | "HEAD"
  | "OPTIONS"

export enum RequestStatus {
  Idle,
  Ongoing,
  Complete,
  Aborted,
  Error,
}

export interface RequestOptions {
  body?: any
  headers?: Headers
  params?: QueryParams
  onProgress?: (percentage: number, loaded: number, total: number) => void
}

export interface Headers {
  [header: string]: string
}

export interface QueryParams {
  [param: string]: string | number | boolean
}

// socket stuff
// IDEA: give this a generic `T`, then define it as { type: string } & T
// extra type safety might not be worth
export interface SocketMessage {
  type: string
  [otherProperty: string]: any
}

export type SocketMessageHandler = (message: SocketMessage) => void
