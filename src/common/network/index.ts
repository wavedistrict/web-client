import { RequestHandler } from "./classes/RequestHandler"
import { API_URL, MEDIA_URL, TUS_RESUMABLE } from "./constants"

export const apiServer = new RequestHandler({
  basePath: API_URL,
  headers: {},
})

export const mediaServer = new RequestHandler({
  basePath: MEDIA_URL,
  headers: {
    "Tus-Resumable": TUS_RESUMABLE,
    "Cache-Control": "no-store",
  },
})
