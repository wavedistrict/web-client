import { mediaServer } from ".."
import { bind } from "../../lang/function/helpers/bind"
import { toSafeAsciiString } from "../../lang/string/helpers/toSafeAsciiString"
import { Emitter } from "../../state/classes/Emitter"
import { LimitedStorage } from "../../storage/class/LimitedStorage"
import { TUS_CHUNK_SIZE } from "../constants"
import { Response } from "./Response"
import axios, { AxiosRequestConfig } from "axios"

const RETRY_COUNT = 5

const storage = new LimitedStorage<string>({
  name: "resumable-uploads",
  limit: 10,
})

export enum FileUploadStatus {
  Checking = "Checking",
  Uploading = "Uploading",
  Complete = "Complete",
}

export enum FileUploadError {
  Invalid = "Invalid",
  Unknown = "Unknown",
  Failed = "Failed",
}

export interface FileUploadEvents {
  error: FileUploadError
  status: FileUploadStatus
  complete: FileUploadResult
  progress: number
}

export interface FileUploadResult {
  id: number
  key: string
}

export class FileUpload extends Emitter<FileUploadEvents> {
  private offset = 0
  private retries = 0

  private destination: string
  private location?: string
  private canceler?: () => void

  constructor(private file: File, private kind: string, private type: string) {
    super()
    this.destination = `/upload/${kind}`
  }

  private async prepare() {
    const { type, destination } = this
    const { size, name } = this.file

    const headers = {
      "Upload-Length": String(size),
      "Upload-Metadata": `filename ${btoa(toSafeAsciiString(name))}`,
    }

    const request = mediaServer.post<any>(destination, null, {
      ...this.cancelOptions,
      params: { type },
      headers,
    })

    try {
      const response = await request
      const location = response.headers.location!

      this.location = location
      storage.set(this.fingerprint, location)

      this.upload()
    } catch {
      this.emit("error", FileUploadError.Invalid)
    }
  }

  private async restore(location: string) {
    const request = mediaServer.head(location)
    this.emit("status", FileUploadStatus.Checking)

    try {
      const response = await request
      const offset = response.headers["upload-offset"]

      this.offset = Number(offset)
      this.upload()
    } catch (error) {
      const response: Response = error
      const { statusCode } = response

      if (statusCode === 404) return this.prepare()
      if (statusCode === 410) return this.retry()

      this.emit("error", FileUploadError.Unknown)
    }
  }

  @bind
  private handleProgress(event: ProgressEvent) {
    const { loaded } = event

    const bytesLoaded = this.offset + loaded
    const bytesTotal = this.file.size

    const percentage = (bytesLoaded / bytesTotal) * 100
    this.emit("progress", percentage)
  }

  private async upload(): Promise<void> {
    this.emit("status", FileUploadStatus.Uploading)
    const { offset, file, location } = this

    const start = offset
    const end = offset + TUS_CHUNK_SIZE
    const chunk = file.slice(start, end)

    const config: AxiosRequestConfig = {
      ...this.cancelOptions,
      onUploadProgress: this.handleProgress,
      headers: {
        "content-type": "application/offset+octet-stream",
        "upload-offset": String(this.offset),
      },
    }

    const request = mediaServer.patch<FileUploadResult>(
      location!,
      chunk,
      config,
    )

    try {
      const response = await request

      const offset = response.headers["upload-offset"]
      this.offset = Number(offset)

      if (this.offset === file.size) {
        this.emit("complete", response.data)
        this.emit("status", FileUploadStatus.Complete)
        return
      }

      return this.upload()
    } catch (error) {
      this.handleChunkError(error)
    }
  }

  private handleChunkError(response: Response) {
    const { statusCode } = response

    if (statusCode === 400) {
      this.emit("error", FileUploadError.Invalid)
      return
    }

    return this.retry()
  }

  public start() {
    const location = storage.get(this.fingerprint)

    if (location) {
      this.restore(location)
    } else {
      this.prepare()
    }
  }

  public retry() {
    this.retries += 1

    if (this.retries > RETRY_COUNT) {
      this.emit("error", FileUploadError.Failed)
      return
    }

    storage.remove(this.fingerprint)

    this.offset = 0
    this.location = undefined

    this.start()
  }

  public cancel() {
    if (this.canceler) {
      this.canceler()
      storage.remove(this.fingerprint)

      this.canceler = undefined
    }
  }

  public get fingerprint(): string {
    const { kind, type } = this
    const { name, size, lastModified } = this.file

    const params = JSON.stringify({ kind, type })
    const rawFingerprint = `tus-${name}-${size}-${type}-${lastModified}-${params}`
    const escapedFingerprint = toSafeAsciiString(rawFingerprint)

    return btoa(escapedFingerprint)
  }

  private get cancelOptions() {
    return {
      cancelToken: new axios.CancelToken((token) => {
        this.canceler = token
      }),
    }
  }
}
