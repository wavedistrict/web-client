import { bind } from "../../lang/function/helpers/bind"
import { Headers, RequestMethod, RequestOptions } from "../types"
import axios, { AxiosRequestConfig } from "axios"
// import { Request } from "./Request"

export interface RequestHandlerConfig extends RequestOptions {
  basePath?: string
  headers: Headers
}

type Body = RequestOptions["body"]

/** Handles requests for a specific server */
export class RequestHandler {
  private config: RequestHandlerConfig

  constructor(config: RequestHandlerConfig) {
    this.config = {
      headers: {},
      ...config,
    }
  }

  @bind
  private request<T>(
    method: RequestMethod,
    path: string,
    options: AxiosRequestConfig,
  ) {
    const headers = {
      ...this.config.headers,
      ...options.headers,
    }

    const { basePath = "" } = this.config
    const finalPath = `${basePath}${path}`

    const finalOptions = {
      ...this.config,
      ...options,
      method,
      url: finalPath,
      withCredentials: true,
      headers,
    }

    //const request = new Request<T>(method, finalPath, finalOptions)
    //return request.send()

    return axios(finalOptions)
  }

  public get<T>(path: string, options: AxiosRequestConfig = {}) {
    return this.request<T>("GET", path, options)
  }

  public head<T>(path: string, options: AxiosRequestConfig = {}) {
    return this.request<T>("HEAD", path, options)
  }

  public options<T>(path: string, options: AxiosRequestConfig = {}) {
    return this.request<T>("OPTIONS", path, options)
  }

  public delete<T>(path: string, options: AxiosRequestConfig = {}) {
    return this.request<T>("DELETE", path, options)
  }

  public post<T>(path: string, body: Body, options: AxiosRequestConfig = {}) {
    return this.request<T>("POST", path, {
      ...options,
      data: body,
    })
  }

  public put<T>(path: string, body: Body, options: AxiosRequestConfig = {}) {
    return this.request<T>("PUT", path, {
      ...options,
      data: body,
    })
  }

  public patch<T>(path: string, body: Body, options: AxiosRequestConfig = {}) {
    return this.request<T>("PATCH", path, {
      ...options,
      data: body,
    })
  }

  public setHeader(key: string, value: string | null) {
    if (value) {
      this.config.headers[key] = value
    } else {
      delete this.config.headers[key]
    }
  }
}
