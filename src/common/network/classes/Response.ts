import { AnyObject } from "../../lang/object/types"
import { Headers, RequestMethod } from "../types"

export interface ResponseOptions {
  body: string
  headerString: string
  statusCode: number
  statusText: string
  method: RequestMethod
}

/** Represents a response */
export class Response<D = AnyObject> {
  private headerString: ResponseOptions["headerString"]
  public headers: Headers

  public statusCode: ResponseOptions["statusCode"]
  public statusText: ResponseOptions["statusText"]
  public method: ResponseOptions["method"]

  private body: ResponseOptions["body"]
  public data: D

  constructor(options: ResponseOptions) {
    const { body, headerString, statusCode, statusText, method } = options

    this.body = body
    this.headerString = headerString
    this.statusCode = statusCode
    this.statusText = statusText
    this.method = method

    this.headers = this.parseHeaders()
    this.data = this.parseBody()
  }

  private parseBody() {
    const contentType = this.getHeader("content-type")

    if (!contentType) {
      return null
    }

    if (["HEAD", "OPTIONS"].includes(this.method)) {
      return null
    }

    if (contentType.startsWith("application/json")) {
      return JSON.parse(this.body)
    }

    return this.body
  }

  private parseHeaders() {
    const result: Headers = {}

    const splitHeaders = this.headerString.split("\r\n")

    for (const headerString of splitHeaders) {
      const [key, value] = headerString.split(": ")
      if (key && value) result[key.toLowerCase()] = value
    }

    return result
  }

  public getHeader(name: string) {
    return this.headers[name] || null
  }
}
