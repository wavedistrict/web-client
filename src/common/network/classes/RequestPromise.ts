import { Response } from "./Response"

type Executor<T> = (resolve: Resolve<T>, reject: Reject) => void
type Resolve<T> = (value?: T | PromiseLike<T>) => void
type Reject = (reason?: {}) => void

type AbortFunction = () => void

/** Represents a promise hybrid returned by Request's send method */
export class RequestPromise<T = unknown> extends Promise<Response<T>> {
  public abort: AbortFunction

  constructor(executor: Executor<Response<T>>, abort: AbortFunction) {
    super(executor)
    this.abort = abort
  }
}
