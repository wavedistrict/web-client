import { bind } from "../../lang/function/helpers/bind"
import { AnyObject } from "../../lang/object/types"
import {
  QueryParams,
  RequestMethod,
  RequestOptions,
  RequestStatus,
} from "../types"
import { RequestPromise } from "./RequestPromise"
import { Response } from "./Response"

/** Represents a HTTP request */
export class Request<T = AnyObject> {
  public path: string

  private method: RequestMethod
  private options: RequestOptions

  private reject: (reason: any) => any = (reason: any) => {}
  private xhr = new XMLHttpRequest()

  public progress = 0
  public status = RequestStatus.Idle

  constructor(
    method: RequestMethod,
    path: string,
    options: RequestOptions = {},
  ) {
    this.xhr.withCredentials = true
    this.path = path
    this.method = method
    this.options = options
  }

  /** Serializes an object into query params */
  private serializeQueryParams(params?: QueryParams) {
    if (!params) return ""

    const keyValuePairs = Object.keys(params).map((key) => {
      const value = params[key]
      return `${key}=${value}`
    })

    return `?${keyValuePairs.join("&")}`
  }

  private setHeaders() {
    const { options, xhr } = this
    const { headers } = options

    if (!headers) return
    const headerKeys = Object.keys(headers)

    for (const key of headerKeys) {
      const value = headers[key]
      if (value != null) xhr.setRequestHeader(key, value)
    }
  }

  private getContentTypeFromBody() {
    const { body } = this.options

    if (body === null) return "text/plain"
    if (typeof body === "object" && body instanceof Blob)
      return "application/octet-stream"
    if (typeof body === "object" && body.constructor === Object)
      return "application/json"

    return "application/body"
  }

  private serializeBody(contentType: string) {
    const { body } = this.options

    if (contentType === "application/json") return JSON.stringify(body)

    return body
  }

  @bind
  public send(): RequestPromise<T> {
    if (this.status === RequestStatus.Ongoing)
      throw new Error("Cannot send an ongoing request")

    const { xhr, options, method, path } = this
    const fullPath = `${path}${this.serializeQueryParams(options.params)}`

    const contentType = this.getContentTypeFromBody()
    const body = this.serializeBody(contentType)

    xhr.open(method, fullPath)
    xhr.setRequestHeader("content-type", contentType)

    this.setHeaders()
    this.status = RequestStatus.Ongoing

    return new RequestPromise<T>((resolve, reject) => {
      this.reject = reject

      /** A response has been recieved */
      xhr.onload = () => {
        const response = new Response<T>({
          body: xhr.response,
          headerString: xhr.getAllResponseHeaders(),
          statusCode: xhr.status,
          statusText: xhr.statusText,
          method,
        })

        const isError = xhr.status >= 400
        if (isError) return reject(response)

        resolve(response)
      }

      /** Progress has updated */
      xhr.upload.onprogress = (event) => {
        if (!event.lengthComputable) return

        const percentage = (event.loaded / event.total) * 100
        this.progress = percentage

        if (options.onProgress)
          options.onProgress(percentage, event.loaded, event.total)
      }

      /** Handle error */
      xhr.onerror = () => {
        reject(new Error("An unknown error occurred while fetching"))
      }

      xhr.send(body)
    }, this.abort)
  }

  @bind
  public abort() {
    this.status = RequestStatus.Aborted
    this.reject(new Error("Request aborted"))

    this.xhr.abort()
  }
}
