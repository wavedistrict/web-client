import { SocketMessage, SocketMessageHandler } from "../types"
import { Manager } from "../../state/types/Manager"

export interface SocketListenerOptions {
  [key: string]: SocketMessageHandler
}

export const createSocketListener = (
  manager: Manager,
  handlers: SocketListenerOptions,
) => {
  const { socketStore } = manager.stores

  const handleMessage = (message: SocketMessage) => {
    const handler = handlers[message.type]

    if (handler) handler(message)
  }

  socketStore.on("message", handleMessage)
}
