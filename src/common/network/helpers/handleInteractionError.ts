import { UnknownError } from "../../lang/errors/types"
import { extractNetworkErrorMessage } from "./extractNetworkErrorMessage"
import { Manager } from "../../state/types/Manager"

/**
 * Use this function for handling errors for user interaction, such as favoriting a track, following
 * a user, adding a comment to a track, and so on
 */
export const handleInteractionError = (
  manager: Manager,
  error: UnknownError,
) => {
  const { toastStore } = manager.stores

  const errorMessage = extractNetworkErrorMessage(error)
  const toastMessage = `An error occurred: ${errorMessage}`

  toastStore.addToast(toastMessage)

  console.error(errorMessage)
}
