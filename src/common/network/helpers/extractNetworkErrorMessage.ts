import { UnknownError } from "../../lang/errors/types"

export function extractNetworkErrorMessage(error: UnknownError): string {
  return (error.data && error.data.message) || error.message || String(error)
}
