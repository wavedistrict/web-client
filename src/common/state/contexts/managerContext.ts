import { createContext } from "react"
import { Manager } from "../types/Manager"

export const managerContext = createContext<Manager | undefined>(undefined)
