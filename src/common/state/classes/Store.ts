import { createUniqueKey } from "../helpers/createUniqueKey"

export type ItemKey = number
export type ItemIndex = string

/**
 * Keeps a map of keyed values, which can be indexed, then retrieved by key or index
 */
export class Store<T> {
  public items = new Map<ItemKey, T>()
  public indexes = new Map<ItemIndex, ItemKey>()

  /** Adds an item to the store using a key or an index. When given an index, a unique key will be created. */
  public addItem(item: T, id?: ItemKey | ItemIndex): ItemKey {
    let itemKey = typeof id === "number" ? id : createUniqueKey()
    this.items.set(itemKey, item)

    if (typeof id === "string") this.addIndex(id, itemKey)

    return itemKey
  }

  /** Removes an item from the store by key or index, and returns true if the item was successfully removed */
  public removeItem(id: ItemIndex | ItemKey): boolean {
    if (typeof id !== "string") return this.items.delete(id)

    const indexedKey = this.getKeyFromIndex(id)
    if (indexedKey) return this.items.delete(indexedKey)

    return false
  }

  /** Gets an item from the store by key or index and returns it if it exists, otherwise returns undefined */
  public getItem(id: ItemIndex | ItemKey): T | undefined {
    if (typeof id !== "string") return this.items.get(id)

    const key = this.getKeyFromIndex(id)
    if (key) return this.items.get(key)

    return undefined
  }

  /** Rename an item's key */
  public renameItemKey(key: ItemKey, newKey: ItemKey) {
    const item = this.items.get(key)
    if (!item) return

    this.items.delete(key)
    this.items.set(newKey, item)
  }

  /** Store a string which can be used to get an item later */
  public addIndex(index: ItemIndex, key: ItemKey): void {
    this.indexes.set(index, key)
  }

  /** Find the item key of a given index */
  public getKeyFromIndex(index: ItemIndex): ItemKey | undefined {
    return this.indexes.get(index)
  }
}
