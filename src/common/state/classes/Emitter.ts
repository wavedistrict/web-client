import { bind } from "../../lang/function/helpers/bind"

export type HandlerAction<T> = (data: T) => void

export interface EventHandler<T = any> {
  once: boolean
  action: HandlerAction<T>
}

/** A typesafe EventEmitter */
export class Emitter<EventMap extends object> {
  private pipedEmitter?: Emitter<EventMap>
  private listeners = new Map<keyof EventMap, EventHandler[]>()

  @bind
  public emit<E extends keyof EventMap>(type: E, data: EventMap[E]) {
    if (this.pipedEmitter) this.pipedEmitter.emit(type, data)

    const handlers = this.listeners.get(type)
    if (!handlers) return

    const newHandlers = []

    for (const handler of handlers) {
      handler.action(data)
      if (!handler.once) newHandlers.push(handler)
    }

    this.listeners.set(type, newHandlers)
  }

  private addHandler<E extends keyof EventMap>(type: E, handler: EventHandler) {
    const handlers = this.listeners.get(type) || []
    this.listeners.set(type, [...handlers, handler])
  }

  @bind
  public on<E extends keyof EventMap>(
    type: E,
    action: HandlerAction<EventMap[E]>,
  ) {
    this.addHandler(type, { action, once: false })
  }

  @bind
  public once<E extends keyof EventMap>(
    type: E,
    action: HandlerAction<EventMap[E]>,
  ) {
    this.addHandler(type, { action, once: true })
  }

  @bind
  public off<E extends keyof EventMap>(
    type: E,
    action: HandlerAction<EventMap[E]>,
  ) {
    const handlers = (this.listeners.get(type) || []).filter(
      (handler) => handler.action !== action,
    )

    this.listeners.set(type, handlers)
  }

  public pipe(emitter: Emitter<EventMap>) {
    this.pipedEmitter = emitter
  }

  public unpipe() {
    this.pipedEmitter = undefined
  }

  public removeAllListeners() {
    this.listeners.clear()
  }
}
