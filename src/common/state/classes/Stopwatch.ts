import { Emitter } from "./Emitter"

export enum StopwatchState {
  RUNNING,
  STOPPED,
}

export class Stopwatch extends Emitter<{ tick: void }> {
  public state = StopwatchState.STOPPED

  private delta = new Date()
  private _time = 0
  private timer: any

  private collect() {
    this._time += Math.abs(this.delta.getTime() - new Date().getTime())
    this.resetDelta()
    this.emit("tick", undefined)
  }

  public stop() {
    this.pause()
    this.reset()
  }

  public reset() {
    this._time = 0
    this.resetDelta()
  }

  public start() {
    if (this.state === StopwatchState.RUNNING) return

    this.state = StopwatchState.RUNNING
    this.resetDelta()
    this.timer = setInterval(() => this.collect(), 20)
  }

  public pause() {
    clearInterval(this.timer)

    this.state = StopwatchState.STOPPED
    this.timer = undefined
  }

  private resetDelta() {
    this.delta = new Date()
  }

  public get milliseconds() {
    return this._time
  }

  public get seconds() {
    return this._time / 1000
  }
}
