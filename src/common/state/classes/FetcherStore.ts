import { action } from "mobx"
import { bind } from "../../lang/function/helpers/bind"
import { Emitter } from "./Emitter"
import { Model, ModelConstructor } from "./Model"
import { ModelFetcher, PrefetchedModelFetcher } from "./ModelFetcher"
import { Store } from "./Store"
import { Manager } from "../types/Manager"
import { InitializableStore } from "./InitializableStore"
import { StoreManager } from "./StoreManager"

interface GetIndexResult {
  index: string
  newId: number
}

export interface FetcherStoreEvents<TModel extends Model> {
  delete: ModelFetcher<TModel>
}

export type SerializedData = {
  items: any[]
}

export abstract class FetcherStore<
  TModel extends Model = Model
> extends InitializableStore {
  private emitter = new Emitter<FetcherStoreEvents<TModel>>()
  private store = new Store<ModelFetcher<TModel>>()
  private ModelClass: ModelConstructor<TModel>

  constructor(manager: Manager, ModelClass: ModelConstructor<TModel>) {
    super(manager)
    this.ModelClass = ModelClass
  }

  @action
  private getOrCreateFetcher(source: string, id: number | string) {
    let fetcher = this.store.getItem(id)
    let returnedId = 0

    if (!fetcher) {
      fetcher = new ModelFetcher(source, this.ModelClass, this.manager)
      returnedId = this.store.addItem(fetcher, id)
    }

    return { id: returnedId, fetcher }
  }

  @action
  protected fetch(
    source: string,
    id: number | string,
    getIndex?: (item: TModel) => GetIndexResult,
  ) {
    const { ssrStore } = this.manager.stores
    const { fetcher, id: oldId } = this.getOrCreateFetcher(source, id)

    const handleFetch = (item: TModel) => {
      if (getIndex) {
        const { newId, index } = getIndex(item)

        if (newId) this.store.renameItemKey(oldId, newId)
        if (index) this.store.addIndex(index, newId)
      }
    }

    if (!fetcher.isFetched) {
      const promise = fetcher.fetch().then(handleFetch)
      ssrStore.register(promise)
    }

    return fetcher
  }

  /** Create a fetcher and assign some data already existing onto it */
  @action
  protected prepopulate(
    source: string,
    id: number,
    data: TModel["data"],
    index?: string,
  ) {
    if (index) this.store.addIndex(index, id)

    const { fetcher } = this.getOrCreateFetcher(source, id)
    fetcher.createOrUpdateItem(data)

    // adding the data to the fetcher means that the model should always exist for this fetcher
    // so cast it accordingly
    return fetcher as PrefetchedModelFetcher<TModel>
  }

  /** Returns a model object based on data */
  @action
  public getModelFromData(data: TModel["data"]): TModel {
    const fetcher = this.addPrefetched(data)
    return fetcher.model
  }

  /** Returns a model if it exists */
  public getModel(id: number | string) {
    const fetcher = this.store.getItem(id)

    if (fetcher && fetcher.model) return fetcher.model
  }

  @bind
  public removeFetcher(id: number) {
    const fetcher = this.store.getItem(id)

    if (!fetcher) return

    this.store.removeItem(id)
    this.emit("delete", fetcher)
  }

  public get fetchers() {
    return [...this.store.items.entries()].map(([, fetcher]) => {
      return fetcher
    })
  }

  public get models() {
    return this.fetchers
      .filter((fetcher) => !!fetcher.model)
      .map((fetcher) => fetcher.model!)
  }

  private get emit() {
    return this.emitter.emit
  }

  public get on() {
    return this.emitter.on
  }

  public get off() {
    return this.emitter.off
  }

  public abstract addPrefetched(
    data: TModel["data"],
  ): PrefetchedModelFetcher<TModel>

  public serialize(): SerializedData {
    const items = [...this.store.items].map(([, item]) => item.data)

    return { items }
  }

  public hydrate(data: SerializedData) {
    for (const item of data.items) {
      this.addPrefetched(item)
    }
  }
}

export const createFetcherStoreFactory = <
  S extends FetcherStore<TModel>,
  TModel extends Model = Model
>(
  storeClass: new (
    manager: StoreManager<any>,
    model: ModelConstructor<TModel>,
  ) => S,
  model: ModelConstructor<TModel>,
) => (manager: StoreManager<any>): S => new storeClass(manager, model)
