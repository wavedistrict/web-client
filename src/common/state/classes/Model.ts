import { action, observable } from "mobx"
import { FetcherList, FetcherListOptions } from "./FetcherList"

// import { Manager } from "../types/Manager"

/** Model class used for representing an api resource, to be extended and add additional functionality to handle the data it holds */
export abstract class Model<DataInterface = {}> {
  private lists: Record<string, FetcherList> = {}

  public data: DataInterface
  public rawData: DataInterface

  constructor(data: DataInterface, protected manager: any) {
    this.data = observable(data)
    this.rawData = data
  }

  /** Gets prepared options for creating a FetcherList */
  protected getListMap(): Record<string, FetcherListOptions<any>> {
    return {}
  }

  /** Gets an existing FetcherList or creates a new one */
  protected getOrCreateList(key: string): FetcherList<any> {
    const existingList = this.lists[key]
    if (existingList) return existingList

    const options = this.getListMap()[key]

    if (!options) {
      throw new Error(`List options for key ${key} do not exist on this model.`)
    }

    const newList = (this.lists[key] = new FetcherList(options, this.manager))
    return newList
  }

  @action
  public update(data: DataInterface) {
    Object.assign(this.data, data)
    this.rawData = data

    this.onUpdate(data)
  }

  @action
  protected onUpdate(data: DataInterface) {}
}

export type ModelConstructor<M extends Model> = new (
  data: M["data"],
  manager: any,
) => M
