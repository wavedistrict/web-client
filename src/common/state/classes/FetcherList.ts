import { action, computed, observable } from "mobx"
import { removeDuplicates } from "../../lang/array/helpers/removeDuplicates"
import { bind } from "../../lang/function/helpers/bind"
import { AnyObject } from "../../lang/object/types"
import { apiServer } from "../../network"
import { QueryParams } from "../../network/types"
import { Emitter } from "./Emitter"
import { FetcherStore } from "./FetcherStore"
import { Model } from "./Model"
import { ModelFetcher, PrefetchedModelFetcher } from "./ModelFetcher"
import { Manager } from "../types/Manager"
import { AxiosPromise } from "axios"

interface ListResponseData {
  limit: number
  offset: number
  rows: AnyObject[]
}

export interface FetcherListOptions<M extends Model<D>, D = {}> {
  store: FetcherStore<M>
  path: string
  items?: D[]
  pending?: boolean
  limit?: number
  offset?: number
  params?: QueryParams
  maxItems?: number
}

export enum FetcherListStatus {
  Idle,
  Fetching,
  Error,
}

export type ListAddPosition = "first" | "last"

export interface FetcherListEvents<TModel> {
  modelsReceived: TModel[]
}

export class FetcherList<TModel extends Model = Model> extends Emitter<
  FetcherListEvents<TModel>
> {
  protected store: FetcherStore<TModel>

  private ongoingRequest?: AxiosPromise<ListResponseData>
  private timestamp!: string

  public path: string
  public params: QueryParams = {}

  @observable private fetchers: PrefetchedModelFetcher<TModel>[] = []
  @observable private pending: PrefetchedModelFetcher<TModel>[] = []

  @observable public status = FetcherListStatus.Idle
  @observable public hasNext = true
  @observable public hasFetched = false
  @observable public fingerprint: string

  public limit = 5
  public offset = 0
  public initialOffset = 0

  constructor(
    private options: FetcherListOptions<TModel>,
    private manager: Manager,
  ) {
    super()

    const { authStore } = manager.stores
    const { path, items = [], limit, offset, params, store } = options

    this.refreshTimestamp()

    this.fingerprint = authStore.fingerprint
    this.store = store

    this.limit = limit || this.limit
    this.offset = offset || this.offset
    this.initialOffset = offset || this.offset

    if (items.length > 0) {
      this.fetchers = items.map((item) => this.store.addPrefetched(item))
      this.offset = this.offset + items.length
      this.hasFetched = true
    }

    this.path = path
    this.params = params || {}

    this.store.on("delete", this.remove)
  }

  /** Safe push method which removes duplicates */
  @bind
  private push(...items: PrefetchedModelFetcher<TModel>[]) {
    this.fetchers = removeDuplicates([...this.fetchers, ...items])
  }

  /** Safe unshift method which removes duplicates */
  @bind
  private unshift(...items: PrefetchedModelFetcher<TModel>[]) {
    this.fetchers = removeDuplicates([...items, ...this.fetchers])
  }

  @bind
  public remove(fetcherToRemove: ModelFetcher<any>) {
    const filterFn = (fetcher: ModelFetcher<any>) => fetcherToRemove !== fetcher

    this.fetchers = this.fetchers.filter(filterFn)
    this.pending = this.pending.filter(filterFn)
  }

  public async fetch() {
    if (this.status === FetcherListStatus.Fetching) return
    this.status = FetcherListStatus.Fetching

    const { authStore } = this.manager.stores
    const { maxItems = Infinity } = this.options

    const params = {
      limit: this.limit,
      offset: this.offset,
      olderThan: this.timestamp,
      ...this.params,
    }

    this.ongoingRequest = apiServer.get<ListResponseData>(this.path, { params })

    try {
      const { data } = await this.ongoingRequest

      this.offset = this.offset + this.limit
      this.hasNext = data.rows.length === this.limit && this.offset < maxItems

      const fetchers = data.rows.map((row) => this.store.addPrefetched(row))
      const models = fetchers.map((fetcher) => fetcher.model)

      this.push(...fetchers)
      this.status = FetcherListStatus.Idle

      this.fingerprint = authStore.fingerprint
      this.hasFetched = true
      this.ongoingRequest = undefined
      this.emit("modelsReceived", models)

      return
    } catch (error) {
      this.status = FetcherListStatus.Error
      throw error
    }
  }

  @action
  public clear() {
    this.fetchers = []
    this.status = FetcherListStatus.Idle
    this.hasNext = true
    this.hasFetched = false
    this.offset = 0
    this.refreshTimestamp()
  }

  public refresh() {
    this.clear()
    this.fetch()
  }

  private refreshTimestamp() {
    this.timestamp = new Date().toISOString()
  }

  /** Add an item directly into the list */
  @bind
  public add(data: TModel["data"], position: ListAddPosition) {
    const fetcher = this.store.addPrefetched(data)

    if (position === "first") {
      this.unshift(fetcher)
    } else {
      this.refreshTimestamp()
      this.push(fetcher)
    }
  }

  /** Add an item into pending */
  @bind
  public addPending(data: TModel["data"]) {
    const fetcher = this.store.addPrefetched(data)
    this.pending.push(fetcher)
  }

  /** Consumes pending fetchers into the list */
  public consumePending(position: ListAddPosition) {
    const add = { first: this.unshift, last: this.push }[position]

    add(...this.pending)
    this.pending.length = 0
  }

  /** Returns a new FetcherList with the same properties as this one */
  public clone() {
    const list = new FetcherList(
      {
        path: this.path,
        store: this.store,
        offset: this.offset,
        limit: this.limit,
        params: this.params,
      },
      this.manager,
    )

    list.hasFetched = this.hasFetched
    list.hasNext = this.hasNext
    list.initialOffset = this.initialOffset
    list.timestamp = this.timestamp
    list.fetchers = [...this.fetchers]
    list.pending = this.pending

    return list
  }

  @action
  public setQueryParams(params: QueryParams) {
    this.params = params
    this.refresh()
  }

  @computed
  public get models() {
    return this.fetchers.map((f) => f.model)
  }

  @computed
  public get pendingCount() {
    return this.pending.length
  }

  @computed
  public get isFresh() {
    return this.status === FetcherListStatus.Idle && !this.hasFetched
  }

  @computed
  public get isEmpty() {
    return this.hasFetched && this.models.length === 0
  }

  @computed
  public get valid() {
    const { authStore } = this.manager.stores
    return this.fingerprint === authStore.fingerprint
  }
}
