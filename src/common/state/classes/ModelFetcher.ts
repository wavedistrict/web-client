import { action, computed, observable } from "mobx"
import { getValueFromMap } from "../../lang/object/helpers/getValueFromMap"
import { apiServer } from "../../network"
import { Model, ModelConstructor } from "./Model"
import { Manager } from "../types/Manager"
import { AxiosResponse } from "axios"

export enum ModelFetcherStatus {
  Idle,
  Fetching,
  Success,
  NotFound,
  PermissionDenied,
  Error,
}

/** Manages the state for the fetching and refetching of a model */
export class ModelFetcher<TModel extends Model = Model> {
  private ModelClass: ModelConstructor<TModel>
  @observable public model?: TModel
  private source: string

  @observable public isDeleted = false
  @observable public status = ModelFetcherStatus.Idle
  @observable public fingerprint: string

  constructor(
    source: string,
    ModelClass: ModelConstructor<TModel>,
    private manager: Manager,
  ) {
    const { authStore } = manager.stores

    this.source = source
    this.ModelClass = ModelClass
    this.fingerprint = authStore.fingerprint
  }

  /** Creates the item or if it exists updates it */
  @action
  public createOrUpdateItem(data: TModel["data"]) {
    if (!this.model) {
      this.model = new this.ModelClass(data, this.manager)
    } else {
      this.model.update(data)
    }
    return this.model
  }

  /** Fetch the data and add it to the model */
  @action
  public fetch(): Promise<TModel> {
    const handleSuccess = action((response: AxiosResponse) => {
      const { authStore } = this.manager.stores

      this.status = ModelFetcherStatus.Success
      this.fingerprint = authStore.fingerprint

      return this.createOrUpdateItem(response.data)
    })

    const handleError = action((error: any) => {
      const { authStore } = this.manager.stores

      if (!error.statusCode) {
        this.status = ModelFetcherStatus.Error
        throw this
      }

      const statusMap = {
        404: ModelFetcherStatus.NotFound,
        401: ModelFetcherStatus.PermissionDenied,
        500: ModelFetcherStatus.Error,
      }

      if (error.statusCode === 404) {
        this.fingerprint = authStore.fingerprint
      }

      this.status = getValueFromMap(
        statusMap,
        error.statusCode,
        ModelFetcherStatus.Error,
      )
      throw this
    })

    this.status = ModelFetcherStatus.Fetching

    return apiServer
      .get<any>(this.source)
      .then(handleSuccess)
      .catch(handleError)
  }

  @computed
  get data(): TModel["data"] | undefined {
    return this.model ? this.model.data : undefined
  }

  @computed
  get isFetched() {
    return this.model != null
  }

  @computed
  public get valid() {
    const { authStore } = this.manager.stores
    return this.fingerprint === authStore.fingerprint
  }
}

/** Represents a ModelFetcher which has been pre-fetched and already has data; the model will always exist */
// IDEA: maybe rename to something like "NonNullModelFetcher"?
export interface PrefetchedModelFetcher<M extends Model>
  extends ModelFetcher<M> {
  model: M
}
