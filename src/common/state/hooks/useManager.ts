import { useContext } from "react"
import { managerContext } from "../contexts/managerContext"

/**
 * This is scaffolding for SSR. Eventually it will get the manager from context.
 */
export const useManager = () => {
  const manager = useContext(managerContext)

  if (!manager) {
    throw new Error("Manager not provided")
  }

  return manager
}
