import { autorun } from "mobx"
import { useEffect } from "react"

export const useAutorun = (callback: () => any) => {
  useEffect(() => autorun(callback))
}
