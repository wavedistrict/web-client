import { Model } from "../classes/Model"
import { ModelFetcher, ModelFetcherStatus } from "../classes/ModelFetcher"
import { useObserver } from "mobx-react-lite"
import { useUniversalEffect } from "../../ui/react/hooks/useUniversalEffect"
import { useStores } from "./useStores"

export type UseFetcherResult<T extends Model> = [
  T | undefined,
  ModelFetcherStatus,
]

export const useFetcher = <T extends Model>(
  fetcher: ModelFetcher<T>,
): UseFetcherResult<T> => {
  const { ssrStore } = useStores()
  const { model, status, valid } = useObserver(() => ({
    model: fetcher.model,
    status: fetcher.status,
    valid: fetcher.valid,
  }))

  useUniversalEffect(() => {
    const shouldFetch =
      !valid &&
      status !== ModelFetcherStatus.Fetching &&
      status !== ModelFetcherStatus.NotFound

    if (shouldFetch) {
      ssrStore.register(fetcher.fetch())
    }
  }, [fetcher, status, valid])

  return [model, status]
}
