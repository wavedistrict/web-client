import { StoreManager } from "./classes/StoreManager"
import { stores } from "./stores"

export const createManager = () => new StoreManager(stores)
