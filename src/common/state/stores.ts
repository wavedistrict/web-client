import { StoreFactories } from "./types/StoreFactories"

import {
  audioMediaStore,
  imageMediaStore,
  AudioMediaStore,
  ImageMediaStore,
} from "../media/stores/mediaStore"
import {
  processingAudioStore,
  ProcessingAudioStore,
} from "../media/stores/processingMediaStore"
import {
  conversationStore,
  ConversationStore,
} from "../../modules/messaging/stores/conversationStore"
import {
  notificationStore,
  NotificationStore,
} from "../../modules/notification/stores/notificationStore"
import {
  userPresenceStore,
  UserPresenceStore,
} from "../../modules/user/stores/userPresenceStore"
import {
  trackUploadStore,
  TrackUploadStore,
} from "../../modules/track/stores/trackUploadStore"
import {
  collectionStore,
  CollectionStore,
} from "../../modules/collection/stores/collectionStore"
import {
  activityStore,
  ActivityStore,
} from "../../modules/auth/stores/activityStore"
import {
  settingsStore,
  SettingsStore,
} from "../../modules/settings/stores/settingsStore"
import { routingStore, RoutingStore } from "../routing/stores/routingStore"
import {
  popoverStore,
  PopoverStore,
} from "../../modules/popover/stores/popoverStore"
import {
  landingStore,
  LandingStore,
} from "../../modules/landing/stores/landingStore"
import {
  messageStore,
  MessageStore,
} from "../../modules/messaging/stores/messageStore"
import {
  exploreStore,
  ExploreStore,
} from "../../modules/explore/stores/exploreStore"
import {
  commentStore,
  CommentStore,
} from "../../modules/commenting/stores/commentStore"
import { configStore, ConfigStore } from "../../modules/core/stores/configStore"
import {
  playerStore,
  PlayerStore,
} from "../../modules/player/stores/playerStore"
import {
  searchStore,
  SearchStore,
} from "../../modules/search/stores/searchStore"
import { queueStore, QueueStore } from "../../modules/player/stores/queueStore"
import { toastStore, ToastStore } from "../../modules/toast/stores/toastStore"
import { trackStore, TrackStore } from "../../modules/track/stores/trackStore"
import { modalStore, ModalStore } from "../../modules/modal/stores/modalStore"
import { imageStore, ImageStore } from "../ui/image/stores/imageStore"
import { authStore, AuthStore } from "../../modules/auth/stores/authStore"
import { userStore, UserStore } from "../../modules/user/stores/userStore"
import { uiStore, UiStore } from "../../modules/core/stores/uiStore"
import { socketStore, SocketStore } from "../network/stores/socketStore"
import { MetaStore, metaStore } from "../ui/meta/stores/metaStore"
import { SSRStore, ssrStore } from "../../modules/core/stores/ssrStore"

export type Stores = {
  processingAudioStore: ProcessingAudioStore
  conversationStore: ConversationStore
  notificationStore: NotificationStore
  userPresenceStore: UserPresenceStore
  trackUploadStore: TrackUploadStore
  audioMediaStore: AudioMediaStore
  imageMediaStore: ImageMediaStore
  collectionStore: CollectionStore
  activityStore: ActivityStore
  settingsStore: SettingsStore
  routingStore: RoutingStore
  popoverStore: PopoverStore
  landingStore: LandingStore
  messageStore: MessageStore
  exploreStore: ExploreStore
  commentStore: CommentStore
  socketStore: SocketStore
  configStore: ConfigStore
  playerStore: PlayerStore
  searchStore: SearchStore
  queueStore: QueueStore
  toastStore: ToastStore
  trackStore: TrackStore
  modalStore: ModalStore
  imageStore: ImageStore
  metaStore: MetaStore
  authStore: AuthStore
  userStore: UserStore
  ssrStore: SSRStore
  uiStore: UiStore
}

export const stores: StoreFactories = {
  processingAudioStore,
  conversationStore,
  notificationStore,
  userPresenceStore,
  trackUploadStore,
  audioMediaStore,
  imageMediaStore,
  collectionStore,
  activityStore,
  settingsStore,
  routingStore,
  popoverStore,
  landingStore,
  messageStore,
  exploreStore,
  commentStore,
  configStore,
  socketStore,
  playerStore,
  searchStore,
  queueStore,
  toastStore,
  trackStore,
  modalStore,
  imageStore,
  authStore,
  userStore,
  metaStore,
  ssrStore,
  uiStore,
}
