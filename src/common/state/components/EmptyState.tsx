import React from "react"
import { getSpacing, getTransparency } from "../../../modules/theme/helpers"
import { styled } from "../../../modules/theme/themes"
import { IconType } from "../../ui/icon"
import { Icon } from "../../ui/icon/components/Icon"

export interface EmptyStateProps {
  icon: IconType
  message: React.ReactNode
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  padding: ${getSpacing("7")}px 0px;

  > .icon {
    width: 84px;
    margin: ${getSpacing("4")}px;

    svg {
      fill: ${getTransparency("strongPositive")};
    }
  }
`

const Message = styled.p`
  margin-top: ${getSpacing("4")}px;
  font-size: 18px;
  color: ${getTransparency("strongPositive")};
  text-align: center;
`

export class EmptyState extends React.Component<EmptyStateProps> {
  public render() {
    const { icon, message, children } = this.props

    return (
      <Container>
        <div className="icon">
          <Icon name={icon} />
        </div>
        <Message>{message}</Message>
        {children}
      </Container>
    )
  }
}
