import React, { HTMLAttributes } from "react"
import { styled } from "../../../modules/theme/themes"
import {
  getTransparency,
  getColor,
  getDuration,
} from "../../../modules/theme/helpers"

export interface ProgressBarProps extends HTMLAttributes<HTMLDivElement> {
  value: number
}

const Container = styled.div`
  height: 2px;
  width: 100%;

  background: ${getTransparency("weakPositive")};
`

const Fill = styled.div`
  background: ${getColor("accent")};

  width: 100%;
  height: 2px;

  transform-origin: 0;
  transition: ${getDuration("normal")} ease transform;
`

export function ProgressBar(props: ProgressBarProps) {
  const { value, ...rest } = props
  const safeValue = Math.min(Math.max(value, 0), 1)

  return (
    <Container
      role="progressbar"
      aria-valuenow={Math.floor(safeValue * 100)}
      aria-valuemin={0}
      aria-valuemax={100}
      {...rest}
    >
      <Fill
        style={{
          transform: `scaleX(${value})`,
        }}
      />
    </Container>
  )
}
