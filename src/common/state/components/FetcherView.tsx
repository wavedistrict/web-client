import React from "react"
import { ModelFetcher, ModelFetcherStatus } from "../classes/ModelFetcher"
import { EmptyStateProps, EmptyState } from "./EmptyState"
import { useFetcher } from "../hooks/useFetcher"
import { Model } from "../classes/Model"
import { styled } from "../../../modules/theme/themes"
import { LoadingIndicator } from "./LoadingIndicator"
import { HEADER_HEIGHT } from "../../../modules/core/constants"

export interface FetcherViewProps<M extends Model> {
  fetcher: ModelFetcher<M>
  empty: EmptyStateProps
  children: (model: M) => React.ReactNode
}

const Container = styled.div`
  height: calc(100vh - 88px - ${HEADER_HEIGHT}px);

  display: flex;
  justify-content: center;
  align-items: center;
`

export function FetcherView<M extends Model>(props: FetcherViewProps<M>) {
  const { fetcher, empty, children } = props
  const [model, status] = useFetcher(fetcher)

  if (status === ModelFetcherStatus.NotFound) {
    return (
      <Container>
        <EmptyState {...empty} />
      </Container>
    )
  }

  if (model) {
    return <>{children(model)}</>
  }

  return (
    <Container>
      <LoadingIndicator />
    </Container>
  )
}
