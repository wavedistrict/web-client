import React from "react"
import { keyframes } from "@emotion/core"
import { styled } from "../../../modules/theme/themes"
import { size, transparentize } from "polished"
import { getColor } from "../../../modules/theme/helpers"
import { range } from "../../lang/array/helpers/range"
import { DelayedRenderer } from "../../perf/components/DelayedRenderer"

export interface LoadingIndicatorProps {
  bar?: boolean
}

const ripple = keyframes`
  0% {
    opacity: 0;
    transform: scale(0);
  }

  40% {
    opacity: 1;
    transform: scale(1);
  }

  80% {
    opacity: 0;
    transform: scale(2);
  }

  100% {
    opacity: 0;
  }
`

const glow = keyframes`
  0% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
`

const RIPPLE_COUNT = 3

const RippleContainer = styled.div`
  position: relative;
  ${size(24)};

  > div {
    opacity: 0;
    ${size("100%")}

    border: solid ${getColor("accent")} 2px;
    border-radius: 100%;

    animation-name: ${ripple};
    animation-timing-function: linear;
    animation-fill-mode: forwards;
    animation-duration: 1500ms;
    animation-iteration-count: infinite;

    position: absolute;

    ${range(RIPPLE_COUNT).map(
      (x) => `
      &:nth-of-type(${x}) {
        animation-delay: ${x * 300}ms;
      }
    `,
    )}
  }
`

const BarContainer = styled.div`
  position: relative;
  width: 100%;
  height: 3px;

  > div {
    width: 100%;
    height: 100%;
    opacity: 0;

    background: ${getColor("accent")};
    position: absolute;

    animation-name: ${glow};
    animation-duration: 1000ms;
    animation-direction: alternate-reverse;
    animation-iteration-count: infinite;

    ${range(RIPPLE_COUNT).map(
      (x) => `
      &:nth-of-type(${x}) {
        animation-delay: ${x * 300}ms;
      }
    `,
    )}

    &:before {
      content: "";
      display: block;

      width: 100%;
      height: 100%;

      box-shadow: 0px 0px 29px 0px
        ${(props) => transparentize(0.3, props.theme.colors.accent)};
    }
  }
`

export function LoadingIndicator(props: LoadingIndicatorProps) {
  const Component = props.bar ? BarContainer : RippleContainer

  return (
    <DelayedRenderer>
      <Component role="progressbar">
        {range(RIPPLE_COUNT).map((x) => (
          <div key={x} />
        ))}
      </Component>
    </DelayedRenderer>
  )
}
