import { Stores } from "../stores"
import { Manager } from "./Manager"

export type StoreFactories = {
  [K in keyof Stores]: (manager: Manager) => Stores[K]
}
