import { StoreManager } from "../classes/StoreManager"
import { Stores } from "../stores"

export type Manager = StoreManager<Stores>

// Temporary until types are fixed
//export type Manager = StoreManager<any>
