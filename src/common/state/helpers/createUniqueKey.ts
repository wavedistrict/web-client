let nextKey = 0

/**
 * A helper function which is guaranteed to return a new key each time it is called.
 */
export const createUniqueKey = () => nextKey++
