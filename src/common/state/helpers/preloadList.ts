import { FetcherList, FetcherListStatus } from "../classes/FetcherList"

export const preloadList = (list: FetcherList) => {
  if (list.hasFetched || list.status !== FetcherListStatus.Idle) return
  list.fetch()
}
