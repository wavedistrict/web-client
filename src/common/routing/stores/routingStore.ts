import { Location } from "history"
import { observable } from "mobx"
import queryString from "query-string"
import { bind } from "../../lang/function/helpers/bind"
import {
  InitializableStore,
  createStoreFactory,
} from "../../state/classes/InitializableStore"
import { createUniversalHistory } from "../helpers/createUniversalHistory"

export class RoutingStore extends InitializableStore {
  private history = createUniversalHistory()
  public status = 200

  @observable public location = this.history.location

  public async init() {
    this.history.listen((location) => this.handleLocationChange(location))
  }

  //@action.bound
  private handleLocationChange(location: Location) {
    this.location = location
  }

  @bind
  public push(path: string) {
    if (path === this.location.pathname) return
    this.history.push(path)

    // Necessary otherwise scroll position is retained on new pages, which is bad for UX
    window.scrollTo(0, 0)
  }

  public replace(path: string) {
    if (path === this.location.pathname) return
    this.history.replace(path)
  }

  public get query(): Record<string, any> {
    return queryString.parse(this.location.search)
  }

  public set query(obj: Record<string, any>) {
    const string = queryString.stringify(obj)

    this.history.replace({
      pathname: this.location.pathname,
      search: string,
    })
  }
}

export const routingStore = createStoreFactory(RoutingStore)
