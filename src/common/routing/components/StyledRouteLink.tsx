import { styled } from "../../../modules/theme/themes"
import { RouteLink } from "./RouteLink"
import { linkStyle } from "../../ui/navigation/components/Link"

export const StyledRouteLink = styled(RouteLink)`
  ${(props) => linkStyle(props.theme)}
`
