import React, { ComponentPropsWithoutRef } from "react"
import { ThemedCSS } from "../../../modules/theme/types/ThemedCSS"
import { useRouteLink } from "../hooks/useRouteLink"

type AnchorProps = ComponentPropsWithoutRef<"a">

export type ThemedRouteLink = ThemedCSS<{
  active: boolean
}>

export interface RouteLinkProps extends AnchorProps {
  to: string
  activeOn?: string
  onClick?: () => void
}

export function RouteLink(props: RouteLinkProps) {
  const { to, activeOn, onClick, ...rest } = props
  const [_, click] = useRouteLink(to, activeOn)

  const handleClick = (event: React.MouseEvent) => {
    click(event)
    if (onClick) onClick()
  }

  return <a onClick={handleClick} href={to} {...rest} />
}
