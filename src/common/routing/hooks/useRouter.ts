import UrlPattern from "url-pattern"
import React from "react"

import { useStores } from "../../state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

export interface Route {
  name: string
  pattern: string
  render: (params: any) => React.ReactElement
}

export const useRouter = (routes: Route[]) => {
  const { routingStore } = useStores()

  return useObserver(() => {
    const { pathname } = routingStore.location

    for (const route of routes) {
      const match = new UrlPattern(route.pattern, {
        segmentValueCharset: "a-zA-Z0-9-_~%.",
      }).match(pathname)

      if (match) {
        return [() => route.render(match), route.name] as const
      }
    }

    return [() => null, undefined] as const
  })
}
