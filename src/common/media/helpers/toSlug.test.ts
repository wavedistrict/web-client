import { toSlug } from "./toSlug"

describe("toSlug", () => {
  it("groups alphanumeric characters, lowercases them, and joins them by a dash", () => {
    const input = "--@#$#@hElLo--    ?   -WoRlD--0!!!!!."
    const output = "hello-world-0"

    expect(toSlug(input)).toBe(output)
  })

  it("returns empty string if there are no alphanumeric characters", () => {
    const input = "@#$%^&*"
    const output = ""

    expect(toSlug(input)).toBe(output)
  })
})
