import { SocketMessageConsumer } from "../../../modules/realtime/classes/SocketMessageConsumer"
import { bind } from "../../lang/function/helpers/bind"
import { SocketMessage } from "../../network/types"
import { ProcessingMediaStore } from "../stores/processingMediaStore"
import { Manager } from "../../state/types/Manager"
import { FilteredKeyof } from "../../lang/types/FilteredKeyof"

export const createProcessingMediaConsumer = <S, M>(
  storeName: FilteredKeyof<Manager["stores"], ProcessingMediaStore<S, M>>,
) => {
  const getStore = (manager: Manager) => {
    return manager.stores[storeName]
  }

  class Consumer extends SocketMessageConsumer {
    protected getListeners() {
      return {
        "update-media-meta": this.handleMetaUpdate,
        "update-media-status": this.handleStatusUpdate,
        "source-list": this.handleSourceList,
        "source-progress": this.handleSourceProgress,
        "processing-source": this.handleActiveSource,
      }
    }

    @bind
    private handleMetaUpdate(message: SocketMessage) {
      const { key, metadata } = message
      const item = getStore(this.manager).getByKey(key)

      if (item) item.update({ ...item.data, metadata })
    }

    @bind
    private handleStatusUpdate(message: SocketMessage) {
      const { key, status } = message
      const item = getStore(this.manager).getByKey(key)

      if (item) item.update({ ...item.data, status })
    }

    @bind
    private handleSourceList(message: SocketMessage) {
      const { key, sources } = message
      const item = getStore(this.manager).getByKey(key)

      if (item) item.setSourceList(sources)
    }

    @bind
    private handleSourceProgress(message: SocketMessage) {
      const { key, progress } = message
      const item = getStore(this.manager).getByKey(key)

      if (item) item.setProgress(progress)
    }

    @bind
    private handleActiveSource(message: SocketMessage) {
      const { key, name } = message
      const item = getStore(this.manager).getByKey(key)

      if (item) item.setActiveSource(name)
    }
  }

  return (manager: Manager) => new Consumer(manager)
}
