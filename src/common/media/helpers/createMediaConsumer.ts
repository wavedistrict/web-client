import { SocketMessageConsumer } from "../../../modules/realtime/classes/SocketMessageConsumer"
import { bind } from "../../lang/function/helpers/bind"
import { SocketMessage } from "../../network/types"
import { MediaStore } from "../stores/mediaStore"
import { MediaReferenceData, MediaSource } from "../types/MediaReferenceData"
import { Manager } from "../../state/types/Manager"
import { FilteredKeyof } from "../../lang/types/FilteredKeyof"
import { MediaModel } from "../models/Media"

export const createMediaConsumer = <
  T extends MediaReferenceData<S>,
  S extends MediaSource
>(
  storeName: FilteredKeyof<Manager["stores"], MediaStore<T, S>>,
) => {
  const getStore = (manager: Manager) => {
    return manager.stores[storeName]
  }

  class Consumer extends SocketMessageConsumer {
    protected getListeners() {
      return {
        "update-media-sources": this.handleSourcesUpdate,
        "update-media-status": this.handleStatusUpdate,
        "add-media-download": this.handleDownload,
      }
    }

    private update(key: string, data: Partial<MediaReferenceData>) {
      const item = getStore(this.manager).getByKey(key) as any

      if (item) {
        item.update({ ...item.data, ...data })
      }
    }

    @bind
    private handleSourcesUpdate(message: SocketMessage) {
      const { key, sources } = message
      this.update(key, { sources })
    }

    @bind
    private handleStatusUpdate(message: SocketMessage) {
      const { key, status } = message
      this.update(key, { status })
    }

    @bind
    private handleDownload(message: SocketMessage) {
      const item = getStore(this.manager).getByKey(message.key)

      if (item) item.data.stats.downloads += 1
    }
  }

  return (manager: Manager) => new Consumer(manager)
}
