export const toSlug = (text: string) => {
  const alphanumGroups = text.match(/\w+/gi) || []
  return alphanumGroups.map((s) => s.toLocaleLowerCase()).join("-")
}
