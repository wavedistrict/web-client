import { MediaModel } from "../models/Media"

export const getPathSafely = (
  model: MediaModel<any> | undefined,
  name: string,
) => {
  if (!model) return ""

  const source = model.getSource(name)
  if (!source) return ""

  return source.path
}
