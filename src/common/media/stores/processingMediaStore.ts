import { Store } from "../../state/classes/Store"
import { ProcessingMedia } from "../models/ProcessingMedia"
import { AudioReferenceMetadata } from "../types/AudioReferenceData"
import { Manager } from "../../state/types/Manager"
import { InitializableStore } from "../../state/classes/InitializableStore"

export class ProcessingMediaStore<S, M = {}> extends InitializableStore {
  private store = new Store<ProcessingMedia<S, M>>()

  public add(options: { id: number; key: string; metadata: M }) {
    const item = new ProcessingMedia<S, M>(
      {
        status: "idle",
        ...options,
      },
      this.manager,
    )

    this.store.addItem(item, options.key)
    return item
  }

  public getByKey(key: string) {
    return this.store.getItem(key)
  }
}

export type ProcessingAudioStore = ProcessingMediaStore<
  unknown,
  AudioReferenceMetadata
>

export const processingAudioStore = (manager: Manager) =>
  new ProcessingMediaStore<unknown, AudioReferenceMetadata>(manager)
