import { Store } from "../../state/classes/Store"
import { MediaModel } from "../models/Media"
import { AudioReferenceData, AudioSource } from "../types/AudioReferenceData"
import { ImageReferenceData, ImageSource } from "../types/ImageReferenceData"
import { MediaReferenceData, MediaSource } from "../types/MediaReferenceData"
import { Manager } from "../../state/types/Manager"
import { InitializableStore } from "../../state/classes/InitializableStore"

export class MediaStore<
  T extends MediaReferenceData<S>,
  S extends MediaSource
> extends InitializableStore {
  private store = new Store<MediaModel<T, S>>()

  constructor(manager: Manager, public type: string) {
    super(manager)
  }

  /** Get an item from the store. If it exists, update the data */
  public update(data: T): MediaModel<T, S> | undefined {
    const existingItem = this.store.getItem(data.key)

    if (existingItem) {
      existingItem.update(data)
      return existingItem
    }
  }

  /** Add an item to the store, or return an existing one */
  public add(data: T): MediaModel<T, S> {
    const existingItem = this.update(data)
    if (existingItem) return existingItem

    const item = new MediaModel<T, S>(data, this.type, this.manager)
    this.store.addItem(item, item.data.key)
    return item
  }

  public assign(data?: T): MediaModel<T, S> | undefined {
    if (!data) return undefined
    return this.add(data)
  }

  public getByKey(key: string) {
    return this.store.getItem(key)
  }
}

export type ImageMediaStore = MediaStore<ImageReferenceData, ImageSource>

export const imageMediaStore = (manager: Manager) =>
  new MediaStore<ImageReferenceData, ImageSource>(manager, "images")

export type AudioMediaStore = MediaStore<AudioReferenceData, AudioSource>

export const audioMediaStore = (manager: Manager) =>
  new MediaStore<AudioReferenceData, AudioSource>(manager, "audio")
