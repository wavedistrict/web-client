import React from "react"
import { PopoverMenu } from "../../../modules/popover/components/PopoverMenu"
import { PopoverTrigger } from "../../../modules/popover/components/PopoverTrigger"
import { SecondaryButton } from "../../ui/button/components/SecondaryButton"
import { MediaModel } from "../models/Media"
import { MediaReferenceData } from "../types/MediaReferenceData"
import { PrimaryButtonVariants } from "../../ui/button/components/PrimaryButton"

export type DownloadButtonProps = PrimaryButtonVariants & {
  model: MediaModel<MediaReferenceData>
}

export function DownloadButton(props: DownloadButtonProps) {
  const { model, ...rest } = props

  return (
    <PopoverTrigger placement="bottom-end">
      {({ isActive, toggle, ref }) => (
        <SecondaryButton
          ref={ref}
          title="Download"
          icon="download"
          label={String(model.data.stats.downloads || "")}
          onClick={toggle}
          active={isActive}
          {...rest}
        />
      )}
      {(props) => {
        const items = model.sources.map((source) => {
          const action = () => {
            window.location.href = source.downloadPath
            props.close()
          }

          return (
            <PopoverMenu.Item onClick={action} key={source.data.name}>
              {source.humanizedName}
            </PopoverMenu.Item>
          )
        })

        return (
          <PopoverMenu>
            <PopoverMenu.Item>Download</PopoverMenu.Item>
            <hr />
            {items}
          </PopoverMenu>
        )
      }}
    </PopoverTrigger>
  )
}
