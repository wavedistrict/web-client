import { MediaReferenceData, MediaSource } from "./MediaReferenceData"

export interface ImageSource extends MediaSource {
  width: number
  height: number
}

export type ImageReferenceData = MediaReferenceData<
  ImageSource,
  {
    name: string
    width: number
    height: number
  }
>
