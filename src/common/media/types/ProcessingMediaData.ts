import { MediaReferenceStatus } from "./MediaReferenceData"

export type ProcessingSourceStatus = "idle" | "processing" | "complete"

export type ProcessingSourceData<T> = {
  name: string
  progress: number
  status: "idle" | "processing"
} & T

export interface ProcessingMediaData<M> {
  status: MediaReferenceStatus
  metadata: M
  key: string
  id: number
}
