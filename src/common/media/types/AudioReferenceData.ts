import { MediaReferenceData, MediaSource } from "./MediaReferenceData"

export interface Loudness {
  truePeak: number
  lufs: number
}

export interface AudioSource extends MediaSource {
  qualityRating: number
  bitdepth?: number
  bitrate?: number
  codec: string
}

export interface AudioReferenceMetadata {
  duration: number
  channels: number
  loudness?: Loudness
}

export type AudioReferenceData = MediaReferenceData<
  AudioSource,
  AudioReferenceMetadata
>
