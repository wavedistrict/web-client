export interface MediaSource {
  name: string
  path: string
  ext: string
  mime: string
  size: number
  /** The source is downloadable */
  downloadable: boolean
  /** The source requires a premium account to access */
  premium: boolean
  /** The source can be streamed */
  streamable: boolean
}

export type MediaReferenceStatus = "idle" | "processing" | "completed" | "error"

export interface MediaReferenceData<
  S extends MediaSource = MediaSource,
  M extends object = {}
> {
  key: string
  sources: S[]
  status: MediaReferenceStatus
  metadata: M
  stats: {
    downloads: number
  }
}
