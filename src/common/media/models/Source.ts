import { computed } from "mobx"
import { MEDIA_URL } from "../../network/constants"
import { Model } from "../../state/classes/Model"
import { MediaSource } from "../types/MediaReferenceData"
import { MediaModel } from "./Media"
import { Manager } from "../../state/types/Manager"

export class SourceModel<T extends MediaSource> extends Model<T> {
  constructor(
    data: T,
    manager: Manager,
    private reference: MediaModel<any, T>,
  ) {
    super(data, manager)
  }

  @computed
  public get path() {
    const { type, data } = this.reference
    const { ext, name } = this.data

    return `${MEDIA_URL}/${type}/${data.key}.${ext}?source=${name}`
  }

  @computed
  public get downloadPath() {
    const { data } = this.reference
    const { ext, name } = this.data

    return `${MEDIA_URL}/download/${data.key}.${ext}?source=${name}`
  }

  @computed
  public get humanizedName() {
    return this.data.name
      .replace(/^./, (c) => c.toUpperCase())
      .replace(/-/g, " ")
  }
}
