import { computed, observable } from "mobx"
import { Model } from "../../state/classes/Model"
import { ProcessingMediaData } from "../types/ProcessingMediaData"
import { ProcessingMediaSource } from "./ProcessingMediaSource"

export interface SourceEntry<T> {
  name: string
  data: T
}

/** Represents a media reference that is being processed */
export class ProcessingMedia<S, M> extends Model<ProcessingMediaData<M>> {
  @observable public sources: ProcessingMediaSource<S>[] = []
  @observable private activeSource?: ProcessingMediaSource<S>

  public setSourceList(entries: SourceEntry<S>[]) {
    this.sources = entries.map(({ name, data }) => {
      return new ProcessingMediaSource<S>(
        {
          ...data,
          status: "idle",
          progress: 0,
          name,
        },
        this.manager,
      )
    })
  }

  public setActiveSource(name: string) {
    if (this.activeSource) {
      this.activeSource.setStatus("complete")
      this.activeSource.setProgress(100)
    }

    const source = this.getSourceByName(name)

    this.activeSource = source
    source.setStatus("processing")
  }

  public setProgress(progress: number) {
    if (this.activeSource) {
      this.activeSource.setProgress(progress)
    }
  }

  private getSourceByName(name: string) {
    const source = this.sources.find(({ data }) => data.name === name)
    if (!source) throw new Error("ProcessingSource not found")

    return source
  }

  @computed
  public get source() {
    return this.activeSource
  }

  @computed
  public get progress() {
    const sum = this.sources.reduce((a, b) => {
      return a + b.data.progress
    }, 0)

    return sum / this.sources.length
  }
}
