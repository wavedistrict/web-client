import { computed } from "mobx"
import { Model } from "../../state/classes/Model"
import {
  ProcessingSourceData,
  ProcessingSourceStatus,
} from "../types/ProcessingMediaData"

export class ProcessingMediaSource<T> extends Model<ProcessingSourceData<T>> {
  public setStatus(status: ProcessingSourceStatus) {
    this.update({ ...this.data, status })
  }

  public setProgress(progress: number) {
    this.update({ ...this.data, progress })
  }

  @computed
  public get humanizedName() {
    return this.data.name
      .replace(/^./, (c) => c.toUpperCase())
      .replace(/-/g, " ")
  }
}
