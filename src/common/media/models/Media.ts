import { computed, observable } from "mobx"
import { Model } from "../../state/classes/Model"
import { MediaReferenceData, MediaSource } from "../types/MediaReferenceData"
import { SourceModel } from "./Source"
import { Manager } from "../../state/types/Manager"

export class MediaModel<
  T extends MediaReferenceData<S>,
  S extends MediaSource = MediaSource
> extends Model<Omit<T, "sources">> {
  @observable public sources: SourceModel<S>[]

  constructor({ sources, ...rest }: T, public type: string, manager: Manager) {
    super(rest, manager)

    this.sources = sources.map((data) => {
      return new SourceModel<S>(data, this.manager, this)
    })
  }

  public onUpdate({ sources }: T) {
    const existingSources = this.sources.map((source) => source.data.name)

    const newSources = sources.filter((source) => {
      return !existingSources.includes(source.name)
    })

    for (const source of newSources) {
      this.sources.push(new SourceModel<S>(source, this.manager, this))
    }
  }

  public getSource(name: string): SourceModel<S> | undefined {
    return this.sources.find((s) => s.data.name === name)
  }

  @computed
  public get isStreamable(): boolean {
    const source = this.sources.find(({ data }) => data.streamable)
    return !!source
  }
}
