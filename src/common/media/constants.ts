export const SQUARE_DIMENSIONS = {
  width: 800,
  height: 800,
}

export const WIDE_DIMENSIONS = {
  width: 1280,
  height: 720,
}
