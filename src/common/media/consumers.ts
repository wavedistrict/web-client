import { createMediaConsumer } from "./helpers/createMediaConsumer"
import { createProcessingMediaConsumer } from "./helpers/createProcessingMediaConsumer"

export const imageMediaConsumer = createMediaConsumer("imageMediaStore")
export const audioMediaConsumer = createMediaConsumer("audioMediaStore")
export const processingAudioConsumer = createProcessingMediaConsumer(
  "processingAudioStore",
)
