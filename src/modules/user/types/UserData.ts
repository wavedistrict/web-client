import { ImageReferenceData } from "../../../common/media/types/ImageReferenceData"
import { Role } from "../../access-control/types/Role"

export interface UserData {
  id: number
  username: string
  displayName: string
  bio?: string
  country?: string
  city?: string
  firstName?: string
  lastName?: string
  avatar?: ImageReferenceData
  coverAvatar?: ImageReferenceData
  roles: Role[]
  createdAt: string
  stats: {
    tracks: number
    collections: number
    followers: number
    comments: number
  }
  byCurrentUser: {
    followed: boolean
    following: boolean
  }
}
