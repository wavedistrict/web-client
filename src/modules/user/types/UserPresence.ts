export interface PlayerPresence {
  track: number
  isPaused: boolean
  timeOffset: number
  playedOn: string
}

export interface UserPresence {
  status: "online" | "offline"
  player?: PlayerPresence
}

export interface UserPresenceMap {
  [userId: number]: UserPresence
}
