import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { ImageFieldController } from "../../../common/ui/form/controllers/ImageFieldController"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { validateStringLength } from "../../../common/ui/form/validators/string/validateStringLength"
import { UserModel } from "../models/User"

export const createUserEditForm = (user: UserModel) => {
  const { displayName, firstName, lastName, city, country, bio } = user.data

  return new FormManager({
    displayName: new TextFieldController({
      value: displayName,
      isRequired: true,
      requiredWarning: "A display name must be specified",
      validators: [validateStringLength({ min: 3, max: 32 })],
    }),
    firstName: new TextFieldController({
      value: firstName,
    }),
    lastName: new TextFieldController({
      value: lastName,
    }),
    country: new TextFieldController({
      value: country,
    }),
    city: new TextFieldController({
      value: city,
    }),
    bio: new TextFieldController({
      value: bio,
      validators: [validateStringLength({ min: 0, max: 4096 })],
    }),
    avatar: new ImageFieldController({
      type: "avatar",
      reference: user.avatar,
    }),
    coverAvatar: new ImageFieldController({
      type: "coverAvatar",
      reference: user.coverAvatar,
    }),
  })
}

export type UserEditFormManager = ReturnType<typeof createUserEditForm>
