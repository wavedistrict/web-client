import { UserModel } from "../models/User"
import { StatItem } from "../../core/components/Stats"

export const getUserStats = (user: UserModel) => {
  const { followers, tracks, collections } = user.data.stats

  const items: StatItem[] = [
    {
      name: "Followers",
      icon: "personFilled",
      value: followers,
    },
    {
      name: "Tracks",
      icon: "trackFilled",
      value: tracks,
    },
    {
      name: "Collections",
      icon: "collectionFilled",
      value: collections,
    },
  ]

  return items
}
