import { LinkedTabItem } from "../../../common/ui/navigation/components/LinkedTabs"
import { routes } from "../../core/constants"
import { UserModel } from "../models/User"

export const getUserTabs = (user: UserModel): LinkedTabItem[] => {
  const { username } = user.data

  return [
    {
      to: routes.user(username, "tracks"),
      icon: "trackFilled",
      label: "Tracks",
    },
    {
      to: routes.user(username, "collections"),
      icon: "collectionFilled",
      label: "Collections",
    },
    {
      to: routes.user(username, "comments"),
      icon: "commentFilled",
      label: "Comments",
    },
  ]
}
