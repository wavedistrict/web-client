import { getImagePath } from "../../../common/ui/meta/helpers/getImagePath"
import { getNormalizedMeta } from "../../../common/ui/meta/helpers/getNormalizedMeta"
import { getSafeString } from "../../../common/ui/meta/helpers/getSafeString"
import { UserModel } from "../models/User"

export const getUserMeta = (user: UserModel) => {
  const { displayName, bio } = user.data

  const avatar = getImagePath(user.avatar)
  const safeBio =
    getSafeString(bio) || `Listen to ${displayName}'s tracks on WaveDistrict`

  return {
    description: safeBio,
    title: displayName,
    image: avatar,
    url: user.clientLink,
    type: "profile",
  }
}
