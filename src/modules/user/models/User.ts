import { computed, observable } from "mobx"
import { hashString } from "../../../common/lang/string/helpers/hashString"
import { Model } from "../../../common/state/classes/Model"
import { ImageModel } from "../../../common/ui/image/models/ImageModel"
import { QuotaData } from "../../auth/types/QuotaData"
import { routes } from "../../core/constants"
import { UserData } from "../types/UserData"
import { Manager } from "../../../common/state/types/Manager"

export class UserModel extends Model<UserData> {
  public hash = hashString(this.data.username)

  public clientLink: string
  public apiPath: string

  @observable public avatar?: ImageModel
  @observable public coverAvatar?: ImageModel
  @observable public quota?: QuotaData

  constructor(data: UserData, manager: Manager) {
    super(data, manager)

    const { imageMediaStore } = manager.stores

    this.clientLink = routes.user(data.username)
    this.apiPath = `/users/${data.id}`

    this.avatar = imageMediaStore.assign(data.avatar)
    this.coverAvatar = imageMediaStore.assign(data.coverAvatar)
  }

  protected getListMap() {
    const {
      trackStore,
      collectionStore,
      notificationStore,
      conversationStore,
      commentStore,
      userStore,
    } = this.manager.stores

    return {
      tracks: {
        store: trackStore,
        path: `/users/${this.data.id}/tracks`,
      },
      collections: {
        store: collectionStore,
        path: `/users/${this.data.id}/collections`,
      },
      favoritedTracks: {
        store: trackStore,
        path: `${this.apiPath}/favorited-tracks`,
      },
      notifications: {
        store: notificationStore,
        path: "/notifications",
        pending: false,
      },
      conversations: {
        store: conversationStore,
        path: "/conversations",
        pending: false,
      },
      comments: {
        store: commentStore,
        path: `${this.apiPath}/comments`,
      },
      followers: {
        store: userStore,
        path: `${this.apiPath}/followers`,
      },
    }
  }

  public get tracks() {
    return this.getOrCreateList("tracks")
  }

  public get collections() {
    return this.getOrCreateList("collections")
  }

  public get favoritedTracks() {
    return this.getOrCreateList("favoritedTracks")
  }

  public get notifications() {
    return this.getOrCreateList("notifications")
  }

  public get conversations() {
    return this.getOrCreateList("conversations")
  }

  public get comments() {
    return this.getOrCreateList("comments")
  }

  public get followers() {
    return this.getOrCreateList("followers")
  }

  protected onUpdate(data: UserData) {
    const { imageMediaStore } = this.manager.stores

    this.avatar = imageMediaStore.assign(data.avatar)
    this.coverAvatar = imageMediaStore.assign(data.coverAvatar)
  }

  @computed
  public get presence() {
    const { userPresenceStore } = this.manager.stores

    return userPresenceStore.getPresenceByUserId(this.data.id)
  }

  @computed
  public get isSelf() {
    const { authStore } = this.manager.stores
    const { user } = authStore
    const { id } = this.data

    return user ? user.data.id === id : false
  }
}
