import { size } from "polished"
import React from "react"
import { RouteLink } from "../../../common/routing/components/RouteLink"
import { StatItem, Stats } from "../../core/components/Stats"
import { getFontSize, getFontWeight, getSpacing } from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { styled } from "../../theme/themes"
import { UserModel } from "../models/User"
import { OnlineStatusIndicator } from "./OnlineStatusIndicator"
import { UserAvatar } from "./UserAvatar"

interface UserSummaryProps {
  user: UserModel
}

const Container = styled.div`
  display: flex;
`

const Avatar = styled(RouteLink)`
  flex-shrink: 0;
  ${size(48)}
  ${imageContainer("100%")}
`

const PrimaryInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  margin-left: ${getSpacing("4")}px;

  font-size: ${getFontSize("2")}px;
  font-weight: ${getFontWeight("bold")};
`

const Header = styled.div`
  display: flex;
  align-items: baseline;
`

const Status = styled.div`
  margin-left: ${getSpacing("3")}px;
`

const StatsContainer = styled.div`
  margin-top: ${getSpacing("2")}px;
`

export function UserSummary(props: UserSummaryProps) {
  const { user } = props
  const { stats, displayName } = user.data

  const items: StatItem[] = [
    {
      name: "Tracks",
      icon: "trackFilled",
      value: stats.tracks,
    },
    {
      name: "Followers",
      icon: "personFilled",
      value: stats.followers,
    },
  ]

  return (
    <Container>
      <Avatar to={user.clientLink}>
        <UserAvatar id="summary" user={user} />
      </Avatar>
      <PrimaryInfo>
        <Header>
          <RouteLink to={user.clientLink}>{displayName}</RouteLink>
          <Status>
            <OnlineStatusIndicator user={user} />
          </Status>
        </Header>
        <StatsContainer>
          <Stats compact items={items} />
        </StatsContainer>
      </PrimaryInfo>
    </Container>
  )
}
