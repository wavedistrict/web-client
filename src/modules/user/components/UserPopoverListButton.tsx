import { size } from "polished"
import { styled } from "../../theme/themes"
import { Button } from "../../../common/ui/button/components/Button"
import {
  getSpacing,
  getFontWeight,
  getTransparency,
  getDuration,
} from "../../theme/helpers"

export const UserPopoverListButton = styled(Button)`
  align-items: center;

  padding: ${getSpacing("4")}px;
  border-radius: 3px;

  overflow: hidden;
  transition: ${getDuration("normal")} ease background;

  > .icon {
    ${size(18)}
    margin-right: ${getSpacing("4")}px;
  }

  > .label {
    font-weight: ${getFontWeight("bold")}
  }

  &:hover {
    background: ${getTransparency("weakPositive")}
  }

  &:active {
    background: ${getTransparency("strongNegative")}
  }
`
