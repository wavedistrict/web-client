import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { useSetting } from "../../settings/hooks/useSetting"
import { getDuration, getTransparency } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { UserModel } from "../models/User"

export interface OnlineStatusIndicatorProps {
  user: UserModel
}

const Container = styled.div<{
  online: boolean
}>`
  ${size(8)};

  background: ${getTransparency("weakPositive")};
  border-radius: 50px;

  transition: ${getDuration("normal")} ease all;

  ${(props) =>
    props.online &&
    `
    background: ${props.theme.colors.success};
  `}
`

export const OnlineStatusIndicator = observer(function OnlineStatusIndicator(
  props: OnlineStatusIndicatorProps,
) {
  const { user } = props
  const { status } = user.presence

  const setting = useSetting("general", "offlineMode")

  return <Container online={status === "online" && !setting} />
})
