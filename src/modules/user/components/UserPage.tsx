import React from "react"
import { useMediaQuery } from "../../../common/ui/react/hooks/useMediaQuery"
import { getUserMeta } from "../helpers/getUserMeta"
import { SimpleUserView } from "./SimpleUserView/SimpleUserView"
import { UserView } from "./UserView/UserView"
import { FetcherView } from "../../../common/state/components/FetcherView"
import { useStores } from "../../../common/state/hooks/useStores"
import { UserModel } from "../models/User"
import { useMeta } from "../../../common/ui/meta/hooks/useMeta"

export interface UserPageProps {
  username: string
}

export type UserPageRendererProps = {
  user: UserModel
}

export function UserPageRenderer(props: UserPageRendererProps) {
  const { user } = props
  const [simple] = useMediaQuery(1135)

  useMeta(getUserMeta(user))

  return simple ? <SimpleUserView user={user} /> : <UserView user={user} />
}

export function UserPage(props: UserPageProps) {
  const { username } = props
  const { userStore } = useStores()

  return (
    <FetcherView
      empty={{ message: "User not found", icon: "personFilled" }}
      fetcher={userStore.fetchByUsernameOrId(username)}
    >
      {(model) => <UserPageRenderer user={model} />}
    </FetcherView>
  )
}
