import React from "react"
import { RouteLink } from "../../../common/routing/components/RouteLink"
import { UserModel } from "../models/User"
import { OnlineStatusIndicator } from "./OnlineStatusIndicator"
import { UserAvatar } from "./UserAvatar"
import { useObserver } from "mobx-react-lite"
import { styled } from "../../theme/themes"
import {
  getColor,
  getShadow,
  getSpacing,
  getFontWeight,
  getFontSize,
} from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { ellipsis } from "polished"
import { Stats } from "../../core/components/Stats"
import { getUserStats } from "../helpers/getUserStats"

export interface UserGridItemProps {
  user: UserModel
}

const Container = styled.li`
  display: flex;
  flex-direction: column;

  background: ${getColor("primary")};

  border-radius: 3px;
  overflow: hidden;

  box-shadow: ${getShadow("light")};
`

const Avatar = styled(RouteLink)`
  position: relative;

  width: 100%;
  padding-bottom: 100%;

  ${imageContainer()}
`

const Footer = styled.div`
  padding: ${getSpacing("4")}px;
`

const PrimaryInfo = styled.div`
  display: flex;
  align-items: flex-end;
`

const DisplayName = styled(RouteLink)`
  margin-right: ${getSpacing("3")}px;

  font-size: ${getFontSize("2")}px;
  font-weight: ${getFontWeight("bold")};

  ${ellipsis("100%")}
`

const Status = styled.div`
  margin-bottom: 3px;
  flex: 1;
`

const SecondaryInfo = styled.div`
  display: flex;
  align-items: center;
  margin-top: ${getSpacing("2")}px;
`

export function UserGridItem(props: UserGridItemProps) {
  const { user } = props

  return useObserver(() => (
    <Container>
      <Avatar to={user.clientLink}>
        <UserAvatar id="grid-item" absolute user={user} />
      </Avatar>
      <Footer>
        <PrimaryInfo>
          <DisplayName to={user.clientLink}>
            {user.data.displayName}
          </DisplayName>
          <Status>
            <OnlineStatusIndicator user={user} />
          </Status>
        </PrimaryInfo>
        <SecondaryInfo>
          <Stats tiny compact items={getUserStats(user)} />
        </SecondaryInfo>
      </Footer>
    </Container>
  ))
}

/*
@observer
export class UserGridItem extends React.Component<UserGridItemProps> {
  public render() {
    const { user } = this.props

    return (
      <RouteLink className="UserGridItem" to={user.clientLink}>
        <div className="avatar">
          <UserAvatar id="grid-item" absolute user={user} />
        </div>
        <div className="footer">
          <p className="username">{user.data.displayName}</p>
          <div className="status">
            <OnlineStatusIndicator user={user} />
          </div>
        </div>
      </RouteLink>
    )
  }
}
*/
