import css from "@emotion/css"
import { size } from "polished"
import React from "react"
import { Button } from "../../../common/ui/button/components/Button"
import { HEADER_HEIGHT } from "../../core/constants"
import { PopoverTrigger } from "../../popover/components/PopoverTrigger"
import { getColor, getDuration, getTransparency } from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { styled } from "../../theme/themes"
import { Theme } from "../../theme/types/Theme"
import { UserModel } from "../models/User"
import { OnlineStatusIndicator } from "./OnlineStatusIndicator"
import { UserAvatar } from "./UserAvatar"
import { UserPopoverContent } from "./UserPopoverContent"

interface UserPopoverButtonProps {
  user: UserModel
}

const Container = (theme: Theme) => css`
  height: ${HEADER_HEIGHT}px;
  position: relative;

  display: flex;
  justify-content: center;
  align-items: center;

  outline: none;

  &:hover > .frame,
  &:focus > .frame {
    background: ${theme.transparencies.strongPositive};
  }
`

const Avatar = styled.div`
  ${imageContainer("100%")};

  position: relative;
  height: 100%;
  width: 100%;
`

const Frame = styled.div`
  ${size(35)};

  border-radius: 100%;
  overflow: hidden;

  background: ${getTransparency("weakPositive")};
  transition: ${getDuration("normal")} ease all;
  padding: 2px;
`

const Status = styled.div`
  position: absolute;

  bottom: 9px;
  right: 0px;

  background: ${getColor("primary")};
  padding: 2px;
  border-radius: 100%;
`

export function UserPopoverButton(props: UserPopoverButtonProps) {
  const { user } = props

  return (
    <PopoverTrigger placement="bottom-end">
      {(props) => (
        <Button css={Container} ref={props.ref} onClick={() => props.toggle()}>
          <Frame className="frame">
            <Avatar>
              <UserAvatar id="popoverButton" user={user} />
            </Avatar>
          </Frame>
          <Status>
            <OnlineStatusIndicator user={user} />
          </Status>
        </Button>
      )}
      {(props) => <UserPopoverContent {...props} user={user} />}
    </PopoverTrigger>
  )
}
