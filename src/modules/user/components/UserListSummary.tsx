import { useObserver } from "mobx-react-lite"
import { size } from "polished"
import React, { useEffect } from "react"
import { RouteLink } from "../../../common/routing/components/RouteLink"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { LoadingIndicator } from "../../../common/state/components/LoadingIndicator"
import { getSpacing } from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { styled } from "../../theme/themes"
import { UserModel } from "../models/User"
import { UserAvatar } from "./UserAvatar"

export const USER_LIST_SUMMARY_LIMIT = 6

interface UserListSummaryProps {
  list: FetcherList<UserModel>
}

const Container = styled.ul`
  display: flex;
  justify-content: space-between;
`

const Loading = styled.div`
  margin-top: ${getSpacing("2")};
  width: 100%;

  display: flex;
  justify-content: center;
`

const Item = styled.li`
  ${size(38)}
  ${imageContainer("100%")}
`

export function UserListSummary(props: UserListSummaryProps) {
  const { list } = props

  useEffect(() => {
    if (list.isFresh) {
      list.limit = 6
      list.fetch()
    }
  })

  const renderLoading = () => (
    <Loading>
      <LoadingIndicator />
    </Loading>
  )

  const renderList = () =>
    Array(USER_LIST_SUMMARY_LIMIT)
      .fill(undefined)
      .map((_, index) => {
        const user = list.models[index]

        if (user) {
          return (
            <Item key={user.data.id}>
              <RouteLink to={user.clientLink}>
                <UserAvatar id="list-summary" user={user} />
              </RouteLink>
            </Item>
          )
        }

        return <Item key={`${performance.now()}-${index}`} />
      })

  const content = useObserver(() =>
    list.hasFetched ? renderList() : renderLoading(),
  )

  return <Container>{content}</Container>
}
