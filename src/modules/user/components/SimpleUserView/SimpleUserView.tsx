import { observer } from "mobx-react"
import React from "react"
import { LinkedTabs } from "../../../../common/ui/navigation/components/LinkedTabs"
import { getColor, getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { getUserTabs } from "../../helpers/getUserTabs"
import { About } from "../UserView/About"
import { RoleList } from "../UserView/RoleList"
import { UserViewProps } from "../UserView/UserView"
import { ActionBar } from "./ActionBar"
import { Hero } from "./Hero"
import { UserPageRouter } from "../UserPageRouter"

const Container = styled.div`
  > .tabs {
    background: ${getColor("primary")};
    padding: 0px ${getSpacing("5")}px;
  }

  > .content {
    position: relative;

    background: ${getColor("primary")};
    padding: ${getSpacing("5")}px;
    padding-top: 0px;

    margin-top: -${getSpacing("5")}px;
  }
`

export const SimpleUserView = observer(function SimpleUserView(
  props: UserViewProps,
) {
  const { user } = props

  return (
    <Container>
      <Hero user={user} />
      <div className="content">
        <ActionBar user={user} />
        <RoleList user={user} />
        <About user={user} />
      </div>
      <div className="tabs">
        <LinkedTabs variants={["borderless"]} items={getUserTabs(user)} />
      </div>
      <UserPageRouter user={user} />
    </Container>
  )
})
