import { observer } from "mobx-react"
import { cover, size } from "polished"
import React from "react"
import { getColor, getShadow, getSpacing } from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { styled } from "../../../theme/themes"
import { UserAvatar } from "../UserAvatar"
import { Background } from "../UserView/Background"
import { HeroProps } from "../UserView/Hero"
import { PrimaryInfo } from "../UserView/PrimaryInfo"

const GRADIENT_LENGTH = 64

const Container = styled.div`
  position: relative;

  > .background {
    ${cover()}
  }

  > .foreground {
    display: flex;
    align-items: center;

    position: relative;
    padding: ${getSpacing("5")}px;
    z-index: 1;
  }

  > .foreground > .avatar {
    ${imageContainer(3)}
    ${size(72)}

    box-shadow: ${getShadow("light")};
  }

  > .foreground > .content {
    flex: 1;
    margin-left: ${getSpacing("4")}px;
  }

  > .fade {
    position: absolute;
    left: 0px;
    right: 0px;
    bottom: 0px;

    height: ${GRADIENT_LENGTH * 2}px;
    background: linear-gradient(transparent, ${getColor("primary")})
  }
`

export const Hero = observer(function Hero(props: HeroProps) {
  const { user } = props

  return (
    <Container>
      <div className="background">
        <Background user={user} />
      </div>
      <div className="foreground">
        <div className="avatar">
          <UserAvatar id="view" lightbox user={user} />
        </div>
        <div className="content">
          <PrimaryInfo user={user} />
        </div>
      </div>
      <div className="fade" />
    </Container>
  )
})
