import { observer } from "mobx-react"
import React from "react"
import { SecondaryButton } from "../../../../common/ui/button/components/SecondaryButton"
import { Stats } from "../../../core/components/Stats"
import { FollowButton } from "../../../following/components/FollowButton"
import { spawnNewMessageModal } from "../../../messaging/actions/spawnNewMessageModal"
import { getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { spawnUserEditModal } from "../../actions/spawnUserEditModal"
import { USER_SIDEBAR_MODULE_SPACING } from "../../constants"
import { UserModel } from "../../models/User"
import { getUserStats } from "../../helpers/getUserStats"
import { PrimaryButtonVariants } from "../../../../common/ui/button/components/PrimaryButton"
import { useManager } from "../../../../common/state/hooks/useManager"

export interface StatsProps {
  user: UserModel
}

const Container = styled.div`
  margin-top: ${getSpacing(USER_SIDEBAR_MODULE_SPACING)}px;

  display: flex;
  justify-content: space-between;
  align-items: center;

  > .actions > * + * {
    margin-left: ${getSpacing("4")}px;
  }
`

export const ActionBar = observer(function ActionBar(props: StatsProps) {
  const { user } = props
  const manager = useManager()

  const variants: PrimaryButtonVariants = {
    outlined: true,
  }

  const renderAnonymousActions = () => {
    if (user.isSelf) return null

    return (
      <>
        <FollowButton iconOnly item={user} {...variants} />
        <SecondaryButton
          icon="envelope"
          onClick={() => spawnNewMessageModal(manager, user)}
          {...variants}
        />
      </>
    )
  }

  const renderOwnActions = () => {
    if (!user.isSelf) return null

    return (
      <>
        <SecondaryButton
          icon="pencil"
          onClick={() => spawnUserEditModal(manager, user)}
          {...variants}
        />
      </>
    )
  }

  return (
    <Container>
      <div className="stats">
        <Stats items={getUserStats(user)} />
      </div>
      <div className="actions">
        {renderAnonymousActions()}
        {renderOwnActions()}
      </div>
    </Container>
  )
})
