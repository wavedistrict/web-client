import { observer } from "mobx-react"
import React from "react"
import { styled } from "../../../theme/themes"
import { OnlineStatusIndicator } from "../OnlineStatusIndicator"
import { PrimaryInfoProps } from "../UserView/PrimaryInfo"

const Container = styled.div``

export const PrimaryInfo = observer(function PrimaryInfo(
  props: PrimaryInfoProps,
) {
  const { user } = props
  const { displayName, username } = user.data

  return (
    <Container>
      <div className="displayName">
        <h1 title={displayName} className="text">
          {displayName}
        </h1>
        <div className="status">
          <OnlineStatusIndicator user={user} />
        </div>
      </div>
    </Container>
  )
})
