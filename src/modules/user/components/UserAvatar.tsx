import { createImageComponent } from "../../../common/ui/image/helpers/createImageComponent"
import { UserModel } from "../models/User"

export const UserAvatar = createImageComponent<{ user: UserModel }>((props) => {
  const { user, id } = props

  return {
    id: `User:${id}`,
    reference: user.avatar,
    placeholder: {
      hash: user.hash,
      icon: "personFilled",
    },
  }
})
