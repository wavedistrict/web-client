import React from "react"
import { CollectionList } from "../../collection/components/CollectionList"
import { CommentList } from "../../commenting/components/CommentList"
import { routes } from "../../core/constants"
import { TrackList } from "../../track/components/TrackList"
import { UserModel } from "../models/User"
import { useObserver } from "mobx-react-lite"
import { Route, useRouter } from "../../../common/routing/hooks/useRouter"
import { useRedirect } from "../../../common/routing/hooks/useRedirect"

export interface UserPageRouterProps {
  user: UserModel
}

export function UserPageRouter(props: UserPageRouterProps) {
  const { user } = props

  const { username, displayName } = useObserver(() => ({
    username: user.data.username,
    displayName: user.data.displayName,
  }))

  const routeList: Route[] = [
    {
      name: "tracks",
      pattern: routes.user(username, "tracks"),
      render: () => (
        <TrackList
          clientLink={user.clientLink}
          name={`${displayName}'s tracks`}
          list={user.tracks}
        />
      ),
    },
    {
      name: "collections",
      pattern: routes.user(username, "collections"),
      render: () => <CollectionList list={user.collections} />,
    },
    {
      name: "comments",
      pattern: routes.user(username, "comments"),
      render: () => <CommentList list={user.comments} />,
    },
  ]

  const [renderRoute] = useRouter(routeList)
  useRedirect(`/@${user.data.username}`, user.clientLink)

  return renderRoute()
}
