import { observer } from "mobx-react"
import React from "react"
import {
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { UserModel } from "../../models/User"
import { OnlineStatusIndicator } from "../OnlineStatusIndicator"

export interface PrimaryInfoProps {
  user: UserModel
}

const Container = styled.div`
  > .displayName {
    > .text {
      display: inline;

      font-size: ${getFontSize("3")}px;
      font-weight: ${getFontWeight("bold")};

      line-height: 24px;
    }

    > .status {
      margin-left: ${getSpacing("3")}px;
      display: inline-block;
    }
  }

  > .username {
    font-size: ${getFontSize("2")}px;
    color: ${getFontColor("muted")};

    margin-top: ${getSpacing("1")}px;
  }
`

export const PrimaryInfo = observer(function PrimaryInfo(
  props: PrimaryInfoProps,
) {
  const { user } = props
  const { displayName, username } = user.data

  return (
    <Container>
      <div className="displayName">
        <h1 title={displayName} className="text">
          {displayName}
        </h1>
        <div className="status">
          <OnlineStatusIndicator user={user} />
        </div>
      </div>
      <h2 className="username">@{username}</h2>
    </Container>
  )
})
