import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import timeago from "s-ago"
import { IconType } from "../../../../common/ui/icon"
import { Icon } from "../../../../common/ui/icon/components/Icon"
import { getFontColor, getFontSize, getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { USER_SIDEBAR_MODULE_SPACING } from "../../constants"
import { UserModel } from "../../models/User"

export interface SecondaryInfoProps {
  user: UserModel
}

const Container = styled.div`
  margin-top: ${getSpacing(USER_SIDEBAR_MODULE_SPACING)}px;

  > .item {
    display: flex;
    align-items: center;

    margin-top: ${getSpacing("4")}px;
  }

  > .item > .icon {
    ${size(18)};
    margin-right: ${getSpacing("3")}px;
  }

  > .item > .text {
    font-size: ${getFontSize("1")}px;
    color: ${getFontColor("muted")};
  }
`

export const SecondaryInfo = observer(function SecondaryInfo(
  props: SecondaryInfoProps,
) {
  const { user } = props
  const { firstName, lastName, country, city, createdAt } = user.data

  const items: React.ReactNode[] = []

  const renderPoint = (icon: IconType, text: string) => {
    return (
      <div key={icon} className="item">
        <div className="icon">
          <Icon name={icon} />
        </div>
        <div className="text">{text}</div>
      </div>
    )
  }

  if (firstName) {
    const names = [firstName, lastName]
    const fullName = names.filter((x) => x).join(" ")

    items.push(renderPoint("personFilled", fullName))
  }

  if (country || city) {
    const locations = [city, country]
    const fullLocation = locations.filter((x) => x).join(", ")

    items.push(renderPoint("location", fullLocation))
  }

  const humanizedDate = timeago(new Date(createdAt))
  items.push(renderPoint("calendar", `Joined ${humanizedDate}`))

  return <Container>{items}</Container>
})
