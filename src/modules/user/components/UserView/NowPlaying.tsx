import { observer } from "mobx-react"
import React from "react"
import { useFetcher } from "../../../../common/state/hooks/useFetcher"
import { Section } from "../../../core/components/Section"
import { PlayerPresenceDisplay } from "../../../player/components/PlayerPresenceDisplay"
import { getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { USER_SIDEBAR_MODULE_SPACING } from "../../constants"
import { UserModel } from "../../models/User"
import { PlayerPresence } from "../../types/UserPresence"
import { useStores } from "../../../../common/state/hooks/useStores"

export interface NowListeningProps {
  user: UserModel
}

const Container = styled.div`
  margin-top: ${getSpacing(USER_SIDEBAR_MODULE_SPACING)}px;
`

export const NowListening = observer(function NowListening(
  props: NowListeningProps,
) {
  const { presence } = props.user
  const { player, status } = presence

  if (!player || player.isPaused || status === "offline") {
    return null
  }

  return <Content presence={player} />
})

export const Content = observer(function Content(props: {
  presence: PlayerPresence
}) {
  const { presence } = props
  const { trackStore } = useStores()

  const [track] = useFetcher(trackStore.fetchById(presence.track))

  if (!track || track.data.visibility !== "listed") {
    return null
  }

  return (
    <Container>
      <Section title="Now listening">
        <PlayerPresenceDisplay track={track} />
      </Section>
    </Container>
  )
})
