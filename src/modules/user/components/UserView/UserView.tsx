import { observer } from "mobx-react"
import React from "react"
import { LinkedTabs } from "../../../../common/ui/navigation/components/LinkedTabs"
import { getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { USER_SIDEBAR_PADDING } from "../../constants"
import { getUserTabs } from "../../helpers/getUserTabs"
import { UserModel } from "../../models/User"
import { Hero } from "./Hero"
import { Sidebar } from "./Sidebar"
import { UserPageRouter } from "../UserPageRouter"

const Container = styled.div`
  padding-top: ${getSpacing("6")}px;

  > .content {
    display: flex;
  }

  > .content > .router {
    flex: 1;
    margin-top: ${getSpacing(USER_SIDEBAR_PADDING)}px;
  }

  > .content > .router > .tabs {
    margin-bottom: ${getSpacing("5")}px;
  }
`

export interface UserViewProps {
  user: UserModel
}

export const UserView = observer(function UserView(props: UserViewProps) {
  const { user } = props

  return (
    <Container>
      <Hero user={user} />
      <div className="content">
        <Sidebar user={user} />
        <div className="router">
          <div className="tabs">
            <LinkedTabs items={getUserTabs(user)} />
          </div>
          <UserPageRouter user={user} />
        </div>
      </div>
    </Container>
  )
})
