import { observer } from "mobx-react"
import React from "react"
import { Description } from "../../../core/components/Description"
import { Section } from "../../../core/components/Section"
import { getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { USER_SIDEBAR_MODULE_SPACING } from "../../constants"
import { UserModel } from "../../models/User"
import { SecondaryInfo } from "./SecondaryInfo"

export interface AboutProps {
  user: UserModel
}

const Container = styled.div`
  margin-top: ${getSpacing(USER_SIDEBAR_MODULE_SPACING)}px;
`

export const About = observer(function About(props: AboutProps) {
  const { user } = props
  const { bio } = user.data

  return (
    <Container>
      <Section title="About">
        <Description noDescNotice="No bio provided" text={bio} />
        <SecondaryInfo user={user} />
      </Section>
    </Container>
  )
})
