import { observer } from "mobx-react"
import React from "react"
import { RoleRenderer } from "../../../access-control/components/RoleRenderer"
import { Role } from "../../../access-control/types/Role"
import { getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { USER_SIDEBAR_MODULE_SPACING } from "../../constants"
import { UserModel } from "../../models/User"

export interface RoleListProps {
  user: UserModel
}

const VISIBLE_ROLES: Role[] = [
  "Administrator",
  "Moderator",
  "Developer",
  "Amplitude",
]

const Container = styled.ul`
  margin-top: ${getSpacing(USER_SIDEBAR_MODULE_SPACING)}px;
  margin-bottom: -${getSpacing("3")}px;

  > .item {
    display: inline-block;

    margin-bottom: ${getSpacing("3")}px;
    margin-right: ${getSpacing("3")}px;
  }
`

export const RoleList = observer(function RoleList(props: RoleListProps) {
  const { user } = props
  const { roles } = user.data

  const filteredRoles = roles.filter((role) => {
    const canShow = VISIBLE_ROLES.includes(role)
    const notAmplitude = roles[0] === "Amplitude" || role !== "Amplitude"

    return canShow && notAmplitude
  })

  if (filteredRoles.length === 0) return null

  return (
    <Container>
      {filteredRoles.map((role) => (
        <li key={role} className="item">
          <RoleRenderer name={role} />
        </li>
      ))}
    </Container>
  )
})
