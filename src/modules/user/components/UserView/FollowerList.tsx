import { observer } from "mobx-react"
import React from "react"
import { Section } from "../../../core/components/Section"
import { getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { USER_SIDEBAR_MODULE_SPACING } from "../../constants"
import { UserModel } from "../../models/User"
import { UserListSummary } from "../UserListSummary"

export interface FollowerListProps {
  user: UserModel
}

const Container = styled.div`
  margin-top: ${getSpacing(USER_SIDEBAR_MODULE_SPACING)}px;
`

export const FollowerList = observer(function FollowerList(
  props: FollowerListProps,
) {
  const { user } = props
  const { followers } = user.data.stats

  if (followers === 0) return null

  return (
    <Container>
      <Section title="Followers">
        <UserListSummary list={user.followers} />
      </Section>
    </Container>
  )
})
