import { observer } from "mobx-react"
import { cover, size } from "polished"
import React from "react"
import { SecondaryButton } from "../../../../common/ui/button/components/SecondaryButton"
import { Stats } from "../../../core/components/Stats"
import { FollowButton } from "../../../following/components/FollowButton"
import { spawnNewMessageModal } from "../../../messaging/actions/spawnNewMessageModal"
import { Overlay } from "../../../theme/components/Overlay"
import { getShadow, getSpacing } from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { styled } from "../../../theme/themes"
import { spawnUserEditModal } from "../../actions/spawnUserEditModal"
import { USER_SIDEBAR_PADDING, USER_SIDEBAR_WIDTH } from "../../constants"
import { UserModel } from "../../models/User"
import { Background } from "./Background"
import { getUserStats } from "../../helpers/getUserStats"
import { ButtonList } from "../../../../common/ui/button/components/ButtonList"
import { useManager } from "../../../../common/state/hooks/useManager"

export interface HeroProps {
  user: UserModel
}

const Container = styled.div`
  ${imageContainer(3)}
  box-shadow: ${getShadow("light")};

  > .background {
    ${cover()}
  }

  > .foreground {
    position: relative;
    padding: ${getSpacing(USER_SIDEBAR_PADDING)}px;

    display: flex;
  }
`

const AvatarSpace = styled.div`
  ${size(USER_SIDEBAR_WIDTH)}
`

const Content = styled.div`
  margin-left: ${getSpacing("6")}px;

  display: flex;
  flex: 1;

  align-items: flex-end;
  justify-content: space-between;
`

export const Hero = observer(function Hero(props: HeroProps) {
  const manager = useManager()
  const { user } = props

  const renderAnonymousActions = () => {
    if (user.isSelf) return null

    return (
      <>
        <FollowButton item={user} />
        <SecondaryButton
          icon="envelope"
          label="Message"
          onClick={() => spawnNewMessageModal(manager, user)}
        />
      </>
    )
  }

  const renderOwnActions = () => {
    if (!user.isSelf) return null

    return (
      <>
        <SecondaryButton
          label="Edit Profile"
          icon="pencil"
          onClick={() => spawnUserEditModal(manager, user)}
        />
      </>
    )
  }

  return (
    <Container>
      <Overlay>
        <div className="background">
          <Background user={user} />
        </div>
        <div className="foreground">
          <AvatarSpace />
          <Content>
            <div className="stats">
              <Stats items={getUserStats(user)} />
            </div>
            <ButtonList horizontal>
              {renderAnonymousActions()}
              {renderOwnActions()}
            </ButtonList>
          </Content>
        </div>
      </Overlay>
    </Container>
  )
})
