import { observer } from "mobx-react"
import React from "react"
import { ImageBackground } from "../../../../common/ui/image/components/ImageBackground"
import { UserModel } from "../../models/User"

export interface BackgroundProps {
  user: UserModel
}

export const Background = observer(function Background(props: BackgroundProps) {
  const { user } = props
  const { coverAvatar, avatar, hash } = user

  const reference = coverAvatar || avatar
  const blur = coverAvatar ? 0 : 8
  const hasCover = !!coverAvatar

  return (
    <ImageBackground
      isClear={hasCover}
      hasGradient={hasCover}
      reference={reference}
      hash={hash}
      blur={blur}
    />
  )
})
