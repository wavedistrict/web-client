import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { getShadow, getSpacing } from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { styled } from "../../../theme/themes"
import {
  USER_SIDEBAR_MODULE_SPACING,
  USER_SIDEBAR_PADDING,
  USER_SIDEBAR_WIDTH,
} from "../../constants"
import { UserModel } from "../../models/User"
import { UserAvatar } from "../UserAvatar"
import { About } from "./About"
import { FollowerList } from "./FollowerList"
import { NowListening } from "./NowPlaying"
import { PrimaryInfo } from "./PrimaryInfo"
import { RoleList } from "./RoleList"

export interface SidebarProps {
  user: UserModel
}

const Container = styled.div`
  position: relative;
  margin-top: -103px;

  padding: ${getSpacing(USER_SIDEBAR_PADDING)}px;
  max-width: ${(props) => {
    const spacing = props.theme.spacings[USER_SIDEBAR_PADDING]
    return USER_SIDEBAR_WIDTH + spacing * 2
  }}px;

  box-sizing: border-box;
  flex-shrink: 0;

  > .avatar {
    ${size(USER_SIDEBAR_WIDTH)}
    ${imageContainer(3)}

    box-shadow: ${getShadow("light")};
    margin-bottom: ${getSpacing(USER_SIDEBAR_MODULE_SPACING)}px;
  }
`

export const Sidebar = observer(function Sidebar(props: SidebarProps) {
  const { user } = props

  return (
    <Container>
      <div className="avatar">
        <UserAvatar user={user} id="view" lightbox />
      </div>
      <PrimaryInfo user={user} />
      <RoleList user={user} />
      <NowListening user={user} />
      <About user={user} />
      <FollowerList user={user} />
    </Container>
  )
})
