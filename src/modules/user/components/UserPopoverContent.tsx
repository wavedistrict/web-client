import React from "react"
import { useObserver } from "mobx-react-lite"
import { cover, size } from "polished"
import { RouteLink } from "../../../common/routing/components/RouteLink"
import { ImageBackground } from "../../../common/ui/image/components/ImageBackground"
import { routes } from "../../core/constants"
import { RenderPopoverContentProps } from "../../popover/types/PopoverData"
import { spawnSettingsModal } from "../../settings/actions/spawnSettingsModal"
import { getFontSize, getFontWeight, getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { UserModel } from "../models/User"
import { UserAvatar } from "./UserAvatar"
import { UserPopoverListButton } from "./UserPopoverListButton"
import { useStores } from "../../../common/state/hooks/useStores"
import { useManager } from "../../../common/state/hooks/useManager"

interface UserPopoverContentProps extends RenderPopoverContentProps {
  user: UserModel
}

const Container = styled.div`
  width: 380px;
`

const Hero = styled.div`
  position: relative;
  height: 115px;
`

const Background = styled.div`
  ${size("100%")}
`

const Foreground = styled.div`
  ${cover()}

  display: flex;
  align-items: flex-end;
  padding: ${getSpacing("4")}px;
`

const AvatarPlaceholder = styled.div`
  width: 60px;
`

const ForegroundContent = styled.div`
  margin-left: ${getSpacing("4")}px;
  display: flex;
  flex-grow: 1;

  align-items: flex-end;
  justify-content: space-between;

  font-size: ${getFontSize("2")}px;
  font-weight: ${getFontWeight("bold")};
`

const Content = styled.div`
  padding: ${getSpacing("4")}px;
`

const Avatar = styled(RouteLink)`
  ${size(60)}

  display: block;
  margin-top: -60px;

  border-radius: 100%;
  overflow: hidden;
`

const Body = styled.div`
  margin-top: ${getSpacing("4")}px;
`

export function UserPopoverContent(props: UserPopoverContentProps) {
  const manager = useManager()

  const { authStore, routingStore } = useStores()
  const { close, user } = props

  const reference = useObserver(() => user.coverAvatar || user.avatar)
  const isClear = useObserver(() => !!user.coverAvatar)
  const blur = isClear ? 0 : 8

  const background = (
    <ImageBackground
      isClear={isClear}
      reference={reference}
      hash={user.hash}
      blur={blur}
      hasGradient
    />
  )

  const actions = (
    <>
      <UserPopoverListButton
        stretch
        label="View profile"
        icon="personFilled"
        onClick={() => {
          routingStore.push(user.clientLink)
          close()
        }}
      />
      <UserPopoverListButton
        stretch
        label="Library"
        icon="trackFilled"
        onClick={() => {
          routingStore.push(routes.library("tracks"))
          close()
        }}
      />
      <UserPopoverListButton
        stretch
        label="Settings"
        icon="cog"
        onClick={() => {
          spawnSettingsModal(manager)
          close()
        }}
      />
      <UserPopoverListButton
        stretch
        label="Sign out"
        icon="exit"
        onClick={() => {
          authStore.logout()
          close()
        }}
      />
    </>
  )

  return (
    <Container>
      <Hero>
        <Background>{background}</Background>
        <Foreground>
          <AvatarPlaceholder />
          <ForegroundContent>
            <RouteLink onClick={close} to={user.clientLink}>
              {user.data.displayName}
            </RouteLink>
          </ForegroundContent>
        </Foreground>
      </Hero>
      <Content>
        <Avatar onClick={close} to={user.clientLink}>
          <UserAvatar id="popoverContent" user={user} />
        </Avatar>
        <Body>{actions}</Body>
      </Content>
    </Container>
  )
}
