import React from "react"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { renderWindowList } from "../../../common/ui/list/helpers/renderWindowList"
import { ScrollerContext } from "../../../common/ui/scroll/components/ScrollerContext"
import { UserModel } from "../models/User"
import { UserGridItem } from "./UserGridItem"
import { styled } from "../../theme/themes"
import { getSpacing } from "../../theme/helpers"

export interface UserGridProps {
  list: FetcherList<UserModel>
}

export const SMALL_USER_GRID_BREAKPOINT = "350px"
export const MEDIUM_USER_GRID_BREAKPOINT = "500px"
export const BIG_USER_GRID_BREAKPOINT = "850px"

const Container = styled.ul`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: ${getSpacing("6")}px;

  @media (min-width: ${SMALL_USER_GRID_BREAKPOINT}) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (min-width: ${MEDIUM_USER_GRID_BREAKPOINT}) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media (min-width: ${BIG_USER_GRID_BREAKPOINT}) {
    grid-template-columns: repeat(5, 1fr);
  }
`

export function UserGrid(props: UserGridProps) {
  return renderWindowList({
    list: props.list,
    render: ({ onScroll, items }) => (
      <ScrollerContext onScroll={onScroll}>
        {() => (
          <Container>
            {items.map((item) => {
              return <UserGridItem key={item.data.id} user={item} />
            })}
          </Container>
        )}
      </ScrollerContext>
    ),
  })
}
