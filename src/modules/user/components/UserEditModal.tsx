import React from "react"
import { FormField } from "../../../common/ui/form/components/FormField"
import { FormLayout } from "../../../common/ui/form/components/FormLayout"
import { renderControlledImageInput } from "../../../common/ui/input/components/ControlledImageInput"
import { renderControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { FormModal } from "../../modal/components/FormModal"
import { useModal } from "../../modal/hooks/useModal"
import { submitUserEdit } from "../actions/submitUserEdit"
import { UserEditFormManager } from "../helpers/createUserEditForm"
import { useManager } from "../../../common/state/hooks/useManager"

export interface UserEditModalProps {
  userId: number
  form: UserEditFormManager
}

export function UserEditModal(props: UserEditModalProps) {
  const { form, userId } = props
  const { fields } = form

  const { dismiss } = useModal()
  const manager = useManager()

  async function onSubmit() {
    await submitUserEdit(manager, form, userId)
    dismiss()
  }

  return (
    <FormModal
      title="Edit profile"
      variants={["wide"]}
      onSubmit={onSubmit}
      form={form}
    >
      <FormLayout>
        <FormLayout.Header>
          <FormField
            label="Cover"
            controller={fields.coverAvatar}
            renderInput={renderControlledImageInput()}
          />
        </FormLayout.Header>
        <FormLayout.Sidebar>
          <FormField
            label="Avatar"
            controller={fields.avatar}
            renderInput={renderControlledImageInput({
              isSquare: true,
            })}
          />
        </FormLayout.Sidebar>
        <FormLayout.Grid>
          <FormLayout.GridCell size="full">
            <FormField
              label="Display name"
              controller={fields.displayName}
              renderInput={renderControlledTextInput({ autoFocus: true })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Bio"
              controller={fields.bio}
              renderInput={renderControlledTextInput({ multiline: true })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="half">
            <FormField
              label="First name"
              controller={fields.firstName}
              renderInput={renderControlledTextInput()}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="half">
            <FormField
              label="Last name"
              controller={fields.lastName}
              renderInput={renderControlledTextInput()}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="half">
            <FormField
              label="Country"
              controller={fields.country}
              renderInput={renderControlledTextInput()}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="half">
            <FormField
              label="City"
              controller={fields.city}
              renderInput={renderControlledTextInput()}
            />
          </FormLayout.GridCell>
        </FormLayout.Grid>
      </FormLayout>
    </FormModal>
  )
}
