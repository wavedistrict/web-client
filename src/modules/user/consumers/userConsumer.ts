import { FetcherStoreConsumer } from "../../realtime/classes/FetcherStoreConsumer"
import { UserModel } from "../models/User"
import { UserData } from "../types/UserData"
import { Manager } from "../../../common/state/types/Manager"

class UserConsumer extends FetcherStoreConsumer<UserData, UserModel> {
  protected getTargetListeners() {
    return {
      "add-user-follow": this.handleAddFollow,
      "remove-user-follow": this.handleRemoveFollow,
    }
  }

  private handleAddFollow(user: UserModel) {
    user.data.stats.followers += 1
  }

  private handleRemoveFollow(user: UserModel) {
    user.data.stats.followers -= 1
  }
}

export const userConsumer = (manager: Manager) =>
  new UserConsumer(manager, "user", "userStore")
