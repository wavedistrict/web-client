import { bind } from "../../../common/lang/function/helpers/bind"
import { SocketMessage } from "../../../common/network/types"
import { SocketMessageConsumer } from "../../realtime/classes/SocketMessageConsumer"
import { UserPresence, UserPresenceMap } from "../types/UserPresence"
import { Manager } from "../../../common/state/types/Manager"

class UserPresenceConsumer extends SocketMessageConsumer {
  protected getListeners() {
    return {
      presence: this.handlePresence,
      "all-presences": this.handleAllPresence,
    }
  }

  @bind
  private handlePresence(message: SocketMessage) {
    const { userPresenceStore } = this.manager.stores

    const presence = message.presence as UserPresence
    const userId = message.user as number

    userPresenceStore.assign({ [userId]: presence })
  }

  @bind
  private handleAllPresence(message: SocketMessage) {
    const { userPresenceStore } = this.manager.stores

    const presences = message.presences as {
      presence: UserPresence
      userId: number
    }[]
    const mappedPresence: UserPresenceMap = {}

    for (const { userId, presence } of presences) {
      mappedPresence[userId] = presence
    }

    userPresenceStore.assign(mappedPresence)
  }
}

export const userPresenceConsumer = (manager: Manager) =>
  new UserPresenceConsumer(manager)
