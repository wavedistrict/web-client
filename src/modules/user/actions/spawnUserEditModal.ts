import React from "react"
import { UserEditModal } from "../components/UserEditModal"
import { createUserEditForm } from "../helpers/createUserEditForm"
import { UserModel } from "../models/User"
import { Manager } from "../../../common/state/types/Manager"

export const spawnUserEditModal = (manager: Manager, user: UserModel) => {
  const { modalStore } = manager.stores

  const { id } = user.data
  const form = createUserEditForm(user)

  modalStore.spawn({
    key: `edit-user-${id}`,
    render: () => React.createElement(UserEditModal, { form, userId: id }),
  })
}
