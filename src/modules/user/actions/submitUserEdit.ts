import { UserEditFormManager } from "../helpers/createUserEditForm"
import { UserData } from "../types/UserData"
import { Manager } from "../../../common/state/types/Manager"

export const submitUserEdit = async (
  manager: Manager,
  form: UserEditFormManager,
  userId: number,
) => {
  const { userStore } = manager.stores

  const response = await form.submit<UserData>(`/users/${userId}`, "patch")
  userStore.addPrefetched(response.data)
}
