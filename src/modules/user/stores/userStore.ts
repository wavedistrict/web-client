import { action } from "mobx"
import {
  FetcherStore,
  createFetcherStoreFactory,
} from "../../../common/state/classes/FetcherStore"
import { UserModel } from "../models/User"

export class UserStore extends FetcherStore<UserModel> {
  @action
  public fetchByUsernameOrId(id: number | string) {
    const source = `/users/${id}`

    const getIndex = (item: UserModel) => ({
      newId: item.data.id,
      index: item.data.username,
    })

    return this.fetch(source, id, getIndex)
  }

  @action
  public addPrefetched(data: UserModel["data"]) {
    const source = `/users/${data.id}`
    const index = data.username

    return this.prepopulate(source, data.id, data, index)
  }
}

export const userStore = createFetcherStoreFactory(UserStore, UserModel)
