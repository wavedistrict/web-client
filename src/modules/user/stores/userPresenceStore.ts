import { action, observable } from "mobx"
import { UserPresence, UserPresenceMap } from "../types/UserPresence"
import {
  createStoreFactory,
  InitializableStore,
} from "../../../common/state/classes/InitializableStore"

const defaultPresence: UserPresence = {
  status: "offline",
  player: undefined,
}

export class UserPresenceStore extends InitializableStore {
  @observable private presences = new Map<number, UserPresence>()

  public init() {}

  @action
  public assign(presences: UserPresenceMap) {
    for (const [userId, presence] of Object.entries(presences)) {
      this.presences.set(Number(userId), presence)
    }
  }

  public getPresenceByUserId(userId: number) {
    return this.presences.get(userId) || defaultPresence
  }
}

export const userPresenceStore = createStoreFactory(UserPresenceStore)
