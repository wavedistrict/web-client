import { bind } from "../../../common/lang/function/helpers/bind"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { CollectionModel } from "../../collection/models/Collection"
import { TrackModel } from "../../track/models/Track"
import { UserModel } from "../../user/models/User"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

export class ExploreStore extends InitializableStore {
  public tracks!: FetcherList<TrackModel>
  public users!: FetcherList<UserModel>
  public collections!: FetcherList<CollectionModel>

  @bind
  private handleCreateTrack(track: TrackModel) {
    this.tracks.addPending(track.data)
  }

  @bind
  private handleCreateUser(user: UserModel) {
    this.users.addPending(user.data)
  }

  @bind
  private handleCreateCollection(collection: CollectionModel) {
    this.collections.addPending(collection.data)
  }

  public async init() {
    const { trackStore, userStore, collectionStore } = this.manager.stores

    this.tracks = new FetcherList(
      { store: trackStore, path: "/tracks" },
      this.manager,
    )

    this.users = new FetcherList(
      {
        store: userStore,
        path: "/users",
        limit: 10,
      },
      this.manager,
    )

    this.collections = new FetcherList(
      {
        store: collectionStore,
        path: "/collections",
      },
      this.manager,
    )

    /*
    trackConsumer.on("create", this.handleCreateTrack)
    userConsumer.on("create", this.handleCreateUser)
    collectionConsumer.on("create", this.handleCreateCollection)*/
  }
}

export const exploreStore = createStoreFactory(ExploreStore)
