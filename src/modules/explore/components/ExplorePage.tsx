import React from "react"
import { LinkedTabItem } from "../../../common/ui/navigation/components/LinkedTabs"
import { CollectionList } from "../../collection/components/CollectionList"
import { PageHeader } from "../../core/components/PageHeader"
import { routes } from "../../core/constants"
import { TrackList } from "../../track/components/TrackList"
import { UserGrid } from "../../user/components/UserGrid"
import { useStores } from "../../../common/state/hooks/useStores"
import { Route, useRouter } from "../../../common/routing/hooks/useRouter"
import { useRedirect } from "../../../common/routing/hooks/useRedirect"
import { useMeta } from "../../../common/ui/meta/hooks/useMeta"

export function ExplorePage() {
  const { exploreStore } = useStores()

  const tabs: LinkedTabItem[] = [
    {
      to: routes.explore("tracks"),
      icon: "trackFilled",
      label: "Tracks",
    },
    {
      to: routes.explore("users"),
      icon: "personFilled",
      label: "Users",
    },
    {
      to: routes.explore("collections"),
      icon: "collectionFilled",
      label: "Collections",
    },
  ]

  const routeList: Route[] = [
    {
      name: "tracks",
      pattern: routes.explore("tracks"),
      render: () => (
        <TrackList
          name="Explore"
          list={exploreStore.tracks}
          clientLink={routes.explore("tracks")}
        />
      ),
    },
    {
      name: "users",
      pattern: routes.explore("users"),
      render: () => <UserGrid list={exploreStore.users} />,
    },
    {
      name: "collections",
      pattern: routes.explore("collections"),
      render: () => <CollectionList list={exploreStore.collections} />,
    },
  ]

  useMeta({
    title: "Explore",
    description:
      "Discover newest tracks, users, and collections on WaveDistrict",
  })

  const [renderRoute] = useRouter(routeList)
  useRedirect("/explore", routes.explore("tracks"))

  return (
    <>
      <PageHeader title="Explore" icon="explore" tabs={tabs} />
      {renderRoute()}
    </>
  )
}
