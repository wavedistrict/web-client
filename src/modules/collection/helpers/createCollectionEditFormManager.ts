import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { ArrayFieldController } from "../../../common/ui/form/controllers/ArrayFieldController"
import { ImageFieldController } from "../../../common/ui/form/controllers/ImageFieldController"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { validateStringLength } from "../../../common/ui/form/validators/string/validateStringLength"
import { CollectionModel } from "../models/Collection"

export const createCollectionEditFormManager = (
  collection: CollectionModel,
) => {
  const { title, description } = collection.data
  return new FormManager({
    title: new TextFieldController({
      value: title,
      isRequired: true,
      validators: [validateStringLength({ max: 128 })],
    }),
    description: new TextFieldController({
      value: description,
      validators: [validateStringLength({ max: 2048 })],
    }),
    artwork: new ImageFieldController({
      type: "artwork",
      reference: collection.artwork,
    }),
    tracks: new ArrayFieldController<number>({
      value: collection.data.items.trackIds,
    }),
  })
}

export type CollectionEditFormManager = ReturnType<
  typeof createCollectionEditFormManager
>
