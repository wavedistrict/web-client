import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { ArrayFieldController } from "../../../common/ui/form/controllers/ArrayFieldController"
import { SelectFieldController } from "../../../common/ui/form/controllers/SelectFieldController"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { getDropdownOptions } from "../../../common/ui/form/helpers/getDropdownOptions"
import { getSlugFieldOptions } from "../../../common/ui/form/helpers/getSlugFieldOptions"
import { validateStringFromEndpoint } from "../../../common/ui/form/validators/string/validateStringFromEndpoint"
import { validateStringLength } from "../../../common/ui/form/validators/string/validateStringLength"
import { TrackModel } from "../../track/models/Track"
import { Manager } from "../../../common/state/types/Manager"

export const createCollectionFormManager = (
  manager: Manager,
  track: TrackModel,
) => {
  const { configStore } = manager.stores

  const typeOptions = [
    ...getDropdownOptions(configStore.config.collectionTypes, "id", "name"),
    { key: "other", label: "Other" },
  ]

  const slug = getSlugFieldOptions()

  return new FormManager({
    title: new TextFieldController({
      isRequired: true,
      validators: [validateStringLength({ max: 128 })],
    }),
    slug: new TextFieldController({
      isRequired: true,
      validators: [
        ...slug.validators,
        validateStringFromEndpoint({
          endpoint: "/collections/test",
          validator: "slug",
          message: "You have another collection with this slug",
        }),
      ],
      modifiers: slug.modifiers,
    }),
    type: new SelectFieldController({
      value: 1,
      options: typeOptions,
    }),
    tracks: new ArrayFieldController<number>({
      value: [track.data.id],
    }),
  })
}

export type CollectionFormManager = ReturnType<
  typeof createCollectionFormManager
>
