import { pluralize } from "../../../common/lang/string/helpers/pluralize"
import { getImagePath } from "../../../common/ui/meta/helpers/getImagePath"
import { getSafeString } from "../../../common/ui/meta/helpers/getSafeString"
import { CollectionModel } from "../models/Collection"

export const getCollectionMeta = (collection: CollectionModel) => {
  const { title, description, items } = collection.data

  const trackCount = items.trackIds.length

  const artwork = getImagePath(collection.artwork)
  const safeDescription =
    getSafeString(description) ||
    `Listen to ${pluralize("track", trackCount)} from this collection`

  return {
    title,
    image: artwork,
    type: "music.playlist",
    url: collection.clientLink,
    description: safeDescription,
  }
}
