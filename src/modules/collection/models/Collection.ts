import { computed, observable } from "mobx"
import { hashString } from "../../../common/lang/string/helpers/hashString"
import { Model } from "../../../common/state/classes/Model"
import { routes } from "../../core/constants"
import { CollectionData } from "../types"

export class CollectionModel extends Model<CollectionData> {
  public hash = hashString(`${this.data.user.username}_${this.data.slug}`)

  public clientLink = routes.collection(this.data.user.username, this.data.slug)
  public apiPath = `/collections/${this.data.id}`

  public user = this.manager.stores.userStore.getModelFromData(this.data.user)
  @observable public artwork = this.manager.stores.imageMediaStore.assign(
    this.data.artwork,
  )

  public getListMap() {
    const { trackStore, commentStore } = this.manager.stores

    return {
      tracks: {
        store: trackStore,
        path: `/collections/${this.data.id}/tracks`,
        items: this.data.items.tracks,
        params: {
          ascending: true,
        },
      },
      comments: {
        store: commentStore,
        path: `/collections/${this.data.id}/comments`,
      },
    }
  }

  /** Cannot be lazy loaded because it is used by computed values */
  public tracks = this.getOrCreateList("tracks")

  public get comments() {
    return this.getOrCreateList("comments")
  }

  @computed
  public get isActive() {
    const { queueStore } = this.manager.stores
    return !!(queueStore.list && queueStore.list.path === this.tracks.path)
  }

  @computed
  public get isOwn() {
    const { authStore } = this.manager.stores
    return authStore.user && authStore.user.data.id === this.user.data.id
  }

  @computed
  public get isPlaying() {
    const { playerStore } = this.manager.stores
    return this.isActive && playerStore.isPlaying
  }

  public get firstTrack() {
    return this.tracks.models[0]
  }

  @computed
  public get isPlayable() {
    return this.firstTrack && this.firstTrack.isPlayable
  }

  protected onUpdate(data: CollectionData) {
    const { imageMediaStore } = this.manager.stores
    this.artwork = imageMediaStore.assign(data.artwork)
  }
}
