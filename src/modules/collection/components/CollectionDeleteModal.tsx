import React from "react"
import { apiServer } from "../../../common/network"
import { handleInteractionError } from "../../../common/network/helpers/handleInteractionError"
import { ConfirmationModal } from "../../modal/components/ConfirmationModal"
import { CollectionModel } from "../models/Collection"
import { useManager } from "../../../common/state/hooks/useManager"

interface CollectionDeleteModalProps {
  collection: CollectionModel
}

export function CollectionDeleteModal(props: CollectionDeleteModalProps) {
  const { collection } = props
  const manager = useManager()

  async function action() {
    try {
      await apiServer.delete(collection.apiPath)
    } catch (error) {
      handleInteractionError(manager, error)
    }
  }

  return (
    <ConfirmationModal
      title="Delete collection"
      actionButtonLabel="Delete"
      action={action}
    >
      <p>
        Are you sure you want to delete the collection{" "}
        <strong>"{collection.data.title}"</strong>?
      </p>
    </ConfirmationModal>
  )
}
