import { createImageComponent } from "../../../common/ui/image/helpers/createImageComponent"
import { CollectionModel } from "../models/Collection"

export const CollectionArtwork = createImageComponent<{
  collection: CollectionModel
}>((props) => {
  const { collection, id } = props

  return {
    id: `Collection:${id}`,
    reference: collection.artwork,
    placeholder: {
      hash: collection.hash,
      icon: "collectionFilled",
    },
  }
})
