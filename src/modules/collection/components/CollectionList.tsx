import React from "react"
import { CellMeasurerCache } from "react-virtualized"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { createWindowListComponent } from "../../../common/ui/list/helpers/createWindowListComponent"
import { renderWindowList } from "../../../common/ui/list/helpers/renderWindowList"
import { wrapDynamicHeightRow } from "../../../common/ui/list/helpers/wrapDynamicHeightRow"
import { CollectionModel } from "../models/Collection"
import {
  CollectionListItem,
  COLLECTION_LIST_ITEM_SIZE,
  COLLECTION_LIST_ITEM_SPACING,
} from "./CollectionListItem/CollectionListItem"

export interface CollectionListProps {
  list: FetcherList<CollectionModel>
}

const cache = new CellMeasurerCache({
  minHeight: COLLECTION_LIST_ITEM_SIZE,
  fixedWidth: true,
})

const List = createWindowListComponent<CollectionModel>({
  rowHeight: cache.rowHeight,
  renderItem: wrapDynamicHeightRow(
    ({ item, style }) => (
      <div style={{ ...style, paddingBottom: COLLECTION_LIST_ITEM_SPACING }}>
        <CollectionListItem collection={item} />
      </div>
    ),
    cache,
  ),
  cache,
})

export class CollectionList extends React.Component<CollectionListProps> {
  public render() {
    return renderWindowList({
      state: {
        icon: "collectionFilled",
        message: "No collections yet",
      },
      render: (props) => <List {...props} />,
      list: this.props.list,
    })
  }
}
