import React from "react"
import { FormField } from "../../../common/ui/form/components/FormField"
import { FormLayout } from "../../../common/ui/form/components/FormLayout"
import { renderControlledDropdownInput } from "../../../common/ui/input/components/ControlledDropdownInput"
import { renderControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { FormModal } from "../../modal/components/FormModal"
import { useModal } from "../../modal/hooks/useModal"
import { CollectionFormManager } from "../helpers/createCollectionFormManager"

export interface CollectionCreateModalProps {
  form: CollectionFormManager
}

export function CollectionCreateModal(props: CollectionCreateModalProps) {
  const { form } = props
  const { fields } = form

  const { dismiss } = useModal()

  async function onSubmit() {
    await form.submit("/collections", "post")
    dismiss()
  }

  return (
    <FormModal title="Create collection" onSubmit={onSubmit} form={form}>
      <FormLayout>
        <FormLayout.Grid>
          <FormLayout.GridCell size="full">
            <FormField
              label="Title"
              controller={fields.title}
              renderInput={renderControlledTextInput({
                autoFocus: true,
              })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="half">
            <FormField
              label="Slug"
              controller={fields.slug}
              renderInput={renderControlledTextInput()}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="half">
            <FormField
              label="Type"
              controller={fields.type}
              renderInput={renderControlledDropdownInput()}
            />
          </FormLayout.GridCell>
        </FormLayout.Grid>
      </FormLayout>
    </FormModal>
  )
}
