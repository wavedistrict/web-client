import React from "react"
import { PlayButton } from "../../player/components/PlayButton"
import { CollectionModel } from "../models/Collection"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

interface CollectionPlayButtonProps {
  collection: CollectionModel
  size?: "big" | "small"
}

export function CollectionPlayButton(props: CollectionPlayButtonProps) {
  const { collection, size } = props
  const { playerStore, queueStore } = useStores()

  const handleClick = () => {
    if (collection.isActive) {
      return playerStore.toggle()
    }

    playerStore.setTrack(collection.firstTrack)

    queueStore.setQueueData({
      clientLink: collection.clientLink,
      list: collection.tracks,
      name: collection.data.title,
    })

    playerStore.play()
  }

  const handleHover = () => {
    const shouldPreload =
      collection.firstTrack &&
      collection.firstTrack.isPlayable &&
      !collection.isActive

    if (shouldPreload) {
      playerStore.preload(collection.firstTrack)
    }
  }

  return useObserver(() => (
    <PlayButton
      active={collection.isActive}
      playing={collection.isPlaying}
      disabled={!collection.isPlayable}
      onClick={handleClick}
      onMouseOver={handleHover}
      size={size}
    />
  ))
}
