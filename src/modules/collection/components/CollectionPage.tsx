import React from "react"
import { size } from "polished"
import { observer } from "mobx-react"
import { useObserver } from "mobx-react-lite"
import { pluralize } from "../../../common/lang/string/helpers/pluralize"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { LinkedTabs } from "../../../common/ui/navigation/components/LinkedTabs"
import { CommentForm } from "../../commenting/components/CommentForm"
import { CommentList } from "../../commenting/components/CommentList"
import { Description } from "../../core/components/Description"
import { Section } from "../../core/components/Section"
import { routes } from "../../core/constants"
import { FavoriteButton } from "../../favoriting/components/FavoriteButton"
import { TrackList } from "../../track/components/TrackList"
import { UserSummary } from "../../user/components/UserSummary"
import { spawnCollectionEditModal } from "../actions/spawnCollectionEditModal"
import { getCollectionMeta } from "../helpers/getCollectionMeta"
import { CollectionModel } from "../models/Collection"
import { CollectionArtwork } from "./CollectionArtwork"
import { CollectionPlayButton } from "./CollectionPlayButton"
import { useStores } from "../../../common/state/hooks/useStores"
import { FetcherView } from "../../../common/state/components/FetcherView"
import { Route, useRouter } from "../../../common/routing/hooks/useRouter"
import { useRedirect } from "../../../common/routing/hooks/useRedirect"
import { styled } from "../../theme/themes"
import {
  getSpacing,
  getShadow,
  getFontSize,
  getFontWeight,
} from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { ButtonList } from "../../../common/ui/button/components/ButtonList"
import { useManager } from "../../../common/state/hooks/useManager"
import { useMeta } from "../../../common/ui/meta/hooks/useMeta"

const SIDEBAR_WIDTH = "320px"
const MARGIN_TOP = "5"

interface CollectionViewProps {
  collection: CollectionModel
}

export interface CollectionPageProps {
  username: string
  slug: string
}

const Container = styled.div`
  display: flex;
  padding-top: ${getSpacing("6")}px;
`
const Content = styled.div`
  flex-grow: 1;
  margin-left: ${getSpacing("6")}px;
`
const Sidebar = styled.div`
  ${size("auto", SIDEBAR_WIDTH)}
`
const Tabs = styled.div`
  margin-bottom: ${getSpacing("4")}px;
`

// Because `ButtonList horizontal` doesn't have margin bottom
const Actions = styled(ButtonList)`
  margin-bottom: ${getSpacing("4")}px;
`

const Artwork = styled.div`
  ${size(SIDEBAR_WIDTH)};
  ${imageContainer("3px")};
  box-shadow: ${getShadow("light")};
  flex-shrink: 0;
`
const Header = styled.header`
  margin-top: ${getSpacing(MARGIN_TOP)}px;
  display: flex;
  align-items: center;
`
const PlayButton = styled.div`
  margin-right: ${getSpacing("4")}px;
`
const Title = styled.div`
  margin-top: -2px;
  font-size: ${getFontSize("1")};
  font-weight: ${getFontWeight("bold")};
`
const About = styled.div`
  margin-top: ${getSpacing(MARGIN_TOP)}px;
`
const Summary = styled.div`
  margin-top: ${getSpacing(MARGIN_TOP)}px;
`
const List = styled.div`
  margin-top: ${getSpacing(MARGIN_TOP)}px;
`
const PrimaryInfo = styled.div`
  /*  */
`
const Type = styled.div`
  /*  */
`

const CollectionView = observer(function CollectionView(
  props: CollectionViewProps,
) {
  const { collection } = props
  const manager = useManager()

  function renderTabs() {
    const { slug, items, stats } = collection.data
    const { username } = collection.user.data

    const tabs = [
      {
        label: pluralize("Track", items.trackIds.length),
        to: routes.collection(username, slug, "tracks"),
      },
      {
        label: pluralize("Comment", stats.comments),
        to: routes.collection(username, slug, "comments"),
      },
    ]

    return (
      <Tabs>
        <LinkedTabs items={tabs} />
      </Tabs>
    )
  }

  function renderSidebar() {
    return (
      <Sidebar>
        {renderArtwork()}
        {renderHeader()}
        {renderAbout()}
        {renderUserSummary()}
      </Sidebar>
    )
  }

  function renderContent() {
    return (
      <Content>
        {renderActions()}
        {renderTabs()}
        <CollectionViewRouter collection={collection} />
      </Content>
    )
  }

  function renderActions() {
    const actions: JSX.Element[] = []

    if (collection.isOwn) {
      actions.unshift(
        <SecondaryButton
          key="edit"
          icon="pencil"
          onClick={() => spawnCollectionEditModal(manager, collection)}
        />,
      )
    } else {
      actions.unshift(
        <FavoriteButton
          key="favorite"
          label={`${collection.data.stats.favorites || "Favorite"}`}
          item={collection}
        />,
      )
    }

    return <Actions horizontal>{actions}</Actions>
  }

  function renderArtwork() {
    return (
      <Artwork>
        <CollectionArtwork id="view" collection={collection} lightbox />
      </Artwork>
    )
  }

  function renderHeader() {
    const { title, type } = collection.data
    return (
      <Header>
        <PlayButton>
          <CollectionPlayButton size="big" collection={collection} />
        </PlayButton>
        <PrimaryInfo>
          <Title>{title}</Title>
          <Type>{type}</Type>
        </PrimaryInfo>
      </Header>
    )
  }

  function renderDescription() {
    const { description } = collection.data

    return <Description text={description} />
  }

  function renderAbout() {
    if (!collection.data.description) return null

    return (
      <About>
        <Section title="About">{renderDescription()}</Section>
      </About>
    )
  }

  function renderUserSummary() {
    const { user } = collection

    return (
      <Summary>
        <Section title="Creator">
          <UserSummary user={user} />
        </Section>
      </Summary>
    )
  }

  useMeta(getCollectionMeta(collection))

  return (
    <Container>
      {renderSidebar()}
      {renderContent()}
    </Container>
  )
})

export function CollectionViewRouter(props: CollectionViewProps) {
  const { collection } = props

  const { slug, username } = useObserver(() => ({
    slug: collection.data.slug,
    username: collection.user.data.username,
  }))

  const routeList: Route[] = [
    {
      pattern: routes.collection(username, slug, "tracks"),
      name: "tracks",
      render: () => (
        <TrackList
          clientLink={collection.clientLink}
          name={collection.data.title}
          list={collection.tracks}
        />
      ),
    },
    {
      pattern: routes.collection(username, slug, "comments"),
      name: "comments",
      render: () => (
        <>
          <CommentForm path={collection.comments.path} />
          <List>
            <CommentList showContext={false} list={collection.comments} />
          </List>
        </>
      ),
    },
  ]

  const [renderRoute] = useRouter(routeList)
  useRedirect(
    `/@${username}/collections/${slug}`,
    routes.collection(username, slug),
  )

  return renderRoute()
}

export function CollectionPage(props: CollectionPageProps) {
  const { username, slug } = props
  const { collectionStore } = useStores()

  return (
    <FetcherView
      empty={{ icon: "collectionFilled", message: "Collection not found" }}
      fetcher={collectionStore.fetchByUsernameAndSlug(username, slug)}
    >
      {(model) => <CollectionView collection={model} />}
    </FetcherView>
  )
}
