import { observer } from "mobx-react"
import React, { useRef } from "react"
import { PrimaryButton } from "../../../../common/ui/button/components/PrimaryButton"
import { createVirtualizedListComponent } from "../../../../common/ui/list/helpers/createVirtualizedListComponent"
import { renderContainerList } from "../../../../common/ui/list/helpers/renderContainerList"
import { wrapRowInLi } from "../../../../common/ui/list/helpers/wrapRowInLi"
import { ListComponentProps } from "../../../../common/ui/list/types/ListComponentProps"
import { RenderPopoverContentProps } from "../../../popover/types/PopoverData"
import { getColor, getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { TrackModel } from "../../../track/models/Track"
import { UserModel } from "../../../user/models/User"
import { spawnCreateCollectionModal } from "../../actions/spawnCreateCollectionModal"
import { CollectionModel } from "../../models/Collection"
import {
  AddCollectionListItem,
  addCollectionListItemHeight,
} from "./AddCollectionListItem"
import { useManager } from "../../../../common/state/hooks/useManager"

interface AddCollectionPopoverProps {
  track: TrackModel
  user: UserModel
}

type ComponentType = React.ComponentType<ListComponentProps<CollectionModel>>

const Container = styled.div`
  width: 360px;
  max-width: 100vw;

  > .header {
    padding: ${getSpacing("4")}px;
    font-weight: 700;

    border-bottom: 1px solid ${getColor("divider")};
  }

  > .list {
    height: 340px;
    position: relative;
  }

  > .actions {
    border-top: 1px solid ${getColor("divider")};
    padding: ${getSpacing("4")}px;

    display: flex;
    flex-direction: column;
  }
`

export const AddCollectionPopover = observer(function AddCollectionPopover(
  props: AddCollectionPopoverProps & RenderPopoverContentProps,
) {
  const { track, close, user } = props

  const listRef = useRef<ComponentType>()
  const manager = useManager()

  if (!listRef.current) {
    listRef.current = createVirtualizedListComponent<CollectionModel>({
      rowHeight: addCollectionListItemHeight,
      renderItem: wrapRowInLi((item) => (
        <AddCollectionListItem track={track} collection={item} />
      )),
    })
  }

  const List = listRef.current

  return (
    <Container>
      <h1 className="header">Add to collection</h1>
      <div className="list">
        {renderContainerList({
          list: user.collections,
          render: (props) => <List {...props} />,
          state: {
            icon: "collectionFilled",
            message: "You have no collections",
          },
        })}
      </div>
      <div className="actions">
        <PrimaryButton
          label="Create new collection"
          icon="add"
          onClick={() => {
            spawnCreateCollectionModal(manager, track)
            close()
          }}
        />
      </div>
    </Container>
  )
})
