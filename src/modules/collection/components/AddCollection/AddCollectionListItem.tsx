import styled from "@emotion/styled"
import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { AsyncButton } from "../../../../common/ui/button/components/AsyncButton"
import { humanizeSeconds } from "../../../player/helpers/humanizeSeconds"
import { getFontColor, getFontWeight, getSpacing } from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { TrackModel } from "../../../track/models/Track"
import { addTrackToCollection } from "../../actions/addTrackToCollection"
import { removeTrackFromCollection } from "../../actions/removeTrackFromCollection"
import { CollectionModel } from "../../models/Collection"
import { CollectionArtwork } from "../CollectionArtwork"

export interface AddCollectionListItemProps {
  collection: CollectionModel
  track: TrackModel
}

const Container = styled.article`
    padding: ${getSpacing("4")}px;
  display: flex;

  align-items: center;

  > .artwork {
    ${size(36)};
    ${imageContainer(3)}

    margin-right: ${getSpacing("4")}px;
  }

  > .info {
    flex-grow: 1;
  }

  > .info > .title {
    font-weight: ${getFontWeight("bold")};
  }

  > .info > .count {
    margin-top: 2px;
    color: ${getFontColor("muted")};
  }
`

export const AddCollectionListItem = observer(function AddCollectionListItem(
  props: AddCollectionListItemProps,
) {
  const { collection, track } = props
  const { trackIds } = collection.data.items

  const isActive = trackIds.includes(track.data.id)
  const label = isActive ? "Remove" : "Add"

  const action = isActive
    ? () => removeTrackFromCollection(collection, track)
    : () => addTrackToCollection(collection, track)

  return (
    <Container>
      <div className="artwork">
        <CollectionArtwork id="addCollection" collection={collection} />
      </div>
      <div className="info">
        <h1 className="title">{collection.data.title}</h1>
        <h2 className="count">
          {trackIds.length} tracks, {humanizeSeconds(collection.data.duration)}
        </h2>
      </div>
      <div className="actions">
        <AsyncButton kind="secondary" outlined label={label} onClick={action} />
      </div>
    </Container>
  )
})

export const addCollectionListItemHeight = 74
