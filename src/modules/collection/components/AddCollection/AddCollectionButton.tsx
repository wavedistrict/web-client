import React from "react"
import { SecondaryButton } from "../../../../common/ui/button/components/SecondaryButton"
import { requireAuth } from "../../../auth/helpers/requireAuth"
import {
  PopoverChildrenFunctionProps,
  PopoverTrigger,
} from "../../../popover/components/PopoverTrigger"
import { RenderPopoverContentProps } from "../../../popover/types/PopoverData"
import { TrackModel } from "../../../track/models/Track"
import { AddCollectionPopover } from "./AddCollectionPopover"
import { PrimaryButtonVariants } from "../../../../common/ui/button/components/PrimaryButton"
import { useStores } from "../../../../common/state/hooks/useStores"
import { useManager } from "../../../../common/state/hooks/useManager"

export type AddCollectionButtonProps = PrimaryButtonVariants & {
  track: TrackModel
}

export function AddCollectionButton(props: AddCollectionButtonProps) {
  const { authStore } = useStores()
  const manager = useManager()
  const { track, ...rest } = props

  const renderPopoverContent = (props: RenderPopoverContentProps) => {
    return (
      <AddCollectionPopover user={authStore.user!} track={track} {...props} />
    )
  }

  const renderTriggerContent = (props: PopoverChildrenFunctionProps) => {
    const onClick = requireAuth(props.toggle)

    return (
      <SecondaryButton
        ref={props.ref}
        title="Add to Collection"
        icon="addToList"
        onClick={() => onClick(manager)}
        active={props.isActive}
        {...rest}
      />
    )
  }

  return (
    <PopoverTrigger placement="left-end">
      {renderTriggerContent}
      {renderPopoverContent}
    </PopoverTrigger>
  )
}
