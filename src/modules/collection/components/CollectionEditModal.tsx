import React from "react"
import { FormField } from "../../../common/ui/form/components/FormField"
import { FormLayout } from "../../../common/ui/form/components/FormLayout"
import { renderControlledImageInput } from "../../../common/ui/input/components/ControlledImageInput"
import { renderControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { FormModal } from "../../modal/components/FormModal"
import { useModal } from "../../modal/hooks/useModal"
import { CollectionEditFormManager } from "../helpers/createCollectionEditFormManager"
import { CollectionModel } from "../models/Collection"

export interface CollectionEditModalProps {
  collection: CollectionModel
  form: CollectionEditFormManager
}

export function CollectionEditModal(props: CollectionEditModalProps) {
  const { form, collection } = props
  const { fields } = form

  const { dismiss } = useModal()

  async function onSubmit() {
    await form.submit(collection.apiPath, "patch")
    dismiss()
  }

  return (
    <FormModal
      title="Edit collection"
      variants={["wide"]}
      onSubmit={onSubmit}
      form={form}
    >
      <FormLayout>
        <FormLayout.Sidebar>
          <FormField
            label="Artwork"
            controller={fields.artwork}
            renderInput={renderControlledImageInput({
              isSquare: true,
            })}
          />
        </FormLayout.Sidebar>
        <FormLayout.Grid>
          <FormLayout.GridCell size="full">
            <FormField
              label="Title"
              controller={fields.title}
              renderInput={renderControlledTextInput({ autoFocus: true })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Description"
              controller={fields.description}
              renderInput={renderControlledTextInput({ multiline: true })}
            />
          </FormLayout.GridCell>
        </FormLayout.Grid>
      </FormLayout>
    </FormModal>
  )
}
