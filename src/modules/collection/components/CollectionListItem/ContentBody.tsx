import { observer } from "mobx-react"
import React from "react"
import { getColor } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { CollectionListItemProps } from "./CollectionListItem"
import { TrackListItem } from "./TrackListItem"

const Container = styled.div`
  border-top: solid 1px ${getColor("divider")};
`

export const ContentBody = observer(function ContentBody(
  props: CollectionListItemProps,
) {
  const { collection } = props

  const renderList = () => {
    const { models: tracks } = collection.tracks

    return tracks
      .slice(0, 5)
      .map((track) => (
        <TrackListItem
          key={track.data.id}
          track={track}
          collection={collection}
        />
      ))
  }

  return <Container>{renderList()}</Container>
})
