import css from "@emotion/css"
import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { RouteLink } from "../../../../common/routing/components/RouteLink"
import { getColor, getShadow, getSpacing } from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { styled } from "../../../theme/themes"
import { Theme } from "../../../theme/types/Theme"
import { CollectionModel } from "../../models/Collection"
import { CollectionArtwork } from "../CollectionArtwork"
import { ContentBody } from "./ContentBody"
import { ContentFooter } from "./ContentFooter"
import { ContentHeader } from "./ContentHeader"

export interface CollectionListItemProps {
  collection: CollectionModel
}

export const COLLECTION_LIST_ITEM_SIZE = 184
export const COLLECTION_LIST_ITEM_SPACING = 32

const Container = styled.article`
  display: flex;
  align-items: flex-start;

  > .content {
    border-radius: 3px;
    overflow: hidden;
    box-shadow: ${getShadow("light")};
  }

  > .content {
    background: ${getColor("primary")};
    margin-left: ${getSpacing("4")}px;
    flex: 1;
  }
`

const artworkStyle = (theme: Theme) => css`
  ${size(COLLECTION_LIST_ITEM_SIZE)}
  ${imageContainer()({ theme })};

  flex-shrink: 0;

  border-radius: 3px;
  overflow: hidden;
  box-shadow: ${theme.shadows.light};
`

export const CollectionListItem = observer(function CollectionListItem(
  props: CollectionListItemProps,
) {
  const { collection } = props

  return (
    <Container>
      <RouteLink css={artworkStyle} to={collection.clientLink}>
        <CollectionArtwork id="listItem" collection={collection} />
      </RouteLink>
      <div className="content">
        <ContentHeader collection={collection} />
        <ContentBody collection={collection} />
        <ContentFooter collection={collection} />
      </div>
    </Container>
  )
})
