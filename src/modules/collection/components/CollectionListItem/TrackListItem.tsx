import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { humanizeSeconds } from "../../../player/helpers/humanizeSeconds"
import { QueueData } from "../../../player/types/QueueData"
import {
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
} from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { styled } from "../../../theme/themes"
import { TrackArtwork } from "../../../track/components/TrackArtwork"
import { TrackList } from "../../../track/components/TrackList"
import { TrackPlayTrigger } from "../../../track/components/TrackPlayTrigger"
import { TrackModel } from "../../../track/models/Track"
import { CollectionModel } from "../../models/Collection"

export interface TrackListItemProps {
  track: TrackModel
  collection: CollectionModel
}

const Container = styled.div<{
  active: boolean
}>`
  padding: ${getSpacing("4")}px;

  display: flex;
  align-items: center;

  transition: ${getDuration("normal")} ease all;

  > .artwork {
    ${size(24)};
    ${imageContainer(3)};
  }

  > .title {
    margin-left: ${getSpacing("4")}px;

    font-size: ${getFontSize("1")}px;
    flex: 1;

    transition: ${getDuration("normal")} ease all;
  }

  > .duration {
    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("bold")};
  }

  &:hover {
    background: ${getTransparency("weakNegative")};
    cursor: pointer;
  }

  ${(props) =>
    props.active &&
    `
    > .title {
      color: ${props.theme.colors.accent};
    } 
  `}
`

export const TrackListItem = observer(function TrackListItem(
  props: TrackListItemProps,
) {
  const { collection, track } = props
  const { title, duration } = track.data

  const data: QueueData = {
    list: collection.tracks,
    name: collection.data.title,
    clientLink: collection.clientLink,
  }

  const isActive = track.isActive && collection.isActive

  return (
    <TrackList.Provider value={data}>
      <TrackPlayTrigger track={track}>
        {(props) => {
          return (
            <Container active={isActive} onClick={props.click}>
              <div className="artwork">
                <TrackArtwork id="collectionItem" track={track} />
              </div>
              <h1 className="title">{title}</h1>
              <h2 className="duration">{humanizeSeconds(duration)}</h2>
            </Container>
          )
        }}
      </TrackPlayTrigger>
    </TrackList.Provider>
  )
})
