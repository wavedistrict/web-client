import { observer } from "mobx-react"
import React from "react"
import { HumanizedDate } from "../../../core/components/HumanizedDate"
import { RouteLink } from "../../../../common/routing/components/RouteLink"
import {
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { CollectionPlayButton } from "../CollectionPlayButton"
import { CollectionListItemProps } from "./CollectionListItem"

const Container = styled.div`
  padding: ${getSpacing("4")}px;
  display: flex;
`

const PrimaryInfo = styled.div`
  flex: 1;
  margin-left: ${getSpacing("4")}px;

  display: flex;
  flex-direction: column;

  > .title {
    font-size: ${getFontSize("2")}px;
    font-weight: ${getFontWeight("bold")};
  }

  > .author {
    margin-top: ${getSpacing("1")}px;
    font-size: ${getFontSize("1")}px;

    color: ${getFontColor("muted")};
  }
`

const SecondaryInfo = styled.div`
  margin-left: ${getSpacing("4")}px;

  display: flex;
  flex-direction: column;
  align-items: flex-end;
  flex-shrink: 0;

  > .type {
    font-size: ${getFontSize("1")}px;
  }

  > .createdAt {
    margin-top: ${getSpacing("1")}px;
    font-size: ${getFontSize("1")}px;

    color: ${getFontColor("muted")};
  }
`

export const ContentHeader = observer(function ContentHeader(
  props: CollectionListItemProps,
) {
  const { collection } = props
  const { user } = collection

  const { title, type, createdAt } = collection.data
  const { displayName } = user.data

  return (
    <Container>
      <div>
        <CollectionPlayButton collection={collection} />
      </div>
      <PrimaryInfo>
        <RouteLink to={collection.clientLink} className="title">
          {title}
        </RouteLink>
        <RouteLink to={user.clientLink} className="author">
          {displayName}
        </RouteLink>
      </PrimaryInfo>
      <SecondaryInfo>
        <div className="type">{type}</div>
        <div className="createdAt">
          <HumanizedDate>{createdAt}</HumanizedDate>
        </div>
      </SecondaryInfo>
    </Container>
  )
})
