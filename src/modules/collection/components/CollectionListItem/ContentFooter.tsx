import { observer } from "mobx-react"
import React from "react"
import { SecondaryButton } from "../../../../common/ui/button/components/SecondaryButton"
import { StatItem, Stats } from "../../../core/components/Stats"
import { FavoriteButton } from "../../../favoriting/components/FavoriteButton"
import { humanizeSeconds } from "../../../player/helpers/humanizeSeconds"
import { getColor, getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { spawnCollectionDeleteModal } from "../../actions/spawnCollectionDeleteModal"
import { spawnCollectionEditModal } from "../../actions/spawnCollectionEditModal"
import { CollectionListItemProps } from "./CollectionListItem"
import { PrimaryButtonVariants } from "../../../../common/ui/button/components/PrimaryButton"
import { ButtonList } from "../../../../common/ui/button/components/ButtonList"
import { useManager } from "../../../../common/state/hooks/useManager"

const Container = styled.div`
  border-top: solid 1px ${getColor("divider")};
  padding: ${getSpacing("4")}px;

  display: flex;
  align-items: center;

  > .stats {
    flex: 1;
  }
`

const Actions = styled(ButtonList)``

export const ContentFooter = observer(function ContentFooter(
  props: CollectionListItemProps,
) {
  const manager = useManager()

  const { collection } = props
  const { stats, duration, items } = collection.data
  const tracks = items.trackIds

  const variants: PrimaryButtonVariants = {
    compact: true,
    outlined: true,
    noLabel: true,
  }

  const renderStats = () => {
    const items: StatItem[] = [
      {
        name: "Duration",
        icon: "wait",
        value: humanizeSeconds(duration),
      },
      {
        name: "Tracks",
        icon: "trackFilled",
        value: tracks.length,
      },
      {
        name: "Plays",
        icon: "play",
        value: stats.plays,
      },
      {
        name: "Comments",
        icon: "commentFilled",
        value: stats.comments,
      },
    ]

    if (collection.isOwn) {
      items.push({
        name: "Favorites",
        icon: "heartFilled",
        value: stats.favorites,
      })
    }

    return <Stats compact items={items} />
  }

  const renderAnonymousActions = () => {
    if (collection.isOwn) return null

    return (
      <FavoriteButton
        {...variants}
        noLabel={!stats.favorites}
        label={String(stats.favorites || "")}
        key="favorite"
        item={collection}
      />
    )
  }

  const renderOwnActions = () => {
    if (!collection.isOwn) return null

    return (
      <>
        <SecondaryButton
          title="Edit"
          icon="pencil"
          onClick={() => spawnCollectionEditModal(manager, collection)}
          {...variants}
        />
        <SecondaryButton
          title="Delete"
          icon="trashcan"
          onClick={() => spawnCollectionDeleteModal(manager, collection)}
          {...variants}
        />
      </>
    )
  }

  return (
    <Container>
      <div className="stats">{renderStats()}</div>
      <Actions compact horizontal>
        {renderAnonymousActions()}
        {renderOwnActions()}
      </Actions>
    </Container>
  )
})
