import {
  FetcherStore,
  createFetcherStoreFactory,
} from "../../../common/state/classes/FetcherStore"
import { CollectionModel } from "../models/Collection"
import { CollectionData } from "../types"

export class CollectionStore extends FetcherStore<CollectionModel> {
  private getIndex(item: CollectionModel) {
    return {
      newId: item.data.id,
      index: `${item.data.user.username}:${item.data.slug}`,
    }
  }

  public fetchById(id: number) {
    const source = `/collections/${id}`
    return this.fetch(source, id, this.getIndex)
  }

  public fetchByUsernameAndSlug(username: string, slug: string) {
    const source = `/users/${username}/collections/${slug}`
    const index = `${username}:${slug}`

    return this.fetch(source, index, this.getIndex)
  }

  public addPrefetched(data: CollectionData) {
    const source = `/collections/${data.id}`
    const index = `${data.user.username}:${data.slug}`

    return this.prepopulate(source, data.id, data, index)
  }
}

export const collectionStore = createFetcherStoreFactory(
  CollectionStore,
  CollectionModel,
)
