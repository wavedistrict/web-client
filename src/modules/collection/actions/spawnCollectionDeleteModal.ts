import React from "react"
import { CollectionDeleteModal } from "../components/CollectionDeleteModal"
import { CollectionModel } from "../models/Collection"
import { Manager } from "../../../common/state/types/Manager"

export const spawnCollectionDeleteModal = (
  manager: Manager,
  collection: CollectionModel,
) => {
  const { modalStore } = manager.stores
  const { id } = collection.data

  modalStore.spawn({
    key: `collection-delete-${id}`,
    render: () => React.createElement(CollectionDeleteModal, { collection }),
  })
}
