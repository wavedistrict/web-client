import { apiServer } from "../../../common/network"
import { TrackModel } from "../../track/models/Track"
import { CollectionModel } from "../models/Collection"

export const addTrackToCollection = async (
  collection: CollectionModel,
  track: TrackModel,
) => {
  const newTracks = [...collection.data.items.trackIds, track.data.id]

  await apiServer.patch(collection.apiPath, {
    tracks: newTracks,
  })
}
