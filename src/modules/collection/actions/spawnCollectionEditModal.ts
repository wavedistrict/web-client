import React from "react"
import { CollectionEditModal } from "../components/CollectionEditModal"
import { createCollectionEditFormManager } from "../helpers/createCollectionEditFormManager"
import { CollectionModel } from "../models/Collection"
import { Manager } from "../../../common/state/types/Manager"

export const spawnCollectionEditModal = (
  manager: Manager,
  collection: CollectionModel,
) => {
  const { modalStore } = manager.stores
  const form = createCollectionEditFormManager(collection)

  modalStore.spawn({
    key: `edit-collection-${collection.data.id}`,
    render: () =>
      React.createElement(CollectionEditModal, { form, collection }),
  })
}
