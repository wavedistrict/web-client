import React from "react"

import { TrackModel } from "../../track/models/Track"
import { CollectionCreateModal } from "../components/CollectionCreateModal"
import { createCollectionFormManager } from "../helpers/createCollectionFormManager"
import { Manager } from "../../../common/state/types/Manager"

export const spawnCreateCollectionModal = (
  manager: Manager,
  track: TrackModel,
) => {
  const { modalStore } = manager.stores
  const form = createCollectionFormManager(manager, track)

  modalStore.spawn({
    key: `create-collection-${track.data.id}`,
    render: () => React.createElement(CollectionCreateModal, { form }),
  })
}
