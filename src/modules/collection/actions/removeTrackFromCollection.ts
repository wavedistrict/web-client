import { apiServer } from "../../../common/network"
import { TrackModel } from "../../track/models/Track"
import { CollectionModel } from "../models/Collection"

export const removeTrackFromCollection = async (
  collection: CollectionModel,
  track: TrackModel,
) => {
  const newTracks = collection.data.items.trackIds.filter(
    (id) => id !== track.data.id,
  )

  await apiServer.patch(collection.apiPath, {
    tracks: newTracks,
  })
}
