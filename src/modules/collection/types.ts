import { ImageReferenceData } from "../../common/media/types/ImageReferenceData"
import { TrackData } from "../track/types/TrackData"
import { UserData } from "../user/types/UserData"

export interface CollectionStats {
  tracks: number
  favorites: number
  comments: number
  plays: number
}

export interface CollectionData {
  id: number
  title: string
  description: string
  slug: string
  type: string
  createdAt: string
  updatedAt: string
  user: UserData
  artwork?: ImageReferenceData
  items: {
    tracks: TrackData[]
    trackIds: number[]
  }
  duration: number
  stats: CollectionStats
  byCurrentUser: {
    favorited: boolean
    commented: boolean
    played: boolean
  }
}
