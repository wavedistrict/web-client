import { bind } from "../../../common/lang/function/helpers/bind"
import { FetcherStoreConsumer } from "../../realtime/classes/FetcherStoreConsumer"
import { CollectionModel } from "../models/Collection"
import { CollectionData } from "../types"
import { Manager } from "../../../common/state/types/Manager"

export class CollectionConsumer extends FetcherStoreConsumer<
  CollectionData,
  CollectionModel
> {
  protected addListeners() {
    this.on("create", this.handleCreateCollection)
  }

  @bind
  private handleCreateCollection(collection: CollectionModel) {
    const { user } = collection
    user.collections.addPending(collection.data)
  }
}

export const collectionConsumer = (manager: Manager) =>
  new CollectionConsumer(manager, "collection", "collectionStore")
