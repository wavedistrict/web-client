import React from "react"
import { LinkedTabItem } from "../../../common/ui/navigation/components/LinkedTabs"
import { PageHeader } from "../../core/components/PageHeader"
import { routes } from "../../core/constants"
import { TrackList } from "../../track/components/TrackList"
import { UserModel } from "../../user/models/User"
import { RequireAuthentication } from "../../auth/components/RequireAuthentication"
import { useRedirect } from "../../../common/routing/hooks/useRedirect"
import { useMeta } from "../../../common/ui/meta/hooks/useMeta"

export interface LibraryViewProps {
  user: UserModel
}

export function LibraryView(props: LibraryViewProps) {
  const { user } = props

  const tabs: LinkedTabItem[] = [
    {
      to: routes.library("tracks"),
      icon: "trackFilled",
      label: "Tracks",
    },
  ]

  useRedirect("/library", routes.library("tracks"))

  useMeta({
    title: "Your library",
    description: "See your liked tracks and collections here",
  })

  return (
    <>
      <PageHeader title="Library" tabs={tabs} />
      <TrackList
        list={user.favoritedTracks}
        clientLink={routes.library("tracks")}
        name="your library"
      />
    </>
  )
}

export function LibraryPage() {
  return (
    <RequireAuthentication>
      {(user) => <LibraryView user={user} />}
    </RequireAuthentication>
  )
}
