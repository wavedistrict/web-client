import { DropdownOption } from "../../common/ui/input/components/DropdownInput"

import { createGzip, createDeflate } from "zlib"
import { Transform } from "stream"

export const routes = {
  home: "/",
  upload: "/upload",
  search: "/search",
  explore: (subroute: string) => `/explore/${subroute}`,
  library: (subroute: string) => `/library/${subroute}`,
  user: (username: string, subroute = "tracks") => `/@${username}/${subroute}`,
  track: (username: string, slug: string) => `/@${username}/tracks/${slug}`,
  collection: (username: string, slug: string, subroute = "tracks") =>
    `/@${username}/collections/${slug}/${subroute}`,
}

export const visbilityOptions: DropdownOption[] = [
  {
    key: "listed",
    label: "Listed",
  },
  {
    key: "unlisted",
    label: "Unlisted",
  },
  {
    key: "private",
    label: "Private",
  },
]

export const CANONICAL_HOST = "https://wavedistrict.com"
export const IS_SERVER = process.env.__SERVER__ === "true"
export const IS_PROD = process.env.NODE_ENV === "production"

export const BUILD_FOLDER = "./build"
export const BUILD_PUBLIC_FOLDER = "./build/public"

export const BODY_WIDTH = 1200
export const HEADER_HEIGHT = 55
export const HEADER_BREAKPOINT = 810

export const USER_FRIENDLY_DELAY = 200

export const SERVER_SUPPORTED_ENCODINGS = {
  gzip: createGzip,
  deflate: createDeflate,
  // Identity encoding means the browser doesn't accept above compression modes.
  // Creates a transform stream that doesn't transform anything
  identity: () =>
    new Transform({ transform: (data, _, callback) => callback(null, data) }),
} as const
