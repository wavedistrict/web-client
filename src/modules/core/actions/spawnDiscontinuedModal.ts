import { Manager } from "../../../common/state/types/Manager"
import { DiscontinuedModal } from "../components/DiscontinuedModal"
import React from "react"

export const spawnDiscontinuedModal = (manager: Manager) => {
  const { modalStore } = manager.stores

  modalStore.spawn({
    key: "discontinued",
    dismissOnClickout: false,
    render: () => React.createElement(DiscontinuedModal),
  })
}
