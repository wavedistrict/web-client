import React from "react"

import { styled } from "../../theme/themes"
import { BODY_WIDTH } from "../constants"
import { HEADER_HEIGHT } from "./Header"
import { PLAYER_CONTROLS_HEIGHT } from "../../player/constants"

import { Route, useRouter } from "../../../common/routing/hooks/useRouter"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

import {
  CollectionPageProps,
  CollectionPage,
} from "../../collection/components/CollectionPage"
import { ExplorePage } from "../../explore/components/ExplorePage"
import { LandingPage } from "../../landing/components/LandingPage"
import { LibraryPage } from "../../library/components/LibraryPage"
import { SearchPage } from "../../search/components/SearchPage"
import { UserPage, UserPageProps } from "../../user/components/UserPage"
import { NotFoundPage } from "./NotFoundPage"
import { TrackUploadPage } from "../../track/components/TrackUploadPage/TrackUploadPage"
import {
  TrackPage,
  TrackPageProps,
} from "../../track/components/TrackPage/TrackPage"

const Container = styled.div`
  display: flex;
  justify-content: center;

  position: relative;
  z-index: 0;

  margin-top: ${HEADER_HEIGHT}px;
`

const Wrapper = styled.div`
  position: relative;
  width: ${BODY_WIDTH}px;
  max-width: 100vw;
`

const Content = styled.div<{ hasPlayer: boolean }>`
  width: 100%;
  position: absolute;
  z-index: 1;

  padding-bottom: ${({ theme, hasPlayer }) =>
    hasPlayer
      ? `${theme.spacings[6] + PLAYER_CONTROLS_HEIGHT}`
      : theme.spacings[6]}px;
`

export const APP_ROUTES: Route[] = [
  {
    name: "home",
    pattern: "/",
    render: () => <LandingPage />,
  },

  {
    name: "explore",
    pattern: "/explore(/*)",
    render: () => <ExplorePage />,
  },
  {
    name: "upload",
    pattern: "/upload",
    render: () => <TrackUploadPage />,
  },
  {
    name: "library",
    pattern: "/library(/*)",
    render: () => <LibraryPage />,
  },
  {
    name: "search",
    pattern: "/search",
    render: () => <SearchPage />,
  },
  {
    name: "track",
    pattern: "/@:username/tracks/:slug",
    render: (params: TrackPageProps) => <TrackPage {...params} />,
  },
  {
    name: "collection",
    pattern: "/@:username/collections/:slug(/*)",
    render: (params: CollectionPageProps) => <CollectionPage {...params} />,
  },
  {
    name: "user",
    pattern: "/@:username(/*)",
    render: (params: UserPageProps) => <UserPage {...params} />,
  },
  {
    name: "not-found",
    pattern: "*",
    render: () => <NotFoundPage />,
  },
]

export function Body() {
  const { playerStore } = useStores()
  const hasPlayer = useObserver(() => !!playerStore.track)

  const [renderRoute, routeName] = useRouter(APP_ROUTES)

  return (
    <Container>
      <Wrapper>
        <Content key={routeName} hasPlayer={hasPlayer}>
          {renderRoute()}
        </Content>
      </Wrapper>
    </Container>
  )
}
