import React from "react"
import { getFontColor, getFontSize, getTransparency } from "../../theme/helpers"
import { styled } from "../../theme/themes"

const Container = styled.div`
  display: inline-block;
  padding: 5px 12px;
  border-radius: 100px;

  font-weight: 600;
  font-size: ${getFontSize("1")}px;

  background: ${getTransparency("weakPositive")};
  color: ${getFontColor("normal")};

  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

export const Tag: React.SFC<{ name: string }> = (props) => {
  return <Container>{props.name}</Container>
}
