import styled from "@emotion/styled"
import { size } from "polished"
import React from "react"
import { IconType } from "../../../common/ui/icon"
import { Icon } from "../../../common/ui/icon/components/Icon"
import {
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../theme/helpers"

export interface NavLinkContentProps {
  label: string
  icon: IconType
}

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

export const IconContainer = styled.div`
  ${size(18)};
  margin-right: ${getSpacing("4")}px;

  svg {
    fill: ${getFontColor("normal")};
  }
`

// need to import from @emotion/styled to use this in a component selector
// https://github.com/emotion-js/emotion/issues/1320
export const Label = styled.span`
  font-size: ${getFontSize("2")}px;
  font-weight: ${getFontWeight("bold")};
`

export function NavLinkContent(props: NavLinkContentProps) {
  const { label, icon } = props

  return (
    <Container>
      <IconContainer>
        <Icon name={icon} />
      </IconContainer>
      <Label>{label}</Label>
    </Container>
  )
}
