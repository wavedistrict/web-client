import { PrimaryModal } from "../../modal/components/PrimaryModal"
import React from "react"
import { ExternalLink } from "../../../common/ui/navigation/components/ExternalLink"
import { ButtonList } from "../../../common/ui/button/components/ButtonList"
import { useModal } from "../../modal/hooks/useModal"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"

export function DiscontinuedModal() {
  const modal = useModal()

  const onClick = () => {
    modal.dismiss()
    localStorage.setItem("dismissed-discontinuation", "true")
  }

  return (
    <PrimaryModal title="Discontinuation">
      <PrimaryModal.Body>
        WaveDistrict was discontinued in September 2019.{" "}
        <ExternalLink href="https://twitter.com/wavedistrictapp/status/1177803195902517248">
          Read more here.
        </ExternalLink>
        <br />
        <br />
        This version is a demo running on minimal hardware so that I can display
        it on my portfolio.
        <br />
        <br />
        You're welcome to create an account and upload whatever you like but
        please do not abuse WaveDistrict or upload stuff you don't have rights
        to.
      </PrimaryModal.Body>
      <PrimaryModal.Footer>
        <ButtonList>
          <PrimaryButton onClick={onClick} label="OK" />
        </ButtonList>
      </PrimaryModal.Footer>
    </PrimaryModal>
  )
}
