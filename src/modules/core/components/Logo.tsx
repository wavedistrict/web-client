import React from "react"
import { size } from "polished"
import { useTheme, styled } from "../../theme/themes"
import { createUniqueKey } from "../../../common/state/helpers/createUniqueKey"

export type LogoProps = {
  className?: string
}

const Container = styled.svg`
  position: relative;
  box-sizing: content-box;
  display: block;

  ${size("100%")}
`

export function Logo(props: LogoProps) {
  const { gradients } = useTheme()
  const { degrees, stops } = gradients.logo

  const id = createUniqueKey()

  return (
    <Container className={props.className} viewBox="0 0 512 512" fill="none">
      <defs>
        <linearGradient
          id={`${id}-logo-gradient`}
          gradientTransform={`rotate(${Math.floor(degrees)} 0.5 0.5)`}
        >
          {stops.map(([color, offset]) => (
            <stop key={offset} stopColor={color} offset={`${offset}%`} />
          ))}
        </linearGradient>
      </defs>
      <path
        fill={`url('#${id}-logo-gradient')`}
        d="M0,0v256c0,70.7,57.3,128,128,128V128C128,57.3,70.7,0,0,0z M474.6,37.4C451.2,14.4,419.2,0,384,0v352c0,17.7-14.3,32-32,32c-17.7,0-32-14.3-32-32V224 c0-35.2-14.4-67.2-37.4-90.6C259.2,110.4,227.2,96,192,96v256c0,88.4,71.6,160,160,160c88.4,0,160-71.6,160-160V128 C512,92.8,497.6,60.8,474.6,37.4z"
      />
    </Container>
  )
}
