import { observer } from "mobx-react"
import React from "react"
import { ScrollLock } from "../../../common/ui/scroll/components/ScrollLock"
import { ModalOverlay } from "../../modal/components/ModalOverlay"
import { PlayerControls } from "../../player/components/PlayerControls"
import { PopoverOverlay } from "../../popover/components/PopoverOverlay"
import { Theme } from "../../theme/components/Theme"
import { styled } from "../../theme/themes"
import { ToastOverlay } from "../../toast/components/ToastOverlay"
import { TrackUploadOverlay } from "../../track/components/TrackUploadOverlay"
import { Body } from "./Body"
import { Header } from "./Header"
import { SidebarOverlay } from "./SidebarOverlay"
import { CookieDisclaimer } from "../../legal/components/CookieDisclaimer"
import { useStores } from "../../../common/state/hooks/useStores"
import { IS_SERVER } from "../constants"
import { Head } from "./Head"
import { GlobalStyles } from "../../theme/components/GlobalStyles"

const Wrapper = styled.div``

const Overlays = styled.div`
  position: absolute;
  top: 0;
`

export const App = observer(function App() {
  const { modalStore, uiStore } = useStores()

  const renderHead = () => {
    if (IS_SERVER) return null
    return <Head />
  }

  const shouldLock =
    modalStore.visibleModals.length > 0 ||
    uiStore.playerPanelOpen ||
    uiStore.sidebarOpen

  return (
    <ScrollLock locked={shouldLock}>
      {(style) => (
        <Theme>
          {renderHead()}
          <GlobalStyles />
          <div style={style}>
            <Wrapper>
              <Header />
              <Body />
              <PlayerControls />
            </Wrapper>
            {!IS_SERVER && (
              <Overlays>
                <CookieDisclaimer />
                <SidebarOverlay />
                <ModalOverlay />
                <TrackUploadOverlay />
                <ToastOverlay />
                <PopoverOverlay />
              </Overlays>
            )}
          </div>
        </Theme>
      )}
    </ScrollLock>
  )
})
