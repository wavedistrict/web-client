import { border, position, size } from "polished"
import React from "react"
import { getColor, getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { routes } from "../constants"
import { HeaderAuth } from "./HeaderAuth"
import { HeaderLink } from "./HeaderLink"
import { Logo } from "./Logo"
import { SidebarToggleButton } from "./SidebarToggleButton"
import { RouteLink } from "../../../common/routing/components/RouteLink"

export const HEADER_HEIGHT = 55
export const HEADER_BREAKPOINT = 810

const Root = styled.header`
  height: ${HEADER_HEIGHT}px;
  padding: 0px ${getSpacing("5")}px;

  background: ${getColor("primary")};

  z-index: 1;
  transform: translateZ(0);

  display: flex;
  align-items: center;

  ${(props) => {
    return [
      border("bottom", 1, "solid", props.theme.colors.divider),
      position("fixed", 0, 0, 0, 0),
    ]
  }}
`

const LogoContainer = styled(RouteLink)`
  margin-right: ${getSpacing("5")}px;
  ${size(24)};

  @media (max-width: ${HEADER_BREAKPOINT}px) {
    display: none;
  }
`

const Nav = styled.nav`
  display: flex;
  flex: 1;

  align-items: stretch;
  height: 100%;

  @media (max-width: ${HEADER_BREAKPOINT}px) {
    display: none;
  }
`

const Auth = styled.div`
  display: flex;
  align-items: center;

  @media (max-width: ${HEADER_BREAKPOINT}px) {
    flex: 1;
    justify-content: flex-end;
  }
`

export function Header() {
  return (
    <Root>
      <LogoContainer to="/">
        <Logo />
      </LogoContainer>
      <Nav>
        <HeaderLink to={routes.home} label="Home" icon="home" />
        <HeaderLink
          to={routes.explore("tracks")}
          activeOn={routes.explore("*")}
          label="Explore"
          icon="explore"
        />
        <HeaderLink to={routes.search} label="Search" icon="search" />
      </Nav>
      <SidebarToggleButton />
      <Auth>
        <HeaderAuth />
      </Auth>
    </Root>
  )
}
