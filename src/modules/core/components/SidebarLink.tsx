import css from "@emotion/css"
import React from "react"
import { IconType } from "../../../common/ui/icon"
import { RouteLinkProps } from "../../../common/routing/components/RouteLink"
import { NavLinkContent } from "./NavLinkContent"
import { styled } from "../../theme/themes"
import { getSpacing, getDuration } from "../../theme/helpers"
import { useRouteLink } from "../../../common/routing/hooks/useRouteLink"

export interface SidebarLinkProps extends RouteLinkProps {
  label: string
  icon: IconType
}

const Container = styled.a<{ active: boolean }>`
  padding: ${getSpacing("4")}px ${getSpacing("5")}px;
  display: flex;
  align-items: center;

  border-right: solid 3px transparent;

  transition: ${getDuration("normal")} ease;
  transition-property: background border color;

  svg {
    transition: ${getDuration("normal")} ease fill;
  }

  ${(props) => {
    const { theme } = props

    const active = css`
      border-right-color: ${theme.colors.accent};
      color: ${theme.colors.accent};

      svg {
        fill: ${theme.colors.accent};
      }

      &:hover,
      &:focus {
        background-color: ${theme.transparencies.weakNegative};
        border-right-color: inherit;
      }
    `

    const inactive = css`
      &:hover,
      &:focus {
        background-color: ${theme.transparencies.weakNegative};
        border-right-color: ${theme.transparencies.strongPositive};
      }
    `

    return props.active ? active : inactive
  }}
`

export function SidebarLink(props: SidebarLinkProps) {
  const { label, icon, to, activeOn } = props
  const [active, click] = useRouteLink(to, activeOn)

  return (
    <Container href={to} onClick={click} active={active}>
      <NavLinkContent label={label} icon={icon} />
    </Container>
  )
}
