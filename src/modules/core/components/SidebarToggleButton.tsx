import { observer } from "mobx-react"
import React from "react"
import { styled } from "../../theme/themes"
import { HEADER_BREAKPOINT } from "../constants"
import { HeaderButton } from "./HeaderButton"
import { useStores } from "../../../common/state/hooks/useStores"

const Container = styled.div`
  @media (min-width: ${HEADER_BREAKPOINT}px) {
    display: none;
  }
`

export const SidebarToggleButton = observer(function SidebarToggleButton() {
  const { uiStore } = useStores()

  const onClick = () => {
    uiStore.openSidebar()
  }

  return (
    <Container>
      <HeaderButton
        icon="hamburger"
        title="Open sidebar"
        onClick={onClick}
        active={uiStore.sidebarOpen}
      />
    </Container>
  )
})
