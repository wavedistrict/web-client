import { differenceInMilliseconds } from "date-fns"
import React, { useEffect, useState } from "react"
import timeago from "s-ago"

export interface HumanizedDateProps {
  children: string
}

const ONE_MINUTE = 1000 * 60
const ONE_HOUR = ONE_MINUTE * 60
const ONE_DAY = ONE_HOUR * 24

export function HumanizedDate(props: HumanizedDateProps) {
  const [humanizedDate, setHumanizedDate] = useState(
    timeago(new Date(props.children)),
  )

  useEffect(() => {
    let timeoutId = 0

    const tick = () => {
      const time = new Date(props.children).getTime()
      const now = Date.now()
      const difference = differenceInMilliseconds(now, time)

      setHumanizedDate(timeago(new Date(props.children)))

      if (difference > ONE_DAY) return

      if (difference > ONE_HOUR) {
        timeoutId = window.setTimeout(tick, ONE_HOUR)
        return
      }

      timeoutId = window.setTimeout(tick, ONE_MINUTE)
    }

    tick()

    return () => {
      if (timeoutId) clearTimeout(timeoutId)
    }
  }, [props.children])

  return <span>{humanizedDate}</span>
}
