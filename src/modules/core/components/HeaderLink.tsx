import React from "react"
import { RouteLinkProps } from "../../../common/routing/components/RouteLink"
import { useRouteLink } from "../../../common/routing/hooks/useRouteLink"
import { getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { HEADER_HEIGHT } from "./Header"
import {
  IconContainer,
  NavLinkContent,
  NavLinkContentProps,
} from "./NavLinkContent"

type HeaderLinkProps = RouteLinkProps & NavLinkContentProps

const Container = styled.a<{ active: boolean }>`
  height: ${HEADER_HEIGHT};

  margin: 0px ${getSpacing("4")}px;
  padding: 0px ${getSpacing("2")}px;

  border-bottom: solid 2px transparent;

  display: flex;
  align-items: center;

  transition: all 200ms ease;

  svg {
    transition: all 200ms ease;
  }

  ${(props) => {
    const { theme } = props

    const active = `
      border-bottom-color: ${theme.colors.accent};
      color: ${theme.colors.accent};

      ${IconContainer} svg {
        fill: ${theme.colors.accent};
      }
    `

    const inactive = `
      &:hover {
        border-bottom-color: ${theme.transparencies.weakPositive};
      }
    `

    return props.active ? active : inactive
  }}
`

export function HeaderLink(props: HeaderLinkProps) {
  const { label, icon, to, activeOn } = props
  const [active, click] = useRouteLink(to, activeOn)

  return (
    <Container href={to} onClick={click} active={active}>
      <NavLinkContent icon={icon} label={label} />
    </Container>
  )
}
