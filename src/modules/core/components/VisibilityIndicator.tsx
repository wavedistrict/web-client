import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { Model } from "../../../common/state/classes/Model"
import { IconType } from "../../../common/ui/icon"
import { Icon } from "../../../common/ui/icon/components/Icon"
import { styled } from "../../theme/themes"
import { Visibility } from "../types/Visibility"

export interface VisibilityLikeData {
  visibility: Visibility
}

export interface VisibilityIndicatorProps {
  model: Model<VisibilityLikeData>
}

const visibilityIconMap: Record<Exclude<Visibility, "listed">, IconType> = {
  unlisted: "hidden",
  private: "lock",
}

const Container = styled.div`
  ${size(18)};
`

export const VisibilityIndicator = observer(function VisibilityIndicator(
  props: VisibilityIndicatorProps,
) {
  const { visibility } = props.model.data

  if (visibility === "listed") return null

  return (
    <Container title={visibility} className="VisibilityIndicator">
      <Icon name={visibilityIconMap[visibility]} />
    </Container>
  )
})
