import React from "react"
import { ExternalLink } from "../../../common/ui/navigation/components/ExternalLink"
import { Document } from "./Document"
import { PageBody } from "./PageBody"
import { PageHeader } from "./PageHeader"

export class HomePage extends React.Component {
  public render() {
    return (
      <>
        <PageHeader title="Welcome to WaveDistrict" />
        <PageBody>
          <Document>
            <p>
              WaveDistrict is an up and coming audio sharing platform with
              clean, modern design, and an open-source web client.
              <br />
              The project is currently in an alpha stage, under active
              development.
            </p>
            <p>
              We have a trello where you can watch what features are planned as
              well,{" "}
              <ExternalLink href="https://trello.com/b/An8Mhmgp/wavedistrict">
                check it out here
              </ExternalLink>
              <br />
              If you'd like to help us out or contribute in some way, check out
              these links:
            </p>
            <ul>
              <li>
                <ExternalLink href="https://discord.gg/GV8qJSR">
                  Join us on Discord
                </ExternalLink>
              </li>
              <li>
                <ExternalLink href="https://twitter.com/wavedistrictapp">
                  Our official Twitter
                </ExternalLink>
              </li>
              <li>
                <ExternalLink href="https://www.patreon.com/wavedistrict">
                  Our Patreon
                </ExternalLink>
              </li>
              <li>
                <ExternalLink href="https://gitlab.com/wavedistrict/web-client">
                  GitLab Source
                </ExternalLink>
              </li>
            </ul>
          </Document>
        </PageBody>
      </>
    )
  }
}
