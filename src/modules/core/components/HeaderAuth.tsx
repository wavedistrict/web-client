import { observer } from "mobx-react"
import React from "react"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { spawnLoginModal } from "../../auth/actions/spawnLoginModal"
import { spawnRegisterModal } from "../../auth/actions/spawnRegisterModal"
import { MessageButton } from "../../messaging/components/MessageButton"
import { NotificationButton } from "../../notification/components/NotificationButton"
import { getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { TrackUploadButton } from "../../track/components/TrackUploadButton"
import { UserPopoverButton } from "../../user/components/UserPopoverButton"
import { ButtonList } from "../../../common/ui/button/components/ButtonList"
import { useStores } from "../../../common/state/hooks/useStores"
import { useManager } from "../../../common/state/hooks/useManager"

const Container = styled.div<{ anonymous?: boolean }>`
  display: flex;
  align-items: center;

  > * + * {
    margin-left: ${getSpacing("6")}px;
  }
`

export const HeaderAuth = observer(function HeaderAuth() {
  const manager = useManager()
  const { authStore } = useStores()

  if (authStore.user) {
    return (
      <Container>
        <TrackUploadButton />
        <NotificationButton />
        <MessageButton />
        <UserPopoverButton user={authStore.user} />
      </Container>
    )
  } else {
    return (
      <ButtonList horizontal>
        <PrimaryButton
          label="Sign in"
          onClick={() => spawnLoginModal(manager, {})}
        />
        <SecondaryButton
          label="Create account"
          onClick={() => spawnRegisterModal(manager)}
        />
      </ButtonList>
    )
  }
})
