import css from "@emotion/css"
import { size } from "polished"
import React from "react"
import { IconType } from "../../../common/ui/icon"
import { Icon } from "../../../common/ui/icon/components/Icon"
import {
  getDuration,
  getFontColor,
  getFontSize,
  getSpacing,
} from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { Theme } from "../../theme/types/Theme"

type StatsProps = Modifiers & {
  items: StatItem[]
}

export type StatItem = {
  name: string
  value: number | string
  icon: IconType
}

type Modifiers = {
  compact?: boolean
  tiny?: boolean
}

const Container = styled.div<Modifiers>`
  display: flex;

  > .noStats {
    opacity: 0.5;
    font-size: ${getFontSize("1")}px;
  }

  > .item {
    display: flex;
    align-items: center;
    opacity: 0.5;

    transition: ${getDuration("normal")} ease opacity;
  }

  > .item + .item {
    margin-left: ${getSpacing("4")}px;
  }

  > .item > .icon {
    ${size(16)};
    margin-right: ${getSpacing("3")}px;
  }

  > .item > .count {
    color: ${getFontColor("normal")};
    font-size: ${getFontSize("1")}px;
    font-weight: 600;

    margin-bottom: 1px;
  }

  > .item:hover {
    opacity: 1;
    cursor: default;
  }

  ${(props) => props.compact && compactStyle(props.theme)};
  ${(props) => props.tiny && tinyStyle(props.theme)};
`

const compactStyle = (theme: Theme) => css`
  > .item + .item {
    margin-left: ${theme.spacings[4]}px;
  }
`

const tinyStyle = (theme: Theme) => css`
  > .noStats {
    font-size: ${theme.fontSizes[1]}px;
  }

  > .item > .icon {
    ${size(12)};
    margin-right: ${theme.spacings[2]}px;
  }
`

export function Stats({ items, ...modifiers }: StatsProps) {
  const renderItems = () => {
    const nonZeroStats = items.filter((stat) => stat.value)

    if (nonZeroStats.length === 0) {
      return <div className="noStats">{"No stats yet"}</div>
    }

    return nonZeroStats.map((item) => (
      <div key={item.name} className="item" title={item.name}>
        <Icon className="icon" name={item.icon} />
        <div className="count">{item.value}</div>
      </div>
    ))
  }

  return <Container {...modifiers}>{renderItems()}</Container>
}
