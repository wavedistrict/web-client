import { observer } from "mobx-react"
import { linearGradient } from "polished"
import React, { useEffect, useRef, useState } from "react"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { Markdown } from "../../../common/ui/markdown/components/Markdown"
import { getDuration, getSpacing, getFontSize } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { TagList } from "./TagList"

export interface DescriptionProps {
  text?: string
  tags?: string[]
  noDescNotice?: string
}

const Container = styled.div<{
  maxHeight: number
  showMore: boolean
  isOverflowing: boolean
  noDescription: boolean
}>`
  position: relative;
  font-size: ${getFontSize("2")}px;

  > .content {
    max-height: ${(props) => props.maxHeight}px;
    overflow: hidden;

    transition: max-height ${getDuration("extended")};
    transition-delay: 200ms;
  }

  > .content > .tags {
    margin-top: ${getSpacing("4")}px;
  }

  > .more {
    pointer-events: none;
    position: absolute;
    bottom: 0px;
    left: 0px;
    right: 0px;
  }

  > .more > .filter {
    height: 120px;

    position: absolute;
    bottom: 0px;
    left: 0px;
    right: 0px;

    background: ${({ theme }) =>
      linearGradient({
        colorStops: ["transparent 0%", `${theme.colors.background} 100%`],
        toDirection: "to bottom",
      })};

    opacity: 0;
    transition: all ${getDuration("extended")} ease;
  }

  > .more > .button {
    transform: translateY(20px);
    transition: all ${getDuration("extended")} ease;
    transition-delay: 200ms;

    opacity: 0;
  }

  ${(props) =>
    props.noDescription &&
    `
    > .content > .text {
      color: ${props.theme.fontColors.muted};
      font-style: italic;
    }
  `}

  ${(props) =>
    props.isOverflowing &&
    `
    > .more {
      pointer-events: all;
    }

    > .more > .filter {
      opacity: 1;
    }

    > .more > .button {
      opacity: 1;
    }

    > .content {
      margin-bottom: ${props.theme.spacings["6"]}px;
    }
  `}

  ${(props) =>
    props.showMore &&
    `
    > .content {
      transition-delay: 0ms;
    }

    > .more > .filter {
      opacity: 0;
      transition-delay: 200ms;
    }

    > .button {
      transition-delay: 0ms;
    }
  `}
`

export const Description = observer(function Description(
  props: DescriptionProps,
) {
  const { text, tags, noDescNotice = "No description provided" } = props

  const contentRef = useRef<HTMLDivElement>(null)

  const [isOverflowing, setIsOverflowing] = useState(false)
  const [overflowingHeight, setOverflowingHeight] = useState(0)
  const [showMore, setShowMore] = useState(false)

  useEffect(() => {
    const { current: container } = contentRef

    if (container) {
      setIsOverflowing(container.scrollHeight > container.clientHeight)
      setOverflowingHeight(container.scrollHeight)
    }
  }, [text])

  const renderTags = () => {
    if (!tags) return

    return (
      <div className="tags">
        <TagList tags={tags} />
      </div>
    )
  }

  const containerProps = {
    maxHeight: showMore ? overflowingHeight : 250,
    showMore,
    isOverflowing,
    noDescription: !text,
  }

  return (
    <Container {...containerProps}>
      <div ref={contentRef} className="content">
        <div className="text">
          <Markdown text={text || noDescNotice} />
        </div>
        {renderTags()}
      </div>
      <div className="more">
        <div className="filter" />
        <div className="button">
          <SecondaryButton
            flat
            stretch
            label={showMore ? "Show less" : "Show more"}
            onClick={() => {
              setShowMore(!showMore)
            }}
          />
        </div>
      </div>
    </Container>
  )
})
