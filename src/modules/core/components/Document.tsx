import React from "react"
import { getFontColor, getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"

const Container = styled.article`
  > * + * {
    margin-top: ${getSpacing("4")}px;
  }

  ul {
    list-style-type: disc;
    color: ${getFontColor("muted")};
  }

  ul > li {
    display: list-item;
    //color: color(accent);
    margin-left: ${getSpacing("4")}px;

    + li {
      margin-top: ${getSpacing("2")}px;
    }

    > span {
      color: ${getFontColor("normal")};
    }
  }
`

export function Document(props: { children: React.ReactNode }) {
  return <Container children={props.children} />
}
