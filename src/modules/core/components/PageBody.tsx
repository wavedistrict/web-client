import React from "react"
import { getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { BODY_WIDTH } from "../constants"

const Container = styled.main`
  @media (max-width: ${BODY_WIDTH}px) {
    padding: 0px ${getSpacing("5")}px;
    margin-top: ${getSpacing("5")}px;
  }
`

export function PageBody(props: { children: React.ReactNode }) {
  return <Container>{props.children}</Container>
}
