import React from "react"
import { getFontColor, getFontWeight, getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { HEADER_HEIGHT } from "./Header"
import { useMeta } from "../../../common/ui/meta/hooks/useMeta"
import { useStores } from "../../../common/state/hooks/useStores"
import { useUniversalEffect } from "../../../common/ui/react/hooks/useUniversalEffect"

const Container = styled.div`
  height: calc(100vh - 88px - ${HEADER_HEIGHT}px);

  display: flex;
  justify-content: center;
  align-items: center;

  > .content {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  > .content > .header {
    font-size: 128px;
    font-weight: ${getFontWeight("bold")};
    margin-bottom: ${getSpacing("4")}px;
  }

  > .content > .text {
    font-style: italic;
    color: ${getFontColor("normal")};
  }
`

export function NotFoundPage() {
  const { routingStore } = useStores()

  useUniversalEffect(() => {
    routingStore.status = 404

    return () => {
      routingStore.status = 200
    }
  })

  useMeta({
    type: "unknown",
    title: "404 - Not found",
    description: "The page you're looking for does not exist",
  })

  return (
    <Container className="NotFoundView">
      <div className="content">
        <div className="header">{`404`}</div>
        <div className="text">{`The page you're looking for does not exist`}</div>
      </div>
    </Container>
  )
}
