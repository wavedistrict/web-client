import { size } from "polished"
import React from "react"
import { IconType } from "../../../common/ui/icon"
import { Icon } from "../../../common/ui/icon/components/Icon"
import { LinkedTabs } from "../../../common/ui/navigation/components/LinkedTabs"
import {
  getColor,
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
} from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { BODY_WIDTH } from "../constants"

interface PageHeaderProps {
  title: string
  icon?: IconType
  tabs?: Tab[]
}

interface Tab {
  to: string
  label: string
}

const mq = `@media (max-width: ${BODY_WIDTH}px)`

const Container = styled.header`
  display: flex;
  align-items: flex-end;

  margin-top: ${getSpacing("6")}px;
  margin-bottom: ${getSpacing("5")}px;

  ${mq} {
    padding: 0px ${getSpacing("5")}px;
    padding-top: ${getSpacing("5")}px;

    margin: 0;

    background: ${getColor("primary")};
    border-bottom: solid 2px ${getColor("primary")};
    display: block;
  }
`

const Details = styled.div`
  display: flex;
  align-items: center;
  flex: 1;

  border-bottom: solid 2px ${getTransparency("weakPositive")};
  padding-bottom: ${getSpacing("4")}px;

  ${mq} {
    border: none;
    margin-bottom: ${getSpacing("2")}px;
  }
`

const IconContainer = styled.div`
  ${size(22)};
  margin-top: 1px;

  svg {
    fill: ${getFontColor("normal")};
  }
`

const Title = styled.h1<{ hasIcon: boolean }>`
  margin-left: ${({ hasIcon, theme }) => (hasIcon ? theme.spacings[4] : 0)}px;
  margin-right: ${getSpacing("4")}px;

  font-size: ${getFontSize("3")}px;
  font-weight: ${getFontWeight("bold")};

  flex: 1;
`

const Tabs = styled.div`
  border-bottom: solid 2px ${getTransparency("weakPositive")};

  > * {
    margin-bottom: -2px;
  }

  ${mq} {
    border: none;
  }
`

export function PageHeader(props: PageHeaderProps) {
  const { icon, tabs = [], title } = props

  const renderIcon = () =>
    icon && (
      <IconContainer>
        <Icon name={icon} />
      </IconContainer>
    )

  return (
    <Container>
      <Details>
        {renderIcon()}
        <Title hasIcon={!!icon}>{title}</Title>
      </Details>
      <Tabs>
        <LinkedTabs variants={["borderless"]} items={tabs} />
      </Tabs>
    </Container>
  )
}
