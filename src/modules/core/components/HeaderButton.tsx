import { css } from "@emotion/core"
import { size } from "polished"
import { HEADER_HEIGHT } from "./Header"
import { styled } from "../../theme/themes"
import { Button } from "../../../common/ui/button/components/Button"
import { getDuration, getFontColor, getColor } from "../../theme/helpers"

export type HeaderButtonVariants = {
  active?: boolean
  hasAlert?: boolean
}

export const HeaderButton = styled<typeof Button, HeaderButtonVariants>(Button)`
  position: relative;
  height: ${HEADER_HEIGHT}px;

  display: flex;
  justify-content: center;
  align-items: center;

  > .icon {
    ${size(24)}

    fill: ${getFontColor("muted")};
    transition: ${getDuration("normal")} ease fill;
  }

  ${({ theme, active }) =>
    !active &&
    css`
      &:hover > .icon {
        fill: ${theme.fontColors.normal};
      }
    `}

  &::after {
    content: "";
    display: block;

    border-radius: 100%;

    background: ${getColor("accent")};
    border: solid 2px ${getColor("primary")};

    ${size(7)}

    position: absolute;

    top: 0px;
    right: 0px;

    transition: ${getDuration("normal")} ease;
    transition-property: transform opacity;

    transform: scale(0);
    opacity: 0;
  }

  ${({ theme, active }) =>
    active &&
    css`
      > .icon {
        fill: ${theme.colors.accent};
      }
    `}

  ${({ theme, hasAlert }) =>
    hasAlert &&
    css`
      > .icon {
        fill: ${theme.fontColors.normal};
      }

      &::after {
        transform: scale(1);
        opacity: 1;
      }
    `}
`
