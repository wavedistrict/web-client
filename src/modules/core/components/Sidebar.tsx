import { observer } from "mobx-react"
import React from "react"
import { getColor, getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { routes } from "../constants"
import { HEADER_HEIGHT } from "./Header"
import { Logo } from "./Logo"
import { SidebarLink } from "./SidebarLink"
import { useStores } from "../../../common/state/hooks/useStores"

const SIDEBAR_WIDTH = 240

const Container = styled.div`
  width: ${SIDEBAR_WIDTH}px;
  max-width: 100vw;

  height: 100vh;
  background: ${getColor("primary")};
`

const Header = styled.div`
  height: ${HEADER_HEIGHT}px;
  padding-left: ${getSpacing("5")}px;

  background: ${getColor("primary")};
  border-bottom: solid 1px ${getColor("divider")};

  display: flex;
  align-items: center;
`

export const Sidebar = observer(function Sidebar() {
  const { uiStore } = useStores()

  const handleClick = () => {
    uiStore.closeSidebar()
  }

  return (
    <Container>
      <Header className="header">
        <Logo />
      </Header>
      <div onClick={handleClick}>
        <SidebarLink to={routes.home} label="Home" icon="home" />
        <SidebarLink
          to={routes.explore("tracks")}
          activeOn={routes.explore("*")}
          label="Explore"
          icon="explore"
        />
        <SidebarLink to={routes.search} label="Search" icon="search" />
      </div>
    </Container>
  )
})
