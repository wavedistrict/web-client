import React from "react"
import { getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { Tag } from "./Tag"

const Container = styled.div`
  > * {
    margin-right: ${getSpacing("3")}px;
    margin-bottom: ${getSpacing("3")}px;
  }
`

export const TagList: React.SFC<{ tags: string[] }> = (props) => {
  return (
    <Container>
      {props.tags.map((tag) => (
        <Tag key={tag} name={tag} />
      ))}
    </Container>
  )
}
