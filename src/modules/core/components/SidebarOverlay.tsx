import { observer } from "mobx-react"
import { cover, position } from "polished"
import React from "react"
import { getDuration, getOverlay } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { Sidebar } from "./Sidebar"
import { useStores } from "../../../common/state/hooks/useStores"

const Container = styled.div<{ open: boolean }>`
  ${position("fixed", 0, 0, 0)};
  z-index: 2;
  height: 100vh;

  transition: ${getDuration("extended")} ease all;
  pointer-events: ${({ open }) => (open ? "all" : "none")};
  transform: translateZ(0);
`

const Filter = styled.div<{ open: boolean }>`
  ${cover()}

  opacity: ${({ open }) => (open ? "0.5" : "0")};
  background-color: ${getOverlay("background")};
  transition: ${getDuration("extended")} ease all;
`

const SidebarContainer = styled.div<{ open: boolean }>`
  position: absolute;

  transform: translateX(${({ open }) => (open ? "0%" : "-100%")});
  transition: ${getDuration("extended")} ease all;
`

export const SidebarOverlay = observer(function SidebarOverlay() {
  const { uiStore } = useStores()

  const handleClick = () => {
    uiStore.closeSidebar()
  }

  const cssProps = { open: uiStore.sidebarOpen }

  return (
    <Container {...cssProps}>
      <Filter {...cssProps} onClick={handleClick} />
      <SidebarContainer {...cssProps}>
        <Sidebar />
      </SidebarContainer>
    </Container>
  )
})
