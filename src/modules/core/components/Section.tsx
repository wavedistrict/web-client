import { observer } from "mobx-react"
import React from "react"
import {
  getColor,
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
} from "../../theme/helpers"
import { styled } from "../../theme/themes"

interface SectionProps {
  title: string
  children?: React.ReactNode
  collapsible?: boolean
}

const Container = styled.div`
  & + & {
    margin-top: ${getSpacing("5")}px;
  }
`

const Title = styled.h3`
  color: ${getColor("accent")};

  font-size: ${getFontSize("2")}px;
  font-weight: ${getFontWeight("bold")};

  text-transform: uppercase;
  display: flex;
  align-items: center;

  margin-bottom: ${getSpacing("4")}px;

  &::after {
    flex-grow: 1;

    content: "";
    display: block;

    height: 2px;
    background: ${getTransparency("weakPositive")};

    margin-left: ${getSpacing("4")}px;
  }
`

export const Section = observer(function Section(props: SectionProps) {
  const { title, children } = props

  return (
    <Container>
      <Title>{title}</Title>
      <div>{children}</div>
    </Container>
  )
})
