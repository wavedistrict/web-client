import React from "react"
import {
  getColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../theme/helpers"
import { styled } from "../../theme/themes"

export interface HeaderListPopoverProps {
  title: string
  children: React.ReactNode
}

const POPOVER_WIDTH = 450
const POPOVER_HEIGHT = 500
const POPOVER_MAX_HEIGHT = "calc(100vh - 200px)"

const Container = styled.div`
  > .header {
    padding: ${getSpacing("4")}px;

    font-size: ${getFontSize("3")}px;
    font-weight: ${getFontWeight("bold")};

    border-bottom: solid 1px ${getColor("divider")};
    width: ${POPOVER_WIDTH}px;
  }

  > .list {
    height: ${POPOVER_HEIGHT}px;
    max-height: ${POPOVER_MAX_HEIGHT};
  }
`

export function HeaderListPopover(props: HeaderListPopoverProps) {
  const { title, children } = props

  return (
    <Container>
      <div className="header">{title}</div>
      <div className="list">{children}</div>
    </Container>
  )
}
