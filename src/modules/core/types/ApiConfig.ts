export interface ApiConfig {
  trackTypes: ApiType[]
  collectionTypes: ApiType[]
  security: {
    insecurePasswords: string[]
  }
}

export interface ApiType {
  id: number
  name: string
}
