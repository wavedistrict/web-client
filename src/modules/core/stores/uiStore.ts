import { action, observable } from "mobx"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

export class UiStore extends InitializableStore {
  @observable public sidebarOpen = false
  @observable public playerPanelOpen = false

  @action.bound
  public openSidebar() {
    this.sidebarOpen = true
  }

  @action.bound
  public closeSidebar() {
    this.sidebarOpen = false
  }

  @action.bound
  public toggleSidebar() {
    this.sidebarOpen = !this.sidebarOpen
  }

  public async init() {}
}

export const uiStore = createStoreFactory(UiStore)
