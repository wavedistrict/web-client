import { action, observable } from "mobx"
import { StoredValue } from "../../../common/dom/classes/StoredValue"
import { apiServer } from "../../../common/network"
import { ApiConfig } from "../types/ApiConfig"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

const DEFAULT_CONFIG: ApiConfig = {
  trackTypes: [],
  collectionTypes: [],
  security: {
    insecurePasswords: [],
  },
}

const configStorage = new StoredValue<ApiConfig>("config", DEFAULT_CONFIG)

export class ConfigStore extends InitializableStore {
  @observable public debug = false

  @observable
  public config: ApiConfig = DEFAULT_CONFIG
  private didFetch = false

  @action
  public setConfig(config: ApiConfig) {
    this.config = config
    configStorage.save(config)
  }

  public async init() {
    if (this.didFetch) return

    this.config = await configStorage.restore()
    await this.loadFromServer()
  }

  private async loadFromServer() {
    const request = await apiServer.get<ApiConfig>(`/config`)

    this.didFetch = true
    this.setConfig(request.data)
  }
}

export const configStore = createStoreFactory(ConfigStore)
