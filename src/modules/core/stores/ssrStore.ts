import { IS_SERVER } from "../../../modules/core/constants"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

export class SSRStore extends InitializableStore {
  public promises: Promise<any>[] = []
  public lazy = false

  public init() {}

  public reset() {
    this.promises = []
  }

  public register(promise: Promise<any>) {
    if (IS_SERVER) {
      this.promises.push(promise)
    }

    return promise
  }

  public async waitFor() {
    await Promise.all(this.promises)
    this.promises = []
  }
}

export const ssrStore = createStoreFactory(SSRStore)
