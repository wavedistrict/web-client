import { join } from "path"

/**
 * Gets the full public network url for a given public file,
 * here so that we only have to change one string if the public path changes
 */
export const getPublicFileUrl = (path: string) => {
  return join("/", path)
}
