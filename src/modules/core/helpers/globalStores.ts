import { Manager } from "../../../common/state/types/Manager"

export function assignStores(
  manager: Manager,
  namespace: { [key: string]: any },
) {
  namespace.stores = manager.stores
}
