import React from "react"
import { CellMeasurerCache } from "react-virtualized"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { EmptyState } from "../../../common/state/components/EmptyState"
import { createVirtualizedListComponent } from "../../../common/ui/list/helpers/createVirtualizedListComponent"
import { renderContainerList } from "../../../common/ui/list/helpers/renderContainerList"
import { wrapDynamicHeightRowInLi } from "../../../common/ui/list/helpers/wrapDynamicHeightRowInLi"
import { NotificationModel } from "../models/Notification"
import { NotificationListItem } from "./NotificationListItem/NotificationListItem"

interface NotificationListProps {
  list: FetcherList<NotificationModel>
}

const cache = new CellMeasurerCache({
  minHeight: NotificationListItem.minHeight,
  fixedWidth: true,
})

const List = createVirtualizedListComponent<NotificationModel>({
  renderItem: wrapDynamicHeightRowInLi(
    (item) => <NotificationListItem notification={item} />,
    cache,
  ),
  rowHeight: cache.rowHeight,
  cache,
})

export class NotificationList extends React.Component<NotificationListProps> {
  private renderEmptyState() {
    return <EmptyState icon="bell" message="No notifications yet" />
  }

  public render() {
    const { list } = this.props

    if (list.isEmpty) return this.renderEmptyState()

    return renderContainerList({
      list,
      render: (props) => <List {...props} />,
    })
  }
}
