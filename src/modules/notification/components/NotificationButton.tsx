import { observer } from "mobx-react"
import React from "react"
import { HeaderButton } from "../../core/components/HeaderButton"
import { PopoverTrigger } from "../../popover/components/PopoverTrigger"
import { NotificationPopover } from "./NotificationPopover"
import { useStores } from "../../../common/state/hooks/useStores"

export const NotificationButton = observer(function NotificationButton() {
  const { activityStore } = useStores()

  const { unreadNotifications } = activityStore
  const hasUnreadNotifications = unreadNotifications > 0

  const title = hasUnreadNotifications
    ? "Notifications"
    : `You have ${unreadNotifications} unread notifications`

  return (
    <PopoverTrigger placement="bottom-end">
      {(props) => (
        <HeaderButton
          ref={props.ref}
          title={title}
          icon="bell"
          onClick={props.toggle}
          active={props.isActive}
          hasAlert={hasUnreadNotifications}
        />
      )}
      {() => <NotificationPopover />}
    </PopoverTrigger>
  )
})
