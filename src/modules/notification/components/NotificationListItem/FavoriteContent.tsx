import { NotificationModel } from "../../models/Notification"
import { useStores } from "../../../../common/state/hooks/useStores"
import { useMemo } from "react"
import React from "react"
import { ImagePair } from "./ImagePair"
import { IconType } from "../../../../common/ui/icon"
import { ContentBody } from "./ContentBody"
import { Header } from "./Header"
import { Body } from "./Body"
import { Timestamp } from "./Timestamp"

export type FavoriteContentProps = {
  notification: NotificationModel
}

const typeToIconMap: Record<"track" | "collection", IconType> = {
  track: "trackFilled",
  collection: "collectionFilled",
}

export function FavoriteContent(props: FavoriteContentProps) {
  const { imageMediaStore } = useStores()
  const { notification } = props

  const { user, target, big, small } = useMemo(() => {
    const { data: user } = notification.getUser()

    const target = notification.getTarget(["track", "collection"])

    const big = imageMediaStore.assign(user.avatar)
    const small = imageMediaStore.assign(target.data.artwork)

    return { user, target, big, small }
  }, [imageMediaStore, notification])

  const { displayName } = user

  return (
    <>
      <ImagePair
        big={{
          round: true,
          reference: big,
          placeholder: {
            hash: user.id,
            icon: "personFilled",
          },
        }}
        small={{
          reference: small,
          placeholder: {
            hash: target.data.id,
            icon: typeToIconMap[target.type],
          },
        }}
      />
      <ContentBody>
        <Header
          icon="heartFilled"
          action={`${displayName} favorited your ${target.type}`}
        />
        <Body>{target.data.title}</Body>
        <Timestamp notification={notification} />
      </ContentBody>
    </>
  )
}
