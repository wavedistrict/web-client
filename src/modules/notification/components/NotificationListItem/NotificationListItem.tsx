import React from "react"
import css from "@emotion/css"
import { NotificationModel } from "../../models/Notification"
import { styled } from "../../../theme/themes"
import { getSpacing, getDuration } from "../../../theme/helpers"
import { useObserver } from "mobx-react-lite"
import { CommentContent } from "./CommentContent"
import { ReplyContent } from "./ReplyContent"
import { FavoriteContent } from "./FavoriteContent"
import { FollowContent } from "./FollowContent"

export type NotificationListItemProps = {
  notification: NotificationModel
}

const Container = styled.div<{ unread?: boolean }>`
  position: relative;
  padding: ${getSpacing("4")}px;
  display: flex;

  &::before {
    content: "";
    display: block;
    width: 4px;

    position: absolute;
    top: 8px;
    bottom: 8px;
    left: 0px;

    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;

    transition: ${getDuration("normal")} ease background;
  }

  ${(props) =>
    props.unread &&
    css`
      &::before {
        background: ${props.theme.colors.accent};
      }
    `}
`

export function NotificationListItem(props: NotificationListItemProps) {
  const { notification } = props
  const { type } = notification.data

  const renderContent = () => {
    if (type === "comment") {
      return <CommentContent notification={notification} />
    }

    if (type === "comment-reply") {
      return <ReplyContent notification={notification} />
    }

    if (type === "favorite") {
      return <FavoriteContent notification={notification} />
    }

    if (type === "follow") {
      return <FollowContent notification={notification} />
    }
  }

  return useObserver(() => (
    <Container unread={notification.data.unread}>{renderContent()}</Container>
  ))
}

NotificationListItem.minHeight = 77
