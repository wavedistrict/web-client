import { styled } from "../../../theme/themes"
import { getFontSize, getSpacing } from "../../../theme/helpers"

export const Body = styled.div`
  font-size: ${getFontSize("2")}px;
  margin-top: ${getSpacing("2")}px;
`
