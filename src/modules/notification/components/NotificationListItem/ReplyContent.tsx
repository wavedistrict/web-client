import { NotificationModel } from "../../models/Notification"
import { useStores } from "../../../../common/state/hooks/useStores"
import React, { useMemo } from "react"
import { CommentReplyTargetData } from "../../types/NotificationTargets"
import { ImagePair } from "./ImagePair"
import { ContentBody } from "./ContentBody"
import { Header } from "./Header"
import { Body } from "./Body"
import { Context } from "./Context"
import { Timestamp } from "./Timestamp"

export type ReplyContentProps = {
  notification: NotificationModel
}

export function ReplyContent(props: ReplyContentProps) {
  const { imageMediaStore } = useStores()
  const { notification } = props

  const { big, user, comment, target } = useMemo(() => {
    const { data: user } = notification.getUser()

    const target = notification.getTarget(["track", "collection"])
    const comment = notification.getTarget([
      "comment",
    ]) as CommentReplyTargetData

    const big = imageMediaStore.assign(user.avatar)

    return { user, target, comment, big }
  }, [imageMediaStore, notification])

  const { displayName } = user

  return (
    <>
      <ImagePair
        big={{
          round: true,
          reference: big,
          placeholder: {
            hash: user.id,
            icon: "personFilled",
          },
        }}
      />
      <ContentBody>
        <Header
          icon="reply"
          action={`${displayName} replied to your comment`}
        />
        <Body>{comment.data.body}</Body>
        <Context>on {target.data.title}</Context>
        <Timestamp notification={notification} />
      </ContentBody>
    </>
  )
}
