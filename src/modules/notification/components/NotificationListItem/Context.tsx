import { styled } from "../../../theme/themes"
import { getFontColor, getFontSize, getSpacing } from "../../../theme/helpers"
import { ellipsis } from "polished"

export const Context = styled.span`
  ${ellipsis()}

  display: block;
  margin-top: ${getSpacing("2")}px;
  color: ${getFontColor("muted")};
  font-size: ${getFontSize("1")}px;
`
