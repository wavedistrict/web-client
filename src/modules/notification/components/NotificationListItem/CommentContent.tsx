import React from "react"
import { NotificationModel } from "../../models/Notification"
import { useStores } from "../../../../common/state/hooks/useStores"
import { useMemo } from "react"
import { ImagePair } from "./ImagePair"
import { IconType } from "../../../../common/ui/icon"
import { ContentBody } from "./ContentBody"
import { Header } from "./Header"
import { Body } from "./Body"
import { Timestamp } from "./Timestamp"

export type CommentContentProps = {
  notification: NotificationModel
}

const typeToIconMap: Record<"track" | "collection", IconType> = {
  track: "trackFilled",
  collection: "collectionFilled",
}

export function CommentContent(props: CommentContentProps) {
  const { imageMediaStore } = useStores()

  const { notification } = props
  const { type } = notification.data

  const { big, small, user, comment, target } = useMemo(() => {
    const { data: user } = notification.getUser()

    const target = notification.getTarget(["track", "collection"])
    const comment = notification.getTarget(["comment"])

    const big = imageMediaStore.assign(user.avatar)
    const small = imageMediaStore.assign(target.data.artwork)

    return { big, small, user, comment, target, type }
  }, [imageMediaStore, notification, type])

  const { displayName } = user

  return (
    <>
      <ImagePair
        big={{
          round: true,
          reference: big,
          placeholder: {
            hash: user.id,
            icon: "personFilled",
          },
        }}
        small={{
          reference: small,
          placeholder: {
            hash: target.data.id,
            icon: typeToIconMap[target.type],
          },
        }}
      />
      <ContentBody>
        <Header
          icon="commentFilled"
          action={`${displayName} commented on your ${target.type}`}
        />
        <Body>{comment.data.body}</Body>
        <Timestamp notification={notification} />
      </ContentBody>
    </>
  )
}
