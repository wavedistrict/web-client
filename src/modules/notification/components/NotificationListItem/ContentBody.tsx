import { styled } from "../../../theme/themes"
import { getSpacing } from "../../../theme/helpers"

export const ContentBody = styled.div`
  margin-left: ${getSpacing("4")}px;
  overflow: hidden;
`
