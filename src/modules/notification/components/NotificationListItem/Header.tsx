import React from "react"
import { IconType } from "../../../../common/ui/icon"
import { styled } from "../../../theme/themes"
import { Icon } from "../../../../common/ui/icon/components/Icon"
import { size } from "polished"
import { getSpacing, getFontWeight, getFontSize } from "../../../theme/helpers"

export type HeaderProps = {
  icon: IconType
  action: string
}

const Container = styled.div`
  display: flex;
  align-items: flex-start;
`

const TypeIcon = styled(Icon)`
  ${size(16)}
  margin-right: ${getSpacing("3")}px;
`

const Text = styled.h1`
  font-weight: ${getFontWeight("bold")};
  font-size: 14px;
`

export function Header(props: HeaderProps) {
  const { icon, action } = props

  return (
    <Container>
      <TypeIcon name={icon} />
      <Text>{action}</Text>
    </Container>
  )
}
