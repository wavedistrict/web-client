import { NotificationModel } from "../../models/Notification"
import { useStores } from "../../../../common/state/hooks/useStores"
import { useMemo } from "react"
import React from "react"
import { ImagePair } from "./ImagePair"
import { ContentBody } from "./ContentBody"
import { Header } from "./Header"
import { Timestamp } from "./Timestamp"

export type FollowContentProps = {
  notification: NotificationModel
}

export function FollowContent(props: FollowContentProps) {
  const { imageMediaStore } = useStores()
  const { notification } = props

  const { user, big } = useMemo(() => {
    const { data: user } = notification.getUser()
    const big = imageMediaStore.assign(user.avatar)

    return { user, big }
  }, [imageMediaStore, notification])

  const { displayName } = user

  return (
    <>
      <ImagePair
        big={{
          round: true,
          reference: big,
          placeholder: {
            hash: user.id,
            icon: "personFilled",
          },
        }}
      />
      <ContentBody>
        <Header
          icon="personAddFilled"
          action={`${displayName} is now following you`}
        />
        <Timestamp notification={notification} />
      </ContentBody>
    </>
  )
}
