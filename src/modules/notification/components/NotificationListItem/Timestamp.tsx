import { NotificationModel } from "../../models/Notification"
import { styled } from "../../../theme/themes"
import { getSpacing, getFontColor, getFontSize } from "../../../theme/helpers"
import React from "react"
import { HumanizedDate } from "../../../core/components/HumanizedDate"

export type TimestampProps = {
  notification: NotificationModel
}

const Container = styled.span`
  display: block;

  color: ${getFontColor("muted")};
  font-size: ${getFontSize("1")}px;
  font-style: italic;

  margin-top: ${getSpacing("2")}px;
`

export function Timestamp(props: TimestampProps) {
  const { notification } = props

  return (
    <Container>
      <HumanizedDate>{notification.data.createdAt}</HumanizedDate>
    </Container>
  )
}
