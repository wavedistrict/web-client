import React from "react"
import { styled } from "../../../theme/themes"
import { ImageModel } from "../../../../common/ui/image/models/ImageModel"
import { size } from "polished"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { getColor } from "../../../theme/helpers"
import { ImageReferenceRenderer } from "../../../../common/ui/image/components/ImageReferenceRenderer"
import { IconType } from "../../../../common/ui/icon"

export type ImagePairItem = {
  reference?: ImageModel
  placeholder: {
    icon: IconType
    hash: number
  }
  round?: boolean
}

export type ImagePairProps = {
  big: ImagePairItem
  small?: ImagePairItem
}

const IMAGE_SIZE = 45

const Container = styled.div`
  ${size(IMAGE_SIZE)}
  position: relative;
`

const Big = styled.div<{ round?: boolean }>`
  ${size(IMAGE_SIZE)}
  ${(props) => imageContainer(props.round ? IMAGE_SIZE : 4)}
`

const Small = styled.div<{ round?: boolean }>`
  ${size(Math.floor(IMAGE_SIZE / 1.8))}
  ${(props) =>
    imageContainer(props.round ? IMAGE_SIZE : 7)}

  position: absolute;
  right: -6px;
  bottom: -6px;

  border: solid 2px ${getColor("primary")};
`

export function ImagePair(props: ImagePairProps) {
  const { big, small } = props

  const renderSmall = () => {
    if (!small) return

    return (
      <Small round={small.round}>
        <ImageReferenceRenderer
          reference={small.reference}
          placeholder={small.placeholder}
        />
      </Small>
    )
  }

  return (
    <Container>
      <Big round={big.round}>
        <ImageReferenceRenderer
          reference={big.reference}
          placeholder={big.placeholder}
        />
      </Big>
      {renderSmall()}
    </Container>
  )
}
