import React, { useEffect } from "react"
import { HeaderListPopover } from "../../core/components/HeaderListPopover"
import { NotificationList } from "./NotificationList"
import { useStores } from "../../../common/state/hooks/useStores"
import { useAuthUser } from "../../auth/hooks/useAuthUser"

export function NotificationPopover() {
  const { notificationStore } = useStores()
  const user = useAuthUser()

  useEffect(() => {
    return () => {
      notificationStore.clearUnread()
    }
  }, [notificationStore])

  return (
    <HeaderListPopover title="Notifications">
      <NotificationList list={user.notifications} />
    </HeaderListPopover>
  )
}
