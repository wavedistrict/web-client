import { bind } from "../../../common/lang/function/helpers/bind"
import { SocketMessage } from "../../../common/network/types"
import { SocketMessageConsumer } from "../../realtime/classes/SocketMessageConsumer"
import { NotificationLike } from "../types/NotificationTypes"
import { Manager } from "../../../common/state/types/Manager"

export interface NotificationConsumerEvents {
  notification: NotificationLike
}

class NotificationConsumer extends SocketMessageConsumer<
  NotificationConsumerEvents
> {
  protected getListeners() {
    return {
      notification: this.handleNewNotification,
    }
  }

  @bind
  private handleNewNotification(message: SocketMessage) {
    const { notificationStore } = this.manager.stores

    const data = message.data as NotificationLike

    notificationStore.addPrefetched(data)
    notificationStore.handleNewNotification(data)

    this.emit("notification", data)
  }
}

export const notificationConsumer = (manager: Manager) =>
  new NotificationConsumer(manager)
