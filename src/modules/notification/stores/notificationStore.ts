import { action, runInAction } from "mobx"
import { bind } from "../../../common/lang/function/helpers/bind"
import { apiServer } from "../../../common/network"
import {
  FetcherStore,
  createFetcherStoreFactory,
} from "../../../common/state/classes/FetcherStore"
import { NotificationModel } from "../models/Notification"
import { NotificationLike } from "../types/NotificationTypes"

export class NotificationStore extends FetcherStore<NotificationModel> {
  @action.bound
  public handleNewNotification(data: NotificationLike) {
    const { activityStore, authStore } = this.manager.stores

    activityStore.unreadNotifications += 1

    if (authStore.user) {
      authStore.user.notifications.add(data, "first")
    }
  }

  public addPrefetched(data: NotificationLike) {
    const source = `/notifications/${data.id}`
    return this.prepopulate(source, data.id, data)
  }

  @bind
  public async clearUnread() {
    const { activityStore } = this.manager.stores

    await apiServer.post("/notifications/clear-unread", null)

    runInAction(() => {
      activityStore.unreadNotifications = 0

      for (const notification of this.models) {
        notification.data.unread = false
      }
    })
  }
}

export const notificationStore = createFetcherStoreFactory(
  NotificationStore,
  NotificationModel,
)
