import { NotificationData } from "./NotificationData"
import {
  CollectionTargetData,
  CommentReplyTargetData,
  CommentTargetData,
  TrackTargetData,
  UserTargetData,
} from "./NotificationTargets"

export type FavoriteNotification = NotificationData<
  "favorite",
  UserTargetData | TrackTargetData | CollectionTargetData
>
export type FollowNotification = NotificationData<"follow", UserTargetData>
export type CommentNotification = NotificationData<
  "comment",
  UserTargetData | TrackTargetData | CollectionTargetData | CommentTargetData
>
export type CommentReplyNotification = NotificationData<
  "comment-reply",
  | UserTargetData
  | TrackTargetData
  | CollectionTargetData
  | CommentReplyTargetData
>

export type NotificationLike =
  | FavoriteNotification
  | FollowNotification
  | CommentNotification
  | CommentReplyNotification
