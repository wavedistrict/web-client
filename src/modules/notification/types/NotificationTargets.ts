import { ImageReferenceData } from "../../../common/media/types/ImageReferenceData"
import { Target } from "./NotificationData"

export type UserTargetData = Target<
  "user",
  {
    id: number
    displayName: string
    username: string
    avatar?: ImageReferenceData
  }
>

export type TrackTargetData = Target<
  "track",
  {
    id: number
    slug: string
    title: string
    artwork?: ImageReferenceData
  }
>

export type CollectionTargetData = Target<
  "collection",
  {
    id: number
    slug: string
    title: string
    artwork?: ImageReferenceData
  }
>

export type CommentTargetData = Target<
  "comment",
  {
    id: number
    body: string
  }
>

export type CommentReplyTargetData = Target<
  "comment",
  {
    id: number
    body: string
    replyTo: {
      id: number
      body: string
    }
  }
>

export type TargetMap = {
  user: UserTargetData
  track: TrackTargetData
  collection: CollectionTargetData
  comment: CommentTargetData | CommentReplyTargetData
}
