export interface NotificationData<Type extends string, Targets> {
  id: number
  key: string
  type: Type
  data: Record<string, any>
  unread: boolean
  createdAt: string
  targets: Targets[]
}

export interface Target<T extends string, D> {
  type: T
  data: D
}
