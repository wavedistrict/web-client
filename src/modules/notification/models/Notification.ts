import { Model } from "../../../common/state/classes/Model"
import { UserTargetData, TargetMap } from "../types/NotificationTargets"
import { NotificationLike } from "../types/NotificationTypes"

export class NotificationModel extends Model<NotificationLike> {
  public getTarget<E extends keyof TargetMap>(types: E[]): TargetMap[E] {
    // @ts-ignore: Not sure how to fix this error
    return this.data.targets.find((t) => types.includes(t.type))
  }

  public getUser(): UserTargetData {
    return this.getTarget(["user"])
  }
}
