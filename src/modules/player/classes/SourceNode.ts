import { bind } from "../../../common/lang/function/helpers/bind"
import { Emitter } from "../../../common/state/classes/Emitter"
import { AudioSourceModel } from "../models/AudioSourceModel"

export enum SourceNodeError {
  Unknown,
  PlaybackError,
}

export interface SourceNodeEvents {
  error: SourceNodeError
  buffering: void
  playing: void
  paused: void
  ended: void
}

/** Represents an optimized audio player with a source output */
export class SourceNode extends Emitter<SourceNodeEvents> {
  private audio = new Audio()
  private node: MediaElementAudioSourceNode
  private _source!: AudioSourceModel

  constructor(context: AudioContext) {
    super()

    const { audio } = this

    audio.volume = 0.3
    audio.crossOrigin = "anonymous"

    this.setListeners()
    this.node = context.createMediaElementSource(this.audio)
  }

  private setListeners() {
    const { audio } = this

    audio.onerror = () => {
      this.emit("error", SourceNodeError.Unknown)
    }

    audio.onended = () => {
      this.emit("ended", undefined)
    }

    audio.onstalled = () => {
      this.emit("buffering", undefined)
    }

    audio.onwaiting = () => {
      this.emit("buffering", undefined)
    }

    audio.onplaying = () => {
      this.emit("playing", undefined)
    }

    audio.onseeking = () => {
      this.emit("buffering", undefined)
    }
  }

  public setSource(source: AudioSourceModel) {
    this.audio.src = source.path
    this._source = source
  }

  public load() {
    this.audio.load()
  }

  @bind
  public async play() {
    try {
      await this.audio.play()
      this.emit("playing", undefined)
    } catch (error) {
      // Ignore common error that does nothing
      if (error.name === "AbortError") return
      this.emit("error", SourceNodeError.PlaybackError)
      console.error(error, error.message)
    }
  }

  @bind
  public pause() {
    this.audio.pause()
    this.emit("paused", undefined)
  }

  @bind
  public seek(time: number) {
    this.audio.currentTime = time
  }

  public get time() {
    return this.audio.currentTime
  }

  public connect(node: AudioNode) {
    this.node.connect(node)
  }

  public disconnect() {
    this.node.disconnect()
  }

  public get source() {
    return this._source
  }
}
