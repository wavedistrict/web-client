import { clamp } from "../../../common/math/helpers/clamp"
import { Loudness } from "../../../common/media/types/AudioReferenceData"
import { getSetting } from "../../settings/helpers/getSetting"
import { TARGET_LUFS } from "../constants"
import { getDecibelsFromGain } from "../helpers/getDecibelsFromGain"
import { getGainFromDecibels } from "../helpers/getGainFromDecibels"
import { Manager } from "../../../common/state/types/Manager"

const DEFAULT_LOUDNESS = {
  lufs: TARGET_LUFS,
  truePeak: 0,
}

export class LoudnessNode {
  private enabled = true
  private volume = 1
  private default = 1

  private loudness: Loudness = DEFAULT_LOUDNESS

  private defaultGain: GainNode
  private volumedGain: GainNode

  constructor(manager: Manager, context: AudioContext) {
    this.defaultGain = context.createGain()
    this.volumedGain = context.createGain()

    const enabled = getSetting(manager, "audio", "normalize")

    enabled.setHandler((value) => {
      this.enabled = value
      this.updateGain()
    })
  }

  public get loudnessDifference() {
    if (!this.enabled) return 0
    return TARGET_LUFS - Number(this.loudness.lufs) || 0
  }

  private getNormalized(volume: number) {
    return getGainFromDecibels(
      getDecibelsFromGain(volume) + this.loudnessDifference,
    )
  }

  public get normalizedVolume() {
    return this.getNormalized(this.volume)
  }

  public get normalizedDefault() {
    return this.getNormalized(this.default)
  }

  private updateGain() {
    this.defaultGain.gain.value = this.normalizedDefault
    this.volumedGain.gain.value = this.normalizedVolume
  }

  public setVolume(value: number) {
    const clamped = clamp(value, 0, 1)
    const curved = clamped ** 3
    const amplified = curved * 4

    this.volume = amplified
    this.updateGain()
  }

  public setLoudness(loudness?: Loudness) {
    this.loudness = loudness || DEFAULT_LOUDNESS
    this.updateGain()
  }

  public get node() {
    return this.volumedGain
  }

  public get defaultNode() {
    return this.defaultGain
  }
}
