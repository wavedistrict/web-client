export interface StereoFrequencies {
  left: Uint8Array
  right: Uint8Array
}

export class StereoAnalyserNode {
  private context: AudioContext

  private left: AnalyserNode
  private right: AnalyserNode

  private leftFrequencies: Uint8Array
  private rightFrequencies: Uint8Array

  private splitter: ChannelSplitterNode

  constructor(context: AudioContext) {
    this.context = context

    this.left = this.context.createAnalyser()
    this.right = this.context.createAnalyser()

    this.leftFrequencies = new Uint8Array(this.left.frequencyBinCount)
    this.rightFrequencies = new Uint8Array(this.right.frequencyBinCount)

    this.splitter = this.context.createChannelSplitter(2)

    this.splitter.connect(this.left, 0)
    this.splitter.connect(this.right, 1)

    this.setSmoothingTimeConstant(0)
  }

  public getFrequencies(): StereoFrequencies {
    const { left, right, leftFrequencies, rightFrequencies } = this

    left.getByteFrequencyData(leftFrequencies)
    right.getByteFrequencyData(rightFrequencies)

    return { left: leftFrequencies, right: rightFrequencies }
  }

  public setSmoothingTimeConstant(value: number) {
    this.left.smoothingTimeConstant = value
    this.right.smoothingTimeConstant = value
  }

  get node() {
    return this.splitter
  }
}
