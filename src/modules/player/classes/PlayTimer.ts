import { Stopwatch } from "../../../common/state/classes/Stopwatch"
import { postTrackPlay } from "../../track/actions/postTrackPlay"
import { Manager } from "../../../common/state/types/Manager"

const MINIMUM_PLAYBACK_TIME = 60 * 3

/** Tracks how much of a track has been played and automatically submits a play after a threshold */
export class PlayTimer {
  private didSubmitPlay = false
  private stopwatch = new Stopwatch()

  constructor(manager: Manager) {
    this.stopwatch.on("tick", () => {
      const { playerStore } = manager.stores
      const { track } = playerStore
      const { seconds } = this.stopwatch

      if (track) {
        const requiredPlaybackTime = Math.min(
          MINIMUM_PLAYBACK_TIME,
          track.data.duration / 2,
        )

        if (seconds >= requiredPlaybackTime && !this.didSubmitPlay) {
          postTrackPlay(track)
          this.didSubmitPlay = true
        }
      }
    })
  }

  public stop() {
    this.didSubmitPlay = false
    this.stopwatch.stop()
  }

  public start() {
    this.stopwatch.start()
  }

  public pause() {
    this.stopwatch.pause()
  }
}
