import { observable } from "mobx"
import { wait } from "../../../common/lang/promise/helpers/wait"
import { Emitter } from "../../../common/state/classes/Emitter"
import { getSetting } from "../../settings/helpers/getSetting"
import { getAudioSourceFromReference } from "../helpers/getAudioSourceFromReference"
import { AudioModel } from "../models/AudioModel"
import { LoudnessNode } from "./LoudnessNode"
import { PreloadableSourceNode } from "./PreloadableSourceNode"
import { StereoAnalyserNode } from "./StereoAnalyserNode"
import { Manager } from "../../../common/state/types/Manager"

const BUFFERING_TICK_RATE = 50
const TICK_RATE = 200

export interface AudioManagerEvents {
  ended: void
  tick: void
  playing: void
  halted: void
}

export class AudioManager extends Emitter<AudioManagerEvents> {
  private reference!: AudioModel
  private context: AudioContext

  @observable public playing = false
  @observable public buffering = false
  @observable public failed = false
  @observable public quality = 4
  @observable public volume = 1

  private audio: PreloadableSourceNode
  private analyser: StereoAnalyserNode
  private loudness: LoudnessNode

  constructor(manager: Manager) {
    super()

    const Context =
      (<any>window).AudioContext || (<any>window).webkitAudioContext

    if (Context) {
      this.context = new Context()
    } else {
      throw new Error("AudioContext unavailable")
    }

    const audioQuality = getSetting(manager, "audio", "preferredQuality")

    audioQuality.setHandler((value) => {
      this.quality = value
    })

    this.audio = new PreloadableSourceNode(this.context)
    this.analyser = new StereoAnalyserNode(this.context)
    this.loudness = new LoudnessNode(manager, this.context)

    this.audio.on("playing", () => {
      this.playing = true
      this.buffering = false
      this.emit("playing", undefined)
    })

    this.audio.on("paused", () => {
      this.buffering = false
      this.playing = false
      this.emit("halted", undefined)
    })

    this.audio.on("error", () => {
      this.failed = true
      this.buffering = false
      this.playing = false
      this.emit("halted", undefined)
    })

    this.audio.on("buffering", () => {
      this.buffering = true
      this.emit("halted", undefined)
    })

    this.audio.on("ended", () => this.emit("ended", undefined))

    this.connect()
    this.tick()
  }

  private async tick() {
    this.emit("tick", undefined)
    await wait(this.buffering ? BUFFERING_TICK_RATE : TICK_RATE)
    this.tick()
  }

  private connect() {
    this.audio.connect(this.analyser.node)

    this.audio.connect(this.loudness.node)
    this.audio.connect(this.loudness.defaultNode)

    this.loudness.defaultNode.connect(this.analyser.node)
    this.loudness.node.connect(this.context.destination)
  }

  public async load(reference: AudioModel) {
    this.reference = reference
    this.buffering = false
    this.failed = false

    const source = getAudioSourceFromReference(reference, this.quality)

    this.audio.setSource(source)
    this.loudness.setLoudness(reference.data.metadata.loudness)
  }

  public preload(reference: AudioModel) {
    const source = getAudioSourceFromReference(reference, this.quality)
    this.audio.preload(source)
  }

  public setQuality(quality: number) {
    this.quality = quality

    const source = getAudioSourceFromReference(this.reference, quality)
    const time = this.audio.time

    this.audio.setSource(source)
    this.audio.seek(time)

    if (this.playing) this.audio.play()
  }

  public setVolume(value: number) {
    this.volume = value
    this.loudness.setVolume(value)
  }

  public async play() {
    await this.context.resume()
    return this.audio.play()
  }

  public get pause() {
    return this.audio.pause
  }

  public async toggle() {
    return this.playing ? this.pause() : this.play()
  }

  public get seek() {
    return this.audio.seek
  }

  public get time() {
    return this.audio.time
  }

  public get frequencies() {
    return this.analyser.getFrequencies()
  }

  public get source() {
    return this.audio.source
  }
}
