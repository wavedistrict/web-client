import { observable } from "mobx"
import { bind } from "../../../common/lang/function/helpers/bind"
import { Manager } from "../../../common/state/types/Manager"

export enum RepeatState {
  NoRepeat = "NO_REPEAT",
  RepeatQueue = "REPEAT_QUEUE",
  RepeatTrack = "REPEAT_TRACK",
}

export class PlayerScheduler {
  @observable public repeatState = RepeatState.NoRepeat

  constructor(private manager: Manager) {}

  @bind
  public cycleRepeatState() {
    const states = Object.values(RepeatState)
    const index = states.indexOf(this.repeatState)
    const nextState = states[index + 1] || states[0]

    this.repeatState = nextState
  }

  @bind
  public cycle() {
    const { playerStore, queueStore } = this.manager.stores

    if (this.repeatState === RepeatState.RepeatTrack) {
      playerStore.play()
      return
    }

    if (queueStore.next) {
      playerStore.setTrack(queueStore.next)
      playerStore.play()
      return
    }

    if (this.repeatState === RepeatState.RepeatQueue) {
      if (!queueStore.first) return playerStore.pause()

      playerStore.setTrack(queueStore.first)
      playerStore.play()
      return
    }

    playerStore.pause()
  }

  @bind
  public next() {
    const { playerStore, queueStore } = this.manager.stores
    const track = queueStore.next || queueStore.first

    if (track) {
      playerStore.setTrack(track)
      playerStore.play()
    }
  }

  @bind
  public previous() {
    const { playerStore, queueStore } = this.manager.stores
    const track = queueStore.previous || queueStore.last

    if (track) {
      playerStore.setTrack(track)
      playerStore.play()
    }
  }
}
