import { bind } from "../../../common/lang/function/helpers/bind"
import { wait } from "../../../common/lang/promise/helpers/wait"
import { Emitter } from "../../../common/state/classes/Emitter"
import { AudioSourceModel } from "../models/AudioSourceModel"
import { SourceNode, SourceNodeEvents } from "./SourceNode"

export class PreloadableSourceNode extends Emitter<SourceNodeEvents> {
  private current: SourceNode
  private preloaded: SourceNode

  // The gain node is used as a single output
  private destination: GainNode

  constructor(context: AudioContext) {
    super()

    this.current = new SourceNode(context)
    this.preloaded = new SourceNode(context)
    this.destination = context.createGain()

    this.current.pipe(this)
    this.reconnect()
  }

  @bind
  public preload(source: AudioSourceModel) {
    if (this.preloaded.source === source) return

    this.preloaded.setSource(source)
    this.preloaded.load()
  }

  @bind
  public setSource(source: AudioSourceModel) {
    if (this.preloaded.source === source) {
      const { preloaded, current } = this

      current.unpipe()
      preloaded.pipe(this)

      this.current = preloaded
      this.preloaded = current

      this.reconnect()
    }

    this.current.setSource(source)
  }

  private async reconnect() {
    this.preloaded.disconnect()
    await wait(1)
    this.current.connect(this.destination)
  }

  public connect(node: AudioNode) {
    this.destination.connect(node)
  }

  public get play() {
    return this.current.play
  }

  public get pause() {
    return this.current.pause
  }

  public get seek() {
    return this.current.seek
  }

  public get time() {
    return this.current.time
  }

  public get source() {
    return this.current.source
  }
}
