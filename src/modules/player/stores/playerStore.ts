import { action, computed, observable } from "mobx"
import {
  StoredValue,
  serverStorage,
} from "../../../common/dom/classes/StoredValue"
import { bind } from "../../../common/lang/function/helpers/bind"
import { getSetting } from "../../settings/helpers/getSetting"
import { TrackModel } from "../../track/models/Track"
import { TrackData } from "../../track/types/TrackData"
import { AudioManager } from "../classes/AudioManager"
import { PlayerScheduler, RepeatState } from "../classes/PlayerScheduler"
import { PlayTimer } from "../classes/PlayTimer"
import { Manager } from "../../../common/state/types/Manager"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"
import { IS_SERVER } from "../../core/constants"

interface PlayerLocalStorageData {
  repeatState: RepeatState
  currentTime: number
  track?: TrackData
  volume: number
}

interface PlayerSessionStorageData {
  isPlaying: boolean
}

const STORAGE_PLAYER = "player"
const SECONDS_LEFT = 10

const playerStorage = new StoredValue<PlayerLocalStorageData>(STORAGE_PLAYER, {
  repeatState: RepeatState.NoRepeat,
  currentTime: 0,
  volume: 1,
})

const isPlayingStorage = new StoredValue<PlayerSessionStorageData>(
  STORAGE_PLAYER,
  {
    isPlaying: false,
  },
  IS_SERVER ? serverStorage : sessionStorage,
)

export class PlayerStore extends InitializableStore {
  private audioManager?: AudioManager
  private scheduler = new PlayerScheduler(this.manager)
  private timer = new PlayTimer(this.manager)

  @observable public track?: TrackModel

  private didPreloadBeforeEnd = false

  @bind
  public async init() {
    if (IS_SERVER) return

    try {
      this.audioManager = new AudioManager(this.manager)
    } catch (error) {
      console.warn("Player could not initialize. Playback will be unavailable.")
      console.error(error)
      return
    }

    this.restoreState()

    const { audioManager: audio } = this

    audio.on("ended", () => {
      this.scheduler.cycle()
    })

    audio.on("tick", () => {
      this.saveState()
      this.preloadBeforeEnd()
    })

    audio.on("playing", () => {
      this.timer.start()
    })

    audio.on("halted", () => {
      this.timer.pause()
    })
  }

  private preloadBeforeEnd() {
    const { track, manager } = this
    const { queueStore } = manager.stores

    if (!track || this.didPreloadBeforeEnd || !queueStore.next) return

    if (track.data.duration - SECONDS_LEFT < this.currentTime) {
      this.didPreloadBeforeEnd = true
      this.preload(queueStore.next)
    }
  }

  @bind
  public setTrack(track: TrackModel) {
    if (!this.audioManager) return

    this.track = track
    this.didPreloadBeforeEnd = false

    try {
      this.audioManager.load(track.audio)
    } catch {
      this.stop()
      this.reportError("Failed to load audio")
    }

    this.timer.stop()
    this.updatePlayerPresence()
    this.saveState()
    this.setMediaSession(track)

    this.manager.stores.queueStore.fetchIfNecessary()
  }

  @bind
  public setQuality(quality: number) {
    if (!this.track) return
    this.audioManager!.setQuality(quality)
  }

  @bind
  public stop() {
    this.track = undefined
    //this.audio.clear()
    this.updatePlayerPresence()
  }

  @bind
  public preload(track: TrackModel) {
    const setting = getSetting(this.manager, "audio", "preload")
    if (!this.audioManager || !setting.value) return

    this.audioManager.preload(track.audio)
  }

  @bind
  public async play() {
    if (this.audioManager) {
      await this.audioManager.play()
      this.updatePlayerPresence()
    }
  }

  @bind
  public pause() {
    if (this.audioManager) {
      this.audioManager.pause()
      this.updatePlayerPresence()
    }
  }

  @bind
  public async toggle() {
    if (this.audioManager) {
      await this.audioManager.toggle()
      this.updatePlayerPresence()
      this.saveState()
    }
  }

  private reportError(message: string) {
    const { toastStore } = this.manager.stores

    toastStore.addToast({
      content: `Player error: ${message}`,
      variant: "error",
    })
  }

  private setMediaSession(track: TrackModel) {
    if (!navigator.mediaSession) return
    if (typeof MediaMetadata === "undefined") return

    const { mediaSession } = navigator

    const artwork = track.artwork
      ? track.artwork.sources
          .filter((s) => s.data.mime !== "image/webp")
          .map((source) => {
            const { width, height, mime } = source.data

            return {
              src: source.path,
              sizes: `${width}x${height}`,
              type: mime,
            }
          })
      : undefined

    mediaSession.metadata = new MediaMetadata({
      title: track.data.title,
      artist: track.user.data.displayName,
      artwork,
    })

    mediaSession.setActionHandler("play", this.play)
    mediaSession.setActionHandler("pause", this.pause)
    mediaSession.setActionHandler("nexttrack", this.next)
    mediaSession.setActionHandler("previoustrack", this.previous)
  }

  get next() {
    return this.scheduler.next
  }

  get previous() {
    return this.scheduler.previous
  }

  get cycleRepeatState() {
    return this.scheduler.cycleRepeatState
  }

  get toggleMute() {
    return () => {}
  }

  @computed
  get isPlaying() {
    return (this.audioManager && this.audioManager.playing) || false
  }

  @computed
  get isBuffering() {
    return (this.audioManager && this.audioManager.buffering) || false
  }

  @computed
  get hasFailed() {
    return this.audioManager && this.audioManager.failed
  }

  @computed
  get source() {
    return this.audioManager && this.audioManager.source
  }

  @computed
  get repeatState() {
    return this.scheduler.repeatState
  }

  @computed
  get isMuted() {
    return (this.audioManager && this.audioManager.volume === 0) || false
  }

  @computed
  get volume() {
    return (this.audioManager && this.audioManager.volume) || 0
  }

  setVolume(value: number) {
    if (this.audioManager) {
      this.audioManager.setVolume(value)
      this.saveState()
    }
  }

  get currentTime() {
    return (this.audioManager && this.audioManager.time) || 0
  }

  set currentTime(value: number) {
    if (this.audioManager) {
      this.audioManager.seek(value)
    }
  }

  get frequencies() {
    return (
      (this.audioManager && this.audioManager.frequencies) || {
        left: new Uint8Array(),
        right: new Uint8Array(),
      }
    )
  }

  @bind
  private updatePlayerPresence() {
    if (!this.track) return

    const data = {
      playedOn: new Date().toISOString(),
      timeOffset: this.currentTime,
      isPaused: !this.isPlaying,
      track: this.track.data.id,
    }

    // socketConnection.setPresence({ player: data })
  }

  /** Saves the player state to localStorage */
  public saveState() {
    if (!this.audioManager) return

    const localStorageData = {
      track: this.track ? this.track.rawData : undefined,
      repeatState: this.scheduler.repeatState,
      currentTime: this.audioManager.time,
      volume: this.volume,
    }

    const sessionStorageData = {
      isPlaying: this.audioManager.playing,
    }

    playerStorage.save(localStorageData)
    isPlayingStorage.save(sessionStorageData)
  }

  /** Loads the player state from localStorage and restores it */
  @action
  public restoreState() {
    const setting = getSetting(this.manager, "audio", "autoresume")
    if (!this.audioManager || !setting.value) return

    const { trackStore } = this.manager.stores

    const localStorageData = playerStorage.restore()
    const sessionStorageData = isPlayingStorage.restore()

    if (localStorageData.track) {
      const trackFetcher = trackStore.addPrefetched(localStorageData.track)
      trackFetcher.fetch()

      if (trackFetcher.model) this.setTrack(trackFetcher.model)
    }

    this.setVolume(localStorageData.volume)
    this.audioManager.seek(localStorageData.currentTime)
    this.scheduler.repeatState = localStorageData.repeatState

    if (sessionStorageData.isPlaying) this.play()
  }
}

export const playerStore = createStoreFactory(PlayerStore)
