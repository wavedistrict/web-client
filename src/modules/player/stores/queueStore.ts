import { action, computed, observable } from "mobx"
import { StoredValue } from "../../../common/dom/classes/StoredValue"
import { shuffleArray } from "../../../common/lang/array/helpers/shuffleArray"
import { bind } from "../../../common/lang/function/helpers/bind"
import {
  FetcherList,
  FetcherListStatus,
} from "../../../common/state/classes/FetcherList"
import { TrackModel } from "../../track/models/Track"
import { TrackData } from "../../track/types/TrackData"
import { QueueData } from "../types/QueueData"
import {
  createStoreFactory,
  InitializableStore,
} from "../../../common/state/classes/InitializableStore"

const FETCH_BUFFER = 10

interface QueueLocalStorageData {
  name: string
  path: string
  params: Record<string, any>
  clientLink: string
  offset: number
  index: number
  isShuffling: boolean
}

const queueStorage = new StoredValue<QueueLocalStorageData | undefined>(
  "queue",
  undefined,
)

const queueTracksStorage = new StoredValue<TrackData[]>("queue-tracks", [])

export class QueueStore extends InitializableStore {
  @observable public list?: FetcherList<TrackModel>
  @observable public isShuffling = false

  @observable private name?: string
  @observable private clientLink?: string

  @observable private internalTracks: TrackModel[] = []
  @observable private shuffledTracks: TrackModel[] = []

  @action
  public setQueueData(data: QueueData) {
    if (this.list) {
      this.list.off("modelsReceived", this.pushTracks)
      this.clear()
    }

    this.list = data.list.clone()
    this.list.limit = 50
    this.list.on("modelsReceived", this.pushTracks)
    this.pushTracks(this.list.models)

    this.name = data.name
    this.clientLink = data.clientLink
    this.saveState()
  }

  private clear() {
    this.internalTracks = []
    this.shuffledTracks = []

    this.saveState()
  }

  @bind
  private pushTracks(tracks: TrackModel[]) {
    this.internalTracks.push(...tracks)
    this.shuffledTracks.push(...this.getShuffledTracks(tracks))

    this.saveState()
  }

  private getShuffledTracks(tracks: TrackModel[]) {
    const shuffledTracks = shuffleArray(tracks).sort((track) => {
      return track.isActive ? -1 : 0
    })

    return shuffledTracks
  }

  @bind
  public toggleShuffle() {
    if (!this.isShuffling) {
      this.shuffledTracks = this.getShuffledTracks(this.internalTracks)
    }

    this.isShuffling = !this.isShuffling
  }

  public get first() {
    return this.tracks[0]
  }

  public get next() {
    if (this.tracks.length < this.index + 2) return undefined
    return this.tracks[this.index + 1]
  }

  public get previous() {
    return this.tracks[this.index - 1]
  }

  public get last() {
    return this.tracks[this.tracks.length - 1]
  }

  public fetchIfNecessary() {
    if (!this.list || this.list.status !== FetcherListStatus.Idle) return

    const handleFetch = () => {
      this.fetchIfNecessary()
    }

    if (this.indexFromEnd < FETCH_BUFFER && this.list.hasNext) {
      this.list.fetch().then(handleFetch)
    }
  }

  @computed
  public get tracks() {
    return this.isShuffling ? this.shuffledTracks : this.internalTracks
  }

  @computed
  public get index() {
    return this.tracks.findIndex((track) => track.isActive)
  }

  @computed
  public get indexFromEnd() {
    return this.tracks.length - this.index - 1
  }

  @computed
  public get metadata() {
    const { name, clientLink } = this
    if (!this.list) return undefined

    return {
      name: name!,
      clientLink: clientLink!,
    }
  }

  /** Save the queue state to localStorage */
  @bind
  public saveState() {
    if (!this.list || !this.metadata) return

    const { name, clientLink } = this.metadata
    const { path, params } = this.list

    const data = {
      name,
      clientLink,
      path,
      params,
      index: this.index,
      offset: this.list.offset,
      isShuffling: this.isShuffling,
    }

    const tracks = this.internalTracks.map((track) => track.rawData)

    queueTracksStorage.save(tracks)
    queueStorage.save(data)
  }

  /** Restore queue state from localStorage */
  public restoreState() {
    const data = queueStorage.restore()
    const tracks = queueTracksStorage.restore()

    if (!data) return

    const { trackStore } = this.manager.stores
    const { path, offset, clientLink, name, params, isShuffling } = data

    const list = new FetcherList(
      {
        store: trackStore,
        items: tracks,
        limit: 10,
        offset,
        params,
        path,
      },
      this.manager,
    )

    this.isShuffling = isShuffling

    this.setQueueData({ list, clientLink, name })
    this.fetchIfNecessary()
  }

  public async init() {
    this.restoreState()
  }
}

export const queueStore = createStoreFactory(QueueStore)
