import { MediaModel } from "../../../common/media/models/Media"
import {
  AudioReferenceData,
  AudioSource,
} from "../../../common/media/types/AudioReferenceData"

export declare class AudioModel extends MediaModel<
  AudioReferenceData,
  AudioSource
> {}
exports.AudioModel = MediaModel
