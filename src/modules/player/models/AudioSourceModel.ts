import { SourceModel } from "../../../common/media/models/Source"
import { AudioSource } from "../../../common/media/types/AudioReferenceData"

export declare class AudioSourceModel extends SourceModel<AudioSource> {}
exports.AudioSourceModel = SourceModel
