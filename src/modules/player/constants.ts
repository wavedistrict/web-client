export const MIME_MAP = {
  "audio/ogg; codecs=opus": ["audio/ogg; codecs=opus", "audio/opus"],
  "audio/flac": ["audio/flac"],
  "audio/mpeg": ["audio/mpeg", "audio/mp3"],
}

export const VOLUME_SCALING_FACTOR = 5

export const TARGET_LUFS = -11

export const PLAYER_CONTROLS_HEIGHT = 56
