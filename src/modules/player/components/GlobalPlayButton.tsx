import React from "react"
import { PlayButton } from "./PlayButton"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

export type GlobalPlayButtonProps = {
  size?: "small" | "big"
}

export function GlobalPlayButton(props: GlobalPlayButtonProps) {
  const { size } = props
  const { playerStore } = useStores()

  return useObserver(() => {
    const { isPlaying, isBuffering } = playerStore

    return (
      <PlayButton
        active={true}
        size={size}
        playing={isPlaying}
        buffering={isBuffering}
        onClick={() => playerStore.toggle()}
      />
    )
  })
}
