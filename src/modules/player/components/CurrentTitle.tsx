import React from "react"
import { TrackModel } from "../../track/models/Track"
import { useTimestamps } from "../../track/hooks/useTimestamps"

export interface CurrentTitleProps {
  track: TrackModel
}

export function CurrentTitle(props: CurrentTitleProps) {
  const { track } = props
  const { current } = useTimestamps(track)

  return <>{current ? current.label : track.data.title}</>
}
