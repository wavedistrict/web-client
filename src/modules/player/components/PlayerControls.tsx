import React from "react"
import { PlayerBar } from "./PlayerBar"
import { PlayerPanel } from "./PlayerPanel/PlayerPanel"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"
import { useMediaQuery } from "../../../common/ui/react/hooks/useMediaQuery"

const breakpoint = 1182

export function PlayerControls() {
  const { playerStore } = useStores()
  const [mobile] = useMediaQuery(`(max-width: ${breakpoint}px)`)

  return useObserver(() => {
    if (!playerStore.track) return null

    if (mobile) {
      return <PlayerPanel track={playerStore.track} />
    } else {
      return <PlayerBar />
    }
  })
}
