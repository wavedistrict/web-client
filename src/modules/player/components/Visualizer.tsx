import { transparentize } from "polished"
import React, { useEffect, useRef } from "react"
import { makeLogarithmic } from "../../../common/lang/array/helpers/makeLogarithmic"
import { resizeArray } from "../../../common/lang/array/helpers/resizeArray"
import { smoothArray } from "../../../common/lang/array/helpers/smoothArray"
import { getSmoothedAverage } from "../../../common/lang/number/helpers/getSmoothedAverage"
import {
  useCanvasAnimation,
  CanvasAnimationHandler,
} from "../../../common/ui/react/hooks/useCanvasAnimation"
import { useWindowEvent } from "../../../common/ui/react/hooks/useWindowEvent"
import { useSetting } from "../../settings/hooks/useSetting"
import { getDuration } from "../../theme/helpers"
import { styled, useTheme } from "../../theme/themes"
import { useStores } from "../../../common/state/hooks/useStores"

const Container = styled.canvas<{
  disabled: boolean
}>`
  width: 100%;
  height: 200px;

  transition: ${getDuration("normal")} ease all;

  ${(props) =>
    props.disabled &&
    `
    opacity: 0;
  `}
`

const DIFFERENCE_POWER = 0.8

// most tracks cut unnecessary frequencies
const LOG_SLICE = 80
const LINEAR_SLICE = 180

export function Visualizer() {
  const { playerStore } = useStores()

  const { current: differenceArray } = useRef<number[]>([])
  const { current: valueArray } = useRef<number[]>([])
  const { fontColors } = useTheme()

  const enabled = useSetting("audio", "visualizer")
  const barWidth = useSetting("audio", "visualizerBarWidth")
  const barSpacing = useSetting("audio", "visualizerBarSpacing")
  const stereo = useSetting("audio", "visualizerStereo")
  const valueSmoothing = useSetting("audio", "visualizerSpeed")
  const logarithmic = useSetting("audio", "visualizerLog")

  const minimumBarHeight = 2
  const valueMultiplier = logarithmic ? 0.8 : 1

  const transformFrequencies = (rawFrequencies: Uint8Array, size: number) => {
    const frequencies = logarithmic
      ? makeLogarithmic(rawFrequencies).slice(0, -LOG_SLICE)
      : rawFrequencies.slice(0, -LINEAR_SLICE)

    const resizedFreqs = resizeArray(frequencies, size)
    const smoothedFreqs = smoothArray(resizedFreqs, 10)

    return smoothedFreqs
  }

  const frame: CanvasAnimationHandler = (context, canvas) => {
    const { frequencies } = playerStore
    const { width, height } = canvas

    context.clearRect(0, 0, width, height)

    const barCount = Math.floor(width / (barWidth + barSpacing))

    const left = transformFrequencies(frequencies.left, barCount)
    const right = transformFrequencies(frequencies.right, barCount)

    for (let i = 0, length = left.length; i < length; i++) {
      const leftValue = left[i]
      const rightValue = right[i]

      const value = getSmoothedAverage(
        valueArray[i] || 0,
        (leftValue + rightValue) / (255 * 2),
        valueSmoothing,
      )

      valueArray[i] = value

      const difference = getSmoothedAverage(
        differenceArray[i] || 0,
        Math.abs(leftValue - rightValue) / 255,
        Math.max(valueSmoothing, 0.8),
      )

      differenceArray[i] = difference

      const valueHeight = value * valueMultiplier * height
      const differenceHeight = difference ** DIFFERENCE_POWER * height

      const barPosX = i * (barSpacing + barWidth)

      if (stereo) {
        const finalValueHeight =
          Math.max(0, valueHeight - differenceHeight) + minimumBarHeight

        context.fillStyle = fontColors.normal
        context.fillRect(barPosX, height, barWidth, -finalValueHeight)

        const bottomDifferenceHeight = height - finalValueHeight
        const topDifferenceHeight = differenceHeight

        context.fillStyle = transparentize(0.5, fontColors.normal)

        context.fillRect(
          barPosX,
          bottomDifferenceHeight,
          barWidth,
          -topDifferenceHeight,
        )
      } else {
        const finalValueHeight = valueHeight + minimumBarHeight

        context.fillStyle = fontColors.normal
        context.fillRect(barPosX, height, barWidth, -finalValueHeight)
      }
    }
  }

  const [canvasRef, canvas] = useCanvasAnimation(frame, enabled)

  const setCanvasSize = () => {
    if (canvas) {
      canvas.width = canvas.offsetWidth
    }
  }

  useWindowEvent("resize", setCanvasSize)
  useEffect(setCanvasSize)

  return <Container disabled={!enabled} ref={canvasRef} />
}
