import { css } from "@emotion/core"
import { size } from "polished"
import React from "react"
import { Icon } from "../../../common/ui/icon/components/Icon"
import { getDuration, invertLightness } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { spinClockwise } from "../../theme/animations"
import { IconType } from "../../../common/ui/icon"

interface PlayButtonProps {
  active: boolean
  playing: boolean
  buffering?: boolean
  disabled?: boolean
  size?: "big" | "small"
  onClick: () => void
  onMouseOver?: () => void
}

const Container = styled.button`
  pointer-events: none;
`

const Content = styled.span<{
  active: boolean
  disabled?: boolean
  size: number
}>`
  ${({ size: s }) => size(s)};

  background: ${({ active, theme }) => {
    if (active) return theme.fontColors.normal
    return theme.transparencies.weakPositive
  }};

  border-radius: 100%;
  overflow: hidden;
  position: relative;

  display: flex;
  justify-content: center;
  align-items: center;

  transition: all ${getDuration("normal")} ease;
  outline: none;
  cursor: pointer;

  pointer-events: all;

  ${({ active, theme }) =>
    !active &&
    css`
      &:hover {
        background: ${theme.transparencies.strongPositive};
      }

      &:active {
        background: ${theme.transparencies.weakPositive};
      }
    `}

  ${(props) =>
    props.disabled &&
    `
    opacity: 0.4;
    pointer-events: none;
  `}
`

const Glyph = styled<typeof Icon, { size: number; active: boolean }>(Icon)`
  ${({ size: s }) => size(Math.floor(s / 2.5))}

  fill: ${({ active, theme }) =>
    active
      ? invertLightness(theme.fontColors.normal)
      : theme.fontColors.normal};
`

const Spinner = styled.div<{ size: number; active: boolean }>`
  ${({ size: s }) => size(Math.floor(s / 2.5))};

  border-radius: 100%;
  border-style: solid;
  border-color: transparent;

  ${({ active, theme }) => {
    const { normal } = theme.fontColors

    const color = active ? invertLightness(normal) : normal

    return css`
      border-top-color: ${color};
      border-bottom-color: ${color};
    `
  }}

  animation: ${spinClockwise} 600ms linear forwards;
  animation-iteration-count: infinite;

  box-sizing: content-box;
`

export function PlayButton(props: PlayButtonProps) {
  const { size, onClick, onMouseOver, ...rest } = props

  const sizesMap = {
    big: 42,
    small: 32,
  }

  const finalSize = size ? sizesMap[size] : 39

  const renderGlyph = (name: IconType) => {
    return <Glyph name={name} active={props.active} size={finalSize} />
  }

  const renderContent = () => {
    const { playing, buffering } = props

    if (!playing) return renderGlyph("play")

    if (buffering) return <Spinner active={props.active} size={finalSize} />

    return renderGlyph("pause")
  }

  return (
    <Container onClick={onClick} onMouseOver={onMouseOver}>
      <Content size={finalSize} {...rest}>
        {renderContent()}
      </Content>
    </Container>
  )
}
