import { observer } from "mobx-react"
import React, { useEffect, useRef } from "react"
import {
  SliderInput,
  SliderInputRef,
} from "../../../common/ui/input/components/SliderInput"
import { getFontColor, getFontWeight, getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { TrackPlayTrigger } from "../../track/components/TrackPlayTrigger"
import { TrackModel } from "../../track/models/Track"
import { humanizeSeconds } from "../helpers/humanizeSeconds"
import { useStores } from "../../../common/state/hooks/useStores"

export interface SeekerProps {
  track: TrackModel
  accented?: boolean
}

const Container = styled.div`
  display: flex;

  font-size: 12px;
  font-weight: ${getFontWeight("bold")};
  color: ${getFontColor("normal")};

  > .input {
    padding: 0px ${getSpacing("4")}px;
    flex: 1;
  }
`

const Time = styled.div`
  font-variant-numeric: tabular-nums;
`

export const Seeker = observer(function Seeker(props: SeekerProps) {
  const { playerStore } = useStores()
  const { track, accented } = props

  const animateRef = useRef(0)

  const inputRef = useRef<SliderInputRef>(null)
  const currentTimeRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    const { current: timeElement } = currentTimeRef
    const { current: inputElement } = inputRef

    if (!track.isActive && timeElement && inputElement) {
      timeElement.innerHTML = "0:00"
      inputElement.setValues(0)
    }
  }, [track.isActive])

  useEffect(() => {
    const frame = (track: TrackModel) => {
      const { current: timeElement } = currentTimeRef
      const { current: inputElement } = inputRef

      const { currentTime } = playerStore

      if (!timeElement || !inputElement) {
        return
      }

      const progress = currentTime / track.data.duration
      const hhmmss = humanizeSeconds(currentTime)

      if (!inputElement.isGrabbing) {
        inputElement.setValues(progress)
        timeElement.innerHTML = hhmmss
      }

      animateRef.current = requestAnimationFrame(() => frame(track))
    }

    if (track.isActive && playerStore.isPlaying) {
      frame(track)
    } else {
      cancelAnimationFrame(animateRef.current)
    }
    return () => cancelAnimationFrame(animateRef.current)
  }, [track.isActive, playerStore.isPlaying, track, playerStore])

  const handleRelease = (value: number, click: () => void) => {
    const newTime = value * track.data.duration

    if (!track.isActive) click()
    playerStore.currentTime = newTime
  }

  const handleMove = (value: number) => {
    const { current: timeElement } = currentTimeRef
    if (!timeElement) return

    const newTime = value * track.data.duration
    timeElement.innerHTML = humanizeSeconds(newTime)
  }

  const initialTime = track.isActive ? playerStore.currentTime : 0

  return (
    <TrackPlayTrigger track={track}>
      {(triggerProps) => (
        <Container onMouseOver={triggerProps.hover}>
          <Time ref={currentTimeRef}>{humanizeSeconds(initialTime)}</Time>
          <div className="input">
            <SliderInput
              onRelease={(v) => handleRelease(v, triggerProps.click)}
              onMove={handleMove}
              fillValue={initialTime / track.data.duration}
              ref={inputRef}
              accented={accented}
              disabled={!triggerProps.isPlayable}
            />
          </div>
          <Time className="duration">
            {humanizeSeconds(track.data.duration)}
          </Time>
        </Container>
      )}
    </TrackPlayTrigger>
  )
})
