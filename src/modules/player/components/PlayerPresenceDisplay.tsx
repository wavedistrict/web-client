import { observer } from "mobx-react"
import { ellipsis, size } from "polished"
import React from "react"
import { RouteLink } from "../../../common/routing/components/RouteLink"
import {
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { styled } from "../../theme/themes"
import { TrackArtwork } from "../../track/components/TrackArtwork"
import { TrackModel } from "../../track/models/Track"

export interface PlayerPresenceDisplayProps {
  track: TrackModel
}

const Container = styled.div`
  display: flex;

  > .artwork {
    ${size(42)};
    ${imageContainer(3)};

    flex-shrink: 0;
    margin-right: ${getSpacing("3")}px;
  }

  > .content {
    flex: 1;
    width: 0;

    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }

  > .content > .title {
    margin-bottom: ${getSpacing("1")}px;
    font-weight: ${getFontWeight("bold")};

    ${ellipsis()}
  }

  > .content > .author {
    font-size: ${getFontSize("1")}px;
    color: ${getFontColor("muted")};
  }
`

export const PlayerPresenceDisplay = observer(function PlayerPresenceDisplay(
  props: PlayerPresenceDisplayProps,
) {
  const { track } = props
  const { user } = track

  return (
    <Container>
      <RouteLink to={track.clientLink} className="artwork">
        <TrackArtwork id="PlayerPresenceDispaly" track={track} />
      </RouteLink>
      <div className="content">
        <RouteLink to={track.clientLink} className="title">
          {track.data.title}
        </RouteLink>
        <RouteLink to={user.clientLink} className="author">
          {user.data.displayName}
        </RouteLink>
      </div>
    </Container>
  )
})
