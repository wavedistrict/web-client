import css from "@emotion/css"
import { size } from "polished"
import { PLAYER_CONTROLS_HEIGHT } from "../constants"
import { styled } from "../../theme/themes"
import { Button } from "../../../common/ui/button/components/Button"
import { getDuration } from "../../theme/helpers"

export type PlayerButtonVariants = {
  flipped?: boolean
  active?: boolean
  bigger?: boolean
}

export const PlayerButton = styled<typeof Button, PlayerButtonVariants>(Button)`
  ${size(PLAYER_CONTROLS_HEIGHT)};

display: flex;
justify-content: center;
align-items: center;

outline: none;

transition: ${getDuration("normal")} ease;
transition-property: opacity, background-color, transform;

opacity: 0.5;

> .icon {
  ${size(24)};

    transition: ${getDuration("normal")} ease fill;

}

&:hover {
  opacity: 1;
  cursor: pointer;
}

${({ theme, active }) =>
  active &&
  css`
    opacity: 1;

    svg {
      fill: ${theme.colors.accent};
    }
  `}

${({ flipped }) =>
  flipped &&
  css`
    transform: rotate(180deg);
  `}

${({ bigger }) =>
  bigger &&
  css`
    > .icon {
      ${size(32)};
    }
  `}
`
