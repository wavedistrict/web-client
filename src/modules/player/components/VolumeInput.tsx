import { observer } from "mobx-react"
import React, { useEffect, useRef } from "react"
import {
  SliderInput,
  SliderInputRef,
} from "../../../common/ui/input/components/SliderInput"
import { styled } from "../../theme/themes"
import { PlayerButton } from "./PlayerButton"
import { useStores } from "../../../common/state/hooks/useStores"

const Container = styled.div`
  display: flex;
  align-items: center;

  > .input {
    flex: 1;
  }
`

export const VolumeInput = observer(function VolumeInput() {
  const { playerStore } = useStores()
  const { volume, isMuted } = playerStore

  const inputRef = useRef<SliderInputRef>(null)

  useEffect(() => {
    const { current: input } = inputRef

    if (input && !input.isGrabbing) {
      input.setValues(volume, undefined, true)
    }
  }, [volume])

  const onClick = () => {
    playerStore.toggleMute()
  }

  const onChange = (value: number) => {
    playerStore.setVolume(value)
  }

  return (
    <Container>
      <div className="button">
        <PlayerButton
          icon={isMuted ? "speakerMute" : "speaker"}
          onClick={onClick}
        />
      </div>
      <div className="input">
        <SliderInput ref={inputRef} fillValue={volume} onChange={onChange} />
      </div>
    </Container>
  )
})
