import React from "react"
import { RouteLink } from "../../../../common/routing/components/RouteLink"
import {
  getColor,
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { QueueList } from "./QueueList"
import { useStores } from "../../../../common/state/hooks/useStores"

const Header = styled.div`
  padding: ${getSpacing("4")}px;
  border-bottom: solid 1px ${getColor("divider")};

  > .title {
    font-weight: ${getFontWeight("bold")};
    font-size: ${getFontSize("3")}px;
  }

  > .name {
    display: inline-block;
    margin-top: ${getSpacing("2")}px;

    font-size: ${getFontSize("1")}px;
    color: ${getFontColor("muted")};
  }
`

const List = styled.div`
  margin-top: 0px;

  min-height: 500px;
  height: calc(100vh - 500px);

  width: 372px;
`

export function QueuePopover() {
  const { queueStore } = useStores()

  const { clientLink, name } = queueStore.metadata || {
    clientLink: "/",
    name: "nowhere",
  }

  return (
    <div>
      <Header>
        <h1 className="title">Queue</h1>
        <RouteLink className="name" to={clientLink}>
          Playing from {name}
        </RouteLink>
      </Header>
      <List>
        <QueueList />
      </List>
    </div>
  )
}
