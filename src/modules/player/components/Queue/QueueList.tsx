import React from "react"
import { EmptyState } from "../../../../common/state/components/EmptyState"
import { FetcherListCursor } from "../../../../common/ui/list/components/FetcherListCursor"
import { FetcherListTail } from "../../../../common/ui/list/components/FetcherListTail"
import { createVirtualizedListComponent } from "../../../../common/ui/list/helpers/createVirtualizedListComponent"
import { wrapRowInLi } from "../../../../common/ui/list/helpers/wrapRowInLi"
import { TrackModel } from "../../../track/models/Track"
import { QueueListItem, QUEUE_LIST_ITEM_HEIGHT } from "./QueueListItem"
import { useStores } from "../../../../common/state/hooks/useStores"

const List = createVirtualizedListComponent<TrackModel>({
  rowHeight: QUEUE_LIST_ITEM_HEIGHT,
  renderItem: wrapRowInLi((track, index) => (
    <QueueListItem index={index} track={track} />
  )),
})

export function QueueList() {
  const { queueStore } = useStores()

  if (!queueStore.list) {
    return <EmptyState icon="queue" message="Nothing in the queue" />
  }

  return (
    <FetcherListCursor tail="bottom" list={queueStore.list}>
      {({ onScroll }) => (
        <>
          <List
            items={queueStore.tracks}
            scrollToIndex={queueStore.index}
            scrollToAlignment="start"
            onScroll={onScroll}
          />
          <FetcherListTail absolute="bottom" list={queueStore.list!} />
        </>
      )}
    </FetcherListCursor>
  )
}
