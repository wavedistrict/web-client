import React from "react"
import { bind } from "../../../../common/lang/function/helpers/bind"
import {
  PopoverChildrenFunctionProps,
  PopoverTrigger,
} from "../../../popover/components/PopoverTrigger"
import { PlayerButton } from "../PlayerButton"
import { QueuePopover } from "./QueuePopover"

export interface QueueButtonProps {}

export class QueueButton extends React.Component<QueueButtonProps> {
  @bind
  public renderPopoverContent() {
    return <QueuePopover />
  }

  @bind
  public renderTriggerContent(props: PopoverChildrenFunctionProps) {
    return (
      <PlayerButton
        icon="queue"
        active={props.isActive}
        onClick={props.toggle}
        ref={props.ref}
      />
    )
  }

  public render() {
    return (
      <PopoverTrigger placement="top-end">
        {this.renderTriggerContent}
        {this.renderPopoverContent}
      </PopoverTrigger>
    )
  }
}
