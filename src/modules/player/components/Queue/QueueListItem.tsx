import { observer } from "mobx-react"
import React from "react"
import {
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
} from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { TrackPlayTrigger } from "../../../track/components/TrackPlayTrigger"
import { TrackModel } from "../../../track/models/Track"
import { useStores } from "../../../../common/state/hooks/useStores"

export interface QueueListItemProps {
  track: TrackModel
  index: number
}

export const QUEUE_LIST_ITEM_HEIGHT = 72

const Container = styled.article<{
  active: boolean
  unplayable: boolean
  passed: boolean
}>`
  padding: ${getSpacing("4")}px;
  transition: ${getDuration("normal")} ease all;

  > .title,
  > .author {
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;

    font-size: ${getFontSize("1")}px;
  }

  > .title {
    font-weight: ${getFontWeight("bold")};
  }

  > .author {
    margin-top: ${getSpacing("2")}px;
  }

  &:hover {
    cursor: pointer;
    opacity: 1;
    background: ${getTransparency("weakNegative")};
  }

  ${(props) =>
    props.active &&
    `
    opacity: 1;
    color: ${props.theme.colors.accent};
  `}

  ${(props) =>
    props.unplayable &&
    `
    opacity: 0.2;
    pointer-events: none;
  `}

  ${(props) =>
    props.passed &&
    `
    opacity: 0.6;
  `}

`

export const QueueListItem = observer(function QueueListItem(
  props: QueueListItemProps,
) {
  const { queueStore } = useStores()
  const { track, index } = props

  const passed = queueStore.index > index

  return (
    <TrackPlayTrigger track={track}>
      {({ click, isActive, isPlayable }) => {
        return (
          <Container
            onClick={click}
            passed={passed}
            active={isActive}
            unplayable={!isPlayable}
          >
            <h1 className="title">{track.data.title}</h1>
            <h2 className="author">{track.data.user.displayName}</h2>
          </Container>
        )
      }}
    </TrackPlayTrigger>
  )
})
