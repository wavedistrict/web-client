import { observer } from "mobx-react"
import React from "react"
import { bind } from "../../../common/lang/function/helpers/bind"
import { PopoverMenu } from "../../popover/components/PopoverMenu"
import {
  PopoverChildrenFunctionProps,
  PopoverTrigger,
} from "../../popover/components/PopoverTrigger"
import { RenderPopoverContentProps } from "../../popover/types/PopoverData"
import { TrackModel } from "../../track/models/Track"
import { compatibleMimes } from "../helpers/getCompatibleCodecs"
import { AudioSourceModel } from "../models/AudioSourceModel"
import { PlayerButton } from "./PlayerButton"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

export type QualityButtonProps = {
  track: TrackModel
}

export type RenderSourceOptions = {
  source: AudioSourceModel
  probably: boolean
  active: boolean
  close: () => void
}

export function QualityButton(props: QualityButtonProps) {
  const { track } = props
  const { playerStore } = useStores()

  const renderSource = (options: RenderSourceOptions) => {
    const { source, probably, active, close } = options
    const { qualityRating, name } = source.data

    const text = probably ? (
      source.humanizedName
    ) : (
      <em>{source.humanizedName}</em>
    )

    return (
      <PopoverMenu.Item
        key={name}
        isActive={active}
        onClick={() => {
          playerStore.setQuality(qualityRating)
          close()
        }}
      >
        {text}
      </PopoverMenu.Item>
    )
  }

  const renderPopoverContent = (props: RenderPopoverContentProps) => {
    const reversedSources = [...track.audio.sources]
      .reverse()
      .filter(
        ({ data }) =>
          Object.keys(compatibleMimes).includes(data.mime) && data.streamable,
      )

    const renderedItems = reversedSources.map((source) => {
      const { mime } = source.data

      const probably = compatibleMimes[mime] === "probably"
      const active = playerStore.source! === source

      return renderSource({
        close: props.close,
        probably,
        active,
        source,
      })
    })

    return <PopoverMenu>{renderedItems}</PopoverMenu>
  }

  const renderTriggerContent = (props: PopoverChildrenFunctionProps) => {
    return (
      <PlayerButton
        ref={props.ref}
        icon="cog"
        onClick={props.toggle}
        active={props.isActive}
      />
    )
  }

  return useObserver(() => (
    <PopoverTrigger placement="top-end">
      {renderTriggerContent}
      {renderPopoverContent}
    </PopoverTrigger>
  ))
}
