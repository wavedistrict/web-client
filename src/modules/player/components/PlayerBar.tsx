import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { RouteLink } from "../../../common/routing/components/RouteLink"
import {
  getColor,
  getDuration,
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { styled } from "../../theme/themes"
import { TrackArtwork } from "../../track/components/TrackArtwork"
import { PLAYER_CONTROLS_HEIGHT } from "../constants"
import { GlobalPlayButton } from "./GlobalPlayButton"
import { QualityButton } from "./QualityButton"
import { QueueButton } from "./Queue/QueueButton"
import { RepeatButton } from "./RepeatButton"
import { Seeker } from "./Seeker"
import { ShuffleButton } from "./ShuffleButton"
import { SkipButton } from "./SkipButton"
import { VolumeInput } from "./VolumeInput"
import { CurrentTitle } from "./CurrentTitle"
import { useStores } from "../../../common/state/hooks/useStores"

const Container = styled.div`
  height: ${PLAYER_CONTROLS_HEIGHT}px;
  padding-right: ${getSpacing("4")}px;

  position: fixed;
  left: 0px;
  right: 0px;
  bottom: 0px;
  z-index: 1;

  transform: translateZ(0);

  border-top: solid 1px ${getColor("divider")};
  background: ${getColor("primary")};

  display: flex;
  align-items: center;

  animation: slide-up ${getDuration("normal")} ease;

  > .transport {
    display: flex;
    align-items: center;
  }

  > .secondary {
    display: flex;
  }

  > .seeker {
    min-width: 300px;
    margin: 0px ${getSpacing("4")}px;
    flex: 1;
  }

  > .volume {
    flex-shrink: 0;
    width: 120px;
    margin-right: ${getSpacing("5")}px;
  }

  > .summary {
    display: flex;
    align-items: center;

    height: ${PLAYER_CONTROLS_HEIGHT}px;

    border-left: solid 1px ${getColor("divider")};
    border-right: solid 1px ${getColor("divider")};

    padding: 0px ${getSpacing("4")}px;
  }

  > .summary > .artwork {
    ${imageContainer(3)}
    ${size(38)};
    flex-shrink: 0;
  }

  > .summary > .info {
    margin-right: ${getSpacing("4")}px;
    text-align: left;
  }

  > .summary > .info > .title {
    display: block;

    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("bold")};

    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

    width: 230px;
  }

  > .summary > .info > .type {
    font-size: ${getFontSize("1")}px;
    color: ${getFontColor("muted")};

    margin-top: ${getSpacing("2")}px;
  }

  > .actions {
    display: flex;
  }
`

export const PlayerBar = observer(function PlayerBar() {
  const { playerStore } = useStores()
  const { track } = playerStore

  if (!track) throw new Error("Track undefined in PlayerBar")

  const { type } = track.data

  return (
    <Container>
      <div className="transport">
        <SkipButton direction="previous" />
        <GlobalPlayButton size="small" />
        <SkipButton direction="next" />
      </div>
      <div className="secondary">
        <RepeatButton />
        <ShuffleButton />
      </div>
      <div className="seeker">
        <Seeker accented track={track} />
      </div>
      <div className="volume">
        <VolumeInput />
      </div>
      <div className="summary">
        <div className="info">
          <RouteLink to={track.clientLink} className="title">
            <CurrentTitle track={track} />
          </RouteLink>
          <div className="type">{type}</div>
        </div>
        <RouteLink to={track.clientLink} className="artwork">
          <TrackArtwork id="PlayerBar" track={track} />
        </RouteLink>
      </div>
      <div className="actions">
        <QualityButton track={track} />
        <QueueButton />
      </div>
    </Container>
  )
})
