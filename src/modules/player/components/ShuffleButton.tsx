import React from "react"
import { PlayerButton, PlayerButtonVariants } from "./PlayerButton"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

export function ShuffleButton(props: PlayerButtonVariants) {
  const { queueStore } = useStores()

  return useObserver(() => (
    <PlayerButton
      title="Shuffle"
      onClick={queueStore.toggleShuffle}
      icon="shuffle"
      active={queueStore.isShuffling}
      {...props}
    />
  ))
}
