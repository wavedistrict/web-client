import React from "react"
import { getValueFromMap } from "../../../common/lang/object/helpers/getValueFromMap"
import { RepeatState } from "../classes/PlayerScheduler"
import { PlayerButton } from "./PlayerButton"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

export function RepeatButton() {
  const { playerStore } = useStores()

  const titleMap = {
    [RepeatState.NoRepeat]: "Not repeating",
    [RepeatState.RepeatQueue]: "Repeating queue",
    [RepeatState.RepeatTrack]: "Repeating track",
  }

  const icon = useObserver(() =>
    playerStore.repeatState !== RepeatState.RepeatTrack
      ? "repeat"
      : "repeatOne",
  )

  const active = useObserver(
    () => playerStore.repeatState !== RepeatState.NoRepeat,
  )

  return (
    <PlayerButton
      title={getValueFromMap(titleMap, playerStore.repeatState, "")}
      active={active}
      onClick={playerStore.cycleRepeatState}
      icon={icon}
    />
  )
}
