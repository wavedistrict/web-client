import { observer } from "mobx-react"
import { size } from "polished"
import React, { useEffect, useState } from "react"
import {
  LocalTabItem,
  LocalTabs,
} from "../../../../common/ui/navigation/components/LocalTabs"
import { Overlay } from "../../../theme/components/Overlay"
import {
  getColor,
  getDuration,
  getFontColor,
  getFontSize,
  getFontWeight,
} from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { TrackModel } from "../../../track/models/Track"
import { PLAYER_CONTROLS_HEIGHT } from "../../constants"
import { GlobalPlayButton } from "../GlobalPlayButton"
import { PlayerButton } from "../PlayerButton"
import { QueueList } from "../Queue/QueueList"
import { NowPlaying } from "./NowPlaying"
import { CurrentTitle } from "../CurrentTitle"
import { useStores } from "../../../../common/state/hooks/useStores"

export interface PlayerPanel {
  track: TrackModel
}

const Container = styled.div<{ open: boolean }>`
  height: 100%;

  position: fixed;
  left: 0px;
  right: 0px;
  bottom: 0px;
  z-index: 2;

  transform: translateZ(0) translateY(calc(100% - ${PLAYER_CONTROLS_HEIGHT}px));

  ${(props) =>
    props.open &&
    `
    border-top: none;
    transform: translateZ(0) translateY(0);
  `}

  border-top: solid 1px ${getColor("divider")};
  background: ${getColor("primary")};

  transition: transform ${getDuration("extended")} ease;

  > .toggle {
    z-index: 6;
    position: absolute;
    top: 0px;
    right: 0px;

    border-left: solid 1px ${getColor("divider")};
  }

  > .headers {
    position: relative;
    z-index: 5;

    padding-right: ${PLAYER_CONTROLS_HEIGHT}px;
    height: ${PLAYER_CONTROLS_HEIGHT}px;

    display: flex;
    align-items: center;

    border-bottom: solid 1px ${getColor("divider")};
    background: ${(props) =>
      props.open ? "transparent" : props.theme.colors.primary};

    transition: background ${getDuration("extended")} ease;
  }

  > .body {
    height: calc(100% - ${PLAYER_CONTROLS_HEIGHT}px);
  }
`

const ClosedHeader = styled.div<{ open: boolean }>`
  display: flex;
  align-items: center;
  width: 100%;

  pointer-events: ${(props) => (props.open ? "none" : "all")};
  opacity: ${(props) => (props.open ? 0 : 1)};

  position: absolute;
  height: ${PLAYER_CONTROLS_HEIGHT}px;
  transition: opacity ${getDuration("extended")} ease;

  > .playButton {
    ${size(PLAYER_CONTROLS_HEIGHT)};

    display: flex;
    justify-content: center;
    align-items: center;
  }

  > .summary {
    display: block;
    font-size: ${getFontSize("1")}px;

    white-space: nowrap;

    width: calc(100% - 164px);
  }

  > .summary > .title {
    overflow: hidden;
    text-overflow: ellipsis;
    font-weight: ${getFontWeight("bold")};
  }

  > .summary > .type {
    color: ${getFontColor("muted")};
  }
`

const OpenHeader = styled.div<{ open: boolean }>`
  display: flex;
  align-items: flex-end;

  pointer-events: none;

  pointer-events: ${(props) => (props.open ? "all" : "none")};
  opacity: ${(props) => (props.open ? 1 : 0)};

  position: absolute;
  height: ${PLAYER_CONTROLS_HEIGHT}px;
  transition: opacity ${getDuration("extended")} ease;
`

export const PlayerPanel = observer(function PlayerPanel(props: PlayerPanel) {
  const { uiStore } = useStores()
  const { track } = props

  const { playerPanelOpen: open } = uiStore
  const [currentTab, setCurrentTab] = useState("nowPlaying")

  useEffect(() => {
    return () => {
      uiStore.playerPanelOpen = false
    }
  }, [uiStore])

  const tabs: Record<string, LocalTabItem> = {
    nowPlaying: {
      icon: "note",
      renderContent: () => <NowPlaying track={track} />,
    },
    queue: {
      icon: "queue",
      renderContent: () => <QueueList />,
    },
  }

  const wrapInOverlay = (element: JSX.Element) => {
    if (currentTab !== "nowPlaying") return element

    return <Overlay>{element}</Overlay>
  }

  return (
    <LocalTabs
      variants={["borderless", "icon-only"]}
      tabs={tabs}
      onChange={setCurrentTab}
    >
      {(props) =>
        wrapInOverlay(
          <Container open={open}>
            <div className="headers">
              <ClosedHeader open={open}>
                <div className="playButton">
                  <GlobalPlayButton size="small" />
                </div>
                <div className="summary">
                  <h1 className="title">
                    <CurrentTitle track={track} />
                  </h1>
                  <h2 className="type">{track.data.type}</h2>
                </div>
              </ClosedHeader>
              <OpenHeader open={open}>
                <div className="tabs">{props.tabs}</div>
              </OpenHeader>
            </div>
            <div className="body">{props.content}</div>
            <div className="toggle">
              <PlayerButton
                icon="chevronUp"
                onClick={() => (uiStore.playerPanelOpen = !open)}
                flipped={open}
              />
            </div>
          </Container>,
        )
      }
    </LocalTabs>
  )
})
