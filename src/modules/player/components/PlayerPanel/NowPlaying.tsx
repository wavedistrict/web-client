import { observer } from "mobx-react"
import { cover, size } from "polished"
import React from "react"
import { ImageBackground } from "../../../../common/ui/image/components/ImageBackground"
import { toggleItemFavorite } from "../../../favoriting/actions/toggleItemFavorite"
import { getFontColor, getFontWeight, getSpacing } from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { styled } from "../../../theme/themes"
import { TrackArtwork } from "../../../track/components/TrackArtwork"
import { TrackModel } from "../../../track/models/Track"
import { PLAYER_CONTROLS_HEIGHT } from "../../constants"
import { GlobalPlayButton } from "../GlobalPlayButton"
import { PlayerButton } from "../PlayerButton"
import { RepeatButton } from "../RepeatButton"
import { Seeker } from "../Seeker"
import { ShuffleButton } from "../ShuffleButton"
import { SkipButton } from "../SkipButton"
import { CurrentTitle } from "../CurrentTitle"
import { useStores } from "../../../../common/state/hooks/useStores"
import { useManager } from "../../../../common/state/hooks/useManager"

export interface NowPlayingProps {
  track: TrackModel
}

const mq = `@media screen and (orientation: landscape)`

const Container = styled.div`
  height: 100%;
  position: relative;
`

const Background = styled.div`
  ${cover()};
  top: -${PLAYER_CONTROLS_HEIGHT}px;
`

const Foreground = styled.div`
  padding: ${getSpacing("5")}px;
  ${cover()};

  display: flex;
  flex-direction: column;
  justify-content: center;

  > .artwork {
    ${size(`calc(100vw - ${24 * 2}px)`)};
    ${imageContainer(3)};

    margin-bottom: ${getSpacing("6")}px;
  }

  ${mq} {
    flex-direction: row;

    > .artwork {
      ${size(`calc(100vh - ${PLAYER_CONTROLS_HEIGHT + 24 * 2}px)`)};

      margin-bottom: 0px;
      margin-right: ${getSpacing("5")}px;

      flex-shrink: 0;
    }
  }
`

const Content = styled.div`
  display: flex;
  flex-direction: column;

  ${mq} {
    flex-grow: 1;

    align-items: center;
    justify-content: center;

    width: 0%;
  }

  .primary {
    display: flex;
    align-items: center;
    justify-content: space-between;

    margin: 0px -${getSpacing("4")}px;
    margin-bottom: ${getSpacing("4")}px;

    ${mq} {
      width: 100%;
    }
  }

  > .primary > .summary {
    margin-top: ${getSpacing("2")}px;
    text-align: center;

    width: calc(100% - 115px);
    padding: 0px ${getSpacing("2")}px;

    ${mq} {
      max-width: 300px;
    }
  }

  > .primary > .summary > .title {
    font-weight: ${getFontWeight("bold")};

    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
  }

  > .primary > .summary > .type {
    margin-top: ${getSpacing("2")}px;
    color: ${getFontColor("muted")};
  }

  > .seeker {
    margin-bottom: ${getSpacing("4")}px;
    width: 100%;

    ${mq} {
      max-width: 400px;
    }
  }

  > .transport {
    display: flex;

    align-items: center;
    justify-content: space-between;

    margin: 0px -${getSpacing("4")}px;

    ${mq} {
      width: 100%;
      max-width: 330px;
    }
  }
`

export const NowPlaying = observer(function NowPlaying(props: NowPlayingProps) {
  const { routingStore, uiStore } = useStores()
  const manager = useManager()
  const { track } = props

  return (
    <Container>
      <Background>
        <ImageBackground
          reference={track.coverArtwork || track.artwork}
          hash={track.hash}
          blur={track.coverArtwork ? 0 : 7}
        />
      </Background>
      <Foreground>
        <div className="artwork">
          <TrackArtwork id="NowPlaying" track={track} />
        </div>
        <Content>
          <div className="primary">
            <PlayerButton
              icon={
                track.data.byCurrentUser.favorited ? "heartFilled" : "heart"
              }
              onClick={() => {
                toggleItemFavorite(manager, track)
              }}
            />
            <div className="summary">
              <h1 className="title">
                <CurrentTitle track={track} />
              </h1>
              <h2 className="type">{track.data.type}</h2>
            </div>
            <PlayerButton
              icon="trackFilled"
              onClick={() => {
                routingStore.push(track.clientLink)
                uiStore.playerPanelOpen = false
              }}
            />
          </div>
          <div className="seeker">
            <Seeker track={track} />
          </div>
          <div className="transport">
            <RepeatButton />
            <SkipButton bigger direction="previous" />
            <GlobalPlayButton size="big" />
            <SkipButton bigger direction="next" />
            <ShuffleButton />
          </div>
        </Content>
      </Foreground>
    </Container>
  )
})
