import React from "react"
import { PlayerButton, PlayerButtonVariants } from "./PlayerButton"
import { useStores } from "../../../common/state/hooks/useStores"

export type SkipButtonProps = PlayerButtonVariants & {
  direction: "previous" | "next"
}

export function SkipButton(props: SkipButtonProps) {
  const { playerStore } = useStores()
  const { direction, ...rest } = props

  const handleClick = () => {
    if (direction === "previous") return playerStore.previous()
    if (direction === "next") return playerStore.next()
  }

  return (
    <PlayerButton
      icon="skip"
      onClick={handleClick}
      flipped={direction === "previous"}
      {...rest}
    />
  )
}
