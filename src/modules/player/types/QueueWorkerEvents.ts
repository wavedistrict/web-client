import { TrackData } from "../../track/types/TrackData"

export interface QueueWorkerServiceEvents {
  shuffledTracks: {
    replace: boolean
    tracks: TrackData[]
  }
}

export interface QueueWorkerChannelEvents {
  shuffleTracks: {
    replace: boolean
    tracks: TrackData[]
  }
}
