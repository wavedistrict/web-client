import { FetcherList } from "../../../common/state/classes/FetcherList"
import { TrackModel } from "../../track/models/Track"

export interface QueueData {
  list: FetcherList<TrackModel>
  name: string
  clientLink: string
}
