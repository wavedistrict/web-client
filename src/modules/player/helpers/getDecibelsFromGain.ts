import { logTen } from "../../../common/math/helpers/logTen"

export const getDecibelsFromGain = (gain: number) => 20 * logTen(gain)
