import { MIME_MAP } from "../constants"
import { IS_SERVER } from "../../core/constants"

export type CompatibilityResult = Record<string, "maybe" | "probably">

export const getCompatibleMimes = (): CompatibilityResult => {
  if (IS_SERVER) return {}

  const audio = new Audio()
  const result: CompatibilityResult = {}

  for (const [key, mimes] of Object.entries(MIME_MAP)) {
    const testResult = mimes.map((mime) => audio.canPlayType(mime))

    if (testResult.includes("maybe")) {
      result[key] = "maybe"
    }

    if (testResult.includes("probably")) {
      result[key] = "probably"
    }
  }

  return result
}

export const compatibleMimes = getCompatibleMimes()
