import { getLastItem } from "../../../common/lang/array/helpers/getLastItem"
import { AudioModel } from "../models/AudioModel"
import { compatibleMimes } from "./getCompatibleCodecs"

export const getAudioSourceFromReference = (
  reference: AudioModel,
  quality: number,
) => {
  const { key } = reference.data

  const availableSources = reference.sources.filter(
    ({ data }) =>
      Object.keys(compatibleMimes).includes(data.mime) && data.streamable,
  )

  const preferredSource = availableSources.find(
    ({ data }) => data.qualityRating >= quality,
  )

  const source = preferredSource || getLastItem(availableSources)

  if (!source) {
    throw new Error(`MediaReference ${key} has no available sources!`)
  }

  return source
}
