export const getGainFromDecibels = (db: number) => Math.pow(10, db / 20)
