import { humanizeSeconds } from "./humanizeSeconds"

describe("convertSecondsToHHMMSS", () => {
  test("seconds", () => {
    expect(humanizeSeconds(30)).toBe("0:30")
  })

  test("minutes", () => {
    expect(humanizeSeconds(60 * 5)).toBe("5:00")
    expect(humanizeSeconds(60 * 20)).toBe("20:00")
  })

  test("hours", () => {
    expect(humanizeSeconds(60 * 60 * 5)).toBe("5:00:00")
    expect(humanizeSeconds(60 * 60 * 5 + 30)).toBe("5:00:30")
    expect(humanizeSeconds(60 * 60 * 5 + 60 * 30)).toBe("5:30:00")
  })
})
