export const humanizeSeconds = (totalSeconds: number) => {
  totalSeconds = totalSeconds || 0
  totalSeconds = Math.round(totalSeconds)
  const h1 = Math.floor(totalSeconds / (60 * 60))
  totalSeconds %= 60 * 60
  const m1 = Math.floor(totalSeconds / 60)
  totalSeconds %= 60
  const h2 = h1 ? h1 + ":" : ""
  const m2 = h1 && m1 < 10 ? "0" + m1 : m1
  const s2 = totalSeconds < 10 ? "0" + totalSeconds : totalSeconds
  return h2 + m2 + ":" + s2
}
