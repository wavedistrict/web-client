import { action, observable } from "mobx"
import React from "react"
import { createUniqueKey } from "../../../common/state/helpers/createUniqueKey"

export type ToastVariant = "info" | "error" | "warning" | "debug"

export interface ToastModelOptions {
  content: React.ReactNode
  duration?: number
  variant?: ToastVariant
  clickAction?: () => void
}

export class ToastModel {
  public id = createUniqueKey()
  @observable public duration: number
  @observable public paused = false

  public content: React.ReactNode
  public maxDuration: number
  public clickAction: () => void
  public variant: ToastVariant = "info"

  constructor(options: ToastModelOptions) {
    const {
      content,
      duration = 3000,
      clickAction = this.dismiss,
      variant = this.variant,
    } = options

    this.content = content
    this.duration = duration
    this.maxDuration = duration
    this.clickAction = clickAction
    this.variant = variant
  }

  @action
  public decreaseDuration(delta: number) {
    this.duration -= delta
  }

  @action.bound
  public dismiss() {
    this.duration = 0
  }

  @action.bound
  public pause() {
    this.paused = true
  }

  @action.bound
  public resume() {
    this.paused = false
  }
}
