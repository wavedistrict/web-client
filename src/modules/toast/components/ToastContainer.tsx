import { observer } from "mobx-react"
import React from "react"
import { ToastModel } from "../models/ToastModel"
import { Toast } from "./Toast"
import { TransitionStatus } from "react-transition-group/Transition"

/** Helper component for "connecting" a Toast element to a ToastModel */
export const ToastContainer = observer(
  ({
    toast,
    transitionStatus,
  }: {
    transitionStatus: TransitionStatus
    toast: ToastModel
  }) => (
    <Toast
      transitionStatus={transitionStatus}
      duration={toast.duration}
      maxDuration={toast.maxDuration}
      variant={toast.variant}
      children={toast.content}
      onClick={toast.clickAction}
      onMouseEnter={toast.pause}
      onMouseLeave={toast.resume}
    />
  ),
)
