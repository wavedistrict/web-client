import React from "react"
import { ToastVariant } from "../models/ToastModel"
import { styled } from "../../theme/themes"
import { getSpacing, getColor } from "../../theme/helpers"
import { getValueFromMap } from "../../../common/lang/object/helpers/getValueFromMap"
import { TransitionStatus } from "react-transition-group/Transition"
import { keyframes, css } from "@emotion/core"
import { Theme } from "../../theme/types/Theme"

export interface ToastProps extends React.HTMLAttributes<HTMLElement> {
  duration?: number
  maxDuration?: number
  variant: ToastVariant
  transitionStatus: TransitionStatus
}

export const TOAST_WIDTH = "200px"
export const TOAST_HEIGHT = "300px"

const animation = (theme: Theme) => keyframes`
  0% {
    transform: translateX(150%);
    max-height: 0px;
    margin-bottom: 0px;
    opacity: 0;
  }

  50% {
    transform: translateX(150%);
    max-height: ${TOAST_HEIGHT};
    margin-bottom: ${theme.spacings[6]}px;
    opacity: 0;
  }

  100% {
    transform: translateX(0%);
    max-height: ${TOAST_HEIGHT};
    margin-bottom: ${theme.spacings[6]}px;
  }
`

const Container = styled.div<{ status: TransitionStatus }>`
  position: relative;
  z-index: 10;

  margin-bottom: ${getSpacing("6")}px;

  ${(props) =>
    props.status === "entering" &&
    css`
      animation-name: ${animation(props.theme)};
      animation-duration: ${props.theme.durations.extended};
      animation-timing-function: ease;
      animation-fill-mode: forwards;
    `}

  ${(props) =>
    props.status === "exiting" &&
    css`
      animation-name: ${animation(props.theme)};
      animation-duration: ${props.theme.durations.extended};
      animation-timing-function: ease;
      animation-fill-mode: forwards;
      animation-direction: reverse;
    `}
`

const Content = styled.div`
  width: ${TOAST_WIDTH};
  position: relative;

  padding: ${getSpacing("4")}px;
  background: ${getColor("primary")};

  border: solid 1px ${getColor("divider")};
  border-radius: 3px;

  z-index: 1;
  overflow: hidden;
`

const DurationBar = styled.div<{ type: ToastVariant }>`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;

  height: 3px;

  background: ${({ theme, type }) =>
    getValueFromMap(
      {
        info: theme.colors.accent,
        error: theme.colors.invalid,
        warning: theme.colors.warning,
        debug: theme.roleColors.Developer,
      },
      type,
      theme.colors.accent,
    )};
`

export function Toast(props: ToastProps) {
  const {
    duration,
    maxDuration,
    children,
    variant,
    transitionStatus,
    ...rest
  } = props

  const progress = duration && maxDuration ? duration / maxDuration : 0

  return (
    <Container status={transitionStatus} {...rest}>
      <Content>
        {children}
        <DurationBar
          type={variant}
          style={{
            transform: `scaleX(${progress})`,
            transformOrigin: "0%",
          }}
        />
      </Content>
    </Container>
  )
}
