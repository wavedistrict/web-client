import React from "react"
import { TransitionGroup } from "react-transition-group"
import Transition, { TransitionProps } from "react-transition-group/Transition"
import { TOAST_LIMIT } from "../constants"
import { Toast } from "./Toast"
import { ToastContainer } from "./ToastContainer"
import { styled } from "../../theme/themes"
import { getSpacing } from "../../theme/helpers"
import { PLAYER_CONTROLS_HEIGHT } from "../../player/constants"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

export type TransitionElement = React.ReactElement<TransitionProps>

const Container = styled.div<{ hasPlayer: boolean }>`
  position: fixed;

  right: ${getSpacing("6")}px;
  bottom: 0px;

  z-index: 5;
  transform: translateZ(0);

  ${(props) =>
    props.hasPlayer &&
    `
    bottom: ${PLAYER_CONTROLS_HEIGHT}px;
  `}
`

export function ToastOverlay() {
  const { toastStore, playerStore } = useStores()
  const { toastModels } = toastStore

  const toasts = useObserver(() => [...toastModels.values()])
  const moreThanLimitCount = toasts.length - TOAST_LIMIT

  const renderLimitNotice = () => {
    if (moreThanLimitCount <= 0) return

    return (
      <Transition key="limit" timeout={500}>
        {(transitionStatus) => (
          <Toast transitionStatus={transitionStatus} variant="info">
            +{moreThanLimitCount} more
          </Toast>
        )}
      </Transition>
    )
  }

  return (
    <Container hasPlayer={!!playerStore.track}>
      <TransitionGroup>
        {toasts.slice(0, TOAST_LIMIT).map((toast) => (
          <Transition key={toast.id} timeout={500}>
            {(transitionStatus) => (
              <ToastContainer
                transitionStatus={transitionStatus}
                toast={toast}
              />
            )}
          </Transition>
        ))}
        {renderLimitNotice()}
      </TransitionGroup>
    </Container>
  )
}
