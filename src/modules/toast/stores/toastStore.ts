import { action, observable } from "mobx"
import { TOAST_LIMIT } from "../constants"
import { ToastModel, ToastModelOptions } from "../models/ToastModel"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"
import { IS_SERVER } from "../../core/constants"

const animationFrame = () => new Promise(requestAnimationFrame)

export class ToastStore extends InitializableStore {
  @observable public toastModels = new Map<ToastModel["id"], ToastModel>()

  public async init() {
    if (!IS_SERVER) {
      this.runToastUpdateLoop()
    }
  }

  @action
  public addToast(options: string | ToastModelOptions) {
    if (typeof options === "string") {
      options = { content: options }
    }

    const toast = new ToastModel(options)
    this.toastModels.set(toast.id, toast)
  }

  @action
  public removeToast(id: ToastModel["id"]) {
    this.toastModels.delete(id)
  }

  public async runToastUpdateLoop() {
    let currentTime = await animationFrame()

    while (true) {
      const frameTime = await animationFrame()
      const deltaTime = frameTime - currentTime
      currentTime = frameTime

      // ignore big delta time values, and only run while the document is focused
      if (document.hasFocus() && deltaTime < 100) {
        // only update the visible toasts,
        // so the rest don't disappear while they aren't visible
        this.update(deltaTime)
      }
    }
  }

  private update(deltaTime: number) {
    const visibleToasts = [...this.toastModels.values()].slice(0, TOAST_LIMIT)
    for (const toast of visibleToasts) {
      this.updateToast(toast, deltaTime)
    }
  }

  @action
  private updateToast(toast: ToastModel, deltaTime: number) {
    if (!toast.paused) {
      toast.decreaseDuration(deltaTime)
    }
    if (toast.duration < 0) {
      this.removeToast(toast.id)
    }
  }
}

export const toastStore = createStoreFactory(ToastStore)
