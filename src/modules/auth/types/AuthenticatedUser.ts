import { UserModel } from "../../user/models/User"
import { QuotaData } from "./QuotaData"

export interface AuthenticatedUser extends UserModel {
  quota: QuotaData
}
