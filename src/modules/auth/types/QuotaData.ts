export interface QuotaData {
  availableSeconds: number
  usedSeconds: number
}
