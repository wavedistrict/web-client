import { LoginFormManager } from "../helpers/createLoginFormManager"
import { Manager } from "../../../common/state/types/Manager"

export const submitLogin = async (manager: Manager, form: LoginFormManager) => {
  const { authStore } = manager.stores

  await form.submit("/auth/login", "post")
  await authStore.authenticate()
}
