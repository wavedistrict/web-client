import React from "react"
import { LoginModal } from "../components/LoginModal"
import { createLoginFormManager } from "../helpers/createLoginFormManager"
import { Manager } from "../../../common/state/types/Manager"

interface Options {
  message?: string
  loginCallback?: () => void
}

export const spawnLoginModal = (manager: Manager, options: Options) => {
  const { modalStore } = manager.stores
  const form = createLoginFormManager()

  modalStore.spawn({
    key: "login",
    render: () => React.createElement(LoginModal, { ...options, form }),
  })
}
