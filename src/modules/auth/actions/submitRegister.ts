import { RegisterFormManager } from "../helpers/createRegisterFormManager"

export const submitRegister = async (form: RegisterFormManager) => {
  await form.submit("/auth/register", "post")
}
