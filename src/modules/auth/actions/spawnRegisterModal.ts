import React from "react"
import { RegisterModal } from "../components/RegisterModal"
import { createRegisterFormManager } from "../helpers/createRegisterFormManager"
import { Manager } from "../../../common/state/types/Manager"

export const spawnRegisterModal = (manager: Manager) => {
  const { modalStore } = manager.stores
  const form = createRegisterFormManager(manager)

  modalStore.spawn({
    key: "register",
    render: () => React.createElement(RegisterModal, { form }),
  })
}
