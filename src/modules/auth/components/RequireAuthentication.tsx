import React, { useEffect } from "react"

import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { ButtonList } from "../../../common/ui/button/components/ButtonList"

import { PageBody } from "../../core/components/PageBody"
import { PageHeader } from "../../core/components/PageHeader"
import { Paragraph } from "../../../common/ui/markdown/components/Paragraph"

import { spawnLoginModal } from "../actions/spawnLoginModal"
import { spawnRegisterModal } from "../actions/spawnRegisterModal"
import { AuthenticatedUser } from "../types/AuthenticatedUser"

import { useObserver } from "mobx-react-lite"
import { useStores } from "../../../common/state/hooks/useStores"

import { styled } from "../../theme/themes"
import { getSpacing } from "../../theme/helpers"
import { useManager } from "../../../common/state/hooks/useManager"
import { useMeta } from "../../../common/ui/meta/hooks/useMeta"

export interface RequireAuthenticationProps {
  children: (user: AuthenticatedUser) => React.ReactNode
}

const Actions = styled(ButtonList)`
  margin-top: ${getSpacing("5")}px;
`

export function SignInPrompt() {
  const manager = useManager()

  useEffect(() => {
    spawnLoginModal(manager, {
      message: "You must be signed in to access this page",
    })
  })

  useMeta({
    title: "Sign in",
    description: "You must be signed in to access this page.",
  })

  return (
    <>
      <PageHeader icon="lock" title="Please sign in" />
      <PageBody>
        <Paragraph>
          <strong>You must be signed in to see this page.</strong>
        </Paragraph>
        <Actions horizontal>
          <PrimaryButton
            label="Sign in"
            onClick={() => spawnLoginModal(manager, {})}
          />
          <SecondaryButton
            label="Create account"
            onClick={() => spawnRegisterModal(manager)}
          />
        </Actions>
      </PageBody>
    </>
  )
}

export function RequireAuthentication(props: RequireAuthenticationProps) {
  const { authStore } = useStores()
  const { children } = props
  const user = useObserver(() => authStore.user)

  if (user) return <>{children(user)}</>
  return <SignInPrompt />
}
