import React from "react"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { FormStatus } from "../../../common/ui/form/components/FormStatus"
import { PrimaryModal } from "../../modal/components/PrimaryModal"
import { useModal } from "../../modal/hooks/useModal"
import { submitRegister } from "../actions/submitRegister"
import { RegisterFormManager } from "../helpers/createRegisterFormManager"
import { RegisterForm } from "./RegisterForm"

interface RegisterModalProps {
  form: RegisterFormManager
}

export function RegisterModal(props: RegisterModalProps) {
  const { form } = props
  const { dismiss } = useModal()

  async function submit() {
    await submitRegister(form)
    dismiss()
  }

  const body = (
    <>
      <RegisterForm onSubmit={submit} form={form} />
    </>
  )

  const footer = (
    <>
      <FormStatus
        submitting="Creating new account..."
        error="An error occured creating your account."
        form={form}
      />
      <SecondaryButton label="Close" onClick={dismiss} />
      <PrimaryButton label="Submit" onClick={submit} />
    </>
  )

  return (
    <PrimaryModal title="Create new account">
      <PrimaryModal.Body>{body}</PrimaryModal.Body>
      <PrimaryModal.Footer>{footer}</PrimaryModal.Footer>
    </PrimaryModal>
  )
}
