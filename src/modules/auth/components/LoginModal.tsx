import React from "react"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { FormStatus } from "../../../common/ui/form/components/FormStatus"
import { Link } from "../../../common/ui/navigation/components/Link"
import { PrimaryModal } from "../../modal/components/PrimaryModal"
import { useModal } from "../../modal/hooks/useModal"
import { spawnRegisterModal } from "../actions/spawnRegisterModal"
import { submitLogin } from "../actions/submitLogin"
import { LoginFormManager } from "../helpers/createLoginFormManager"
import { LoginForm } from "./LoginForm"
import { useManager } from "../../../common/state/hooks/useManager"

interface LoginModalProps {
  form: LoginFormManager
  message?: string
  loginCallback?: () => void
}

export function LoginModal(props: LoginModalProps) {
  const { form, loginCallback, message } = props

  const { dismiss } = useModal()
  const manager = useManager()

  async function onSubmit() {
    await submitLogin(manager, form)

    dismiss()
    loginCallback && loginCallback()
  }

  function handleNewAccount() {
    dismiss()
    spawnRegisterModal(manager)
  }

  const body = (
    <>
      <LoginForm onSubmit={onSubmit} form={form} />

      <div className="registerMessage">
        No account? <Link onClick={handleNewAccount}>Make a new one!</Link>
      </div>
    </>
  )

  const footer = (
    <>
      <FormStatus
        submitting="Logging in..."
        error="Invalid username or password"
        form={form}
      />
      <SecondaryButton label="Close" onClick={dismiss} />
      <PrimaryButton label="Submit" onClick={onSubmit} />
    </>
  )

  return (
    <PrimaryModal title="Sign in">
      <PrimaryModal.Body>{body}</PrimaryModal.Body>
      <PrimaryModal.Footer>{footer}</PrimaryModal.Footer>
    </PrimaryModal>
  )
}
