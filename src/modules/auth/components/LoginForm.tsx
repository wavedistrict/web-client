import React from "react"
import { FormField } from "../../../common/ui/form/components/FormField"
import { FormLayout } from "../../../common/ui/form/components/FormLayout"
import { FormContext } from "../../../common/ui/form/contexts/FormContext"
import { renderControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { LoginFormManager } from "../helpers/createLoginFormManager"

interface LoginFormProps {
  form: LoginFormManager
  onSubmit?: () => void
}

export function LoginForm(props: LoginFormProps) {
  const { form, onSubmit } = props
  const { fields } = form

  return (
    <FormContext.Provider value={{ onSubmit, form }}>
      <FormLayout>
        <FormLayout.Grid>
          <FormLayout.GridCell size="full">
            <FormField
              label="Username"
              controller={fields.username}
              renderInput={renderControlledTextInput({
                autoFocus: true,
                autoComplete: "username",
              })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Password"
              controller={fields.password}
              renderInput={renderControlledTextInput({
                type: "password",
                autoComplete: "current-password",
              })}
            />
          </FormLayout.GridCell>
        </FormLayout.Grid>
      </FormLayout>
    </FormContext.Provider>
  )
}
