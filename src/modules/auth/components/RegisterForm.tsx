import React from "react"
import { FormField } from "../../../common/ui/form/components/FormField"
import { FormLayout } from "../../../common/ui/form/components/FormLayout"
import { FormContext } from "../../../common/ui/form/contexts/FormContext"
import { renderControlledCaptchaInput } from "../../../common/ui/input/components/ControlledCaptchaInput"
import { renderControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { RegisterFormManager } from "../helpers/createRegisterFormManager"

interface RegisterFormProps {
  form: RegisterFormManager
  onSubmit?: () => void
}

export function RegisterForm(props: RegisterFormProps) {
  const { onSubmit, form } = props
  const { fields } = form

  return (
    <FormContext.Provider value={{ onSubmit, form }}>
      <FormLayout>
        <FormLayout.Grid>
          <FormLayout.GridCell size="full">
            <FormField
              label="Username"
              controller={fields.username}
              renderInput={renderControlledTextInput({
                autoFocus: true,
                autoComplete: "username",
              })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Email"
              controller={fields.email}
              renderInput={renderControlledTextInput({
                type: "email",
                autoComplete: "email",
              })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Password"
              controller={fields.password}
              renderInput={renderControlledTextInput({
                type: "password",
                autoComplete: "new-password",
              })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Captcha"
              controller={fields.captcha}
              renderInput={renderControlledCaptchaInput({
                type: "password",
                autoComplete: "new-password",
              })}
            />
          </FormLayout.GridCell>
        </FormLayout.Grid>
      </FormLayout>
    </FormContext.Provider>
  )
}
