import { computed, observable, runInAction } from "mobx"
import { bind } from "../../../common/lang/function/helpers/bind"
import { apiServer } from "../../../common/network"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

export class ActivityStore extends InitializableStore {
  @observable public unreadNotifications = 0
  @observable public unreadMessages = 0

  @bind
  public async fetchActivity() {
    const response = await apiServer.get<any>("/auth/activity")
    const { data } = response

    runInAction(() => {
      this.unreadNotifications = Number(data.unreadNotifications)
      this.unreadMessages = Number(data.unreadMessages)
    })
  }

  @computed
  get unreadAll() {
    return this.unreadNotifications + this.unreadMessages
  }
}

export const activityStore = createStoreFactory(ActivityStore)
