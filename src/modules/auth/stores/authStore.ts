import { computed, observable, runInAction, toJS } from "mobx"
import { StoredValue } from "../../../common/dom/classes/StoredValue"
import { bind } from "../../../common/lang/function/helpers/bind"
import { apiServer } from "../../../common/network"
import { UserData } from "../../user/types/UserData"
import { AuthenticatedUser } from "../types/AuthenticatedUser"
import { QuotaData } from "../types/QuotaData"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

export interface StoredAuthData {
  user: UserData
  quota: QuotaData
}

export interface AuthUserApiResult {
  user: UserData
  quota: QuotaData
}

const authLocalStorage = new StoredValue<StoredAuthData | undefined>(
  "auth",
  undefined,
)

export interface AuthStoreEvents {
  login: UserData
  logout: void
}

export class AuthStore extends InitializableStore {
  @observable public user?: AuthenticatedUser
  private didInit = false

  @computed
  public get isAuthenticated() {
    return this.user !== undefined
  }

  @computed get fingerprint() {
    return this.user ? this.user.data.username : "ANONYMOUS"
  }

  /** Attempts to fetch user data from the API */
  public async authenticate() {
    const { activityStore, userStore } = this.manager.stores

    const response = await apiServer.get<AuthUserApiResult>("/auth/user")
    const { quota, user } = response.data

    if (!user) {
      this.user = undefined
      this.saveState()
      return
    }

    runInAction(() => {
      this.user = userStore.getModelFromData(user) as AuthenticatedUser
      this.user.quota = quota

      this.user.conversations.fetch()
      this.user.notifications.fetch()
    })

    activityStore.fetchActivity()
    this.saveState()
  }

  /** Removes user data from app state and storage */
  @bind
  public async logout() {
    await apiServer.post("/auth/logout", null)

    this.user = undefined
    authLocalStorage.delete()
  }

  /** Loads previously saved auth data */
  public async restoreState() {
    const restoredData = await authLocalStorage.restore()
    if (!restoredData) return

    const { user, quota } = restoredData
    const { activityStore, userStore } = this.manager.stores

    this.user = userStore.getModelFromData(user) as AuthenticatedUser
    this.user.quota = quota
    this.authenticate()

    activityStore.fetchActivity()
  }

  /** Stores the auth data in localStorage */
  public saveState() {
    const { user } = this
    if (!user) return

    authLocalStorage.save({
      user: user.rawData,
      quota: toJS(user.quota),
    })
  }

  public async init() {
    if (this.didInit) return

    await this.restoreState()
    this.didInit = true
  }
}

export const authStore = createStoreFactory(AuthStore)
