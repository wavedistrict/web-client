import { useStores } from "../../../common/state/hooks/useStores"

export const useAuthUser = () => {
  const { authStore } = useStores()
  const { user } = authStore

  if (!user) {
    throw new Error("Cannot use useAuthUser when not logged in")
  }

  return user
}
