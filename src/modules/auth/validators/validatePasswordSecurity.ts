import { Validator } from "../../../common/ui/form/classes/FieldController"
import { Manager } from "../../../common/state/types/Manager"

export const validatePasswordSecurity = (
  manager: Manager,
): Validator<string> => async (value) => {
  const { configStore } = manager.stores
  const { insecurePasswords } = configStore.config.security

  for (const password of insecurePasswords) {
    if (password.toLowerCase() === value.toLowerCase())
      return {
        isValid: false,
        errorMessage: "This password is too common",
      }
  }

  return {
    isValid: true,
    errorMessage: "",
  }
}
