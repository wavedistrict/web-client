import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { CaptchaFieldController } from "../../../common/ui/form/controllers/CaptchaFieldController"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { allowCharacters } from "../../../common/ui/form/modifiers/string/allowCharacters"
import { replaceAll } from "../../../common/ui/form/modifiers/string/replaceAll"
import { setCase } from "../../../common/ui/form/modifiers/string/setCase"
import { validateStringFromEndpoint } from "../../../common/ui/form/validators/string/validateStringFromEndpoint"
import { validateStringLength } from "../../../common/ui/form/validators/string/validateStringLength"
import { validateStringRegex } from "../../../common/ui/form/validators/string/validateStringRegex"
import { validatePasswordSecurity } from "../validators/validatePasswordSecurity"
import { Manager } from "../../../common/state/types/Manager"
import { validateStringReserved } from "../../../common/ui/form/validators/string/validateStringReserved"

export const createRegisterFormManager = (manager: Manager) => {
  return new FormManager({
    username: new TextFieldController({
      isRequired: true,
      requiredWarning: "Enter your username",
      validators: [
        validateStringReserved(["test"]),
        validateStringFromEndpoint({
          endpoint: "/users/test",
          validator: "username",
          message: "Username is taken",
        }),
      ],
      modifiers: [
        setCase("lower"),
        replaceAll(" ", "-"),
        allowCharacters("a-z0-9-"),
      ],
    }),
    email: new TextFieldController({
      isRequired: true,
      requiredWarning: "Enter your email",
      modifiers: [
        // ???
      ],
    }),
    password: new TextFieldController({
      isRequired: true,
      requiredWarning: "Enter your password",
      validators: [
        validateStringLength({ min: 8 }),
        validateStringRegex({
          warningText: "Must contain a number",
          regex: "\\d",
        }),
        validatePasswordSecurity(manager),
      ],
    }),
    captcha: new CaptchaFieldController({
      isRequired: true,
      requiredWarning: "Complete captcha",
    }),
  })
}

export type RegisterFormManager = ReturnType<typeof createRegisterFormManager>
