import { spawnLoginModal } from "../actions/spawnLoginModal"
import { Manager } from "../../../common/state/types/Manager"

export type Callback = (manager: Manager, ...args: any[]) => void

export const requireAuth = <T extends Callback>(fn: T) => {
  return (manager: Manager, ...args: any[]) => {
    const { authStore } = manager.stores
    const { user } = authStore

    if (!user)
      return spawnLoginModal(manager, {
        message: "You must be logged in to do that",
        loginCallback: () => fn(manager, ...args),
      })

    return fn(manager, ...args)
  }
}
