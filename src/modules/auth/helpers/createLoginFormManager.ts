import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { allowCharacters } from "../../../common/ui/form/modifiers/string/allowCharacters"
import { replaceAll } from "../../../common/ui/form/modifiers/string/replaceAll"
import { setCase } from "../../../common/ui/form/modifiers/string/setCase"

export const createLoginFormManager = () => {
  return new FormManager({
    username: new TextFieldController({
      isRequired: true,
      requiredWarning: "Enter your username",
      modifiers: [
        setCase("lower"),
        replaceAll(" ", "-"),
        allowCharacters("a-z0-9-"),
      ],
    }),
    password: new TextFieldController({
      isRequired: true,
      requiredWarning: "Enter your password",
    }),
  })
}

export type LoginFormManager = ReturnType<typeof createLoginFormManager>
