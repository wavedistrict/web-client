import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { validateStringLength } from "../../../common/ui/form/validators/string/validateStringLength"
import { MAX_COMMENT_LENGTH } from "../constants"
import { CommentModel } from "../models/Comment"

export const createCommentFormManager = (comment?: CommentModel) => {
  const value = comment ? comment.data.body : ""

  return new FormManager({
    body: new TextFieldController({
      value,
      isRequired: true,
      requiredWarning: "Your comment cannot be empty",
      validators: [validateStringLength({ max: MAX_COMMENT_LENGTH })],
    }),
  })
}

export type CommentFormManager = ReturnType<typeof createCommentFormManager>
