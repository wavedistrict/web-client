import { action } from "mobx"
import {
  FetcherStore,
  createFetcherStoreFactory,
} from "../../../common/state/classes/FetcherStore"
import { CommentModel } from "../models/Comment"
import { CommentData } from "../types/CommentData"

export class CommentStore extends FetcherStore<CommentModel> {
  @action
  public addPrefetched(data: CommentData) {
    const source = `/comments/${data.id}`
    return this.prepopulate(source, data.id, data)
  }
}

export const commentStore = createFetcherStoreFactory(
  CommentStore,
  CommentModel,
)
