import { UserData } from "../../user/types/UserData"

export interface ReplyToData {
  id: number
  body: string
  user: {
    id: number
    username: string
    displayName: string
  }
}

export interface CommentTarget {
  type: "track" | "collection"
  data: {
    id: number
    title: string
    slug: string
  }
}

export interface CommentData {
  id: number
  body: string
  user: UserData
  replyTo?: ReplyToData
  parent?: { id: number }
  target: CommentTarget
  createdAt: string
  updatedAt: string
  stats: {
    replies: number
    score: number
  }
  byCurrentUser: {
    vote: "up" | "down" | null
  }
}
