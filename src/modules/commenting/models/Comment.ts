import { computed } from "mobx"
import { Model } from "../../../common/state/classes/Model"
import { CommentData } from "../types/CommentData"
import { Manager } from "../../../common/state/types/Manager"
import { UserModel } from "../../user/models/User"

export class CommentModel extends Model<CommentData> {
  public user: UserModel
  public apiPath = `/comments/${this.data.id}`

  constructor(data: CommentData, manager: Manager) {
    super(data, manager)
    const { userStore } = manager.stores

    this.user = userStore.getModelFromData(this.data.user)
  }

  protected getListMap() {
    const { commentStore } = this.manager.stores

    return {
      replies: {
        store: commentStore,
        path: `/comments/${this.data.id}/replies`,
        params: {
          ascending: true,
        },
      },
    }
  }

  public get replies() {
    return this.getOrCreateList("replies")
  }

  public get isOwn() {
    const { authStore } = this.manager.stores
    return authStore.user && this.data.user.id === authStore.user.data.id
  }

  @computed
  public get isEdited() {
    return this.data.createdAt !== this.data.updatedAt
  }
}
