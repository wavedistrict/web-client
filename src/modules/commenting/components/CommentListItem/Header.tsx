import { observer } from "mobx-react"
import React from "react"
import { HumanizedDate } from "../../../core/components/HumanizedDate"
import { RouteLink } from "../../../../common/routing/components/RouteLink"
import {
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { CommentModel } from "../../models/Comment"

export interface HeaderProps {
  comment: CommentModel
}

const Container = styled.div`
  display: flex;

  font-size: ${getFontSize("2")}px;
  font-weight: ${getFontWeight("bold")};
  flex-shrink: 0;

  > .replyTo {
    display: flex;
    align-items: flex-end;

    margin-left: ${getSpacing("3")}px;
    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("normal")};
  }

  > .replyTo > .text {
    color: ${getFontColor("muted")};
    margin-right: ${getSpacing("2")}px;
  }

  > .date {
    color: ${getFontColor("muted")};

    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("normal")};

    text-align: right;
    flex: 1;
    flex-shrink: 0;
  }
`

export const Header = observer(function Header(props: HeaderProps) {
  const { comment } = props
  const { user } = comment

  const { createdAt, updatedAt, replyTo } = comment.data
  const { displayName } = user.data

  const renderReplyTo = () => {
    if (!replyTo) return null

    const path = `/@${replyTo.user.username}`

    return (
      <div className="replyTo">
        <span className="text">{"replying to"}</span>
        <RouteLink to={path}>{replyTo.user.displayName}</RouteLink>
      </div>
    )
  }

  const renderDate = () => {
    const edited = comment.isEdited ? (
      <span className="edited">
        {" (edited "}
        <HumanizedDate>{updatedAt}</HumanizedDate>
        {")"}
      </span>
    ) : null

    return (
      <div className="date">
        <HumanizedDate>{createdAt}</HumanizedDate>
        {edited}
      </div>
    )
  }

  return (
    <Container>
      <RouteLink to={user.clientLink} className="username">
        {displayName}
      </RouteLink>
      {renderReplyTo()}
      {renderDate()}
    </Container>
  )
})
