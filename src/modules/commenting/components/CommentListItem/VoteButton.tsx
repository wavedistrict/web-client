import { styled } from "../../../theme/themes"
import { Button } from "../../../../common/ui/button/components/Button"
import css from "@emotion/css"
import { size } from "polished"
import { getDuration, getSpacing } from "../../../theme/helpers"

export type VoteButtonVariants = {
  upvoted?: boolean
  downvoted?: boolean
}

export const VoteButton = styled<typeof Button, VoteButtonVariants>(Button)`
  transition: ${getDuration("normal")} ease opacity;
  opacity: 0.6;

  > .icon {
    ${size(16)};
  }

  &:hover {
    opacity: 1;
  }

  ${(props) =>
    props.upvoted &&
    css`
      opacity: 1;

      > .icon {
        fill: ${props.theme.colors.success};
      }
    `}

  ${(props) =>
    props.downvoted &&
    css`
      opacity: 1;

      > .icon {
        fill: ${props.theme.colors.invalid};
      }
    `}
`
