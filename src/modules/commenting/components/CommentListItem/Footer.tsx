import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { preloadList } from "../../../../common/state/helpers/preloadList"
import { SecondaryButton } from "../../../../common/ui/button/components/SecondaryButton"
import { requireAuth } from "../../../auth/helpers/requireAuth"
import {
  getFontSize,
  getSpacing,
  getTransparency,
} from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { spawnCommentDeleteModal } from "../../actions/spawnCommentDeleteModal"
import { spawnCommentEditModal } from "../../actions/spawnCommentEditModal"
import { toggleCommentVote } from "../../actions/toggleCommentVote"
import { CommentModel } from "../../models/Comment"
import { VoteButton } from "./VoteButton"
import { PrimaryButtonVariants } from "../../../../common/ui/button/components/PrimaryButton"
import { useManager } from "../../../../common/state/hooks/useManager"

export interface ActionProps {
  comment: CommentModel
  showingReplies: boolean
  onReplying: () => void
  onToggleReplies: () => void
}

const Container = styled.div`
  margin-top: ${getSpacing("3")}px;
  display: flex;
  align-items: center;
  flex-shrink: 0;
`

const Separator = styled.div`
  ${size(4)};

  background: ${getTransparency("strongPositive")};
  border-radius: 4px;

  margin: 0px ${getSpacing("3")}px;
`

const Score = styled.div<{
  negative: boolean
}>`
  font-size: ${getFontSize("1")}px;
  text-align: center;

  margin: 0px ${getSpacing("3")}px;

  ${(props) =>
    props.negative &&
    `
    color: ${props.theme.colors.invalid};
  `}
`

export const Footer = observer(function Footer(props: ActionProps) {
  const manager = useManager()

  const { comment, showingReplies, onReplying, onToggleReplies } = props
  const { stats, byCurrentUser } = comment.data

  const variants: PrimaryButtonVariants = {
    compact: true,
    flat: true,
  }

  const isUpvote = byCurrentUser.vote === "up"
  const isDownvote = byCurrentUser.vote === "down"

  const reply = requireAuth(() => {
    onReplying()
  })

  const renderScore = () => {
    const { score } = stats

    if (score === 0) return <Score negative={false} />

    return <Score negative={score < 0}>{score}</Score>
  }

  const renderVoting = () => (
    <>
      <VoteButton
        upvoted={isUpvote}
        icon="thumbsUp"
        onClick={() => {
          toggleCommentVote(manager, comment, false)
        }}
      />
      {renderScore()}
      <VoteButton
        downvoted={isDownvote}
        key="downvote"
        icon="thumbsDown"
        onClick={() => {
          toggleCommentVote(manager, comment, true)
        }}
      />
    </>
  )

  const renderReplyButton = () => (
    <>
      <Separator />
      <SecondaryButton
        key="reply"
        icon="reply"
        label="Reply"
        onClick={() => reply(manager)}
        {...variants}
      />
    </>
  )

  const renderViewReplies = () => {
    const { replies } = stats

    if (replies === 0) return null

    const showLabel = replies === 1 ? "Show reply" : `Show ${replies} replies`
    const hideLabel = replies === 1 ? "Hide reply" : "Hide replies"

    return (
      <>
        <Separator />
        <SecondaryButton
          key="view-replies"
          label={showingReplies ? hideLabel : showLabel}
          onMouseEnter={() => preloadList(comment.replies)}
          onClick={onToggleReplies}
          {...variants}
        />
      </>
    )
  }

  const renderOwnActions = () => {
    if (!comment.isOwn) return null

    return (
      <>
        <Separator />
        <SecondaryButton
          noLabel
          key="edit-comment"
          icon="pencil"
          onClick={() => spawnCommentEditModal(manager, comment)}
          {...variants}
        />
        <SecondaryButton
          noLabel
          key="delete-comment"
          icon="trashcan"
          onClick={() => spawnCommentDeleteModal(manager, comment)}
          {...variants}
        />
      </>
    )
  }

  return (
    <Container>
      {renderVoting()}
      {renderReplyButton()}
      {renderViewReplies()}
      {renderOwnActions()}
    </Container>
  )
})
