import { observer } from "mobx-react"
import { size } from "polished"
import React, { useState } from "react"
import { Markdown } from "../../../../common/ui/markdown/components/Markdown"
import { RouteLink } from "../../../../common/routing/components/RouteLink"
import { getFontColor, getFontSize, getSpacing } from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { styled } from "../../../theme/themes"
import { UserAvatar } from "../../../user/components/UserAvatar"
import { CommentModel } from "../../models/Comment"
import { CommentForm } from "../CommentForm"
import { CommentList } from "../CommentList"
import { Footer } from "./Footer"
import { Header } from "./Header"

interface CommentListItemProps {
  comment: CommentModel
  showContext?: boolean
}

const Container = styled.div`
  display: flex;

  > .avatar {
    ${size(48)};
    ${imageContainer("100%")};
  }

  & + & {
    padding-top: ${getSpacing("5")}px;
  }
`

const Content = styled.div`
  margin-left: ${getSpacing("4")}px;
  flex: 1;

  display: flex;
  flex-direction: column;

  min-width: 0;

  > .body {
    margin-top: ${getSpacing("3")}px;
    flex: 1;
  }

  > .target {
    margin-bottom: ${getSpacing("2")}px;
    font-size: ${getFontSize("1")}px;
    color: ${getFontColor("muted")};
  }

  > .replyBox {
    width: 100%;
    margin-top: ${getSpacing("4")}px;
  }

  > .replies {
    margin-top: ${getSpacing("5")}px;
  }
`

export const CommentListItem = observer(function CommentListItem(
  props: CommentListItemProps,
) {
  const { comment, showContext } = props
  const { user } = comment

  const { body, target } = comment.data

  const [isReplying, setIsReplying] = useState(false)
  const [isShowingReplies, setIsShowingReplies] = useState(false)

  const handleSubmitOrCancel = () => {
    setIsReplying(false)
  }

  const renderTarget = () => {
    if (!target || !showContext) return null

    return <div className="target">{target.data.title}</div>
  }

  const renderReplyBox = () => {
    if (!isReplying) return null

    return (
      <div className="replyBox">
        <CommentForm
          path={comment.replies.path}
          onSubmit={handleSubmitOrCancel}
          onCancel={handleSubmitOrCancel}
          placeholder="Write your reply here..."
          autoFocus
        />
      </div>
    )
  }

  const renderReplyList = () => {
    if (!isShowingReplies) return null

    return (
      <div className="replies">
        <CommentList showContext={showContext} list={comment.replies} />
      </div>
    )
  }

  return (
    <Container>
      <RouteLink to={user.clientLink} className="avatar">
        <UserAvatar id="comment" user={user} />
      </RouteLink>
      <Content>
        {renderTarget()}
        <Header comment={comment} />
        <div className="body">
          <Markdown text={body} />
        </div>
        <Footer
          comment={comment}
          showingReplies={isShowingReplies}
          onReplying={() => setIsReplying(true)}
          onToggleReplies={() => setIsShowingReplies(!isShowingReplies)}
        />
        {renderReplyBox()}
        {renderReplyList()}
      </Content>
    </Container>
  )
})
