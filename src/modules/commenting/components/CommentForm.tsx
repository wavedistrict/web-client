import { useObserver } from "mobx-react-lite"
import React, { useState } from "react"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { FieldError } from "../../../common/ui/form/components/FieldError"
import { FormContext } from "../../../common/ui/form/contexts/FormContext"
import { ControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import {
  getColor,
  getDuration,
  getFontColor,
  getFontSize,
  getSpacing,
} from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { MAX_COMMENT_LENGTH } from "../constants"
import { createCommentFormManager } from "../helpers/createCommentFormManager"
import { CommentData } from "../types/CommentData"
import { useStores } from "../../../common/state/hooks/useStores"

interface CommentFormProps {
  path: string
  autoFocus?: boolean
  placeholder?: string
  onSubmit?: () => void
  onCancel?: () => void
}

const TRANSITION = "180ms ease all"

const Container = styled.div<{
  active: boolean
}>`
  width: 100%;
  height: 45px;
  transition: ${TRANSITION};
  margin-bottom: ${getSpacing("5")}px;

  > .actions {
    margin-top: ${getSpacing("4")}px;
    opacity: 0;

    transition: ${TRANSITION};
    pointer-events: none;

    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  > .actions > .buttons > * + * {
    margin-left: ${getSpacing("4")}px;
  }

  > .actions > .error {
    flex-grow: 1;

    display: flex;
    align-items: center;

    margin-top: -10px;
    margin-left: ${getSpacing("4")}px;
  }

  > .actions > .characterCount {
    transition: ${getDuration("normal")} ease all;

    font-weight: 600;
    font-size: ${getFontSize("1")}px;
    color: ${getFontColor("normal")};

    &.-near-limit {
      color: ${getColor("warning")};
    }

    &.-exceeded-limit {
      color: ${getColor("invalid")};
    }
  }

  > .input textarea {
    max-height: 37px;
    min-height: 37px;
    transition: ${TRANSITION};
    resize: none;
  }

  ${(props) =>
    props.active &&
    `
    height: 142px;

    > .actions {
      opacity: 1;
      pointer-events: all;
    }

    > .input textarea {
      min-height: 80px;
    }
  `}
`

const CharacterCount = styled.div<{
  nearLimit: boolean
  exceededLimit: boolean
}>`
  transition: ${getDuration("normal")} ease all;

  font-weight: 600;
  font-size: ${getFontSize("1")}px;
  color: ${getFontColor("normal")};

  ${(props) =>
    props.nearLimit &&
    `
    color: ${props.theme.colors.warning};
  `}

  ${(props) =>
    props.exceededLimit &&
    `
  color: ${props.theme.colors.invalid};
`}
`

export function CommentForm(props: CommentFormProps) {
  const {
    path,
    autoFocus,
    onSubmit,
    onCancel,
    placeholder = "Write your comment here",
  } = props

  const { authStore } = useStores()

  const [isActive, setIsActive] = useState(false)
  const [form] = useState(() => createCommentFormManager())
  const commentLength = useObserver(() => form.fields.body.value!.length)

  if (!authStore.user) return null

  async function handleSubmit() {
    if (!form) return

    await form.submit<CommentData>(path, "post")

    form.reset()
    setIsActive(false)

    if (onSubmit) onSubmit()
  }

  async function handleCancel() {
    if (!form) return

    form.reset()
    setIsActive(false)

    if (onCancel) onCancel()
  }

  const handleInputKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === "Enter" && event.ctrlKey) handleSubmit()
  }

  const characterCount = (
    <CharacterCount
      nearLimit={commentLength >= MAX_COMMENT_LENGTH * 0.7}
      exceededLimit={commentLength > MAX_COMMENT_LENGTH}
    >
      {commentLength} / {MAX_COMMENT_LENGTH}
    </CharacterCount>
  )

  return (
    <FormContext.Provider value={{ onSubmit, form }}>
      <Container active={isActive}>
        <div className="input" onKeyDown={handleInputKeyDown}>
          <ControlledTextInput
            autoFocus={autoFocus}
            onFocus={() => setIsActive(true)}
            placeholder={placeholder}
            controller={form.fields.body}
            multiline
          />
        </div>
        <div className="actions">
          <div className="buttons">
            <PrimaryButton label="Submit" onClick={handleSubmit} />
            <SecondaryButton label="Cancel" onClick={handleCancel} />
          </div>
          <div className="error">
            <FieldError controller={form.fields.body} />
          </div>
          {characterCount}
        </div>
      </Container>
    </FormContext.Provider>
  )
}
