import React from "react"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { renderWindowList } from "../../../common/ui/list/helpers/renderWindowList"
import { ScrollerContext } from "../../../common/ui/scroll/components/ScrollerContext"
import { CommentModel } from "../models/Comment"
import { CommentListItem } from "./CommentListItem/CommentListItem"

interface CommentListProps {
  list: FetcherList<CommentModel>
  isReplies?: boolean
  showContext?: boolean
}

export class CommentList extends React.Component<CommentListProps> {
  public render() {
    const { list, showContext = true } = this.props

    return renderWindowList({
      list,
      pending: "last",
      state: {
        icon: "commentFilled",
        message: "No comments yet",
      },
      render: (props) => (
        <ScrollerContext onScroll={props.onScroll}>
          {() => {
            return props.items.map((item) => (
              <CommentListItem
                key={item.data.id}
                showContext={showContext}
                comment={item}
              />
            ))
          }}
        </ScrollerContext>
      ),
    })
  }
}
