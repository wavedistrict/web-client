import React from "react"
import { apiServer } from "../../../common/network"
import { ConfirmationModal } from "../../modal/components/ConfirmationModal"
import { CommentModel } from "../models/Comment"

interface CommentDeleteModalProps {
  comment: CommentModel
}

export function CommentDeleteModal(props: CommentDeleteModalProps) {
  const { comment } = props

  async function action() {
    await apiServer.delete(comment.apiPath)
  }

  return (
    <ConfirmationModal
      title="Delete Comment"
      actionButtonLabel="Delete"
      action={action}
    >
      <p>Are you sure you want to delete this comment?</p>
    </ConfirmationModal>
  )
}
