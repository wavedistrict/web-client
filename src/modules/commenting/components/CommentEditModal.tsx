import React from "react"
import { FormField } from "../../../common/ui/form/components/FormField"
import { renderControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { FormModal } from "../../modal/components/FormModal"
import { useModal } from "../../modal/hooks/useModal"
import { CommentFormManager } from "../helpers/createCommentFormManager"
import { CommentModel } from "../models/Comment"

export interface CommentEditModalProps {
  comment: CommentModel
  form: CommentFormManager
}

export function CommentEditModal(props: CommentEditModalProps) {
  const { comment, form } = props
  const { fields } = form

  const { dismiss } = useModal()

  async function onSubmit() {
    await form.submit(comment.apiPath, "patch")
    dismiss()
  }

  return (
    <FormModal title="Edit Comment" onSubmit={onSubmit} form={form}>
      <div className="FieldGrid">
        <div className="cell -full">
          <FormField
            label="Body"
            controller={fields.body}
            renderInput={renderControlledTextInput({
              autoFocus: true,
              multiline: true,
            })}
          />
        </div>
      </div>
    </FormModal>
  )
}
