import { apiServer } from "../../../common/network"
import { requireAuth } from "../../auth/helpers/requireAuth"
import { CommentModel } from "../models/Comment"

export const toggleCommentVote = requireAuth(
  async (_, comment: CommentModel, isNegative: boolean) => {
    const { vote } = comment.data.byCurrentUser

    const path = `/comments/${comment.data.id}/votes`
    const newDirection = isNegative ? "down" : "up"
    const shouldRemove = newDirection === vote

    if (shouldRemove) {
      await apiServer.delete(path)
      comment.data.byCurrentUser.vote = null

      return
    }

    await apiServer.put(path, null, {
      params: { isNegative },
    })

    comment.data.byCurrentUser.vote = newDirection
  },
)
