import React from "react"
import { requireAuth } from "../../auth/helpers/requireAuth"
import { CommentEditModal } from "../components/CommentEditModal"
import { createCommentFormManager } from "../helpers/createCommentFormManager"
import { CommentModel } from "../models/Comment"

export const spawnCommentEditModal = requireAuth(
  (manager, comment: CommentModel) => {
    const { modalStore } = manager.stores
    const form = createCommentFormManager(comment)

    modalStore.spawn({
      key: `edit-comment-${comment.data.id}`,
      render: () => React.createElement(CommentEditModal, { form, comment }),
    })
  },
)
