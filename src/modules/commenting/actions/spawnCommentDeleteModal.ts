import React from "react"
import { requireAuth } from "../../auth/helpers/requireAuth"
import { CommentDeleteModal } from "../components/CommentDeleteModal"
import { CommentModel } from "../models/Comment"

export const spawnCommentDeleteModal = requireAuth(
  (manager, comment: CommentModel) => {
    const { modalStore } = manager.stores

    modalStore.spawn({
      key: `delete-comment-${comment.data.id}`,
      render: () => React.createElement(CommentDeleteModal, { comment }),
    })
  },
)
