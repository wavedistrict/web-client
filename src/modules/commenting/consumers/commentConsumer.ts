import { bind } from "../../../common/lang/function/helpers/bind"
import { SocketMessage } from "../../../common/network/types"
import { FetcherStoreConsumer } from "../../realtime/classes/FetcherStoreConsumer"
import { CommentModel } from "../models/Comment"
import { CommentData } from "../types/CommentData"
import { Manager } from "../../../common/state/types/Manager"

interface VoteData {
  isNegative: boolean
}

class CommentConsumer extends FetcherStoreConsumer<CommentData, CommentModel> {
  protected getListeners() {
    return {
      ...super.getListeners(),
      "create-reply": this.handleReply,
    }
  }

  protected getTargetListeners() {
    return {
      "add-comment-vote": this.handleAddVote,
      "change-comment-vote": this.handleChangeVote,
      "remove-comment-vote": this.handleRemoveVote,
    }
  }

  private handleAddVote(comment: CommentModel, data: VoteData) {
    const value = data.isNegative ? -1 : 1
    comment.data.stats.score += value
  }

  private handleChangeVote(comment: CommentModel, data: VoteData) {
    const value = data.isNegative ? -2 : 2
    comment.data.stats.score += value
  }

  private handleRemoveVote(comment: CommentModel, data: VoteData) {
    const value = data.isNegative ? 1 : -1
    comment.data.stats.score += value
  }

  @bind
  private handleReply(message: SocketMessage) {
    const data = message.data as CommentData
    const model = this.store.getModelFromData(data)

    const parent = data.parent!
    const parentModel = this.store.getModel(parent.id)

    if (parentModel) {
      if (model.isOwn) {
        parentModel.replies.add(data, "first")
      } else {
        parentModel.replies.addPending(data)
      }

      parentModel.data.stats.replies += 1
    }

    this.handleCommentCreate(model)
  }

  protected addListeners() {
    this.on("create", this.handleCommentCreate)
  }

  @bind
  private handleCommentCreate(comment: CommentModel) {
    const { data } = comment
    const isReply = !!data.parent

    const { trackStore, collectionStore } = this.manager.stores

    const map: Record<string, typeof trackStore | typeof collectionStore> = {
      track: trackStore,
      collection: collectionStore,
    }

    const store = map[data.target.type]
    const model = store.getModel(data.target.data.id)

    if (model) {
      if (!isReply) {
        if (comment.isOwn) {
          model.comments.add(data, "first")
        } else {
          model.comments.addPending(data)
        }
      }
      model.data.stats.comments += 1
    }
  }
}

export const commentConsumer = (manager: Manager) =>
  new CommentConsumer(manager, "comment", "commentStore")
