import { observer } from "mobx-react"
import React from "react"
import { BooleanFieldController } from "../../../common/ui/form/controllers/BooleanFieldController"
import { ObjectFieldController } from "../../../common/ui/form/controllers/ObjectFieldController"
import { RangeFieldController } from "../../../common/ui/form/controllers/RangeFieldController"
import { SelectFieldController } from "../../../common/ui/form/controllers/SelectFieldController"
import { ControlledDropdownInput } from "../../../common/ui/input/components/ControlledDropdownInput"
import { ControlledJSONInput } from "../../../common/ui/input/components/ControlledJSONInput"
import { ControlledSliderInput } from "../../../common/ui/input/components/ControlledSliderInput"
import { ControlledSwitchInput } from "../../../common/ui/input/components/ControlledSwitchInput"
import { NAVIGATION_MQ } from "../../../common/ui/navigation/constants"
import { getFontColor, getFontWeight, getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { SettingsController } from "../classes/SettingsController"
import { SettingInput } from "../types/SettingInput"

export interface SettingsFieldProps {
  controller: SettingsController<any, SettingInput>
}

const Container = styled.div<{
  inline?: boolean
}>`
  display: flex;
  flex-direction: column;

  > .info {
    display: flex;
    flex-direction: column;
    justify-content: center;

    flex: 1;
    margin-right: ${getSpacing("5")}px;
  }

  > .info > .name {
    font-weight: ${getFontWeight("bold")};
  }

  > .info > .description {
    margin-top: ${getSpacing("2")}px;
    color: ${getFontColor("muted")};
  }

  > .input {
    margin-top: ${getSpacing("4")}px;

    display: flex;
    justify-content: flex-start;
    align-items: center;

    min-width: 180px;
  }

  ${(props) =>
    props.inline === undefined &&
    `
    @media ${NAVIGATION_MQ} {
      flex-direction: row;

      > .input {
        margin-top: 0px;
        justify-content: flex-end;
      }
    }
  `}

  ${(props) =>
    props.inline === true &&
    `
    flex-direction: row;

    > .input {
      margin-top: 0px;
      min-width: 0;
    }
    `}
    
  ${(props) =>
    props.inline === false &&
    `
    flex-direction: column;

    > .input {
      width: 100%;
      margin-top: ${props.theme.spacings[4]}px;
      justify-content: flex-start;
    }
  `}

  & + * {
    margin-top: ${getSpacing("5")}px;
  }
`
export const SettingsField = observer(function SettingsField(
  props: SettingsFieldProps,
) {
  const { controller } = props

  if (controller.disabled) return null

  const inlineMap: Record<typeof controller.input, boolean | undefined> = {
    switch: true,
    slider: false,
    json: false,
    dropdown: undefined,
  }

  const inline = inlineMap[controller.input]

  const renderInput = () => {
    const { fieldController } = controller

    if (controller.input === "switch") {
      return (
        <ControlledSwitchInput
          controller={fieldController as BooleanFieldController}
        />
      )
    }

    if (controller.input === "dropdown") {
      return (
        <ControlledDropdownInput
          controller={fieldController as SelectFieldController}
        />
      )
    }

    if (controller.input === "slider") {
      return (
        <ControlledSliderInput
          controller={fieldController as RangeFieldController}
        />
      )
    }

    if (controller.input === "json") {
      return (
        <ControlledJSONInput
          controller={fieldController as ObjectFieldController}
        />
      )
    }
  }

  return (
    <Container inline={inline}>
      <div className="info">
        <div className="name">{controller.label}</div>
        {controller.description && (
          <div className="description">{controller.description}</div>
        )}
      </div>
      <div className="input">{renderInput()}</div>
    </Container>
  )
})
