import React from "react"
import { ActivityTabs } from "../../../common/ui/navigation/components/ActivityTabs/ActivityTabs"
import { Navigation } from "../../../common/ui/navigation/components/ActivityTabs/Navigation"
import { Pane } from "../../../common/ui/navigation/components/ActivityTabs/Pane"
import { NAVIGATION_HEADER_HEIGHT } from "../../../common/ui/navigation/constants"
import { ScrollContainer } from "../../../common/ui/scroll/components/ScrollContainer"
import { ModalCloseButton } from "../../modal/components/ModalCloseButton"
import { PrimaryModal } from "../../modal/components/PrimaryModal"
import { SettingsCategoryName } from "../types/Settings"
import { CategoryRenderer } from "./CategoryRenderer"
import { useStores } from "../../../common/state/hooks/useStores"

export function SettingsModal() {
  const { settingsStore } = useStores()

  const titleMap = Object.fromEntries(
    settingsStore.categories.map(({ metadata }) => [
      metadata.name,
      metadata.label,
    ]),
  )

  return (
    <PrimaryModal.Base>
      <ActivityTabs>
        <Navigation>
          <Navigation.Header title="Settings">
            <ModalCloseButton size={NAVIGATION_HEADER_HEIGHT} />
          </Navigation.Header>
          <Navigation.Body>
            {settingsStore.categories.map(({ metadata }) => (
              <Navigation.Item
                key={metadata.name}
                icon={metadata.icon}
                label={metadata.label}
                value={metadata.name}
              />
            ))}
          </Navigation.Body>
        </Navigation>
        <Pane
          empty={{
            icon: "cog",
            message: "Select a category to the left",
          }}
        >
          <Pane.Header titleMap={titleMap} />
          <ScrollContainer>
            <Pane.Body>
              {(current: SettingsCategoryName) => {
                const category = settingsStore.categories.find(
                  (x) => x.metadata.name === current,
                )

                return <CategoryRenderer category={category!} />
              }}
            </Pane.Body>
          </ScrollContainer>
        </Pane>
      </ActivityTabs>
    </PrimaryModal.Base>
  )
}
