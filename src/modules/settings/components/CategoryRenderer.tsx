import React from "react"
import {
  getColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { SettingsCategory } from "../classes/SettingsCategory"
import { SettingsField } from "./SettingsField"

export interface CategoryRendererProps {
  category: SettingsCategory<any>
}

const Container = styled.div`
  padding: ${getSpacing("4")}px;
`

const Section = styled.div`
  > .title {
    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("bold")};

    text-transform: uppercase;

    color: ${getColor("accent")};
  }

  > .content {
    margin-top: ${getSpacing("4")}px;
  }

  & + & {
    margin-top: ${getSpacing("6")}px;
  }
`

export function CategoryRenderer(props: CategoryRendererProps) {
  const { category } = props

  return (
    <Container>
      {category.sections.map((section) => (
        <Section key={section.name}>
          <div className="title">{section.name}</div>
          <div className="content">
            {section.controllers.map((controller) => (
              <SettingsField key={controller.name} controller={controller} />
            ))}
          </div>
        </Section>
      ))}
    </Container>
  )
}
