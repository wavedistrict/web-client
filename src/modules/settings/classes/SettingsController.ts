import { computed, observe } from "mobx"
import { ReactNode } from "react"
import { FieldController } from "../../../common/ui/form/classes/FieldController"
import { SettingInput } from "../types/SettingInput"
import { Settings } from "../types/Settings"
import { Manager } from "../../../common/state/types/Manager"

export type SettingsControllerPredicate = (
  settings: Settings,
) => "hidden" | "disabled" | undefined

export interface SettingsControllerOptions<I> {
  input: I
  name: string
  label: string
  description?: ReactNode
  predicate?: SettingsControllerPredicate
}

const defaultPredicate: SettingsControllerPredicate = () => undefined

export type Handler<T> = (value: T) => void

export abstract class SettingsController<T, I extends SettingInput> {
  private handlers: Handler<T>[] = []
  private predicate: SettingsControllerPredicate

  public fieldController: FieldController<T>

  constructor(
    private manager: Manager,
    private options: SettingsControllerOptions<I>,
    controller: FieldController<any, T>,
  ) {
    this.fieldController = controller
    this.predicate = options.predicate || defaultPredicate

    observe(this, "value", (change) => {
      for (const handler of this.handlers) {
        handler(change.newValue!)
      }
    })
  }

  public setHandler = (handler: (value: T) => void) => {
    this.handlers.push(handler)
    handler(this.value)

    return () => {
      this.handlers = this.handlers.filter((h) => h !== handler)
    }
  }

  @computed
  public get value() {
    return this.fieldController.serializedValue
  }

  @computed
  public get hidden(): boolean {
    const { settingsStore } = this.manager.stores
    return this.predicate(settingsStore.serialized) === "hidden"
  }

  @computed
  public get disabled(): boolean {
    const { settingsStore } = this.manager.stores
    return this.predicate(settingsStore.serialized) === "disabled"
  }

  public get name() {
    return this.options.name
  }

  public get description() {
    return this.options.description
  }

  public get input() {
    return this.options.input
  }

  public get label() {
    return this.options.label
  }
}
