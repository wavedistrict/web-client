import { computed } from "mobx"
import { IconType } from "../../../common/ui/icon"
import { SettingInput } from "../types/SettingInput"
import { Settings, SettingsCategoryName } from "../types/Settings"
import { SettingsSection } from "../types/SettingsSection"
import { SettingsController } from "./SettingsController"

export interface Metadata<T> {
  name: T
  label: string
  icon: IconType
}

export class SettingsCategory<T extends SettingsCategoryName> {
  constructor(
    public metadata: Metadata<T>,
    public sections: SettingsSection[],
  ) {}

  public assign(data: any) {
    for (const controller of this.controllers) {
      controller.fieldController.setValue(data[controller.name])
    }
  }

  public getController<C extends keyof Settings[T]>(name: C) {
    const controller = this.controllers.find(
      (controller) => controller.name === name,
    )

    return controller as SettingsController<Settings[T][C], SettingInput>
  }

  @computed
  public get serialized(): Settings[T] {
    const obj: any = {}

    for (const controller of this.controllers) {
      obj[controller.name] = controller.value
    }

    return obj
  }

  private get controllers() {
    const controllers = []

    for (const section of this.sections) {
      controllers.push(...section.controllers)
    }

    return controllers
  }
}
