import { SettingsCategory } from "../classes/SettingsCategory"
import { BooleanController } from "../controllers/BooleanController"
import { SelectController } from "../controllers/SelectController"
import { RangeController } from "../controllers/RangeController"
import { advancedPredicate } from "../predicates/advancedPredicate"
import { Manager } from "../../../common/state/types/Manager"

export const audio = (manager: Manager) =>
  new SettingsCategory(
    {
      name: "audio",
      icon: "speaker",
      label: "Audio & Playback",
    },
    [
      {
        name: "Audio",
        controllers: [
          new SelectController(manager, {
            name: "preferredQuality",
            label: "Preferred audio quality",
            input: "dropdown",
            options: [
              {
                key: 4,
                label: "Lossless",
              },
              {
                key: 3,
                label: "High quality",
              },
              {
                key: 1,
                label: "Low quality",
              },
            ],
          }),
          new BooleanController(manager, {
            name: "normalize",
            label: "Loudness normalization",
            description: "Normalizes loudness across tracks so they are equal",
            input: "switch",
          }),
        ],
      },
      {
        name: "Player",
        controllers: [
          new BooleanController(manager, {
            name: "autoresume",
            label: "Auto resume",
            description: "Resume playback on page load",
            input: "switch",
          }),
          new BooleanController(manager, {
            name: "preload",
            label: "Preload tracks",
            description:
              "Automatically preload tracks when needed for a smoother listening experience. Disable to save data.",
            input: "switch",
          }),
        ],
      },
      {
        name: "Visualizer",
        controllers: [
          new BooleanController(manager, {
            name: "visualizer",
            label: "Enabled",
            description: "Disable if you have performance issues",
            input: "switch",
          }),
          new BooleanController(manager, {
            name: "visualizerStereo",
            label: "Stereo",
            description: "Show both channels in the visualizer",
            input: "switch",
          }),
          new BooleanController(manager, {
            name: "visualizerLog",
            label: "Logarithmic scale",
            description: "Distribute frequencies evenly",
            input: "switch",
            predicate: advancedPredicate,
          }),
          new SelectController(manager, {
            name: "visualizerSpeed",
            label: "Animation speed",
            predicate: advancedPredicate,
            description: "Controls how quick frequencies are represented",
            input: "dropdown",
            options: [
              {
                key: 0.99,
                label: "Extremely slow",
              },
              {
                key: 0.9,
                label: "Slow",
              },
              {
                key: 0.7,
                label: "Normal",
              },
              {
                key: 0.5,
                label: "Quick",
              },
              {
                key: 0.3,
                label: "Extremely quick",
              },
            ],
          }),
          new RangeController(manager, {
            predicate: advancedPredicate,
            name: "visualizerBarWidth",
            label: "Bar width",
            input: "slider",
            steps: true,
            max: 7,
            min: 1,
          }),
          new RangeController(manager, {
            predicate: advancedPredicate,
            name: "visualizerBarSpacing",
            label: "Bar spacing",
            input: "slider",
            steps: true,
            max: 8,
            min: 0,
          }),
        ],
      },
    ],
  )
