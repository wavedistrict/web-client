import React from "react"
import { DropdownOption } from "../../../common/ui/input/components/DropdownInput"
import { ExternalLink } from "../../../common/ui/navigation/components/ExternalLink"
import { Themes } from "../../theme/types/Themes"
import { SettingsCategory } from "../classes/SettingsCategory"
import { BooleanController } from "../controllers/BooleanController"
import { ObjectController } from "../controllers/ObjectController"
import { SelectController } from "../controllers/SelectController"
import { advancedPredicate } from "../predicates/advancedPredicate"
import { Manager } from "../../../common/state/types/Manager"

const THEME_INTERFACE_URL =
  "https://gitlab.com/wavedistrict/web-client/blob/master/src/modules/theme/types/Theme.ts"

const themeOptions: DropdownOption<Themes>[] = [
  {
    key: "dark",
    label: "Dark",
  },
  {
    key: "light",
    label: "Light",
  },
  {
    key: "powersave",
    label: "Powersave",
  },
  {
    key: "midnight",
    label: "Midnight",
  },
]

export const appearance = (manager: Manager) =>
  new SettingsCategory(
    {
      name: "appearance",
      icon: "easel",
      label: "Look & Feel",
    },
    [
      {
        name: "Theme",
        controllers: [
          new SelectController(manager, {
            name: "theme",
            label: "Theme",
            description: "Change the current theme",
            input: "dropdown",
            options: themeOptions,
          }),
          new SelectController(manager, {
            name: "overlay",
            label: "Overlay background",
            description: "Changes the overlay background color",
            input: "dropdown",
            options: [
              {
                key: null,
                label: "Inherit",
              },
              {
                key: "white",
                label: "White",
              },
              {
                key: "black",
                label: "Black",
              },
            ],
          }),
          new ObjectController(manager, {
            predicate: advancedPredicate,
            name: "themeOverrides",
            label: "Theme overrides",
            description: (
              <>
                {"Pass in a "}
                <ExternalLink href={THEME_INTERFACE_URL}>Theme</ExternalLink>
                {" object to override theme properties"}
              </>
            ),
            input: "json",
          }),
        ],
      },
      {
        name: "Aesthetics",
        controllers: [
          new BooleanController(manager, {
            name: "blur",
            label: "Blur",
            description: "Enable blurring of image backgrounds",
            input: "switch",
          }),
          new BooleanController(manager, {
            name: "customScrollbars",
            label: "Custom scrollbars",
            description:
              "Enable styled scrollbars in lists. May decrease performance",
            input: "switch",
          }),
        ],
      },
    ],
  )
