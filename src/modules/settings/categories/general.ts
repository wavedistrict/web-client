import { SettingsCategory } from "../classes/SettingsCategory"
import { BooleanController } from "../controllers/BooleanController"
import { advancedPredicate } from "../predicates/advancedPredicate"
import { Manager } from "../../../common/state/types/Manager"

export const general = (manager: Manager) =>
  new SettingsCategory(
    {
      name: "general",
      icon: "cog",
      label: "General",
    },
    [
      {
        name: "Realtime",
        controllers: [
          new BooleanController(manager, {
            name: "offlineMode",
            label: "Offline mode",
            input: "switch",
            description: "Disconnects you from WaveDistrict's realtime server",
          }),
        ],
      },
      {
        name: "Advanced",
        controllers: [
          new BooleanController(manager, {
            name: "advanced",
            label: "Advanced settings",
            input: "switch",
            description: "Enables more in-depth settings for advanced users",
          }),
          new BooleanController(manager, {
            predicate: advancedPredicate,
            name: "debug",
            label: "Debug",
            input: "switch",
            description: "Enable debugging tools",
          }),
        ],
      },
    ],
  )
