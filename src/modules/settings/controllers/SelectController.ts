import { SelectFieldController } from "../../../common/ui/form/controllers/SelectFieldController"
import { DropdownOption } from "../../../common/ui/input/components/DropdownInput"
import {
  SettingsController,
  SettingsControllerOptions,
} from "../classes/SettingsController"
import { Manager } from "../../../common/state/types/Manager"

export interface SelectControllerOptions
  extends SettingsControllerOptions<"dropdown"> {
  options: DropdownOption[]
}

export class SelectController extends SettingsController<
  string | number | null,
  "dropdown"
> {
  public selectOptions: DropdownOption[] = []

  constructor(manager: Manager, options: SelectControllerOptions) {
    super(
      manager,
      options,
      new SelectFieldController({
        options: options.options,
      }),
    )

    this.selectOptions = options.options
  }
}
