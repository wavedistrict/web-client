import {
  RangeFieldController,
  RangeFieldControllerOptions,
} from "../../../common/ui/form/controllers/RangeFieldController"
import {
  SettingsController,
  SettingsControllerOptions,
} from "../classes/SettingsController"
import { Manager } from "../../../common/state/types/Manager"

export interface RangeControllerOptions
  extends Pick<RangeFieldControllerOptions, "max" | "min" | "steps">,
    SettingsControllerOptions<"slider"> {}

export class RangeController extends SettingsController<number, "slider"> {
  constructor(manager: Manager, options: RangeControllerOptions) {
    super(
      manager,
      options,
      new RangeFieldController({
        ...options,
      }),
    )
  }
}
