import "../../../common/state/manager" // no idea why, but if you remove this a circular import happens
import { BooleanController } from "./BooleanController"
import { createManager } from "../../../common/state/manager"

describe("BoooleanController", () => {
  it("calls handler", () => {
    const manager = createManager()
    const mock = jest.fn()

    const controller = new BooleanController(manager, {
      name: "controller",
      label: "Controller",
      input: "switch",
    })

    controller.setHandler(mock)
    controller.fieldController.setValue(true)

    expect(mock).toHaveBeenCalledTimes(2)
  })
})
