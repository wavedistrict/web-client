import { BooleanFieldController } from "../../../common/ui/form/controllers/BooleanFieldController"
import {
  SettingsController,
  SettingsControllerOptions,
} from "../classes/SettingsController"
import { Manager } from "../../../common/state/types/Manager"

export class BooleanController extends SettingsController<boolean, "switch"> {
  constructor(manager: Manager, options: SettingsControllerOptions<"switch">) {
    super(manager, options, new BooleanFieldController({}))
  }
}
