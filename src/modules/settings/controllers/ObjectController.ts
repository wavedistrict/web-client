import { ObjectFieldController } from "../../../common/ui/form/controllers/ObjectFieldController"
import {
  SettingsController,
  SettingsControllerOptions,
} from "../classes/SettingsController"
import { Manager } from "../../../common/state/types/Manager"

export class ObjectController extends SettingsController<object, "json"> {
  constructor(manager: Manager, options: SettingsControllerOptions<"json">) {
    super(manager, options, new ObjectFieldController({}))
  }
}
