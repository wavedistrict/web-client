import { computed, observe } from "mobx"
import { InitializableStore } from "../../../common/state/classes/InitializableStore"
import { Manager } from "../../../common/state/types/Manager"
import { StoredValue } from "../../../common/dom/classes/StoredValue"

import { SettingsCategory } from "../classes/SettingsCategory"
import { DEFAULT_SETTINGS } from "../constants"
import { Settings, SettingsCategoryName } from "../types/Settings"

import { appearance } from "../categories/appearance"
import { audio } from "../categories/audio"
import { general } from "../categories/general"

const storage = new StoredValue<Settings>("settings", DEFAULT_SETTINGS)

export class SettingsStore extends InitializableStore {
  constructor(manager: Manager, public categories: SettingsCategory<any>[]) {
    super(manager)
  }

  public async init() {
    const safe = this.getSafeSettings()

    for (const [name, settings] of Object.entries(safe)) {
      const category = this.categories.find((x) => x.metadata.name === name)

      if (category) {
        category.assign(settings)
      }
    }

    observe(this, "serialized", (change) => {
      storage.save(change.newValue)
    })

    storage.save(safe)
  }

  private getSafeSettings() {
    const stored = storage.restore()
    const result: any = {}

    for (const [name, settings] of Object.entries(DEFAULT_SETTINGS)) {
      const existing = stored[name as SettingsCategoryName] || {}
      result[name] = { ...settings, ...existing }
    }

    return result
  }

  public getController<K extends keyof Settings, C extends keyof Settings[K]>(
    category: K,
    name: C,
  ) {
    const foundCategory = this.categories.find(
      (x) => x.metadata.name === category,
    )! as SettingsCategory<K>

    return foundCategory.getController(name)
  }

  @computed
  public get serialized(): Settings {
    const obj: any = {}

    for (const category of this.categories) {
      obj[category.metadata.name] = category.serialized
    }

    return obj
  }
}

export const settingsStore = (manager: Manager) =>
  new SettingsStore(
    manager,
    [general, appearance, audio].map((x) => x(manager)),
  )
