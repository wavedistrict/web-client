import { SettingsControllerPredicate } from "../classes/SettingsController"

export const advancedPredicate: SettingsControllerPredicate = (settings) => {
  return settings.general.advanced === true ? undefined : "disabled"
}
