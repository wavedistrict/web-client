import { useEffect, useState } from "react"
import { getSetting } from "../helpers/getSetting"
import { Settings } from "../types/Settings"
import { useManager } from "../../../common/state/hooks/useManager"

export const useSetting = <
  K extends keyof Settings,
  C extends keyof Settings[K]
>(
  category: K,
  name: C,
) => {
  const manager = useManager()
  const setting = getSetting(manager, category, name)
  const [value, setValue] = useState(setting.value)

  useEffect(() => {
    const dispose = setting.setHandler(setValue)

    return () => {
      dispose()
    }
  }, [setting])

  return value
}
