import React from "react"
import { SettingsModal } from "../components/SettingsModal"
import { Manager } from "../../../common/state/types/Manager"

export const spawnSettingsModal = (manager: Manager) => {
  const { modalStore } = manager.stores

  modalStore.spawn({
    key: "settings",
    render: () => React.createElement(SettingsModal),
  })
}
