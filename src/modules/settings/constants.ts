import { Settings } from "./types/Settings"

export const DEFAULT_SETTINGS: Settings = {
  general: {
    advanced: false,
    offlineMode: false,
    debug: false,
  },
  appearance: {
    theme: "dark",
    customScrollbars: false,
    themeOverrides: {},
    overlay: null,
    blur: true,
  },
  audio: {
    preferredQuality: 3,
    normalize: true,
    preload: true,
    autoresume: true,
    visualizer: true,
    visualizerLog: true,
    visualizerSpeed: 0.7,
    visualizerBarWidth: 2,
    visualizerBarSpacing: 8,
    visualizerStereo: true,
  },
}
