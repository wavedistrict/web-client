import { SettingsController } from "../classes/SettingsController"

export interface SettingsSection {
  name: string
  controllers: SettingsController<any, any>[]
}
