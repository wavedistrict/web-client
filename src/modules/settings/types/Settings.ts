import { Theme } from "../../theme/types/Theme"
import { Themes } from "../../theme/types/Themes"

export type SettingsCategoryName = keyof Settings

export interface Settings {
  general: {
    advanced: boolean
    offlineMode: boolean
    debug: boolean
  }
  appearance: {
    theme: Themes
    themeOverrides: Partial<Theme>
    overlay: null | "white" | "black"
    customTheme?: Partial<Theme>
    customScrollbars: boolean
    blur: boolean
  }
  audio: {
    preferredQuality: number
    normalize: boolean
    preload: boolean
    autoresume: boolean
    visualizer: boolean
    visualizerLog: boolean
    visualizerSpeed: number
    visualizerStereo: boolean
    visualizerBarWidth: number
    visualizerBarSpacing: number
  }
}
