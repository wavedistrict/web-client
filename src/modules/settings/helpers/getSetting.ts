import { Settings } from "../types/Settings"
import { Manager } from "../../../common/state/types/Manager"

export const getSetting = <
  K extends keyof Settings,
  C extends keyof Settings[K]
>(
  manager: Manager,
  category: K,
  name: C,
) => {
  const { settingsStore } = manager.stores
  return settingsStore.getController(category, name)
}
