export type Themes =
  | "dark"
  | "light"
  | "powersave"
  | "legacy"
  | "midnight"
  | "warm"
