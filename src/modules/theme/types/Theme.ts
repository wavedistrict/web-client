import { Role } from "../../access-control/types/Role"
import { Gradient } from "./Gradient"

export interface Theme {
  colorize: boolean
  inOverlay?: boolean
  colors: {
    primary: string
    background: string
    accent: string
    love: string
    success: string
    warning: string
    invalid: string
    secondaryAccent: string
    divider: string
  }
  gradients: {
    logo: Gradient
  }
  fontColors: {
    normal: string
    muted: string
  }
  roleColors: Record<Role, string>
  transparencies: {
    weakPositive: string
    strongPositive: string
    weakNegative: string
    strongNegative: string
  }
  spacings: [null, number, number, number, number, number, number, number]
  fontSizes: [null, number, number, number, number]
  fontWeights: {
    normal: string
    bold: string
  }
  shadows: {
    light: string
    spread: string
  }
  durations: {
    normal: string
    extended: string
  }
  overlay: {
    opacity: number
    background: string
    blend: boolean
  }
}
