import { Theme } from "./Theme"
import { SerializedStyles } from "@emotion/css"

export type ThemedCSS<T extends object = {}> = (
  props: { theme: Theme } & T,
) => SerializedStyles
