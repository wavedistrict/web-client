export type GradientStop = readonly [string, number]
export type Gradient = {
  degrees: number
  stops: GradientStop[]
}
