import css from "@emotion/css"
import { Theme } from "../types/Theme"

export const imageContainer = (radius: string | number = 0) => (props: {
  theme: Theme
}) => css`
  overflow: hidden;
  border-radius: ${typeof radius === "number" ? `${radius}px` : radius};
  position: relative;
  background: ${props.theme.transparencies.weakNegative};
`
