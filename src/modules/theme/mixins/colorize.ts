import css from "@emotion/css"

const OFFSET = 99999

export const colorize = (color: string) => () => css`
  position: absolute;
  left: -${OFFSET}px;
  filter: drop-shadow(${OFFSET}px 0px 0px ${color});
`
