import React from "react"
import { safeMerge } from "../../../common/lang/object/helpers/safeMerge"
import { useSetting } from "../../settings/hooks/useSetting"
import {
  darkTheme,
  legacyTheme,
  lightTheme,
  midnightTheme,
  powersaveTheme,
  ThemeProvider,
  warmTheme,
} from "../themes"

export function Theme(props: { children: React.ReactNode }) {
  const theme = useSetting("appearance", "theme")
  const override = useSetting("appearance", "themeOverrides")

  const overlayBackground = useSetting("appearance", "overlay")

  const usedTheme =
    {
      dark: darkTheme,
      light: lightTheme,
      powersave: powersaveTheme,
      legacy: legacyTheme,
      midnight: midnightTheme,
      warm: warmTheme,
    }[theme] || darkTheme

  const finalTheme = safeMerge(
    {
      ...usedTheme,
      overlay: {
        ...usedTheme.overlay,
        background: overlayBackground || usedTheme.overlay.background,
      },
    },
    override,
  )

  return <ThemeProvider theme={finalTheme}>{props.children}</ThemeProvider>
}
