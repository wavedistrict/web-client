import { css, Global } from "@emotion/core"
import React from "react"
import { Theme } from "../types/Theme"

export const styles = (theme: Theme) => css`
  /* http://meyerweb.com/eric/tools/css/reset/
   v2.0 | 20110126
   License: none (public domain)
*/

  html,
  body,
  div,
  span,
  applet,
  object,
  iframe,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  blockquote,
  pre,
  a,
  abbr,
  acronym,
  address,
  big,
  cite,
  code,
  del,
  dfn,
  img,
  ins,
  kbd,
  q,
  s,
  samp,
  small,
  strike,
  sub,
  sup,
  tt,
  var,
  b,
  u,
  i,
  center,
  dl,
  dt,
  dd,
  ol,
  ul,
  li,
  fieldset,
  form,
  label,
  legend,
  table,
  caption,
  tbody,
  tfoot,
  thead,
  tr,
  th,
  td,
  article,
  aside,
  canvas,
  details,
  embed,
  figure,
  figcaption,
  footer,
  header,
  hgroup,
  menu,
  nav,
  output,
  ruby,
  section,
  summary,
  time,
  mark,
  audio,
  video {
    margin: 0;
    padding: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }

  /* HTML5 display-role reset for older browsers */
  article,
  aside,
  details,
  figcaption,
  figure,
  footer,
  header,
  hgroup,
  menu,
  nav,
  section {
    display: block;
  }

  ol,
  ul {
    list-style: none;
  }

  li {
    display: block;
  }

  blockquote,
  q {
    quotes: none;
  }

  blockquote:before,
  blockquote:after,
  q:before,
  q:after {
    content: "";
    content: none;
  }

  table {
    border-collapse: collapse;
    border-spacing: 0;
  }

  * {
    box-sizing: border-box;
  }

  noscript {
    font-size: font-size(big);
    margin-top: spacing(4);
  }

  body {
    overflow-y: scroll;
    overflow-x: hidden;
  }

  button {
    padding: 0;
    margin: 0;
    border: none;
    background: none;
    color: inherit;
    text-align: inherit;
    box-sizing: inherit;
    cursor: pointer;
    font: inherit;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
  }

  @import url("https://rsms.me/inter/inter.css");

  @supports (font-variation-settings: normal) {
    html {
      font-family: "Inter var", sans-serif;
    }
  }

  html,
  body {
    font-family: "Inter", sans-serif;
    font-size: ${theme.fontSizes[2]}px;
    background-color: ${theme.colors.background};
    color: ${theme.fontColors.normal};
  }

  a {
    color: ${theme.fontColors.normal};
    text-decoration: none;
  }

  svg {
    fill: ${theme.fontColors.normal};
  }

  input,
  button,
  textarea {
    border: none;
    outline: none !important;
    background: none;
    color: ${theme.fontColors.normal};
  }

  ::-webkit-scrollbar {
    background: ${theme.colors.background};
    width: 4px;
    height: 4px;
  }

  ::-webkit-scrollbar-thumb {
    background: ${theme.transparencies.weakPositive};
    border-radius: 3px;
  }
`

export function GlobalStyles() {
  return <Global<Theme> styles={styles} />
}
