import { readableColor, transparentize } from "polished"
import React from "react"
import { ThemeProvider, useTheme } from "../themes"
import { Theme } from "../types/Theme"

export interface OverlayProps {
  children: React.ReactNode
}

export function Overlay({ children }: OverlayProps) {
  const theme = useTheme()
  const { background } = theme.overlay

  const readableColorful = readableColor(
    background,
    transparentize(0.15, "black"),
    transparentize(0.15, "white"),
  )

  /** Enable overlay */
  const overlayTheme: Partial<Theme> = {
    ...theme,
    inOverlay: true,
    colors: {
      ...theme.colors,
      love: readableColorful,
      accent: readableColorful,
    },
    fontColors: {
      ...theme.fontColors,
      normal: readableColor(
        background,
        transparentize(0.15, "black"),
        transparentize(0.15, "white"),
      ),
      muted: readableColor(
        background,
        transparentize(0.5, "black"),
        transparentize(0.5, "white"),
      ),
    },
    transparencies: {
      ...theme.transparencies,
      weakPositive: readableColor(
        background,
        transparentize(0.9, "black"),
        transparentize(0.9, "white"),
      ),
      strongPositive: readableColor(
        background,
        transparentize(0.8, "black"),
        transparentize(0.8, "white"),
      ),
    },
  }

  return <ThemeProvider theme={overlayTheme}>{children}</ThemeProvider>
}
