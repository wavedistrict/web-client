import { ThemeContext } from "@emotion/core"
import _styled, { CreateStyled } from "@emotion/styled"
import * as emotionTheming from "emotion-theming"
import { transparentize } from "polished"
import { useContext } from "react"
import { Theme } from "./types/Theme"

export const darkTheme: Theme = {
  colorize: false,
  colors: {
    primary: "#0d2c4c",
    background: "#091f35",
    accent: "#00ba8c",
    secondaryAccent: "#008ccd",
    love: "#cc1e42",
    success: "#52ba00",
    warning: "#ffd900",
    invalid: "#ff4141",
    divider: transparentize(0.8, "black"),
  },
  gradients: {
    logo: {
      degrees: 107,
      stops: [
        ["#00d17e", 18.65],
        ["#00ca86", 32.83],
        ["#00b89a", 54.22],
        ["#009bbc", 80.03],
        ["#008CCD", 91.35],
      ],
    },
  },
  fontColors: {
    normal: transparentize(0.15, "white"),
    muted: transparentize(0.5, "white"),
  },
  roleColors: {
    Administrator: "#c3165f",
    Developer: "#4d37a5",
    Moderator: "#2c94e0",
    Amplitude: "#06b99a",
  },
  transparencies: {
    weakPositive: transparentize(0.9, "white"),
    strongPositive: transparentize(0.8, "white"),
    weakNegative: transparentize(0.9, "black"),
    strongNegative: transparentize(0.7, "black"),
  },
  fontSizes: [null, 13, 15, 18, 20],
  spacings: [null, 2, 4, 8, 16, 24, 32, 64],
  fontWeights: {
    normal: "400",
    bold: "600",
  },
  shadows: {
    light: "0 4px 8px rgba(0,0,0,.1)",
    spread: "0 4px 8px rgba(0,0,0,.1)",
  },
  durations: {
    normal: "200ms",
    extended: "500ms",
  },
  overlay: {
    background: "black",
    opacity: 0.5,
    blend: false,
  },
}

export const lightTheme: Theme = {
  ...darkTheme,
  colors: {
    ...darkTheme.colors,
    primary: "white",
    background: "whitesmoke",
    divider: transparentize(0.95, "black"),
  },
  transparencies: {
    ...darkTheme.transparencies,
    weakNegative: transparentize(0.95, "black"),
    weakPositive: transparentize(0.9, "black"),
    strongPositive: transparentize(0.8, "black"),
    strongNegative: transparentize(0.75, "black"),
  },
  fontColors: {
    normal: transparentize(0.15, "black"),
    muted: transparentize(0.5, "black"),
  },
  roleColors: {
    Administrator: "#ff4895",
    Developer: "#6a46fd",
    Moderator: "#2c94e0",
    Amplitude: "#0bd2af",
  },
  overlay: {
    background: "white",
    opacity: 0.95,
    blend: true,
  },
  shadows: {
    light: darkTheme.shadows.light,
    spread: "0px 3px 30px rgba(0, 0, 0, 0.14)",
  },
}

export const powersaveTheme: Theme = {
  ...darkTheme,
  colorize: true,
  colors: {
    ...darkTheme.colors,
    primary: "black",
    background: "black",
    divider: transparentize(0.92, "white"),
  },
  transparencies: {
    ...darkTheme.transparencies,
    weakNegative: transparentize(0.97, "white"),
    strongNegative: transparentize(0.93, "white"),
  },
  overlay: {
    background: "black",
    opacity: 0.9,
    blend: false,
  },
}

export const legacyTheme: Theme = {
  ...darkTheme,
  colors: {
    ...darkTheme.colors,
    primary: "#1f252e",
    accent: "#079e7b",
    background: "#15191e",
    divider: transparentize(0.95, "white"),
  },
}

export const midnightTheme: Theme = {
  ...darkTheme,
  colorize: true,
  colors: {
    ...darkTheme.colors,
    accent: "#e6008f",
    secondaryAccent: "#ffc107",
    primary: "#170e38",
    background: "#0e0921   ",
  },
  gradients: {
    logo: {
      degrees: 107,
      stops: [["#ffc107", 0], ["#e6008f", 100]],
    },
  },
}

export const warmTheme: Theme = {
  ...lightTheme,
  colorize: true,
  colors: {
    ...lightTheme.colors,
    accent: "#db4c18",
    primary: "#efd59b",
    background: "#e8c67f",
  },
  fontColors: {
    normal: transparentize(0.15, "#301910"),
    muted: transparentize(0.5, "#301910"),
  },
  overlay: {
    ...lightTheme.overlay,
    background: "#efd59b",
    blend: true,
  },
  transparencies: {
    weakPositive: transparentize(0.9, "#301910"),
    strongPositive: transparentize(0.8, "#301910"),
    weakNegative: transparentize(0.9, "#301910"),
    strongNegative: transparentize(0.75, "#301910"),
  },
}

export const styled = _styled as CreateStyled<Theme>

export const {
  withTheme,
  ThemeProvider,
} = emotionTheming as emotionTheming.EmotionTheming<Theme>

export function useTheme() {
  const theme = useContext(ThemeContext as React.Context<Theme | undefined>)
  if (!theme) {
    throw new Error(`useTheme() can only be used inside of ThemeProvider`)
  }
  return theme
}
