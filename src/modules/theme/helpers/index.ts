import { createHelper } from "./createHelper"

export const getColor = createHelper("colors")
export const getFontColor = createHelper("fontColors")
export const getSpacing = createHelper("spacings")
export const getDuration = createHelper("durations")
export const getTransparency = createHelper("transparencies")
export const getFontSize = createHelper("fontSizes")
export const getFontWeight = createHelper("fontWeights")
export const getShadow = createHelper("shadows")
export const getOverlay = createHelper("overlay")

export * from "./invertLightness"
