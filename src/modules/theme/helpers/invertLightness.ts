import { setLightness, getLuminance, invert } from "polished"

export const invertLightness = (color: string) => {
  return setLightness(getLuminance(invert(color)), color)
}
