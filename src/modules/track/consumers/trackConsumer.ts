import { bind } from "../../../common/lang/function/helpers/bind"

import { FetcherStoreConsumer } from "../../realtime/classes/FetcherStoreConsumer"
import { TrackModel } from "../models/Track"
import { TrackData } from "../types/TrackData"
import { Manager } from "../../../common/state/types/Manager"

class TrackConsumer extends FetcherStoreConsumer<TrackData, TrackModel> {
  protected getTargetListeners() {
    return {
      "add-track-play": this.handleAddPlay,
      "add-track-favorite": this.handleAddFavorite,
      "remove-track-favorite": this.handleRemoveFavorite,
    }
  }

  protected addListeners() {
    this.on("create", this.handleCreateTrack)
    this.on("delete", this.handleDeleteTrack)
  }

  private handleAddPlay(track: TrackModel) {
    track.data.stats.plays += 1
  }

  private handleAddFavorite(track: TrackModel) {
    track.data.stats.favorites += 1
  }

  private handleRemoveFavorite(track: TrackModel) {
    track.data.stats.favorites -= 1
  }

  @bind
  private handleCreateTrack(track: TrackModel) {
    track.user.tracks.add(track.data, "first")
    track.user.data.stats.tracks += 1

    if (track.user.quota) {
      track.user.quota.usedSeconds += track.data.duration
    }
  }

  @bind
  private handleDeleteTrack(track: TrackModel) {
    const { playerStore } = this.manager.stores

    if (track.user.quota) {
      track.user.quota.usedSeconds -= track.data.duration
    }

    if (playerStore.track === track) playerStore.stop()
  }
}

export const trackConsumer = (manager: Manager) =>
  new TrackConsumer(manager, "track", "trackStore")
