import { ArrayFieldController } from "../../../common/ui/form/controllers/ArrayFieldController"
import { TrackTimestamp } from "../types/TrackData"
import { toJS, computed } from "mobx"

export class TimestampFieldController extends ArrayFieldController<
  TrackTimestamp
> {
  @computed
  public get serializedValue() {
    const rawTimestamps = toJS(this.value)

    return rawTimestamps.map(({ label, seconds }) => ({
      seconds: Number(seconds),
      label,
    }))
  }
}
