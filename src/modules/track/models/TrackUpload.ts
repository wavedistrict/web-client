import { computed, observable } from "mobx"
import { toSlug } from "../../../common/media/helpers/toSlug"
import { createUniqueKey } from "../../../common/state/helpers/createUniqueKey"
import { createTrackFormManager } from "../helpers/createTrackFormManager"
import { TrackData } from "../types/TrackData"
import { TrackModel } from "./Track"
import { Manager } from "../../../common/state/types/Manager"

export class TrackUpload {
  public id = createUniqueKey()
  public form = createTrackFormManager(this.manager)

  @observable public isAttemptingSave = false
  @observable public track?: TrackModel

  constructor(private manager: Manager) {}

  public populate(audio: File, artwork?: File) {
    const { fields } = this.form
    const name = audio.name.replace(/\.\w+$/, "") // remove file extension
    const slug = toSlug(name)

    fields.audio.setValue(audio)
    fields.title.setValue(name)
    fields.slug.setValue(slug)

    if (artwork) fields.artwork.setValue(artwork)
  }

  public save = async () => {
    const { trackStore } = this.manager.stores

    const response = await (this.track
      ? this.form.submit<TrackData>(`/tracks/${this.track.data.id}`, "patch")
      : this.form.submit<TrackData>("/tracks", "post"))

    return (this.track = trackStore.getModelFromData(response.data))
  }

  public stop = () => {
    this.remove()
  }

  public remove = () => {
    const { trackUploadStore } = this.manager.stores

    this.form.dispose()
    trackUploadStore.remove(this.id)
  }

  @computed
  public get progress() {
    return this.form.fields.audio.progress
  }

  @computed
  public get status() {
    return this.form.fields.audio.uploadStatus
  }
}
