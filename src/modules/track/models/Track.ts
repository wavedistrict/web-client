import { computed, observable } from "mobx"
import { hashString } from "../../../common/lang/string/helpers/hashString"
import { Model } from "../../../common/state/classes/Model"
import { ImageModel } from "../../../common/ui/image/models/ImageModel"
import { routes } from "../../core/constants"
import { AudioModel } from "../../player/models/AudioModel"
import { UserModel } from "../../user/models/User"

import { TrackData } from "../types/TrackData"
import { Manager } from "../../../common/state/types/Manager"

export class TrackModel extends Model<TrackData> {
  public hash = hashString(`${this.data.user.username}_${this.data.slug}`)
  public user: UserModel

  public clientLink: string
  public apiPath: string

  @observable public artwork?: ImageModel
  @observable public coverArtwork?: ImageModel

  public audio: AudioModel

  constructor(data: TrackData, manager: Manager) {
    super(data, manager)

    const { userStore, imageMediaStore, audioMediaStore } = manager.stores

    this.user = userStore.getModelFromData(data.user)

    this.clientLink = this.generateClientLink()
    this.apiPath = `/tracks/${data.id}`

    this.artwork = imageMediaStore.assign(data.artwork)
    this.coverArtwork = imageMediaStore.assign(data.coverArtwork)
    this.audio = audioMediaStore.add(data.audio)
  }

  public getListMap() {
    const { commentStore, userStore } = this.manager.stores

    return {
      comments: {
        store: commentStore,
        path: `${this.apiPath}/comments`,
      },
      favoriters: {
        store: userStore,
        path: `${this.apiPath}/favoriters`,
      },
    }
  }

  public get comments() {
    return this.getOrCreateList("comments")
  }

  public get favoriters() {
    return this.getOrCreateList("favoriters")
  }

  public onUpdate(data: TrackData) {
    const { imageMediaStore, audioMediaStore } = this.manager.stores

    this.artwork = imageMediaStore.assign(data.artwork)
    this.coverArtwork = imageMediaStore.assign(data.coverArtwork)
    this.audio = audioMediaStore.add(data.audio)
  }

  private generateClientLink() {
    const { slug, user } = this.data
    return routes.track(user.username, slug)
  }

  get isOwn() {
    const { authStore } = this.manager.stores

    return authStore.user && this.data.user.id === authStore.user.data.id
  }

  @computed
  get isActive() {
    const { playerStore } = this.manager.stores

    if (!playerStore.track) return false
    return this.data.id === playerStore.track.data.id
  }

  @computed
  get isPlayable() {
    return this.audio.isStreamable
  }

  @computed
  get isDownloadable() {
    return this.isOwn || this.data.options.downloadable
  }
}
