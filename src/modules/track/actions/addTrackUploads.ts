import { filterTrackUploadFileSelection } from "../helpers/filterTrackUploadFileSelection"
import { Manager } from "../../../common/state/types/Manager"

export const addTrackUploads = (manager: Manager, files: FileList) => {
  const { trackUploadStore, routingStore } = manager.stores
  const selection = filterTrackUploadFileSelection(Array.from(files))

  for (const pair of selection) {
    trackUploadStore.add(pair.audio, pair.artwork)
  }

  routingStore.push("/upload")
}
