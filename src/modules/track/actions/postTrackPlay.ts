import { apiServer } from "../../../common/network"
import { TrackModel } from "../models/Track"

export const postTrackPlay = (track: TrackModel) => {
  apiServer.post(`/tracks/${track.data.id}/plays`, null)
}
