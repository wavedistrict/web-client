import React from "react"
import { TrackDeleteModal } from "../components/TrackDeleteModal"
import { TrackModel } from "../models/Track"
import { Manager } from "../../../common/state/types/Manager"

export const spawnTrackDeleteModal = (manager: Manager, track: TrackModel) => {
  const { modalStore } = manager.stores
  const { id } = track.data

  modalStore.spawn({
    key: `track-delete-${id}`,
    render: () => React.createElement(TrackDeleteModal, { track }),
  })
}
