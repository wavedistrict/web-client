import { spawnLoginModal } from "../../auth/actions/spawnLoginModal"
import { addTrackUploads } from "./addTrackUploads"
import { Manager } from "../../../common/state/types/Manager"

export const handleAnonymousTrackUpload = (
  manager: Manager,
  files: FileList,
) => {
  spawnLoginModal(manager, {
    message: "You must be logged in to upload",
    loginCallback: () => {
      addTrackUploads(manager, files)
    },
  })
}
