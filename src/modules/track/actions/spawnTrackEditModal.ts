import React from "react"

import { TrackEditModal } from "../components/TrackEditModal"
import { createTrackEditFormManager } from "../helpers/createTrackEditFormManager"
import { TrackModel } from "../models/Track"
import { Manager } from "../../../common/state/types/Manager"

export const spawnTrackEditModal = (manager: Manager, track: TrackModel) => {
  const { modalStore } = manager.stores
  const { id } = track.data
  const form = createTrackEditFormManager(manager, track)

  modalStore.spawn({
    key: `edit-track-${id}`,
    render: () => React.createElement(TrackEditModal, { form, trackId: id }),
  })
}
