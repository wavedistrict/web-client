import { TrackEditFormManager } from "../helpers/createTrackEditFormManager"
import { TrackData } from "../types/TrackData"
import { Manager } from "../../../common/state/types/Manager"

export const submitTrackEdit = async (
  manager: Manager,
  form: TrackEditFormManager,
  trackId: number,
) => {
  const { trackStore } = manager.stores
  const response = await form.submit<TrackData>(`/tracks/${trackId}`, "patch")

  trackStore.addPrefetched(response.data)
}
