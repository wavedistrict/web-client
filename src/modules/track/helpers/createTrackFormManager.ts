import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { AudioFieldController } from "../../../common/ui/form/controllers/AudioFieldController"
import { ImageFieldController } from "../../../common/ui/form/controllers/ImageFieldController"
import { SelectFieldController } from "../../../common/ui/form/controllers/SelectFieldController"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { getDropdownOptions } from "../../../common/ui/form/helpers/getDropdownOptions"
import { getSlugFieldOptions } from "../../../common/ui/form/helpers/getSlugFieldOptions"
import { validateStringFromEndpoint } from "../../../common/ui/form/validators/string/validateStringFromEndpoint"
import { validateStringLength } from "../../../common/ui/form/validators/string/validateStringLength"
import { visbilityOptions } from "../../core/constants"
import { getTagField } from "./getTagField"
import { getTrackOptionsField } from "./getTrackOptionsField"
import { Manager } from "../../../common/state/types/Manager"

export const createTrackFormManager = (manager: Manager) => {
  const { configStore } = manager.stores

  const typeOptions = [
    ...getDropdownOptions(configStore.config.trackTypes, "id", "name"),
    { key: "other", label: "Other" },
  ]

  const slug = getSlugFieldOptions()

  return new FormManager({
    title: new TextFieldController({
      isRequired: true,
      requiredWarning: "A track must have a title",
      validators: [validateStringLength({ max: 128 })],
    }),
    description: new TextFieldController({
      validators: [validateStringLength({ max: 2048 })],
    }),
    slug: new TextFieldController({
      isRequired: true,
      validators: [
        ...slug.validators,
        validateStringFromEndpoint({
          endpoint: "/tracks/test",
          validator: "slug",
          message: "You have another track with this slug",
        }),
      ],
      modifiers: slug.modifiers,
    }),
    type: new SelectFieldController({
      value: 1,
      options: typeOptions,
    }),
    audio: new AudioFieldController(manager, {}),
    artwork: new ImageFieldController({
      type: "artwork",
    }),
    visibility: new SelectFieldController({
      value: "listed",
      options: visbilityOptions,
    }),
    tags: getTagField(),
    options: getTrackOptionsField(),
  })
}

export type TrackFormManager = ReturnType<typeof createTrackFormManager>
