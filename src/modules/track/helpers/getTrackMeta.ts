import { getAudioPath } from "../../../common/ui/meta/helpers/getAudioPath"
import { getImagePath } from "../../../common/ui/meta/helpers/getImagePath"
import { getSafeString } from "../../../common/ui/meta/helpers/getSafeString"
import { TrackModel } from "../models/Track"

export const getTrackMeta = (track: TrackModel) => {
  const { title, description } = track.data

  const artwork = getImagePath(track.artwork)
  const audio = getAudioPath(track.audio)

  const safeDescription =
    getSafeString(description) || `Listen to "${title}" on WaveDistrict`

  return {
    title,
    audio,
    image: artwork,
    type: "music.song",
    url: track.clientLink,
    description: safeDescription,
  }
}
