import { ALLOWED_AUDIO_MIMETYPES } from "../constants"

const audioMime = ALLOWED_AUDIO_MIMETYPES

const imageMime = ["image/png", "image/jpeg", "image/gif"]

export const filterTrackUploadFileSelection = (files: File[]) => {
  const audioFiles = files.filter((x) => audioMime.includes(x.type))
  const imageFiles = files.filter((x) => imageMime.includes(x.type))

  return audioFiles.map((file, index) => {
    let artwork = imageFiles[index] || imageFiles[0]

    return {
      audio: file,
      artwork,
    }
  })
}
