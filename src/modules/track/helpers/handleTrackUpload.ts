import { addTrackUploads } from "../actions/addTrackUploads"
import { handleAnonymousTrackUpload } from "../actions/handleAnonymousTrackUpload"
import { Manager } from "../../../common/state/types/Manager"

export const handleTrackUpload = (manager: Manager, files: FileList) => {
  const { authStore } = manager.stores

  if (authStore.isAuthenticated) {
    addTrackUploads(manager, files)
    return
  }

  handleAnonymousTrackUpload(manager, files)
}
