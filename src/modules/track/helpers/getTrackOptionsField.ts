import { FieldControllerOptions } from "../../../common/ui/form/classes/FieldController"
import { BooleanFieldController } from "../../../common/ui/form/controllers/BooleanFieldController"
import { GroupFieldController } from "../../../common/ui/form/controllers/GroupFieldController"
import { TrackOptions } from "../types/TrackData"

const defaultOptions: TrackOptions = {
  commentable: true,
  downloadable: true,
  explicit: false,
}

export const getTrackOptionsField = (
  existing = defaultOptions,
  options?: Partial<FieldControllerOptions<any>>,
) => {
  return new GroupFieldController({
    value: existing,
    controllers: {
      commentable: new BooleanFieldController({}),
      downloadable: new BooleanFieldController({}),
      explicit: new BooleanFieldController({}),
    },
    ...options,
  })
}
