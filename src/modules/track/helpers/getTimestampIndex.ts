import { TrackTimestamp } from "../types/TrackData"

export const getTimestampIndex = (
  timestamps: TrackTimestamp[],
  time: number,
) => {
  const newIndex = [...timestamps]
    .reverse()
    .findIndex((timestamp) => time >= timestamp.seconds)

  return timestamps.length - 1 - newIndex
}
