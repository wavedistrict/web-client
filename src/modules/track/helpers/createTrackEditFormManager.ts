import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { ImageFieldController } from "../../../common/ui/form/controllers/ImageFieldController"
import { SelectFieldController } from "../../../common/ui/form/controllers/SelectFieldController"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { getDropdownOptions } from "../../../common/ui/form/helpers/getDropdownOptions"
import { validateStringLength } from "../../../common/ui/form/validators/string/validateStringLength"
import { visbilityOptions } from "../../core/constants"
import { TrackModel } from "../models/Track"
import { getTagField } from "./getTagField"
import { getTrackOptionsField } from "./getTrackOptionsField"
import { TimestampFieldController } from "../controllers/TimestampFieldController"
import { Manager } from "../../../common/state/types/Manager"

export const createTrackEditFormManager = (
  manager: Manager,
  track: TrackModel,
) => {
  const { configStore } = manager.stores

  const {
    title,
    description,
    type,
    tags,
    visibility,
    options,
    timestamps,
  } = track.data

  const typeOptions = [
    ...getDropdownOptions(configStore.config.trackTypes, "id", "name"),
    { key: "other", label: "Other" },
  ]

  const typeValue = configStore.config.trackTypes.find((t) => t.name === type)
  const typeId = typeValue && typeValue.id

  return new FormManager({
    title: new TextFieldController({
      value: title,
      isRequired: true,
      requiredWarning: "A track must have a title",
      validators: [validateStringLength({ max: 128 })],
    }),
    description: new TextFieldController({
      value: description,
      validators: [validateStringLength({ max: 2048 })],
    }),
    type: new SelectFieldController({
      value: typeId,
      options: typeOptions,
    }),
    artwork: new ImageFieldController({
      type: "artwork",
      reference: track.artwork,
    }),
    coverArtwork: new ImageFieldController({
      type: "coverArtwork",
      reference: track.coverArtwork,
    }),
    visibility: new SelectFieldController({
      value: visibility,
      options: visbilityOptions,
    }),
    timestamps: new TimestampFieldController({
      value: timestamps,
    }),
    tags: getTagField(tags),
    options: getTrackOptionsField(options),
  })
}

export type TrackEditFormManager = ReturnType<typeof createTrackEditFormManager>
