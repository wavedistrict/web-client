import { TagFieldController } from "../../../common/ui/form/controllers/TagFieldController"
import { validateArrayLength } from "../../../common/ui/form/validators/array/validateArrayLength"

export const getTagField = (values: string[] = []) => {
  return new TagFieldController({
    value: values,
    isRequired: true,
    validators: [validateArrayLength({ min: 1, max: 16 })],
  })
}
