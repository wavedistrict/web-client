import { TrackModel } from "../models/Track"
import { useState, useCallback, useEffect } from "react"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"
import { getTimestampIndex } from "../helpers/getTimestampIndex"
import { useAnimation } from "../../../common/ui/react/hooks/useAnimation"

export const useTimestamps = (track: TrackModel) => {
  const [current, setCurrent] = useState(-1)
  const { playerStore } = useStores()

  const { timestamps, isPlaying } = useObserver(() => ({
    timestamps: track.data.timestamps,
    isPlaying: track.isActive && playerStore.isPlaying,
  }))

  const frame = useCallback(() => {
    if (!timestamps) return

    const index = getTimestampIndex(timestamps, playerStore.currentTime)

    if (index !== current) {
      setCurrent(index)
    }
  }, [current, playerStore.currentTime, timestamps])

  useAnimation(frame, isPlaying && !!timestamps)

  useEffect(() => {
    frame()
  }, [frame])

  return {
    timestamps,
    current: timestamps ? timestamps[current] : undefined,
    index: current,
    isPlaying,
  }
}
