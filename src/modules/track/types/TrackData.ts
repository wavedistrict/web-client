import { AudioReferenceData } from "../../../common/media/types/AudioReferenceData"
import { ImageReferenceData } from "../../../common/media/types/ImageReferenceData"
import { Visibility } from "../../core/types/Visibility"
import { UserData } from "../../user/types/UserData"

export interface TrackOptions {
  commentable: boolean
  downloadable: boolean
  explicit: boolean
}

export interface TrackTimestamp {
  seconds: number
  label: string
  description?: string
}

export interface TrackData {
  id: number
  title: string
  description: string
  slug: string
  type: string
  duration: number
  user: UserData
  audio: AudioReferenceData
  artwork?: ImageReferenceData
  coverArtwork?: ImageReferenceData
  createdAt: string
  options: TrackOptions
  tags: string[]
  timestamps?: TrackTimestamp[]
  visibility: Visibility
  stats: {
    favorites: number
    comments: number
    downloads: number
    plays: number
  }
  byCurrentUser: {
    favorited: boolean
    commented: boolean
    played: boolean
  }
}
