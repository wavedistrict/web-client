import { bind } from "../../../common/lang/function/helpers/bind"
import {
  FetcherStore,
  createFetcherStoreFactory,
} from "../../../common/state/classes/FetcherStore"
import { TrackModel } from "../models/Track"
import { TrackData } from "../types/TrackData"

export class TrackStore extends FetcherStore<TrackModel> {
  private getIndex(item: TrackModel) {
    return {
      newId: item.data.id,
      index: `${item.data.user.username}:${item.data.slug}`,
    }
  }

  @bind
  public fetchById(id: number) {
    const source = `/tracks/${id}`
    return this.fetch(source, id, this.getIndex)
  }

  @bind
  public fetchByUsernameAndSlug(username: string, slug: string) {
    const source = `/users/${username}/tracks/${slug}`
    const index = `${username}:${slug}`

    return this.fetch(source, index, this.getIndex)
  }

  @bind
  public addPrefetched(data: TrackData) {
    const source = `/tracks/${data.id}`
    const index = `${data.user.username}:${data.slug}`

    return this.prepopulate(source, data.id, data, index)
  }
}

export const trackStore = createFetcherStoreFactory(TrackStore, TrackModel)
