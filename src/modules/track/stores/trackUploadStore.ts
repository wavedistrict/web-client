import { action, observable } from "mobx"
import { TrackUpload } from "../models/TrackUpload"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

export class TrackUploadStore extends InitializableStore {
  @observable public items: TrackUpload[] = []

  public init() {}

  @action
  public add(audio: File, artwork?: File) {
    const upload = new TrackUpload(this.manager)
    upload.populate(audio, artwork)

    this.items.push(upload)
    return upload
  }

  @action.bound
  public remove(id: number) {
    this.items = this.items.filter((item) => item.id !== id)
  }
}

export const trackUploadStore = createStoreFactory(TrackUploadStore)
