import React from "react"
import { handleTrackUpload } from "../helpers/handleTrackUpload"
import { styled } from "../../theme/themes"
import { cover } from "polished"
import { getOverlay, getDuration } from "../../theme/helpers"
import { useDragAndDrop } from "../../../common/ui/react/hooks/useDragAndDrop"
import { useManager } from "../../../common/state/hooks/useManager"
import { IS_SERVER } from "../../core/constants"

const Container = styled.div<{ visible: boolean }>`
  ${cover()};
  pointer-events: none;
  position: fixed;
  z-index: 5;

  justify-content: center;
  align-items: center;
  display: flex;

  opacity: 0;
  background: ${getOverlay("background")};
  transition: ${getDuration("extended")} ease opacity;

  font-size: 1.5em;

  ${(props) =>
    props.visible &&
    `
    opacity: 1;
    pointer-events: all;
  `}
`

export function TrackUploadOverlay() {
  const manager = useManager()
  const dragging = useDragAndDrop((files) => handleTrackUpload(manager, files))

  if (IS_SERVER) {
    return null
  }

  return (
    <Container onDragOver={(e) => e.preventDefault()} visible={dragging}>
      Drop audio and optionally artwork to create an upload
    </Container>
  )
}
