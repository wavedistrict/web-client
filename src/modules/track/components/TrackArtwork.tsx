import { createImageComponent } from "../../../common/ui/image/helpers/createImageComponent"
import { TrackModel } from "../models/Track"

export const TrackArtwork = createImageComponent<{ track: TrackModel }>(
  (props) => {
    const { track, id } = props

    return {
      id: `Track:${id}`,
      reference: track.artwork,
      placeholder: {
        hash: track.hash,
        icon: "trackFilled",
      },
    }
  },
)
