import React from "react"
import { HeaderButton } from "../../core/components/HeaderButton"
import { useStores } from "../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"

export function TrackUploadButton() {
  const { routingStore } = useStores()

  const handleClick = () => {
    routingStore.push("/upload")
  }

  return useObserver(() => {
    const active = routingStore.location.pathname === "/upload"

    return (
      <HeaderButton
        icon="upload"
        title="Upload"
        onClick={handleClick}
        active={active}
      />
    )
  })
}
