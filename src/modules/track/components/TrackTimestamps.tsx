import React from "react"
import { useStores } from "../../../common/state/hooks/useStores"
import { TrackModel } from "../models/Track"
import { styled } from "../../theme/themes"
import { TrackTimestamp } from "../types/TrackData"
import {
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
  getDuration,
} from "../../theme/helpers"
import { humanizeSeconds } from "../../player/helpers/humanizeSeconds"
import { useTimestamps } from "../hooks/useTimestamps"

export interface TrackTimestampsProps {
  track: TrackModel
}

export interface TimestampItemProps {
  timestamp: TrackTimestamp
  active: boolean
  passed: boolean
  index: number
  play: () => void
}

const List = styled.table``

const Item = styled.tr<{ active: boolean; passed: boolean }>`
  font-size: ${getFontSize("1")}px;
  opacity: 0.8;

  transition: ${getDuration("normal")} ease;
  transition-property: color, opacity;

  &:hover {
    opacity: 1;
    cursor: pointer;
  }

  ${(props) =>
    props.active &&
    `
    color: ${props.theme.colors.accent};
    opacity: 1;
    pointer-events: none;
  `}

  ${(props) =>
    props.passed &&
    `
    opacity: 0.4;
  `}

  & + tr {
    border-top: solid 1px ${getTransparency("weakPositive")};
  }
`

const Seconds = styled.td`
  font-weight: ${getFontWeight("bold")};
  white-space: nowrap;

  padding: ${getSpacing("3")}px 0px;
`

const Label = styled.td`
  padding: ${getSpacing("3")}px 0px;
  padding-right: ${getSpacing("3")}px;
`

export function TimestampItem(props: TimestampItemProps) {
  const { timestamp, play, active, passed, index } = props
  const { seconds, label } = timestamp

  return (
    <Item passed={passed} active={active} onClick={play}>
      <Label title={label}>
        {index}. {label}
      </Label>
      <Seconds>{humanizeSeconds(seconds)}</Seconds>
    </Item>
  )
}

export function TrackTimestamps(props: TrackTimestampsProps) {
  const { playerStore } = useStores()
  const { track } = props

  const { timestamps, isPlaying, index } = useTimestamps(track)

  if (!timestamps) {
    throw new Error(
      "Can't render timestamp component on a track without timestamps",
    )
  }

  return (
    <List>
      {timestamps.map((timestamp, i) => {
        const active = index === i && isPlaying
        const passed = index > i

        const play = () => {
          if (!isPlaying) {
            playerStore.setTrack(track)
            playerStore.play()
          }

          playerStore.currentTime = timestamp.seconds
        }

        return (
          <TimestampItem
            index={i + 1}
            key={timestamp.seconds}
            {...{ timestamp, active, passed, play }}
          />
        )
      })}
    </List>
  )
}
