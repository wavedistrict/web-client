import css from "@emotion/css"
import { size } from "polished"
import React from "react"
import { StatItem, Stats } from "../../core/components/Stats"
import {
  getDuration,
  getFontSize,
  getFontWeight,
  getSpacing,
  getTransparency,
} from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { styled } from "../../theme/themes"
import { TrackModel } from "../models/Track"
import { TrackArtwork } from "./TrackArtwork"
import {
  TrackPlayTrigger,
  TrackPlayTriggerChildrenFunctionProps,
} from "./TrackPlayTrigger"

interface SimpleTrackListItemProps {
  track: TrackModel
}

const Container = styled.div<{ isActive: boolean }>`
  padding: ${getSpacing("4")}px ${getSpacing("5")}px;
  display: flex;

  transition: ${getDuration("normal")} ease;
  transition-property: background-color, color;

  &:hover {
    background-color: ${getTransparency("weakNegative")};
    cursor: pointer;
  }

  ${(props) =>
    props.isActive &&
    css`
      background-color: ${props.theme.transparencies.strongNegative};
    `}
`

const Artwork = styled.div`
  flex-shrink: 0;
  ${size(42)}
  ${imageContainer(3)}
`

const Content = styled.div`
  margin-left: ${getSpacing("4")}px;
  overflow: hidden;
`

const PrimaryInfo = styled.div`
  display: flex;
  flex-direction: column;

  justify-content: space-between;
  height: 100%;
`

const Title = styled.div<{ isActive: boolean }>`
  font-size: ${getFontSize("1")}px;
  font-weight: ${getFontWeight("bold")};

  white-space: nowrap;
  text-overflow: ellipsis;
  max-width: 100%;
  overflow: hidden;

  ${(props) =>
    props.isActive &&
    css`
      color: ${props.theme.colors.accent};
    `}
`

export function SimpleTrackListItem(props: SimpleTrackListItemProps) {
  const { track } = props
  const { title, stats } = track.data

  const items: StatItem[] = [
    {
      name: "Plays",
      icon: "play",
      value: stats.plays,
    },
    {
      name: "Favorites",
      icon: "heartFilled",
      value: stats.favorites,
    },
    {
      name: "Comments",
      icon: "commentFilled",
      value: stats.comments,
    },
  ]

  function renderItem(props: TrackPlayTriggerChildrenFunctionProps) {
    const { isActive } = props

    return (
      <Container onClick={props.click} isActive={isActive}>
        <Artwork>
          <TrackArtwork id="SimpleTrackListItem" track={track} />
        </Artwork>
        <Content>
          <PrimaryInfo>
            <Title isActive={isActive}>{title}</Title>
            <div>
              <Stats compact tiny items={items} />
            </div>
          </PrimaryInfo>
        </Content>
      </Container>
    )
  }

  return <TrackPlayTrigger track={track}>{renderItem}</TrackPlayTrigger>
}

SimpleTrackListItem.style = {
  height: 74,
}
