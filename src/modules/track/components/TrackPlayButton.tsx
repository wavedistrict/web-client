import React from "react"
import { bind } from "../../../common/lang/function/helpers/bind"
import { PlayButton } from "../../player/components/PlayButton"
import { TrackModel } from "../models/Track"
import {
  TrackPlayTrigger,
  TrackPlayTriggerChildrenFunctionProps,
} from "./TrackPlayTrigger"

export interface TrackPlayButtonProps {
  size?: "big" | "small"
  track: TrackModel
}

export class TrackPlayButton extends React.Component<TrackPlayButtonProps> {
  @bind
  public renderContent(props: TrackPlayTriggerChildrenFunctionProps) {
    const { size } = this.props

    return (
      <PlayButton
        active={props.isActive}
        playing={props.isPlaying}
        buffering={props.isBuffering}
        disabled={!props.isPlayable}
        onClick={props.click}
        onMouseOver={props.hover}
        size={size}
      />
    )
  }

  public render() {
    const { track } = this.props

    return (
      <TrackPlayTrigger track={track}>{this.renderContent}</TrackPlayTrigger>
    )
  }
}
