import React from "react"
import { FormModal } from "../../modal/components/FormModal"
import { useModal } from "../../modal/hooks/useModal"
import { submitTrackEdit } from "../actions/submitTrackEdit"
import { TrackEditFormManager } from "../helpers/createTrackEditFormManager"
import { TrackEditForm } from "./TrackEditForm"
import { useManager } from "../../../common/state/hooks/useManager"

export interface TrackEditModalProps {
  trackId: number
  form: TrackEditFormManager
}

export function TrackEditModal(props: TrackEditModalProps) {
  const { trackId, form } = props
  const { dismiss } = useModal()
  const manager = useManager()

  async function onSubmit() {
    await submitTrackEdit(manager, form, trackId)
    dismiss()
  }

  return (
    <FormModal
      title="Edit track"
      variants={["wide"]}
      onSubmit={onSubmit}
      form={form}
    >
      <TrackEditForm form={form} />
    </FormModal>
  )
}
