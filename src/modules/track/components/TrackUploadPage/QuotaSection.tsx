import { QuotaData } from "../../../auth/types/QuotaData"
import { Section } from "../../../core/components/Section"
import React from "react"
import { Paragraph } from "../../../../common/ui/markdown/components/Paragraph"
import { styled } from "../../../theme/themes"
import { getFontColor, getFontSize, getSpacing } from "../../../theme/helpers"
import { useObserver } from "mobx-react-lite"
import { humanizeSeconds } from "../../../player/helpers/humanizeSeconds"
import { ProgressBar } from "../../../../common/state/components/ProgressBar"

export interface QuotaSectionProps {
  quota: QuotaData
}

const ProgressLabel = styled.label`
  color: ${getFontColor("muted")};
  font-size: ${getFontSize("1")}px;

  display: block;

  margin-top: ${getSpacing("4")}px;
  margin-bottom: ${getSpacing("2")}px;
`

export function QuotaSection(props: QuotaSectionProps) {
  const { quota } = props

  const { usedSeconds, availableSeconds } = useObserver(() => ({
    usedSeconds: quota.usedSeconds,
    availableSeconds: quota.availableSeconds,
  }))

  const usedPercentage = usedSeconds / availableSeconds
  const secondsLeft = availableSeconds - usedSeconds

  return (
    <Section title="Quota">
      <Paragraph>
        Your upload quota usage is the durations of all your tracks summed.
      </Paragraph>
      <Paragraph>You cannot post a track exceeding the quota.</Paragraph>
      <ProgressLabel id="quota-progress-label">
        You have {humanizeSeconds(secondsLeft)} of upload time left.
      </ProgressLabel>
      <ProgressBar
        aria-labelledby="quota-progress-label"
        value={usedPercentage}
      />
    </Section>
  )
}
