import React from "react"
import { AuthenticatedUser } from "../../../auth/types/AuthenticatedUser"
import { RequireAuthentication } from "../../../auth/components/RequireAuthentication"
import { styled } from "../../../theme/themes"
import { getSpacing } from "../../../theme/helpers"
import { PageHeader } from "../../../core/components/PageHeader"
import { UploadList } from "./UploadList"
import { QuotaSection } from "./QuotaSection"
import { BODY_WIDTH } from "../../../core/constants"
import { DisclaimerSection } from "./DisclaimerSection"
import { useMeta } from "../../../../common/ui/meta/hooks/useMeta"

export const TRACK_UPLOAD_PAGE_SIDEBAR_WIDTH = 320

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;

  @media (max-width: ${BODY_WIDTH}px) {
    flex-direction: column;
  }
`

const Sidebar = styled.div`
  margin-top: -35px;
  width: ${TRACK_UPLOAD_PAGE_SIDEBAR_WIDTH}px;
  padding-left: ${getSpacing("6")}px;

  @media (max-width: ${BODY_WIDTH}px) {
    order: 1;
    width: 100%;
    padding: 0px ${getSpacing("5")}px;
    margin-top: ${getSpacing("5")}px;
  }
`

const Header = styled.div`
  width: calc(100% - ${TRACK_UPLOAD_PAGE_SIDEBAR_WIDTH}px);

  @media (max-width: ${BODY_WIDTH}px) {
    width: 100%;
  }
`

const Content = styled.div`
  flex-grow: 1;
  margin-bottom: ${getSpacing("6")}px;

  @media (max-width: ${BODY_WIDTH}px) {
    margin-top: ${getSpacing("5")}px;
    padding: 0px ${getSpacing("5")}px;
    order: 2;
  }
`

export interface TrackUploadPageContentProps {
  user: AuthenticatedUser
}

export function TrackUploadPageContent(props: TrackUploadPageContentProps) {
  const { user } = props

  useMeta({
    title: "Upload",
  })

  return (
    <Container>
      <Header>
        <PageHeader title="Upload" icon="upload" />
      </Header>
      <Content>
        <UploadList />
      </Content>
      <Sidebar>
        <QuotaSection quota={user.quota} />
        <DisclaimerSection />
      </Sidebar>
    </Container>
  )
}

export function TrackUploadPage() {
  return (
    <RequireAuthentication>
      {(user) => <TrackUploadPageContent user={user} />}
    </RequireAuthentication>
  )
}
