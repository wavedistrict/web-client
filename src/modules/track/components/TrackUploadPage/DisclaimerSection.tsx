import { Section } from "../../../core/components/Section"
import React from "react"
import { Paragraph } from "../../../../common/ui/markdown/components/Paragraph"

export function DisclaimerSection() {
  return (
    <Section title="Copyright disclaimer">
      <Paragraph>
        Refrain from uploading content you do not own. If your track is flagged
        and it turns out you do not have the rights to that content, it will be
        taken down.
      </Paragraph>
      <Paragraph>
        If you are not sure, you can ask the rightsholder if it is OK to upload
        their content.
      </Paragraph>
    </Section>
  )
}
