import { useRef } from "react"
import { styled } from "../../../theme/themes"
import React from "react"
import { Icon } from "../../../../common/ui/icon/components/Icon"
import { handleTrackUpload } from "../../helpers/handleTrackUpload"
import {
  getSpacing,
  getTransparency,
  getDuration,
  getFontSize,
  getFontWeight,
  getFontColor,
} from "../../../theme/helpers"
import { size } from "polished"
import { useManager } from "../../../../common/state/hooks/useManager"

const Container = styled.div`
  display: flex;

  justify-content: center;
  align-items: center;

  padding: ${getSpacing("7")}px;
  height: 280px;

  border-radius: 6px;
  border: dotted 4px ${getTransparency("weakPositive")};

  transition: ${getDuration("normal")} ease background;

  &:hover {
    background: ${getTransparency("weakNegative")};
    cursor: pointer;
  }
`

const Content = styled.div`
  display: flex;
  align-items: center;

  > .icon {
    ${size(48)}

    margin-right: ${getSpacing("4")}px;
    margin-top: ${getSpacing("2")}px;

    svg {
      fill: ${getTransparency("weakPositive")};
    }
  }

  > .text > .instructions {
    font-size: ${getFontSize("3")}px;
    font-weight: ${getFontWeight("bold")};
  }

  > .text > .details {
    margin-top: ${getSpacing("2")}px;
    color: ${getFontColor("muted")};
  }
`

export function Dropzone() {
  const inputRef = useRef<HTMLInputElement>(null)
  const manager = useManager()

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault()

    const { current: input } = inputRef
    if (!input) return

    input.value = ""
    input.click()
  }

  const handleFiles = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { files } = event.currentTarget
    if (!files) return

    handleTrackUpload(manager, files)
  }

  return (
    <>
      <Container onClick={handleClick}>
        <Content>
          <div className="icon">
            <Icon name="upload" />
          </div>
          <div className="text">
            <div className="instructions">
              Click here or drag and drop an audio file to start an upload
            </div>
            <div className="details">
              You can drop multiple files, and with an image file to upload
              artwork
            </div>
          </div>
        </Content>
      </Container>
      <input
        type="file"
        accept="audio/*,image/*"
        multiple
        hidden
        ref={inputRef}
        onChange={handleFiles}
      />
    </>
  )
}
