import React from "react"
import { useStores } from "../../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"
import { TrackUploadItem } from "../TrackUploadItem/TrackUploadItem"
import { Dropzone } from "./Dropzone"

export function UploadList() {
  const { trackUploadStore } = useStores()

  const items = useObserver(() =>
    trackUploadStore.items.map((item) => (
      <TrackUploadItem key={item.id} upload={item} />
    )),
  )

  return (
    <>
      {items}
      <Dropzone />
    </>
  )
}
