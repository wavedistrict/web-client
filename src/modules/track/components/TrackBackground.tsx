import { observer } from "mobx-react"
import React from "react"
import { ImageBackground } from "../../../common/ui/image/components/ImageBackground"
import { TrackModel } from "../models/Track"

export interface TrackBackgroundProps {
  track: TrackModel
  clear?: boolean
}

@observer
export class TrackBackground extends React.Component<TrackBackgroundProps> {
  public render() {
    const { track, clear } = this.props

    const reference = track.coverArtwork || track.artwork
    const blur = track.coverArtwork ? 0 : 7

    return (
      <ImageBackground
        isClear={clear}
        reference={reference}
        hash={track.hash}
        blur={blur}
      />
    )
  }
}
