import { TrackModel } from "../../models/Track"
import { styled } from "../../../theme/themes"
import { getSpacing } from "../../../theme/helpers"
import { useObserver } from "mobx-react-lite"
import React from "react"
import { UserSummary } from "../../../user/components/UserSummary"
import { StatItem, Stats } from "../../../core/components/Stats"
import {
  MoreButton,
  MoreButtonItem,
} from "../../../../common/ui/button/components/MoreButton"
import { spawnTrackEditModal } from "../../actions/spawnTrackEditModal"
import { spawnTrackDeleteModal } from "../../actions/spawnTrackDeleteModal"
import { AddCollectionButton } from "../../../collection/components/AddCollection/AddCollectionButton"
import { FavoriteButton } from "../../../favoriting/components/FavoriteButton"
import { BODY_WIDTH } from "../../../core/constants"
import { INFOBAR_BREAKPOINT } from "./constants"
import { ButtonList } from "../../../../common/ui/button/components/ButtonList"
import { useManager } from "../../../../common/state/hooks/useManager"

export interface InfobarProps {
  track: TrackModel
}

const Container = styled.div`
  margin-top: ${getSpacing("6")}px;
  margin-bottom: ${getSpacing("5")}px;

  display: flex;
  align-items: center;

  @media (max-width: ${(props) => BODY_WIDTH + props.theme.spacings[6]}px) {
    padding-left: ${getSpacing("5")}px;
    padding-right: ${getSpacing("5")}px;
  }

  @media (max-width: ${INFOBAR_BREAKPOINT}px) {
    flex-wrap: wrap;
  }
`

const StatsContainer = styled.div`
  flex: 1;

  display: flex;
  justify-content: flex-end;
`

const Actions = styled(ButtonList)`
  display: flex;
  justify-content: flex-end;
  margin-left: ${getSpacing("6")}px;

  @media (max-width: ${INFOBAR_BREAKPOINT}px) {
    width: 100%;
    justify-content: flex-start;

    margin-left: 0px;
    margin-top: ${getSpacing("6")}px;
  }
`

export function Infobar(props: InfobarProps) {
  const manager = useManager()

  return useObserver(() => {
    const { track } = props
    const { user, data } = track
    const { stats } = data
    const { favorites } = stats

    const renderStats = () => {
      const items: StatItem[] = [
        {
          name: "Comments",
          icon: "commentFilled",
          value: stats.comments,
        },
        {
          name: "Plays",
          icon: "play",
          value: stats.plays,
        },
      ]

      if (track.isOwn)
        items.unshift({
          name: "Favorites",
          icon: "heartFilled",
          value: stats.favorites,
        })

      return <Stats items={items} />
    }

    const renderAnonymousActions = () => {
      if (track.isOwn) return

      return (
        <FavoriteButton
          noLabel={!favorites}
          label={favorites ? `${favorites}` : undefined}
          item={track}
        />
      )
    }

    const getMoreActions = (): MoreButtonItem[] => {
      if (track.isOwn) {
        return [
          {
            icon: "pencil",
            label: "Edit",
            onClick: () => spawnTrackEditModal(manager, track),
          },
          {
            icon: "trashcan",
            label: "Delete",
            onClick: () => spawnTrackDeleteModal(manager, track),
          },
        ]
      } else {
        return []
      }
    }

    return (
      <Container>
        <div>
          <UserSummary user={user} />
        </div>
        <StatsContainer>{renderStats()}</StatsContainer>
        <Actions horizontal>
          {renderAnonymousActions()}
          <AddCollectionButton noLabel track={track} />
          <MoreButton noLabel kind="secondary" items={getMoreActions()} />
        </Actions>
      </Container>
    )
  })
}
