import React from "react"
import { TrackModel } from "../../models/Track"
import { useObserver } from "mobx-react-lite"
import { Section } from "../../../core/components/Section"
import { TrackTimestamps } from "../TrackTimestamps"
import { getValueFromMap } from "../../../../common/lang/object/helpers/getValueFromMap"

export interface TimestampSectionProps {
  track: TrackModel
}

const typeToTitleMap: Record<string, string> = {
  Podcast: "Chapters",
  "Continuous Mix": "Tracklist",
}

export function TimestampSection(props: TimestampSectionProps) {
  return useObserver(() => {
    const { track } = props
    const { timestamps, type } = track.data

    if (!timestamps || timestamps.length === 0) return null

    return (
      <Section title={getValueFromMap(typeToTitleMap, type, "Timestamps")}>
        <TrackTimestamps track={track} />
      </Section>
    )
  })
}
