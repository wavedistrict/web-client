import { TrackModel } from "../../models/Track"
import { styled } from "../../../theme/themes"
import { getSpacing, getShadow } from "../../../theme/helpers"
import { size } from "polished"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { useObserver } from "mobx-react-lite"
import React from "react"
import { Section } from "../../../core/components/Section"
import { CommentForm } from "../../../commenting/components/CommentForm"
import { CommentList } from "../../../commenting/components/CommentList"
import { TrackArtwork } from "../TrackArtwork"
import { DescriptionSection } from "./DescriptionSection"
import { FavoritedBySection } from "./FavoritedBySection"
import { BODY_WIDTH } from "../../../core/constants"
import { SINGLE_COLUMN_BREAKPOINT } from "./constants"
import { TimestampSection } from "./TimestampSection"

export interface ContentProps {
  track: TrackModel
}

export const SIDEBAR_WIDTH = 280

const Container = styled.div`
  display: flex;

  @media (max-width: ${(props) => BODY_WIDTH + props.theme.spacings[6]}px) {
    padding-left: ${getSpacing("5")}px;
    padding-right: ${getSpacing("5")}px;
  }

  @media (max-width: ${SINGLE_COLUMN_BREAKPOINT}px) {
    flex-direction: column;
  }
`

const Comments = styled.div`
  flex: 1;
  min-width: 0;
`

const Sidebar = styled.div`
  flex-shrink: 0;
  width: ${SIDEBAR_WIDTH}px;

  margin-left: ${getSpacing("6")}px;

  @media (max-width: ${SINGLE_COLUMN_BREAKPOINT}px) {
    margin-left: 0px;
    margin-bottom: ${getSpacing("4")}px;

    width: 100%;
    order: -1;
  }
`

const Artwork = styled.div`
  ${size(SIDEBAR_WIDTH)};
  ${imageContainer(3)};

  margin-bottom: ${getSpacing("4")}px;
  box-shadow: ${getShadow("light")};
`

export function Content(props: ContentProps) {
  return useObserver(() => {
    const { track } = props

    const renderArtwork = () => {
      if (!track.coverArtwork) return

      return (
        <Artwork>
          <TrackArtwork title="Artwork" id="view" lightbox track={track} />
        </Artwork>
      )
    }

    return (
      <Container>
        <Comments>
          <Section title="Comments">
            <CommentForm path={track.comments.path} />
            <CommentList showContext={false} list={track.comments} />
          </Section>
        </Comments>
        <Sidebar>
          {renderArtwork()}
          <TimestampSection track={track} />
          <DescriptionSection track={track} />
          <FavoritedBySection track={track} />
        </Sidebar>
      </Container>
    )
  })
}
