import React from "react"
import { TrackModel } from "../../models/Track"
import { useStores } from "../../../../common/state/hooks/useStores"
import { useObserver } from "mobx-react-lite"
import { styled } from "../../../theme/themes"
import { getTrackMeta } from "../../helpers/getTrackMeta"
import { Hero } from "./Hero"
import { getSpacing } from "../../../theme/helpers"
import { Infobar } from "./Infobar"
import { Content } from "./Content"
import { FetcherView } from "../../../../common/state/components/FetcherView"
import { useMeta } from "../../../../common/ui/meta/hooks/useMeta"

export interface TrackViewProps {
  track: TrackModel
}

export interface TrackPageProps {
  username: string
  slug: string
}

const Container = styled.div`
  margin-top: ${getSpacing("6")}px;
`

export function TrackView(props: TrackViewProps) {
  const { track } = props

  useMeta(getTrackMeta(track))

  return useObserver(() => (
    <Container>
      <Hero track={track} />
      <Infobar track={track} />
      <Content track={track} />
    </Container>
  ))
}

export function TrackPage(props: TrackPageProps) {
  const { trackStore } = useStores()
  const { username, slug } = props

  return (
    <FetcherView
      empty={{ message: "Track not found", icon: "trackFilled" }}
      fetcher={trackStore.fetchByUsernameAndSlug(username, slug)}
    >
      {(model) => <TrackView track={model} />}
    </FetcherView>
  )
}
