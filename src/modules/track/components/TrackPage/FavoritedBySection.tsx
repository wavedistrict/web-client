import React from "react"
import { TrackModel } from "../../models/Track"
import { useObserver } from "mobx-react-lite"
import { Section } from "../../../core/components/Section"
import { UserListSummary } from "../../../user/components/UserListSummary"

export interface FavoritedBySectionProps {
  track: TrackModel
}

export function FavoritedBySection(props: FavoritedBySectionProps) {
  return useObserver(() => {
    const { track } = props
    const { stats } = track.data

    if (stats.favorites > 0) {
      return (
        <Section title="Favorited by">
          <UserListSummary list={track.favoriters} />
        </Section>
      )
    } else {
      return null
    }
  })
}
