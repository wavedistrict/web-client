import { TrackModel } from "../../models/Track"
import { useObserver } from "mobx-react-lite"
import { styled } from "../../../theme/themes"
import {
  getSpacing,
  getFontColor,
  getFontWeight,
  getFontSize,
  getDuration,
} from "../../../theme/helpers"
import React from "react"
import { TrackPlayButton } from "../TrackPlayButton"
import { getItem } from "../../../../common/lang/array/helpers/getItem"
import { Tag } from "../../../core/components/Tag"
import { HumanizedDate } from "../../../core/components/HumanizedDate"
import { Visualizer } from "../../../player/components/Visualizer"
import { Seeker } from "../../../player/components/Seeker"
import {
  TINY_ARTWORK_BREAKPOINT,
  VIEWPORT_ARTWORK_BREAKPOINT,
} from "./constants"

export interface HeroContentProps {
  track: TrackModel
}

const Container = styled.div<{ hasCover: boolean }>`
  color: ${getFontColor("normal")};
  display: flex;

  flex-direction: column;
  flex: 1;

  ${(props) =>
    props.hasCover &&
    `
    height: 450px;
  `}

  @media (max-width: ${VIEWPORT_ARTWORK_BREAKPOINT}px) {
    margin-left: 0px;
    justify-content: space-between;
  }
`

const Header = styled.div`
  display: flex;
`

const PrimaryInfo = styled.div`
  margin-left: ${getSpacing("4")}px;
  flex: 1;
`

const Title = styled.h1`
  font-size: ${getFontSize("2")}px;
  font-weight: ${getFontWeight("bold")};
`

const Type = styled.h2`
  font-size: ${getFontSize("1")}px;
  font-weight: ${getFontWeight("normal")};
  color: ${getFontColor("muted")};
`

const SecondaryInfo = styled.div`
  display: flex;

  flex-direction: column;
  align-items: flex-end;

  margin-left: ${getSpacing("2")}px;
`

const UploadedAt = styled.span`
  font-size: ${getFontSize("1")}px;
  margin-top: ${getSpacing("2")}px;
`

const Body = styled.div<{ active: boolean }>`
  display: flex;

  flex: 1;
  flex-direction: column;
  justify-content: flex-end;

  padding-bottom: ${getSpacing("4")}px;

  opacity: 0;
  transition: ${getDuration("normal")} ease opacity;

  ${(props) =>
    props.active &&
    `
    opacity: 1;
  `}
`

const VisualizerContainer = styled.div`
  @media (max-width: ${TINY_ARTWORK_BREAKPOINT}px) {
    display: none;
  }
`

const Footer = styled.div``

export function HeroContent(props: HeroContentProps) {
  return useObserver(() => {
    const { track } = props
    const { title, type, tags, createdAt } = track.data

    return (
      <Container hasCover={!!track.coverArtwork}>
        <Header>
          <div>
            <TrackPlayButton size="big" track={track} />
          </div>
          <PrimaryInfo>
            <Title>{title}</Title>
            <Type>{type}</Type>
          </PrimaryInfo>
          <SecondaryInfo>
            <div>
              <Tag name={getItem(tags, 0, "Untagged")} />
            </div>
            <UploadedAt>
              <HumanizedDate>{createdAt}</HumanizedDate>
            </UploadedAt>
          </SecondaryInfo>
        </Header>
        <Body active={track.isActive}>
          <VisualizerContainer>
            <Visualizer />
          </VisualizerContainer>
        </Body>
        <Footer>
          <Seeker track={track} />
        </Footer>
      </Container>
    )
  })
}
