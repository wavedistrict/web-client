import React from "react"
import { TrackModel } from "../../models/Track"
import { styled } from "../../../theme/themes"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { getShadow, getSpacing } from "../../../theme/helpers"
import { cover, size } from "polished"
import { useObserver } from "mobx-react-lite"
import { TrackBackground } from "../TrackBackground"
import { TrackArtwork } from "../TrackArtwork"
import { HeroContent } from "./HeroContent"
import { Overlay } from "../../../theme/components/Overlay"
import {
  SMALL_ARTWORK_BREAKPOINT,
  TINY_ARTWORK_BREAKPOINT,
  VIEWPORT_ARTWORK_BREAKPOINT,
} from "./constants"

export interface HeroProps {
  track: TrackModel
}

const Container = styled.div`
  ${imageContainer(3)};
  box-shadow: ${getShadow("light")};

  @media (max-width: ${SMALL_ARTWORK_BREAKPOINT}px) {
    margin: 0px;
    margin-top: -${getSpacing("6")}px;

    border-radius: 0px;
  }
`

const Background = styled.div`
  ${cover()};
`

const Foreground = styled.div`
  position: relative;
  padding: ${getSpacing("5")}px;

  display: flex;

  @media (max-width: ${VIEWPORT_ARTWORK_BREAKPOINT}px) {
    height: 100%;
    flex-direction: column;
  }
`

const Artwork = styled.div`
  ${size(330)};
  ${imageContainer(3)};

  flex-shrink: 0;
  margin-right: ${getSpacing("5")}px;

  @media (max-width: ${SMALL_ARTWORK_BREAKPOINT}px) {
    ${size(250)};
  }

  @media (max-width: ${TINY_ARTWORK_BREAKPOINT}px) {
    ${size(150)};
  }

  @media (max-width: ${VIEWPORT_ARTWORK_BREAKPOINT}px) {
    ${size("calc(100vw - 48px)")}
    margin-right: 0px;
    margin-bottom: ${getSpacing("6")}px;
  }
`

export function Hero(props: HeroProps) {
  const { track } = props

  const renderArtwork = () => {
    if (track.coverArtwork) return

    return (
      <Artwork>
        <TrackArtwork id="track-hero" track={track} lightbox />
      </Artwork>
    )
  }

  return useObserver(() => (
    <Container>
      <Overlay>
        <Background>
          <TrackBackground track={track} clear={track.isActive} />
        </Background>
        <Foreground>
          {renderArtwork()}
          <HeroContent track={track} />
        </Foreground>
      </Overlay>
    </Container>
  ))
}
