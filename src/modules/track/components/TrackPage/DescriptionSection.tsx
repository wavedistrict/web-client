import React from "react"
import { TrackModel } from "../../models/Track"
import { Section } from "../../../core/components/Section"
import { Description } from "../../../core/components/Description"
import { useObserver } from "mobx-react-lite"

export interface DescriptionSectionProps {
  track: TrackModel
}

export function DescriptionSection(props: DescriptionSectionProps) {
  return useObserver(() => {
    const { track } = props
    const { description, tags } = track.data

    return (
      <Section title="Description">
        <Description text={description} tags={tags} />
      </Section>
    )
  })
}
