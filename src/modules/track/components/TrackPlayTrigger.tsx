import { observer } from "mobx-react"
import React from "react"
import { QueueData } from "../../player/types/QueueData"
import { TrackModel } from "../models/Track"
import { TrackList } from "./TrackList"
import { useStores } from "../../../common/state/hooks/useStores"

export interface TrackPlayTriggerChildrenFunctionProps {
  isActive: boolean
  isPlaying: boolean
  isBuffering: boolean
  isPlayable: boolean
  click: () => void
  hover: () => void
}

export interface TrackPlayTriggerContentProps {
  children: (props: TrackPlayTriggerChildrenFunctionProps) => React.ReactNode
  track: TrackModel
  queue?: QueueData
}

export const TrackPlayTriggerContent = observer(
  function TrackPlayTriggerContent(props: TrackPlayTriggerContentProps) {
    const { playerStore, queueStore } = useStores()
    const { track, children, queue } = props

    const isQueueActive =
      !queue ||
      (queueStore.list && queueStore.list.path === queue.list.path) ||
      false

    const handleClick = () => {
      if (!isQueueActive && queue && track.isActive) {
        queueStore.setQueueData(queue)
        return
      }

      if (!track.isActive) {
        playerStore.setTrack(track)
        playerStore.play()
      } else {
        playerStore.toggle()
      }

      if (queue) queueStore.setQueueData(queue)
    }

    const handleHover = () => {
      if (track.isActive || !track.isPlayable) return
      playerStore.preload(track)
    }

    const childrenProps = {
      isActive: track.isActive && isQueueActive,
      isPlaying: track.isActive && playerStore.isPlaying,
      isPlayable: track.isPlayable,
      isBuffering: playerStore.isBuffering,
      click: handleClick,
      hover: handleHover,
    }

    return children(childrenProps) as any
  },
)

export type TrackPlayTriggerProps = Omit<TrackPlayTriggerContentProps, "queue">

export function TrackPlayTrigger(props: TrackPlayTriggerProps) {
  return (
    <TrackList.Consumer>
      {(data) => <TrackPlayTriggerContent queue={data} {...props} />}
    </TrackList.Consumer>
  )
}
