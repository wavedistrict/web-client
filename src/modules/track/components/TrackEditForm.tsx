import React from "react"
import { FormField } from "../../../common/ui/form/components/FormField"
import { FormLayout } from "../../../common/ui/form/components/FormLayout"
import { renderControlledDropdownInput } from "../../../common/ui/input/components/ControlledDropdownInput"
import { renderControlledImageInput } from "../../../common/ui/input/components/ControlledImageInput"
import { renderControlledTagInput } from "../../../common/ui/input/components/ControlledTagInput"
import { renderControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { LocalTabs } from "../../../common/ui/navigation/components/LocalTabs"
import { TrackEditFormManager } from "../helpers/createTrackEditFormManager"
import { TrackOptionsForm } from "./TrackOptionsForm"
import { renderControlledTimestampInput } from "./ControlledTimestampInput"

export class TrackEditForm extends React.Component<{
  form: TrackEditFormManager
}> {
  private renderGeneral() {
    const { form } = this.props
    const { fields } = form

    return (
      <FormLayout>
        <FormLayout.Sidebar>
          <FormField
            label="Artwork"
            controller={fields.artwork}
            renderInput={renderControlledImageInput({
              isSquare: true,
            })}
          />
        </FormLayout.Sidebar>
        <FormLayout.Grid>
          <FormLayout.GridCell size="full">
            <FormField
              label="Title"
              controller={fields.title}
              renderInput={renderControlledTextInput({ autoFocus: true })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Type"
              controller={fields.type}
              renderInput={renderControlledDropdownInput()}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Description"
              controller={fields.description}
              renderInput={renderControlledTextInput({ multiline: true })}
            />
          </FormLayout.GridCell>
          <FormLayout.GridCell size="full">
            <FormField
              label="Tags"
              controller={fields.tags}
              renderInput={renderControlledTagInput()}
            />
          </FormLayout.GridCell>
        </FormLayout.Grid>
      </FormLayout>
    )
  }

  private renderMore() {
    const { form } = this.props
    const { fields } = form

    return (
      <FormLayout>
        <FormLayout.Header>
          <FormField
            label="Cover artwork"
            controller={fields.coverArtwork}
            renderInput={renderControlledImageInput()}
          />
          <FormField
            label="Timestamps"
            controller={fields.timestamps}
            renderInput={renderControlledTimestampInput()}
          />
        </FormLayout.Header>
      </FormLayout>
    )
  }

  public render() {
    const tabs = {
      general: {
        label: "General",
        renderContent: () => this.renderGeneral(),
      },
      options: {
        label: "Options",
        renderContent: () => <TrackOptionsForm form={this.props.form} />,
      },
      more: {
        label: "More",
        renderContent: () => this.renderMore(),
      },
    }

    return <LocalTabs tabs={tabs} />
  }
}
