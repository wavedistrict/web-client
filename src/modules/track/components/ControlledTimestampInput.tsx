import { ArrayFieldController } from "../../../common/ui/form/controllers/ArrayFieldController"
import { TrackTimestamp } from "../types/TrackData"
import React, { useState } from "react"
import { Table } from "../../../common/ui/markdown/components/Table"
import { styled } from "../../theme/themes"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { getSpacing } from "../../theme/helpers"
import { useObserver } from "mobx-react-lite"
import { TextInput } from "../../../common/ui/input/components/TextInput"
import { TimestampFieldController } from "../controllers/TimestampFieldController"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"

export interface ControlledTimestampInputProps {
  controller: TimestampFieldController
}

const Container = styled.div``

const ButtonContainer = styled.div`
  margin-top: ${getSpacing("4")}px;
`

const SecondsInputContainer = styled(Table.Data)`
  max-width: 80px;
`

const LabelContainer = styled.div`
  display: flex;
`

const LabelInputContainer = styled.div`
  flex: 1;
  margin-right: ${getSpacing("4")}px;
`

export function ControlledTimestampInput(props: ControlledTimestampInputProps) {
  const { controller } = props
  const timestamps = useObserver(() => controller.value)

  const renderRow = (timestamp: TrackTimestamp, index: number) => {
    const { label, seconds } = timestamp

    const update = (label: string, seconds: number) => {
      const newTimestamps = [...timestamps]
      newTimestamps[index] = { label, seconds }

      controller.setValue(newTimestamps)
    }

    const remove = () => {
      controller.setValue(timestamps.filter((_, i) => i !== index))
    }

    return (
      <Table.Row key={index}>
        <SecondsInputContainer>
          <TextInput
            type="number"
            value={seconds}
            onChange={(value) => update(label, value as any)}
          />
        </SecondsInputContainer>
        <Table.Data>
          <LabelContainer>
            <LabelInputContainer>
              <TextInput
                value={label}
                onChange={(value) => update(value, seconds)}
              />
            </LabelInputContainer>
            <SecondaryButton outlined icon="failed" onClick={remove} />
          </LabelContainer>
        </Table.Data>
      </Table.Row>
    )
  }

  return (
    <Container>
      <Table>
        <Table.Head>
          <Table.Row>
            <Table.Heading>Seconds</Table.Heading>
            <Table.Heading>Label</Table.Heading>
          </Table.Row>
        </Table.Head>
        <Table.Body>{timestamps.map(renderRow)}</Table.Body>
      </Table>
      <ButtonContainer>
        <PrimaryButton
          stretch
          label="Add timestamp"
          icon="add"
          onClick={() =>
            controller.setValue([...timestamps, { seconds: 0, label: "" }])
          }
        />
      </ButtonContainer>
    </Container>
  )
}

export const renderControlledTimestampInput = (
  props: Omit<ControlledTimestampInputProps, "controller"> = {},
) => (controller: ArrayFieldController<TrackTimestamp>) => (
  <ControlledTimestampInput {...props} controller={controller} />
)
