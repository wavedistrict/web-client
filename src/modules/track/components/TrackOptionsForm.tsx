import React from "react"
import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { FormField } from "../../../common/ui/form/components/FormField"
import { FormLayout } from "../../../common/ui/form/components/FormLayout"
import { BooleanFieldController } from "../../../common/ui/form/controllers/BooleanFieldController"
import { GroupFieldController } from "../../../common/ui/form/controllers/GroupFieldController"
import { SelectFieldController } from "../../../common/ui/form/controllers/SelectFieldController"
import { renderControlledDropdownInput } from "../../../common/ui/input/components/ControlledDropdownInput"
import { renderControlledSwitchInput } from "../../../common/ui/input/components/ControlledSwitchInput"
import { FormContext } from "../../../common/ui/form/contexts/FormContext"

export interface TrackOptionsFormProps {
  form: FormManager<{
    options: GroupFieldController<
      any,
      {
        commentable: BooleanFieldController
        downloadable: BooleanFieldController
        explicit: BooleanFieldController
      },
      any
    >
    visibility: SelectFieldController
  }>
}

export class TrackOptionsForm extends React.Component<TrackOptionsFormProps> {
  public render() {
    const { form } = this.props
    const { fields } = form

    const { commentable, downloadable, explicit } = fields.options.controllers

    return (
      <FormContext.Provider value={{ form }}>
        <FormLayout>
          <FormLayout.List>
            <FormField
              label="Enable comments"
              variants={["inline"]}
              controller={commentable}
              renderInput={renderControlledSwitchInput()}
            />
            <FormField
              label="Enable downloads"
              variants={["inline"]}
              controller={downloadable}
              renderInput={renderControlledSwitchInput()}
            />
            <FormField
              label="Explicit"
              variants={["inline"]}
              controller={explicit}
              renderInput={renderControlledSwitchInput()}
            />
            <FormField
              label="Visibility"
              variants={["inline"]}
              controller={fields.visibility}
              renderInput={renderControlledDropdownInput()}
            />
          </FormLayout.List>
        </FormLayout>
      </FormContext.Provider>
    )
  }
}
