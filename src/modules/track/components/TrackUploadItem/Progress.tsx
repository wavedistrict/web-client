import React from "react"
import { ProgressBar } from "../../../../common/state/components/ProgressBar"
import { TrackFormManager } from "../../helpers/createTrackFormManager"
import { useObserver } from "mobx-react-lite"

export interface ProgressProps {
  form: TrackFormManager
}

export function Progress(props: ProgressProps) {
  return useObserver(() => {
    const { audio } = props.form.fields
    let progress = audio.progress

    if (audio.processingAudio) {
      progress = audio.processingAudio.progress
    }

    return <ProgressBar value={progress / 100} />
  })
}
