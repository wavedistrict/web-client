import React from "react"
import { useObserver } from "mobx-react-lite"

import { getSpacing, getFontSize, getFontWeight } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"

import { TrackUpload } from "../../models/TrackUpload"
import { StatusIcon } from "./StatusIcon"
import { Status } from "./Status"

import { RouteLink } from "../../../../common/routing/components/RouteLink"
import { StyledRouteLink } from "../../../../common/routing/components/StyledRouteLink"
import { Actions } from "./Actions"

export interface FooterProps {
  upload: TrackUpload
}

const Container = styled.div`
  padding-top: ${getSpacing("4")}px;

  display: flex;
  flex-wrap: wrap;
  align-items: center;

  > .icon {
    margin-right: ${getSpacing("4")}px;
  }

  > .status {
    font-size: ${getFontSize("2")}px;
    font-weight: ${getFontWeight("normal")};

    flex: 1;
    min-width: 60%;
  }

  > .status > .primary {
    font-weight: ${getFontWeight("bold")};
  }

  > .status > .secondary {
    font-size: ${getFontSize("1")}px;
    margin-top: ${getSpacing("2")}px;

    height: 19px;
  }
`

export function Footer(props: FooterProps) {
  const { upload } = props
  const { form } = upload

  const renderSubStatus = () => {
    if (!upload.track) return "Click save to submit track"
    if (upload.isAttemptingSave) return "Saving..."

    return (
      <>
        {"Your track has been submitted. "}
        <StyledRouteLink to={upload.track.clientLink} onClick={upload.remove}>
          {"Click here to go to it!"}
        </StyledRouteLink>
      </>
    )
  }

  return useObserver(() => (
    <Container>
      <div className="icon">
        <StatusIcon form={form} />
      </div>
      <div className="status">
        <div className="primary">
          <Status form={form} />
        </div>
        <div className="secondary">{renderSubStatus()}</div>
      </div>
      <Actions upload={upload} />
    </Container>
  ))
}
