import React from "react"
import { MediaReferenceStatus } from "../../../../common/media/types/MediaReferenceData"
import { FileUploadStatus } from "../../../../common/network/classes/FileUpload"
import { IconType } from "../../../../common/ui/icon"
import { Icon } from "../../../../common/ui/icon/components/Icon"
import { TrackFormManager } from "../../helpers/createTrackFormManager"
import { useObserver } from "mobx-react-lite"
import { styled } from "../../../theme/themes"
import { size } from "polished"
import { getDuration } from "../../../theme/helpers"
import { spinClockwise } from "../../../theme/animations"
import css from "@emotion/css"

export interface StatusIconProps {
  form: TrackFormManager
}

const Container = styled.div<{ icon: IconType }>`
  ${size(36)};

  transition: ${getDuration("normal")} ease;
  transition-property: opacity, transform;

  svg {
    transition: ${getDuration("normal")} ease fill;
  }

  ${(props) =>
    props.icon === "wait" &&
    `
    opacity: 0.4;
  `}

  ${(props) =>
    props.icon === "cog" &&
    css`
      animation-name: ${spinClockwise};
      animation-duration: 2s;
      animation-fill-mode: forwards;
      animation-iteration-count: infinite;
      animation-timing-function: linear;

      svg {
        fill: ${props.theme.colors.accent};
      }
    `}

  ${(props) =>
    props.icon === "done" &&
    `
    svg {
      fill: ${props.theme.colors.success};
    }
  `}

  ${(props) =>
    props.icon === "failed" &&
    `
    transform: scale(1.2);

    svg {
      fill: ${props.theme.colors.invalid};
    }
  `}

  ${(props) =>
    props.icon === "upload" &&
    `
    transform: scale(0.8);
  `}
`

export function StatusIcon(props: StatusIconProps) {
  return useObserver(() => {
    const { audio } = props.form.fields

    const statusMap: Record<FileUploadStatus, IconType> = {
      Checking: "wait",
      Uploading: "upload",
      Complete: "done",
    }

    let icon: IconType = statusMap[audio.uploadStatus]

    if (audio.processingAudio) {
      const statusMap: Record<MediaReferenceStatus, IconType> = {
        idle: "wait",
        processing: "cog",
        completed: "done",
        error: "failed",
      }

      icon = statusMap[audio.processingAudio.data.status]
    }

    if (audio.error) icon = "failed"

    return (
      <Container icon={icon}>
        <Icon name={icon} />
      </Container>
    )
  })
}
