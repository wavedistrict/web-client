import React from "react"
import { FormField } from "../../../../common/ui/form/components/FormField"
import { FormLayout } from "../../../../common/ui/form/components/FormLayout"
import { FormContext } from "../../../../common/ui/form/contexts/FormContext"
import { renderControlledDropdownInput } from "../../../../common/ui/input/components/ControlledDropdownInput"
import { renderControlledImageInput } from "../../../../common/ui/input/components/ControlledImageInput"
import { renderControlledTagInput } from "../../../../common/ui/input/components/ControlledTagInput"
import { renderControlledTextInput } from "../../../../common/ui/input/components/ControlledTextInput"
import { TrackFormManager } from "../../helpers/createTrackFormManager"

export class TrackUploadForm extends React.Component<{
  form: TrackFormManager
}> {
  public render() {
    const { form } = this.props
    const { fields } = form

    return (
      <FormContext.Provider value={{ form }}>
        <FormLayout>
          <FormLayout.Sidebar>
            <FormField
              label="Artwork"
              controller={fields.artwork}
              renderInput={renderControlledImageInput({
                isSquare: true,
              })}
            />
          </FormLayout.Sidebar>
          <FormLayout.Grid>
            <FormLayout.GridCell size="full">
              <FormField
                label="Title"
                controller={fields.title}
                renderInput={renderControlledTextInput()}
              />
            </FormLayout.GridCell>
            <FormLayout.GridCell size="half">
              <FormField
                label="Slug"
                controller={fields.slug}
                renderInput={renderControlledTextInput()}
              />
            </FormLayout.GridCell>
            <FormLayout.GridCell size="half">
              <FormField
                label="Type"
                controller={fields.type}
                renderInput={renderControlledDropdownInput()}
              />
            </FormLayout.GridCell>
            <FormLayout.GridCell size="full">
              <FormField
                label="Description"
                controller={fields.description}
                renderInput={renderControlledTextInput({ multiline: true })}
              />
            </FormLayout.GridCell>
            <FormLayout.GridCell size="full">
              <FormField
                label="Tags"
                controller={fields.tags}
                renderInput={renderControlledTagInput()}
              />
            </FormLayout.GridCell>
          </FormLayout.Grid>
        </FormLayout>
      </FormContext.Provider>
    )
  }
}
