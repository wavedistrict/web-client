import React from "react"
import { styled } from "../../../theme/themes"
import { getColor, getSpacing, getShadow } from "../../../theme/helpers"

import { TrackUpload } from "../../models/TrackUpload"
import { TrackUploadForm } from "./TrackUploadForm"
import { TrackOptionsForm } from "../TrackOptionsForm"

import { Overview } from "./Overview"
import { LocalTabs } from "../../../../common/ui/navigation/components/LocalTabs"
import { Progress } from "./Progress"
import { Footer } from "./Footer"

export interface TrackUploadItemProps {
  upload: TrackUpload
}

const Container = styled.div`
  background: ${getColor("primary")};
  padding: ${getSpacing("5")}px;

  margin-bottom: ${getSpacing("6")}px;
  border-radius: 3px;

  box-shadow: ${getShadow("light")};
`

const ProgressContainer = styled.div`
  margin-top: ${getSpacing("4")}px;
`

export function TrackUploadItem(props: TrackUploadItemProps) {
  const { upload } = props
  const { form } = upload

  const renderTabs = () => {
    const tabs = {
      general: {
        label: "General",
        renderContent: () => <TrackUploadForm form={form} />,
      },
      options: {
        label: "Options",
        renderContent: () => <TrackOptionsForm form={form} />,
      },
      overview: {
        label: "Overview",
        renderContent: () => <Overview form={form} />,
      },
    }

    return <LocalTabs tabs={tabs} />
  }

  return (
    <Container>
      <div>{renderTabs()}</div>
      <ProgressContainer>
        <Progress form={form} />
      </ProgressContainer>
      <Footer upload={upload} />
    </Container>
  )
}
