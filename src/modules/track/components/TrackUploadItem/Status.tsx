import React from "react"
import { getValueFromMap } from "../../../../common/lang/object/helpers/getValueFromMap"
import { MediaReferenceStatus } from "../../../../common/media/types/MediaReferenceData"
import {
  FileUploadError,
  FileUploadStatus,
} from "../../../../common/network/classes/FileUpload"
import { TrackFormManager } from "../../helpers/createTrackFormManager"
import { useObserver } from "mobx-react-lite"

export interface StatusProps {
  form: TrackFormManager
}

export function Status(props: StatusProps) {
  return useObserver(() => {
    const { audio } = props.form.fields

    if (audio.processingAudio) {
      const { status } = audio.processingAudio.data

      const statusMap: Record<MediaReferenceStatus, string> = {
        idle: "Queued.",
        processing: "Processing...",
        error: "Processing failed.",
        completed: "Done.",
      }

      return <span>{getValueFromMap(statusMap, status, "")}</span>
    }

    if (audio.error) {
      const errorMap: Record<FileUploadError, string> = {
        Invalid: "File invalid or corrupt.",
        Unknown: "An unknown error occurred.",
        Failed: "Failed to upload file.",
      }

      return <span>{getValueFromMap(errorMap, audio.error, "")}</span>
    }

    const statusMap: Record<FileUploadStatus, string> = {
      Uploading: `Uploading - ${Math.round(audio.progress)}%`,
      Checking: "Verifying...",
      Complete: "Done.",
    }

    return <span>{getValueFromMap(statusMap, audio.uploadStatus, "")}</span>
  })
}
