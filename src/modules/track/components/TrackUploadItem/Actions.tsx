import React from "react"
import { styled } from "../../../theme/themes"
import { TrackUpload } from "../../models/TrackUpload"
import { getSpacing } from "../../../theme/helpers"
import { useObserver } from "mobx-react-lite"
import { SecondaryButton } from "../../../../common/ui/button/components/SecondaryButton"
import { AsyncButton } from "../../../../common/ui/button/components/AsyncButton"

export interface ActionProps {
  upload: TrackUpload
}

const Container = styled.div`
  margin-top: ${getSpacing("3")}px;
  display: flex;

  > * + * {
    margin-left: ${getSpacing("3")}px;
  }
`

export function Actions(props: ActionProps) {
  const { upload } = props

  return useObserver(() => (
    <Container>
      <SecondaryButton
        label={upload.track ? "Close" : "Cancel"}
        onClick={upload.stop}
      />
      <AsyncButton
        kind="primary"
        label={upload.isAttemptingSave ? "Saving..." : "Save"}
        onClick={upload.save}
      />
    </Container>
  ))
}
