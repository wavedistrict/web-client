import React from "react"
import { ProcessingMediaSource } from "../../../../common/media/models/ProcessingMediaSource"
import { EmptyState } from "../../../../common/state/components/EmptyState"
import { ProgressBar } from "../../../../common/state/components/ProgressBar"
import { humanizeSeconds } from "../../../player/helpers/humanizeSeconds"
import { TrackFormManager } from "../../helpers/createTrackFormManager"
import { styled } from "../../../theme/themes"
import { getSpacing, getFontSize, getFontWeight } from "../../../theme/helpers"
import { useObserver } from "mobx-react-lite"

export interface OverviewProps {
  form: TrackFormManager
}

const Container = styled.div``

const SourceGrid = styled.ul`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 24px;
`

const SourceItem = styled.li`
  margin-bottom: ${getSpacing("4")}px;

  > .name {
    display: block;

    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("bold")};

    margin-bottom: ${getSpacing("4")}px;
  }
`

const MetaList = styled.ul`
  margin-top: ${getSpacing("5")}px;
  padding-bottom: ${getSpacing("3")}px;

  display: flex;
  justify-content: space-between;
`

const MetaItem = styled.li<{ warning: boolean }>`
  padding-right: ${getSpacing("7")}px;

  ${(props) =>
    props.warning &&
    `
    color: ${props.theme.colors.warning}
  `}

  > .name {
    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("bold")};
    text-transform: uppercase;

    margin-bottom: ${getSpacing("2")}px;
  }

  > .value {
    font-size: ${getFontSize("3")}px;
  }
`

export function Overview(props: OverviewProps) {
  return useObserver(() => {
    const { audio } = props.form.fields

    if (!audio.processingAudio) {
      return (
        <Container>
          <EmptyState
            message="Please wait for the file to finish uploading"
            icon="wait"
          />
        </Container>
      )
    }

    const { sources, data } = audio.processingAudio

    const renderSource = (source: ProcessingMediaSource<unknown>) => {
      const { progress } = source.data

      return (
        <SourceItem key={source.data.name}>
          <label id={source.humanizedName} className="name">
            {source.humanizedName}
          </label>
          <div className="progress">
            <ProgressBar
              aria-labelledby={source.humanizedName}
              value={progress / 100}
            />
          </div>
        </SourceItem>
      )
    }

    const renderSources = () => {
      if (sources.length === 0) {
        return <EmptyState message="Waiting for sources..." icon="wait" />
      }

      return <SourceGrid>{sources.map(renderSource)}</SourceGrid>
    }

    const renderMetaItem = (name: string, value: string, warning = false) => {
      return (
        <MetaItem warning={warning}>
          <div className="name">{name}</div>
          <div className="value">{value}</div>
        </MetaItem>
      )
    }

    const renderMeta = () => {
      const processing = audio.processingAudio!
      const { duration, channels, loudness } = data.metadata

      const lufs = loudness ? `${loudness.lufs} LUFS` : "-"
      const isLoud = loudness ? loudness.lufs > -10 : false
      const source = processing.source ? processing.source.humanizedName : "-"

      return (
        <MetaList>
          {renderMetaItem("Source", source)}
          {renderMetaItem("Progress", `${Math.floor(processing.progress)}%`)}
          {renderMetaItem(
            "Duration",
            duration ? humanizeSeconds(duration) : "--:--",
          )}
          {renderMetaItem("Channels", channels ? String(channels) : "--")}
          {renderMetaItem("Loudness", lufs, isLoud)}
        </MetaList>
      )
    }

    return (
      <Container>
        {renderSources()}
        {renderMeta()}
      </Container>
    )
  })
}
