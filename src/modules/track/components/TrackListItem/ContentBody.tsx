import { observer } from "mobx-react"
import React from "react"
import { Seeker } from "../../../player/components/Seeker"
import { styled } from "../../../theme/themes"
import { TrackListItemProps } from "./TrackListItem"

const Container = styled.div`
  flex: 1;
  align-items: flex-end;
  display: flex;

  > .seeker {
    flex: 1;
  }
`

export const ContentBody = observer(function ContentBody(
  props: TrackListItemProps,
) {
  const { track } = props

  return (
    <Container>
      <div className="seeker">
        <Seeker track={track} />
      </div>
    </Container>
  )
})
