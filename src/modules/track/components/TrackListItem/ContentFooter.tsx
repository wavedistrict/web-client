import { observer } from "mobx-react"
import React from "react"
import { DownloadButton } from "../../../../common/media/components/DownloadButton"
import {
  MoreButton,
  MoreButtonItem,
} from "../../../../common/ui/button/components/MoreButton"
import { AddCollectionButton } from "../../../collection/components/AddCollection/AddCollectionButton"
import { StatItem, Stats } from "../../../core/components/Stats"
import { FavoriteButton } from "../../../favoriting/components/FavoriteButton"
import { getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { spawnTrackDeleteModal } from "../../actions/spawnTrackDeleteModal"
import { spawnTrackEditModal } from "../../actions/spawnTrackEditModal"
import { TrackListItemProps } from "./TrackListItem"
import { PrimaryButtonVariants } from "../../../../common/ui/button/components/PrimaryButton"
import { ButtonList } from "../../../../common/ui/button/components/ButtonList"
import { useManager } from "../../../../common/state/hooks/useManager"

const Container = styled.div`
  padding: ${getSpacing("4")}px;

  margin: -${getSpacing("4")}px;
  margin-top: 0px;

  display: flex;
  justify-content: space-between;
  align-items: center;
`

const Actions = styled(ButtonList)``

export const ContentFooter = observer(function ContentFooter(
  props: TrackListItemProps,
) {
  const manager = useManager()

  const { track } = props
  const { stats } = track.data

  const renderStats = () => {
    const items: StatItem[] = [
      {
        name: "Comments",
        icon: "commentFilled",
        value: stats.comments,
      },
      {
        name: "Plays",
        icon: "play",
        value: stats.plays,
      },
    ]

    if (track.isOwn)
      items.unshift({
        name: "Favorites",
        icon: "heartFilled",
        value: stats.favorites,
      })

    return <Stats compact items={items} />
  }

  const renderActions = () => {
    const variants: PrimaryButtonVariants = {
      compact: true,
      outlined: true,
      noLabel: true,
    }

    const actions = []
    const moreActions: MoreButtonItem[] = []

    if (track.isOwn) {
      moreActions.push(
        {
          icon: "pencil",
          label: "Edit",
          onClick: () => spawnTrackEditModal(manager, track),
        },
        {
          icon: "trashcan",
          label: "Delete",
          onClick: () => spawnTrackDeleteModal(manager, track),
        },
      )
    } else {
      actions.unshift(
        <FavoriteButton
          {...variants}
          noLabel={!stats.favorites}
          label={String(stats.favorites || "")}
          key="favorite"
          item={track}
        />,
      )
    }

    if (track.isDownloadable) {
      actions.unshift(
        <DownloadButton key="download" model={track.audio} {...variants} />,
      )
    }

    actions.push(
      <AddCollectionButton key="collection" track={track} {...variants} />,
    )

    return (
      <>
        {actions}
        <MoreButton kind="secondary" items={moreActions} {...variants} />
      </>
    )
  }

  return (
    <Container>
      <div className="stats">{renderStats()}</div>
      <Actions compact horizontal>
        {renderActions()}
      </Actions>
    </Container>
  )
})
