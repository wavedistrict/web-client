import { observer } from "mobx-react"
import React from "react"
import { getColor, getShadow } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { TrackModel } from "../../models/Track"
import { Background } from "./Background"
import { Foreground } from "./Foreground"

export interface TrackListItemProps {
  track: TrackModel
}

export const TRACK_LIST_ITEM_SIZE = 184
export const TRACK_LIST_ITEM_SPACING = 32

const Container = styled.article`
  display: block;

  background: ${getColor("primary")};
  border-radius: 3px;

  overflow: hidden;
  position: relative;

  box-shadow: ${getShadow("light")};
`

export const TrackListItem = observer(function TrackListItem(
  props: TrackListItemProps,
) {
  const { track } = props

  return (
    <Container>
      <Background track={track} />
      <Foreground track={track} />
    </Container>
  )
})
