import css from "@emotion/css"
import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { RouteLink } from "../../../../common/routing/components/RouteLink"
import { Overlay } from "../../../theme/components/Overlay"
import { getSpacing } from "../../../theme/helpers"
import { imageContainer } from "../../../theme/mixins/imageContainer"
import { styled } from "../../../theme/themes"
import { Theme } from "../../../theme/types/Theme"
import { TrackArtwork } from "../TrackArtwork"
import { ContentBody } from "./ContentBody"
import { ContentFooter } from "./ContentFooter"
import { ContentHeader } from "./ContentHeader"
import { TrackListItemProps, TRACK_LIST_ITEM_SIZE } from "./TrackListItem"

const Container = styled.div`
  position: relative;
  display: flex;
`

const Content = styled.div`
  padding: ${getSpacing("4")}px;

  display: flex;
  flex-direction: column;
  flex-grow: 1;
`

const artworkStyle = (theme: Theme) => css`
  ${imageContainer()({ theme })}
  ${size(TRACK_LIST_ITEM_SIZE)};
`

export const Foreground = observer(function Foreground(
  props: TrackListItemProps,
) {
  const { track } = props

  const wrapInOverlay = (content: React.ReactNode) => {
    if (!track.isActive) return content

    return <Overlay>{content}</Overlay>
  }

  return (
    <Container>
      <RouteLink css={artworkStyle} to={track.clientLink}>
        <TrackArtwork id="TrackListItem" track={track} />
      </RouteLink>
      {wrapInOverlay(
        <Content>
          <ContentHeader track={track} />
          <ContentBody track={track} />
          <ContentFooter track={track} />
        </Content>,
      )}
    </Container>
  )
})
