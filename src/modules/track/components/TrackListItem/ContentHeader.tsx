import { observer } from "mobx-react"
import React from "react"
import { HumanizedDate } from "../../../core/components/HumanizedDate"
import { Tag } from "../../../core/components/Tag"
import { VisibilityIndicator } from "../../../core/components/VisibilityIndicator"
import { RouteLink } from "../../../../common/routing/components/RouteLink"
import {
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { TrackPlayButton } from "../TrackPlayButton"
import { TrackListItemProps } from "./TrackListItem"

const Container = styled.div`
  display: flex;
`

const PrimaryInfo = styled.div`
  margin-left: ${getSpacing("4")}px;
  padding-right: ${getSpacing("6")};

  flex-grow: 1;
  width: 0;

  > .top {
    display: flex;
  }

  > .top > .title {
    display: block;

    color: ${getFontColor("normal")};
    font-size: ${getFontSize("2")}px;
    font-weight: ${getFontWeight("bold")};

    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;

    margin-right: ${getSpacing("3")}px;
  }

  > .author {
    display: block;
    color: ${getFontColor("muted")};

    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("normal")};
  }
`

const SecondaryInfo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  flex-shrink: 0;

  > .uploadedAt {
    color: ${getFontColor("muted")};

    margin-top: ${getSpacing("2")}px;
    font-size: ${getFontSize("1")}px;
  }
`

export const ContentHeader = observer(function ContentHeader(
  props: TrackListItemProps,
) {
  const { track } = props
  const { user } = track

  const { title, tags, createdAt } = track.data
  const { displayName } = user.data

  return (
    <Container>
      <div>
        <TrackPlayButton track={track} />
      </div>
      <PrimaryInfo>
        <div className="top">
          <RouteLink title={title} to={track.clientLink} className="title">
            {title}
          </RouteLink>
          <div className="visibility">
            <VisibilityIndicator model={track} />
          </div>
        </div>
        <RouteLink to={user.clientLink} className="author">
          {displayName}
        </RouteLink>
      </PrimaryInfo>
      <SecondaryInfo>
        <div className="tag">
          <Tag name={tags[0]} />
        </div>
        <div className="uploadedAt">
          <HumanizedDate>{createdAt}</HumanizedDate>
        </div>
      </SecondaryInfo>
    </Container>
  )
})
