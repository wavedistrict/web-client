import { observer } from "mobx-react"
import React from "react"
import { canUseWebP } from "../../../../common/dom/helpers/webp"
import { ImageBackground } from "../../../../common/ui/image/components/ImageBackground"
import { getDuration } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { TrackListItemProps, TRACK_LIST_ITEM_SIZE } from "./TrackListItem"

const Container = styled.div<{
  hasCover: boolean
  isActive: boolean
}>`
  position: absolute;
  top: 0px;
  bottom: 0px;
  right: 0px;
  left: ${() => TRACK_LIST_ITEM_SIZE}px;

  overflow: hidden;

  transition: ${getDuration("normal")} ease all;
  opacity: ${({ isActive, hasCover }) => {
    if (isActive) return 1
    if (hasCover) return 0.2

    return 0
  }};

  filter: ${({ isActive, hasCover }) => {
    if (isActive) return "grayscale(0)"
    if (hasCover) return "grayscale(1)"

    return "none"
  }};
`

export const Background = observer(function Background(
  props: TrackListItemProps,
) {
  const { track } = props

  const reference = track.coverArtwork || track.artwork
  const blur = track.coverArtwork ? 0 : 6

  return (
    <Container isActive={track.isActive} hasCover={!!track.coverArtwork}>
      <ImageBackground
        isClear={!track.coverArtwork}
        reference={reference}
        hash={track.hash}
        blur={blur}
        getSource={(sources) =>
          [...sources].reverse().find(({ data }) => {
            const format = canUseWebP() ? "webp" : "jpeg"
            return data.name === `${format}-half` || data.name === `preload`
          })
        }
      />
    </Container>
  )
})
