import React from "react"
import { MediaQuery } from "../../../common/dom/classes/MediaQuery"
import { bind } from "../../../common/lang/function/helpers/bind"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { EmptyState } from "../../../common/state/components/EmptyState"
import { FetcherListCursorRenderProps } from "../../../common/ui/list/components/FetcherListCursor"
import { createWindowListComponent } from "../../../common/ui/list/helpers/createWindowListComponent"
import { renderWindowList } from "../../../common/ui/list/helpers/renderWindowList"
import { wrapRowInLi } from "../../../common/ui/list/helpers/wrapRowInLi"
import { QueueData } from "../../player/types/QueueData"
import { TrackModel } from "../models/Track"
import { SimpleTrackListItem } from "./SimpleTrackListItem"
import {
  TrackListItem,
  TRACK_LIST_ITEM_SIZE,
  TRACK_LIST_ITEM_SPACING,
} from "./TrackListItem/TrackListItem"

// the `defaultValue` argument to `createContext` isn't marked as optional,
// so passing `undefined` manually is required ¯\_(ツ)_/¯
const { Provider, Consumer } = React.createContext<QueueData | undefined>(
  undefined,
)

const breakpoint = 1100
const trackListSmallQuery = new MediaQuery(`(max-width: ${breakpoint}px)`)

interface TrackItemListProps {
  list: FetcherList<TrackModel>
  name: string
  clientLink: string
}

export const TrackListItemList = createWindowListComponent<TrackModel>({
  renderItem: wrapRowInLi((track) => <TrackListItem track={track} />),
  rowHeight: TRACK_LIST_ITEM_SIZE + TRACK_LIST_ITEM_SPACING,
})

export const SimpleTrackListItemList = createWindowListComponent<TrackModel>({
  renderItem: wrapRowInLi((track) => <SimpleTrackListItem track={track} />),
  rowHeight: SimpleTrackListItem.style.height,
})

export class TrackList extends React.Component<TrackItemListProps> {
  public static Provider = Provider
  public static Consumer = Consumer

  public renderItems(props: FetcherListCursorRenderProps<TrackModel>) {
    if (trackListSmallQuery.matches) {
      return <SimpleTrackListItemList {...props} />
    } else {
      return <TrackListItemList {...props} />
    }
  }

  @bind
  public renderEmptyState() {
    return <EmptyState icon="trackFilled" message="No tracks yet" />
  }

  public render() {
    const { list, name, clientLink } = this.props

    const data: QueueData = {
      list,
      name,
      clientLink,
    }

    return (
      <Provider value={data}>
        {renderWindowList({
          state: {
            icon: "trackFilled",
            message: "No tracks yet",
          },
          render: (props) => this.renderItems(props),
          list,
        })}
      </Provider>
    )
  }
}
