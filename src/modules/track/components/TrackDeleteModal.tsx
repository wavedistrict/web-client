import React from "react"
import { apiServer } from "../../../common/network"
import { handleInteractionError } from "../../../common/network/helpers/handleInteractionError"
import { ConfirmationModal } from "../../modal/components/ConfirmationModal"
import { TrackModel } from "../models/Track"
import { useManager } from "../../../common/state/hooks/useManager"

interface TrackDeleteModalProps {
  track: TrackModel
}

export function TrackDeleteModal(props: TrackDeleteModalProps) {
  const manager = useManager()
  const { track } = props

  async function action() {
    try {
      await apiServer.delete(track.apiPath)
    } catch (error) {
      handleInteractionError(manager, error)
    }
  }

  return (
    <ConfirmationModal
      title="Delete track"
      actionButtonLabel="Delete"
      action={action}
    >
      <p>
        Are you sure you want to delete the track{" "}
        <strong>"{track.data.title}"</strong>?
      </p>
    </ConfirmationModal>
  )
}
