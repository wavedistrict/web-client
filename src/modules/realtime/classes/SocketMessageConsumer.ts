import {
  createSocketListener,
  SocketListenerOptions,
} from "../../../common/network/helpers/createSocketListener"
import { Emitter } from "../../../common/state/classes/Emitter"
import { Manager } from "../../../common/state/types/Manager"

export abstract class SocketMessageConsumer<
  EventMap extends object = {}
> extends Emitter<EventMap> {
  constructor(protected manager: Manager) {
    super()

    const listeners = this.getListeners()
    createSocketListener(manager, listeners)
  }

  protected abstract getListeners(): SocketListenerOptions
}

export type SocketMessageConsumerClass = new (
  manager: Manager,
) => SocketMessageConsumer<any>
