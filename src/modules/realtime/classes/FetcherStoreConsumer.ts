import { bind } from "../../../common/lang/function/helpers/bind"
import {
  createSocketListener,
  SocketListenerOptions,
} from "../../../common/network/helpers/createSocketListener"
import { SocketMessage } from "../../../common/network/types"
import { Emitter } from "../../../common/state/classes/Emitter"
import { FetcherStore } from "../../../common/state/classes/FetcherStore"
import { Model } from "../../../common/state/classes/Model"
import { Manager } from "../../../common/state/types/Manager"
import { FilteredKeyof } from "../../../common/lang/types/FilteredKeyof"

interface ApiDataResource {
  id: number
}

export type TargetHandler<M> = (model: M, message: any) => void

export interface FetcherStoreConsumerEvents<M, D> {
  create: M
  update: D
  delete: M
}

export abstract class FetcherStoreConsumer<
  D extends ApiDataResource,
  M extends Model<D>
> extends Emitter<FetcherStoreConsumerEvents<M, D>> {
  public name: string
  public store: FetcherStore<M>

  constructor(
    protected manager: Manager,
    name: string,
    storeName: FilteredKeyof<Manager["stores"], FetcherStore<M>>,
  ) {
    super()

    this.name = name
    this.store = manager.stores[storeName] as any
    this.addListeners()

    createSocketListener(manager, this.getListeners())
  }

  @bind
  protected handleCreate(message: SocketMessage) {
    const data = message.data as D
    const model = this.store.getModelFromData(data)

    this.emit("create", model)
  }

  @bind
  protected handleUpdate(message: SocketMessage) {
    const data = message.data as D
    const model = this.store.getModel(data.id)

    if (model) {
      model.update(data)
    }

    this.emit("update", data)
  }

  @bind
  protected handleDelete(message: SocketMessage) {
    const id = message.id as number

    const model = this.store.getModel(id)

    if (model) {
      this.emit("delete", model)
    }

    this.store.removeFetcher(id)
  }

  public wrapListeners(listeners: Record<string, any>, wrapper: any) {
    const result: Record<string, any> = {}

    for (const [key, fn] of Object.entries(listeners)) {
      result[key] = wrapper(fn)
    }

    return result
  }

  @bind
  private passTargetModel(fn: TargetHandler<M>) {
    return (message: SocketMessage) => {
      const model = this.store.getModel(message.targetId)
      if (model) fn(model, message)
    }
  }

  protected getListeners(): SocketListenerOptions {
    const { name, wrapListeners, passTargetModel } = this

    const defaultListeners = {
      [`create-${name}`]: this.handleCreate,
      [`update-${name}`]: this.handleUpdate,
      [`delete-${name}`]: this.handleDelete,
    }

    const targetListeners = wrapListeners(
      this.getTargetListeners(),
      passTargetModel,
    )

    return { ...defaultListeners, ...targetListeners }
  }

  protected getTargetListeners(): Record<string, TargetHandler<M>> {
    return {}
  }

  /** Subscribe to the channels here */
  protected addListeners() {}
}
