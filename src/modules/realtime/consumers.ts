import { createConsumers } from "./helpers/createConsumers"
import { Manager } from "../../common/state/types/Manager"

import {
  processingAudioConsumer,
  imageMediaConsumer,
  audioMediaConsumer,
} from "../../common/media/consumers"
import { notificationConsumer } from "../notification/consumers/notificationConsumer"
import { collectionConsumer } from "../../modules/collection/consumers/collectionConsumer"
import { trackConsumer } from "../../modules/track/consumers/trackConsumer"
import { conversationConsumer } from "../../modules/messaging/consumers/conversationConsumer"
import { messageConsumer } from "../../modules/messaging/consumers/messageConsumer"
import { userConsumer } from "../../modules/user/consumers/userConsumer"
import { userPresenceConsumer } from "../../modules/user/consumers/userPresenceConsumer"
import { commentConsumer } from "../../modules/commenting/consumers/commentConsumer"

export const instantiateConsumers = (manager: Manager) =>
  createConsumers(manager, [
    processingAudioConsumer,
    conversationConsumer,
    userPresenceConsumer,
    notificationConsumer,
    imageMediaConsumer,
    audioMediaConsumer,
    collectionConsumer,
    commentConsumer,
    messageConsumer,
    trackConsumer,
    userConsumer,
  ])
