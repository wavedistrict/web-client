import { SocketMessageConsumerClass } from "../classes/SocketMessageConsumer"
import { Manager } from "../../../common/state/types/Manager"
import { Consumer } from "react"

export const createConsumers = (
  manager: Manager,
  consumers: ((manager: Manager) => any)[],
) => {
  for (const create of consumers) {
    create(manager)
  }
}
