import { apiServer } from "../../../common/network"
import { handleInteractionError } from "../../../common/network/helpers/handleInteractionError"
import { requireAuth } from "../../auth/helpers/requireAuth"
import { FavoritableItem } from "../types/FavoritableItem"
import { setFavorited } from "./setFavorited"

export const toggleItemFavorite = requireAuth(
  async (manager, item: FavoritableItem) => {
    const { byCurrentUser } = item.data
    const { favorited } = byCurrentUser

    const path = `${item.apiPath}/favorites`

    const apiAction = favorited
      ? apiServer.delete(path)
      : apiServer.put(path, null)

    setFavorited(item, !favorited)

    try {
      await apiAction
    } catch (error) {
      setFavorited(item, favorited)
      handleInteractionError(manager, error)
    }
  },
)
