import { action } from "mobx"
import { FavoritableItem } from "../types/FavoritableItem"

export const setFavorited = action(
  (item: FavoritableItem, favorited: boolean) => {
    item.data.byCurrentUser.favorited = favorited
  },
)
