export interface FavoritableItem {
  apiPath: string
  data: {
    title: string
    byCurrentUser: {
      favorited: boolean
    }
  }
}
