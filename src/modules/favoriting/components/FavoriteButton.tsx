import React from "react"
import css from "@emotion/css"
import { observer } from "mobx-react"
import { readableColor, transparentize } from "polished"
import { useTheme, styled } from "../../theme/themes"
import { toggleItemFavorite } from "../actions/toggleItemFavorite"
import { FavoritableItem } from "../types/FavoritableItem"
import { PrimaryButtonVariants } from "../../../common/ui/button/components/PrimaryButton"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { useManager } from '../../../common/state/hooks/useManager'

export type FavoriteButtonProps = PrimaryButtonVariants & {
  item: FavoritableItem
  label?: string
}

const Container = styled<typeof SecondaryButton, { readable: string }>(
  SecondaryButton,
)`
  ${({ theme, active, readable }) =>
    active &&
    css`
      background: ${theme.colors.love};
      color: ${readable};

      > .icon {
        fill: ${readable};
      }
    `}
`

export const FavoriteButton = observer(function FavoriteButton(
  props: FavoriteButtonProps,
) {
  const manager = useManager()

  const { item, label, ...rest } = props
  const { favorited } = item.data.byCurrentUser

  const handleClick = () => {
    toggleItemFavorite(manager, item)
  }

  const theme = useTheme()

  const readable = readableColor(
    theme.colors.love,
    transparentize(0.15, "black"),
    transparentize(0.15, "white"),
  )

  return (
    <Container
      icon={favorited ? "heartFilled" : "heart"}
      title="Favorite"
      active={favorited}
      readable={readable}
      label={label}
      onClick={handleClick}
      {...rest}
    />
  )
})
