import { IconType } from "../../common/ui/icon"

export interface SellingPointDescriptor {
  icon: IconType
  title: string
  description: string
}

export const SELLING_POINTS: SellingPointDescriptor[] = [
  {
    icon: "trackFilled",
    title: "Great audio quality",
    description: `Listen to your favorite tracks in superior quality. WaveDistrict uses the Opus codec, which is significantly better than mp3, which is still available to those who prefer that.`,
  },
  {
    icon: "heartFilled",
    title: "Community oriented",
    description:
      "Connect with your favorite artists and creators and participate in a community with people who love music and audio. ",
  },
  {
    icon: "pencil",
    title: "Rich features",
    description:
      "We want WaveDistrict to be as feature fledged as possible. That's why we'll be listening to the community and consider new ideas and features when working on future versions.",
  },
  {
    icon: "easel",
    title: "Customizable",
    description:
      "Make WaveDistrict your own by customizing it to your liking through our wide array of settings.",
  },
]
