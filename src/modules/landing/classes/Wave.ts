export interface WaveOptions {
  amplitude: number
  frequency: number
  speed: number
  offset: number
}

/** Represents a rendered sine wave */
export class Wave {
  private time = 0

  constructor(private options: WaveOptions) {}

  public advance() {
    const { speed } = this.options
    this.time += 1 * speed
  }

  public getPoints(count: number) {
    const { amplitude, frequency, offset } = this.options

    return Array(count)
      .fill(0)
      .map((point, index) => {
        const sin = Math.sin(this.time + index / frequency)
        return sin * amplitude + amplitude + offset
      })
  }
}
