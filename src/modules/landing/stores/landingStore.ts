import { FetcherList } from "../../../common/state/classes/FetcherList"
import { TrackModel } from "../../track/models/Track"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

export class LandingStore extends InitializableStore {
  public popularTracks!: FetcherList<TrackModel>

  public init() {
    const { trackStore } = this.manager.stores

    this.popularTracks = new FetcherList(
      {
        store: trackStore,
        path: "/tracks",
        maxItems: 10,
        params: {
          orderBy: "popularity",
        },
      },
      this.manager,
    )
  }
}

export const landingStore = createStoreFactory(LandingStore)
