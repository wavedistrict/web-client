import React from "react"
import { IconType } from "../../../common/ui/icon"
import { Icon } from "../../../common/ui/icon/components/Icon"
import { styled } from "../../theme/themes"
import { size } from "polished"
import { getColor, getSpacing, getFontWeight } from "../../theme/helpers"

export interface SellingPointProps {
  title: string
  description: string
  icon: IconType
}

const Container = styled.div`
  display: flex;
`

const PointIcon = styled(Icon)`
  ${size(64)};

  flex-shrink: 0;
  fill: ${getColor("accent")};

  @media (max-width: 800px) {
    ${size(32)}
  }
`

const Content = styled.div`
  margin-left: ${getSpacing("6")}px;
`

const Title = styled.h2`
  font-size: 24px;
  font-weight: ${getFontWeight("bold")};
`

const Description = styled.p`
  margin-top: ${getSpacing("4")}px;
`

export function SellingPoint(props: SellingPointProps) {
  const { title, description, icon } = props

  return (
    <Container>
      <PointIcon name={icon} />
      <Content>
        <Title>{title}</Title>
        <Description>{description}</Description>
      </Content>
    </Container>
  )
}
