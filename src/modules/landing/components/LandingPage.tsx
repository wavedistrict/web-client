import React from "react"
import { SELLING_POINTS } from "../constants"
import { LandingHero } from "./LandingHero"
import { SellingPoint } from "./SellingPoint"
import { styled } from "../../theme/themes"
import { getSpacing, getFontWeight } from "../../theme/helpers"
import { BODY_WIDTH } from "../../core/constants"
import { PopularTracks } from "./PopularTracks"
import { LandingCallToAction } from "./LandingCallToAction"
import { ConnectLink } from "./ConnectLink"
import { useMeta } from "../../../common/ui/meta/hooks/useMeta"

const Container = styled.div`
  @media (max-width: ${BODY_WIDTH}px) {
    padding: 0px ${getSpacing("5")}px;
  }
`

const Title = styled.h1`
  margin-top: ${getSpacing("7")}px;
  font-size: 36px;
  font-weight: ${getFontWeight("bold")};
`

const Blurb = styled.p`
  margin-top: ${getSpacing("4")}px;
`

const PointList = styled.div`
  margin-top: ${getSpacing("7")}px;

  display: flex;
  flex-wrap: wrap;
`

const Point = styled.div`
  width: 50%;
  padding-right: ${getSpacing("7")}px;
  margin-bottom: ${getSpacing("7")}px;

  @media (max-width: 800px) {
    width: 100%;
    padding-right: 0px;
  }
`

const PopularTitle = styled(Title)`
  margin-bottom: ${getSpacing("5")}px;
  margin-top: 0px;
`

const ConnectTitle = styled(Title)`
  margin-top: ${getSpacing("4")}px;
  margin-bottom: ${getSpacing("5")}px;
`

const ConnectList = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const End = styled(LandingCallToAction)`
  margin: ${getSpacing("5")}px 0px;
`

export function LandingPage() {
  useMeta({
    title: "Welcome",
  })

  return (
    <>
      <LandingHero />
      <Container>
        <Title>Welcome to WaveDistrict</Title>
        <Blurb>
          WaveDistrict is an up and coming audio platform with clean, modern
          design, and an open-source web client. The project is currently in an
          alpha stage, under active development.
        </Blurb>
        <PointList>
          {SELLING_POINTS.map((props, index) => (
            <Point key={index}>
              <SellingPoint {...props} />
            </Point>
          ))}
        </PointList>
        <PopularTitle>Top 10 tracks right now</PopularTitle>
        <PopularTracks />
        <ConnectTitle>Find us here</ConnectTitle>
        <ConnectList>
          <ConnectLink icon="discord" to="https://discord.gg/NanfSAZ" />
          <ConnectLink
            icon="twitter"
            to="https://twitter.com/wavedistrictapp"
          />
          <ConnectLink
            icon="patreon"
            to="https://www.patreon.com/wavedistrict"
          />
          <ConnectLink
            icon="gitlab"
            to="https://gitlab.com/wavedistrict/web-client"
          />
          <ConnectLink
            icon="trello"
            to="https://trello.com/b/An8Mhmgp/wavedistrict"
          />
        </ConnectList>
        <End />
      </Container>
    </>
  )
}
