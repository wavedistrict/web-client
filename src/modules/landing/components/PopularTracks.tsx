import React from "react"
import { useStores } from "../../../common/state/hooks/useStores"
import { styled } from "../../theme/themes"
import { getSpacing } from "../../theme/helpers"
import { TrackList } from "../../track/components/TrackList"

const Container = styled.div`
  @media (max-width: 1100px) {
    margin: 0px -${getSpacing("5")}px;
  }
`

export function PopularTracks() {
  const { landingStore } = useStores()
  const { popularTracks } = landingStore

  return (
    <Container>
      <TrackList list={popularTracks} name="Landing page" clientLink="/" />
    </Container>
  )
}
