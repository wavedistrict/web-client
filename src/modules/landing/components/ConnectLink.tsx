import { IconType } from "../../../common/ui/icon"
import { styled } from "../../theme/themes"
import { Icon } from "../../../common/ui/icon/components/Icon"
import React from "react"
import { size } from "polished"
import { getDuration, getSpacing } from "../../theme/helpers"

export type ConnectLinkProps = {
  icon: IconType
  to: string
}

const Container = styled.a`
  display: block;

  margin-right: ${getSpacing("6")}px;
  margin-bottom: ${getSpacing("6")}px;

  opacity: 0.5;
  transition: ${getDuration("normal")} ease opacity;

  &:hover {
    opacity: 1;
  }

  &:last-child {
    margin-right: 0px;
  }
`

const ConnectIcon = styled(Icon)`
  ${size(48)};
`

export function ConnectLink(props: ConnectLinkProps) {
  const { icon, to } = props

  return (
    <Container href={to} target="_blank" rel="noreferrer nofollow">
      <ConnectIcon name={icon} />
    </Container>
  )
}
