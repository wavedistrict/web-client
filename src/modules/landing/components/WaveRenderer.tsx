import React, { useState } from "react"
import { SizeCalculator } from "../../../common/dom/components/SizeCalculator"
import { Wave } from "../classes/Wave"
import { useTheme } from "../../theme/themes"
import { transparentize } from "polished"
import {
  useCanvasAnimation,
  CanvasAnimationHandler,
} from "../../../common/ui/react/hooks/useCanvasAnimation"

export function WaveRenderer() {
  const theme = useTheme()

  const topColor = transparentize(0.6, theme.colors.accent)
  const bottomColor = transparentize(0.9, theme.colors.secondaryAccent)

  const [waves] = useState(() => [
    new Wave({
      amplitude: 20,
      frequency: 70,
      speed: 0.02,
      offset: 110,
    }),
    new Wave({
      amplitude: 25,
      frequency: 60,
      speed: 0.03,
      offset: 90,
    }),
    new Wave({
      amplitude: 25,
      frequency: 65,
      speed: 0.02,
      offset: 95,
    }),
    new Wave({
      amplitude: 30,
      frequency: 80,
      speed: 0.04,
      offset: 50,
    }),
    new Wave({
      amplitude: 30,
      frequency: 85,
      speed: 0.03,
      offset: 55,
    }),
  ])

  const frame: CanvasAnimationHandler = (context, canvas) => {
    const { width, height } = canvas

    context.clearRect(0, 0, width, height)

    const gradient = context.createLinearGradient(0, height / 2, 0, height)

    gradient.addColorStop(0, topColor)
    gradient.addColorStop(1, bottomColor)

    context.strokeStyle = gradient
    context.fillStyle = gradient

    const fitting = width / 1

    for (const wave of waves) {
      const points = wave.getPoints(fitting)

      /** Start the wave path */
      context.beginPath()
      context.moveTo(-2, height + 1)

      let lastLineX = 0
      let lastLineY = 0

      for (const [index, value] of points.entries()) {
        const pointHeight = value * 1.2 * (index / points.length + 0.2)

        const point = {
          x: index * 1,
          y: height - pointHeight,
          height: pointHeight,
          width: 1,
        }

        lastLineX = point.x
        lastLineY = point.y

        context.lineTo(point.x, point.y)
      }

      /** Prevent stroke from appearing on edge */
      context.lineTo(lastLineX + 2, lastLineY)
      context.lineTo(lastLineX + 2, height + 1)

      context.closePath()

      /** Draw the waves */
      context.fill()
      context.stroke()
      context.stroke()

      /** Advance the sinewave time */
      wave.advance()
    }
  }

  const [canvasRef] = useCanvasAnimation(frame)

  return (
    <SizeCalculator>
      {({ width, height }) => {
        return <canvas ref={canvasRef} width={width} height={height} />
      }}
    </SizeCalculator>
  )
}
