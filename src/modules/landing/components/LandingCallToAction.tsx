import React from "react"
import { styled } from "../../theme/themes"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { spawnRegisterModal } from "../../auth/actions/spawnRegisterModal"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { spawnLoginModal } from "../../auth/actions/spawnLoginModal"
import { getFontSize, getFontWeight, getSpacing } from "../../theme/helpers"
import { useStores } from "../../../common/state/hooks/useStores"
import { useManager } from "../../../common/state/hooks/useManager"

export type LandingCallToActionProps = {
  className?: string
}

const Container = styled.div`
  display: flex;
  align-items: center;
`

const Separator = styled.span`
  display: block;

  font-size: ${getFontSize("3")}px;
  font-weight: ${getFontWeight("bold")};
  font-style: italic;

  margin: 0px ${getSpacing("5")}px;
`

export function LandingCallToAction(props: LandingCallToActionProps) {
  const { className } = props
  const { authStore } = useStores()
  const manager = useManager()

  if (authStore.user) return null

  return (
    <Container className={className}>
      <PrimaryButton
        onClick={() => spawnRegisterModal(manager)}
        label="Join for free"
      />
      <Separator>or</Separator>
      <SecondaryButton
        onClick={() => spawnLoginModal(manager, {})}
        label="Sign in"
      />
    </Container>
  )
}
