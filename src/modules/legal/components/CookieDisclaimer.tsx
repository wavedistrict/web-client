import React from "react"
import { styled, useTheme } from "../../theme/themes"
import { useStoredValue } from "../../../common/dom/hooks/useStoredValue"
import {
  getSpacing,
  getColor,
  getFontSize,
  getFontWeight,
  getShadow,
  getDuration,
} from "../../theme/helpers"
import { Paragraph } from "../../../common/ui/markdown/components/Paragraph"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { Transition } from "react-transition-group"
import { TransitionStatus } from "react-transition-group/Transition"

const Container = styled.div<{ state: TransitionStatus }>`
  display: flex;
  align-items: center;

  position: fixed;
  bottom: 0px;
  left: 0px;
  right: 0px;

  z-index: 4;

  padding: ${getSpacing("5")}px;

  background: ${getColor("primary")};
  box-shadow: ${getShadow("spread")};

  transition: ${getDuration("normal")} ease transform;

  ${(props) =>
    props.state === "entering" &&
    `
    transform: translateY(100%);
  `}

  ${(props) =>
    props.state === "entered" &&
    `
    transform: translateY(0%);
  `}

  ${(props) =>
    ["exiting", "exited", "unmounted"].includes(props.state) &&
    `
    transform: translateY(100%);
  `}

  @media (max-width: 570px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

const Text = styled.div`
  margin-bottom: -${getSpacing("3")}px;
  flex: 1;

  @media (max-width: 570px) {
    margin-bottom: ${getSpacing("4")}px;
  }
`

const Heading = styled.h2`
  font-size: ${getFontSize("3")}px;
  font-weight: ${getFontWeight("bold")};
`

const Actions = styled.div``

export function CookieDisclaimer() {
  const [cleared, setCleared] = useStoredValue("cookie-disclaimed", false)
  const { durations } = useTheme()

  return (
    <Transition
      in={!cleared}
      appear
      unmountOnExit
      mountOnEnter
      timeout={parseInt(durations.normal)}
    >
      {(state) => (
        <Container state={state}>
          <Text>
            <Heading>We store cookies and stuff</Heading>
            <Paragraph>
              By continuing to use WaveDistrict, you agree to the use of cookies
              to store information about your session and preferences.
            </Paragraph>
          </Text>
          <Actions>
            <PrimaryButton label="Alright" onClick={() => setCleared(true)} />
          </Actions>
        </Container>
      )}
    </Transition>
  )
}
