import { TrackModel } from "../../track/models/Track"
import { SearchFetcherList } from "../classes/SearchFetcherList"
import {
  createStoreFactory,
  InitializableStore,
} from "../../../common/state/classes/InitializableStore"

export class SearchStore extends InitializableStore {
  public tracks!: SearchFetcherList<TrackModel>

  public async init() {
    const { trackStore } = this.manager.stores

    this.tracks = new SearchFetcherList(
      {
        store: trackStore,
        path: "/search/tracks",
      },
      this.manager,
    )
  }
}

export const searchStore = createStoreFactory(SearchStore)
