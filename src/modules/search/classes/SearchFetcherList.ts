import { bind } from "../../../common/lang/function/helpers/bind"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { FetcherStore } from "../../../common/state/classes/FetcherStore"
import { Model } from "../../../common/state/classes/Model"
import { Manager } from "../../../common/state/types/Manager"

interface SearchFetcherListOptions<M extends Model> {
  store: FetcherStore<M>
  path: string
  limit?: number
}

export class SearchFetcherList<M extends Model> extends FetcherList<M> {
  private lastQuery = ""

  constructor(options: SearchFetcherListOptions<M>, manager: Manager) {
    super(
      {
        ...options,
        params: { q: "" },
      },
      manager,
    )
  }

  @bind
  public search(rawQuery: string) {
    const query = rawQuery.trim().replace(/\s+/g, " ") // trim white space + remove duplicates

    if (query === "") return
    if (query === this.lastQuery) return

    this.lastQuery = query

    this.setQueryParams({
      q: query,
    })
  }
}
