import React, { useState, useEffect } from "react"
import { TextInput } from "../../../common/ui/input/components/TextInput"
import { PageHeader } from "../../core/components/PageHeader"
import { routes } from "../../core/constants"
import { TrackList } from "../../track/components/TrackList"
import { useStores } from "../../../common/state/hooks/useStores"
import { styled } from "../../theme/themes"
import { getSpacing } from "../../theme/helpers"
import { useMeta } from "../../../common/ui/meta/hooks/useMeta"

const Container = styled.div``

const Results = styled.div`
  margin-top: ${getSpacing("6")}px;
`

export function SearchPage() {
  const { searchStore, routingStore } = useStores()
  const [query, setQuery] = useState(() => routingStore.query.q || "")

  useMeta({
    title: "Search",
    description: "Search tracks, collections, and users",
  })

  useEffect(() => {
    if (query) {
      searchStore.tracks.search(query)
    }
  }, [query, searchStore.tracks])

  const handleInput = (newQuery: string) => {
    setQuery(newQuery)

    if (query) {
      routingStore.query = { q: newQuery }
    } else {
      routingStore.query = {}
    }
  }

  const renderTracks = () => {
    if (!query) return

    return (
      <TrackList
        list={searchStore.tracks}
        clientLink={routes.search}
        name="Searched tracks"
      />
    )
  }

  return (
    <>
      <PageHeader title="Search" icon="search" />
      <Container>
        <TextInput
          autoFocus
          placeholder="Type the title of a track, artist name, genre, etc..."
          value={query}
          onChange={handleInput}
        />
        <Results>{renderTracks()}</Results>
      </Container>
    </>
  )
}
