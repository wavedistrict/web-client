import { action, computed, observable } from "mobx"
import { Modal, ModalOptions } from "../models/Modal"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"
import { IS_SERVER } from "../../core/constants"

export class ModalStore extends InitializableStore {
  @observable private modals: Modal[] = []

  public async init() {}

  /** Spawns a modal, or shows the one with the given key if it already exists */
  @action
  public spawn(options: ModalOptions) {
    if (IS_SERVER) return

    const existingModal = this.getModal(options.key)

    if (existingModal) {
      existingModal.setHidden(false)
      return
    }

    const modal = new Modal(options)
    this.modals.push(modal)
  }

  /** Removes a modal from the pool */
  @action
  public dismissModal(key: string) {
    this.modals = this.modals.filter((modal) => modal.key !== key)
  }

  /** Get a modal by key */
  private getModal(key: string) {
    return this.modals.find((modal) => modal.key === key)
  }

  /** All of the currently visible modals */
  @computed
  public get visibleModals() {
    return this.modals.filter((modals) => !modals.isHidden)
  }
}

export const modalStore = createStoreFactory(ModalStore)
