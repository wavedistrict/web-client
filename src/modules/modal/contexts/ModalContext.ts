import { createContext } from "react"

export interface ModalContextState {
  dismiss: () => void
}

export const ModalContext = createContext<ModalContextState | undefined>(
  undefined,
)
