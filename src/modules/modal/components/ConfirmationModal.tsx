import React, { ReactNode, useState } from "react"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { useModal } from "../hooks/useModal"
import { PrimaryModal } from "./PrimaryModal"

export interface ConfirmationModalProps {
  title?: string
  actionButtonLabel: string
  action: () => Promise<unknown>
  children: ReactNode
}

export function ConfirmationModal(props: ConfirmationModalProps) {
  const { title, actionButtonLabel, action, children } = props

  const { dismiss } = useModal()
  const [isWaiting, setIsWaiting] = useState(false)

  function handleAction() {
    setIsWaiting(true)

    action().then(() => {
      setIsWaiting(false)
      dismiss()
    })
  }

  const footer = (
    <>
      <SecondaryButton label="Cancel" onClick={dismiss} disabled={isWaiting} />
      <PrimaryButton
        label={actionButtonLabel}
        onClick={handleAction}
        disabled={isWaiting}
      />
    </>
  )

  return (
    <PrimaryModal title={title}>
      <PrimaryModal.Body>{children}</PrimaryModal.Body>
      <PrimaryModal.Footer>{footer}</PrimaryModal.Footer>
    </PrimaryModal>
  )
}
