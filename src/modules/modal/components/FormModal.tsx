import { useObserver } from "mobx-react-lite"
import React, { ReactNode } from "react"
import { PrimaryButton } from "../../../common/ui/button/components/PrimaryButton"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import {
  FormManager,
  FormManagerStatus,
} from "../../../common/ui/form/classes/FormManager"
import { FormStatus } from "../../../common/ui/form/components/FormStatus"
import { FormContext } from "../../../common/ui/form/contexts/FormContext"
import { PrimaryModal, PrimaryModalVariants } from "../components/PrimaryModal"
import { useModal } from "../hooks/useModal"

interface FormModalProps {
  children: ReactNode
  form: FormManager<any>
  title: string
  variants?: PrimaryModalVariants[]
  onSubmit: () => void
}

export function FormModal(props: FormModalProps) {
  const { children, form, title, variants, onSubmit } = props
  const { dismiss } = useModal()

  const isSubmitting = useObserver(
    () => form.status === FormManagerStatus.Submitting,
  )
  const areButtonsDisabled = useObserver(
    () =>
      form.status !== FormManagerStatus.Idling &&
      form.status !== FormManagerStatus.Error,
  )
  const saveButtonLabel = isSubmitting ? "Saving..." : "Save"

  const body = (
    <FormContext.Provider value={{ onSubmit, form }}>
      {children}
    </FormContext.Provider>
  )

  const footer = (
    <>
      <FormStatus form={form} />
      <SecondaryButton
        label="Close"
        onClick={dismiss}
        disabled={areButtonsDisabled}
      />
      <PrimaryButton
        label={saveButtonLabel}
        onClick={onSubmit}
        disabled={areButtonsDisabled}
      />
    </>
  )

  return (
    <PrimaryModal variants={variants} title={title}>
      <PrimaryModal.Body>{body}</PrimaryModal.Body>
      <PrimaryModal.Footer>{footer}</PrimaryModal.Footer>
    </PrimaryModal>
  )
}
