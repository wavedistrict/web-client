import { useObserver } from "mobx-react-lite"
import React, { useEffect } from "react"
import { Transition, TransitionGroup } from "react-transition-group"
import { getLastItem } from "../../../common/lang/array/helpers/getLastItem"
import { useViewport } from "../../../common/ui/react/hooks/useViewport"
import { styled } from "../../theme/themes"
import { ModalItem } from "./ModalItem"
import { useStores } from "../../../common/state/hooks/useStores"
import { IS_SERVER } from "../../core/constants"

export function ModalOverlay() {
  const { modalStore } = useStores()
  const dimensions = useViewport()

  const dismiss = () => {
    const modal = getLastItem(modalStore.visibleModals)
    return modal && modalStore.dismissModal(modal.key)
  }

  const handleKeyUp = (event: KeyboardEvent) => {
    if (event.key === "Escape") {
      dismiss()
    }
  }

  const items = useObserver(() =>
    modalStore.visibleModals.map((modal) => (
      <Transition timeout={200} key={modal.key}>
        {(transitionStatus) => (
          <ModalItem modal={modal} transitionStatus={transitionStatus} />
        )}
      </Transition>
    )),
  )

  useEffect(() => {
    window.addEventListener("keyup", handleKeyUp)

    return () => {
      window.removeEventListener("keyup", handleKeyUp)
    }
  })

  if (IS_SERVER) {
    return null
  }

  return (
    <Container height={dimensions.height}>
      <TransitionGroup component={React.Fragment}>{items}</TransitionGroup>
    </Container>
  )
}

const Container = styled.div<{
  height: number
}>`
  position: fixed;
  height: ${(props) => props.height}px;

  top: 0px;
  left: 0px;
  right: 0px;

  z-index: 2;

  pointer-events: none;
  transform: translateZ(0);
`
