import css from "@emotion/css"
import { cover, transparentize } from "polished"
import React from "react"
import { TransitionStatus } from "react-transition-group/Transition"
import { getDuration } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { ModalContext } from "../contexts/ModalContext"
import { Modal } from "../models/Modal"
import { useStores } from "../../../common/state/hooks/useStores"

interface ModalItemProps {
  modal: Modal
  transitionStatus: TransitionStatus
}

export function ModalItem(props: ModalItemProps) {
  const { modal, transitionStatus } = props
  const { render, dismissOnClickout, key } = modal
  const { modalStore } = useStores()

  function dismiss() {
    modalStore.dismissModal(key)
  }

  function hide() {
    if (dismissOnClickout) dismiss()
    else modal.setHidden(true)
  }

  return (
    <ModalContext.Provider value={{ dismiss }}>
      <Container>
        <Filter onClick={hide} transitionStatus={transitionStatus} />
        <Content transitionStatus={transitionStatus}>
          <InnerContent>{render()}</InnerContent>
        </Content>
      </Container>
    </ModalContext.Provider>
  )
}

const Container = styled.div`
  ${cover()}

  z-index: 500;
`

const Filter = styled.div<{ transitionStatus: TransitionStatus }>`
  ${cover()}

  pointer-events: all;
  background: ${({ theme }) => transparentize(0.1, theme.overlay.background)};
  transition: ${getDuration("normal")} ease all;

  opacity: 0;

  ${(props) =>
    (props.transitionStatus === "entering" ||
      props.transitionStatus === "entered") &&
    css`
      opacity: 1;
    `}

  ${(props) =>
    (props.transitionStatus === "exiting" ||
      props.transitionStatus === "exited") &&
    css`
      opacity: 0;
    `}
`

const Content = styled.div<{ transitionStatus: TransitionStatus }>`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  transition: ${getDuration("normal")} ease all;

  transform: scale(0.9);
  opacity: 0;

  ${(props) =>
    (props.transitionStatus === "entering" ||
      props.transitionStatus === "entered") &&
    css`
      opacity: 1;
      transform: scale(1);
    `}

  ${(props) =>
    (props.transitionStatus === "exiting" ||
      props.transitionStatus === "exited") &&
    css`
      transform: scale(0.9);
      opacity: 0;
    `}
`

const InnerContent = styled.div`
  pointer-events: none;
  height: 100%;

  display: flex;
  justify-content: center;
  align-items: center;

  > * {
    pointer-events: all;
  }
`
