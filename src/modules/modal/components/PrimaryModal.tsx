import React from "react"
import { useViewport } from "../../../common/ui/react/hooks/useViewport"
import { ScrollContainer } from "../../../common/ui/scroll/components/ScrollContainer"
import {
  getColor,
  getFontWeight,
  getShadow,
  getSpacing,
} from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { ModalCloseButton } from "./ModalCloseButton"

export type PrimaryModalVariants = "wide" | "thin" | ""

export interface PrimaryModalProps {
  title?: string
  variants?: PrimaryModalVariants[]
  children?: React.ReactNode
}

export const PRIMARY_MODAL_MQ = `@media (min-width: ${900 + 32}px)`

export function PrimaryModal(props: PrimaryModalProps) {
  const { title, children, variants = [] } = props

  return (
    <PrimaryModal.Base variants={variants}>
      <div className="container">
        <Header className="header">
          <div className="title">{title}</div>
          <div className="close">
            <ModalCloseButton size={32} />
          </div>
        </Header>
        {children}
      </div>
    </PrimaryModal.Base>
  )
}

PrimaryModal.Base = function Base(props: {
  variants?: PrimaryModalVariants[]
  children: JSX.Element
}) {
  const { variants = [], children } = props

  return <Container wide={variants.includes("wide")}>{children}</Container>
}

PrimaryModal.Body = function PrimaryModalBody(props: {
  children: React.ReactNode
}) {
  const dimensions = useViewport()

  return (
    <ScrollContainer
      autoHide
      autoHeight
      autoHeightMax={dimensions.height}
      className="body"
    >
      <Body>{props.children}</Body>
    </ScrollContainer>
  )
}

PrimaryModal.Footer = function PrimaryModalFooter(props: {
  children: React.ReactNode
}) {
  return <Footer>{props.children}</Footer>
}

const Container = styled.div<{ wide?: boolean }>`
  min-width: 100vw;
  max-width: 100vw;

  min-height: 100%;
  max-height: 100%;

  background: ${getColor("primary")};
  box-shadow: ${getShadow("spread")};

  overflow: hidden;

  > .container {
    display: flex;
    flex-direction: column;
    box-sizing: border-box;

    padding: ${getSpacing("6")}px;
    max-height: 100vh;
  }

  > .container > .body {
    flex: 1;
    margin: 0;

    display: flex;
    flex-direction: column;
  }

  border-radius: 0px;

  ${PRIMARY_MODAL_MQ} {
    max-width: calc(100vw - ${getSpacing("6")}px);
    max-height: calc(100vh - ${getSpacing("4")}px);

    min-height: 0%;
    min-width: ${(props) => (props.wide ? 720 : 500)}px;

    border-radius: 4px;
  }
`

const Header = styled.div`
  flex-shrink: 0;

  font-weight: ${getFontWeight("normal")};
  font-size: 26px;

  padding-bottom: ${getSpacing("4")}px;
  border-bottom: 1px solid ${getColor("divider")};

  display: flex;

  > .title {
    flex: 1;
  }
`

const Body = styled.div`
  padding: ${getSpacing("6")}px 0;
`

const Footer = styled.div`
  flex-shrink: 0;

  display: flex;
  justify-content: flex-end;
  align-items: center;

  padding-top: ${getSpacing("4")}px;
  border-top: 1px solid ${getColor("divider")};

  > * {
    margin-left: ${getSpacing("4")}px;
  }

  > *:first-child {
    margin-left: 0px;
  }
`
