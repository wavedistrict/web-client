import React from "react"
import { SquareButton } from "../../../common/ui/button/components/SquareButton"
import { useModal } from "../hooks/useModal"

export interface ModalCloseButtonProps {
  size: number
}

export function ModalCloseButton(props: ModalCloseButtonProps) {
  const { size } = props
  const { dismiss } = useModal()

  return <SquareButton icon="failed" onClick={dismiss} size={size} />
}
