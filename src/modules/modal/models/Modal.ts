import { action, observable } from "mobx";
import { ReactNode } from "react";

type RenderFunction = () => ReactNode

export interface ModalOptions {
  key: string
  render: RenderFunction
  dismissOnClickout?: boolean
}

export class Modal {
  public key: string
  public render: RenderFunction

  @observable public isHidden = false
  public dismissOnClickout = false

  constructor(options: ModalOptions) {
    const { key, render, dismissOnClickout } = options

    this.key = key
    this.render = render
    this.dismissOnClickout = dismissOnClickout || false
  }

  @action
  public setHidden(hidden: boolean) {
    this.isHidden = hidden
  }
}
