import React from "react"
import { SecondaryButton } from "../../../common/ui/button/components/SecondaryButton"
import { toggleItemFollow } from "../actions/toggleItemFollow"
import { FollowableItem } from "../types/FollowableItem"
import { PrimaryButtonVariants } from "../../../common/ui/button/components/PrimaryButton"
import { useObserver } from "mobx-react-lite"
import { useManager } from "../../../common/state/hooks/useManager"

export type FollowButtonProps = PrimaryButtonVariants & {
  item: FollowableItem
  iconOnly?: boolean
}

export function FollowButton(props: FollowButtonProps) {
  const manager = useManager()

  return useObserver(() => {
    const { item, iconOnly, ...rest } = props
    const { following } = item.data.byCurrentUser

    const icon = following ? "personFilled" : "personAddFilled"
    const label = following ? "Unfollow" : "Follow"

    return (
      <SecondaryButton
        icon={icon}
        label={iconOnly ? undefined : label}
        onClick={() => toggleItemFollow(manager, item)}
        active={following}
        {...rest}
      />
    )
  })
}
