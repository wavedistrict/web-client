import { action } from "mobx"
import { FollowableItem } from "../types/FollowableItem"

export const setFollowing = action(
  (item: FollowableItem, following: boolean) => {
    item.data.byCurrentUser.following = following
  },
)
