import { apiServer } from "../../../common/network"
import { handleInteractionError } from "../../../common/network/helpers/handleInteractionError"
import { requireAuth } from "../../auth/helpers/requireAuth"
import { FollowableItem } from "../types/FollowableItem"
import { setFollowing } from "./setFollowing"

export const toggleItemFollow = requireAuth(
  async (manager, item: FollowableItem) => {
    const { byCurrentUser } = item.data
    const { following } = byCurrentUser

    const path = `${item.apiPath}/follows`

    const apiAction = following
      ? apiServer.delete(path)
      : apiServer.put(path, null)

    setFollowing(item, !following)

    try {
      await apiAction
    } catch (error) {
      setFollowing(item, following)
      handleInteractionError(manager, error)
    }
  },
)
