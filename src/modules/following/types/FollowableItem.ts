export interface FollowableItem {
  apiPath: string
  data: {
    byCurrentUser: {
      following: boolean
    }
  }
}
