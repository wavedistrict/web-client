import { computed, observable } from "mobx"
import {
  FetcherStore,
  createFetcherStoreFactory,
} from "../../../common/state/classes/FetcherStore"
import { ConversationModel } from "../models/ConversationModel"
import { ConversationData } from "../types/ConversationData"

export class ConversationStore extends FetcherStore<ConversationModel> {
  @observable public selected = 0

  public addPrefetched(data: ConversationData) {
    const source = `/conversations/${data.id}`
    return this.prepopulate(source, data.id, data)
  }

  public fetchById(id: number) {
    const source = `/conversations/${id}`
    return this.fetch(source, id)
  }

  @computed
  public get currentConversation() {
    return this.getModel(this.selected)
  }
}

export const conversationStore = createFetcherStoreFactory(
  ConversationStore,
  ConversationModel,
)
