import {
  FetcherStore,
  createFetcherStoreFactory,
} from "../../../common/state/classes/FetcherStore"
import { MessageModel } from "../models/MessageModel"
import { MessageData } from "../types/MessageData"

export class MessageStore extends FetcherStore<MessageModel> {
  public addPrefetched(data: MessageData) {
    return this.prepopulate("/no-endpoint", data.id, data)
  }
}

export const messageStore = createFetcherStoreFactory(
  MessageStore,
  MessageModel,
)
