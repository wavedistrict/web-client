import { UserData } from "../../user/types/UserData"

export interface MessageData {
  id: number
  body: string
  createdAt: string
  updatedAt: string
  user: UserData
  conversation: {
    id: number
  }
}
