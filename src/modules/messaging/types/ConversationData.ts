import { UserData } from "../../user/types/UserData"

export interface LastMessage {
  id: number
  body: string
  createdAt: string
  updatedAt: string
  user: UserData
}

export interface ConversationData {
  id: number
  unreadMessages: number
  members: UserData[]
  lastMessage: LastMessage
}
