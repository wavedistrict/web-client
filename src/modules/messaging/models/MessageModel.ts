import { Model } from "../../../common/state/classes/Model"
import { MessageData } from "../types/MessageData"

export class MessageModel extends Model<MessageData> {
  public user = this.manager.stores.userStore.getModelFromData(this.data.user)
  public createdAt = new Date(this.data.createdAt)
}
