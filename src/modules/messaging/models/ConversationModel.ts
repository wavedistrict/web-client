import { apiServer } from "../../../common/network"
import { Model } from "../../../common/state/classes/Model"
import { UserModel } from "../../user/models/User"
import { createMessageForm } from "../helpers/createMessageForm"
import { ConversationData } from "../types/ConversationData"
import { Manager } from "../../../common/state/types/Manager"

export class ConversationModel extends Model<ConversationData> {
  public viewing = false

  public form = createMessageForm()
  public members: UserModel[]

  constructor(data: ConversationData, manager: Manager) {
    super(data, manager)
    const { userStore } = manager.stores

    this.members = this.data.members.map((member) => {
      return userStore.getModelFromData(member)
    })
  }

  public async clearUnread() {
    const { id, unreadMessages } = this.data
    const { activityStore } = this.manager.stores

    await apiServer.post(`/conversations/${id}/clear-unread`, null)

    activityStore.unreadMessages -= unreadMessages
    this.data.unreadMessages = 0
  }

  protected getListMap() {
    const { messageStore } = this.manager.stores

    return {
      messages: {
        store: messageStore,
        path: `/conversations/${this.data.id}/messages`,
        pending: false,
        limit: 25,
      },
    }
  }

  public get messages() {
    return this.getOrCreateList("messages")
  }

  public get recipient() {
    const { authStore } = this.manager.stores

    const user = authStore.user
    if (!user) return this.members[0]

    return (
      this.members.find((member) => member.data.id !== user.data.id) ||
      this.members[0]
    )
  }
}
