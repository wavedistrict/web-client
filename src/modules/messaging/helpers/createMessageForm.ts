import { FormManager } from "../../../common/ui/form/classes/FormManager"
import { TextFieldController } from "../../../common/ui/form/controllers/TextFieldController"
import { validateStringLength } from "../../../common/ui/form/validators/string/validateStringLength"

export const createMessageForm = () => {
  return new FormManager({
    body: new TextFieldController({
      isRequired: true,
      requiredWarning: "Cannot send an empty message",
      validators: [validateStringLength({ min: 1, max: 4096 })],
    }),
  })
}

export type MessageForm = ReturnType<typeof createMessageForm>
