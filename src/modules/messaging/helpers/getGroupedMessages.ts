import { groupBy } from "../../../common/lang/array/helpers/groupBy"
import { MessageModel } from "../models/MessageModel"

const separate = (message: MessageModel) => {
  const { user, createdAt } = message

  const date = [
    createdAt.getFullYear(),
    createdAt.getMonth(),
    createdAt.getDay(),
    createdAt.getHours(),
    createdAt.getMinutes(),
  ].join("-")

  return `${user.data.id}_${date}`
}

export const getGroupedMessages = (messages: MessageModel[]) => {
  return groupBy(messages, separate)
}
