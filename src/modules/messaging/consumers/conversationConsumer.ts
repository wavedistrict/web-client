import { FetcherStoreConsumer } from "../../realtime/classes/FetcherStoreConsumer"
import { ConversationModel } from "../models/ConversationModel"
import { ConversationData } from "../types/ConversationData"
import { Manager } from "../../../common/state/types/Manager"

export class ConversationConsumer extends FetcherStoreConsumer<
  ConversationData,
  ConversationModel
> {
  protected addListeners() {
    this.on("create", this.handleCreateConversation)
  }

  private async handleCreateConversation(conversation: ConversationModel) {
    const { authStore, activityStore } = this.manager.stores

    if (authStore.user) {
      authStore.user.conversations.add(conversation.data, "first")

      if (conversation.data.lastMessage.user.id !== authStore.user.data.id) {
        activityStore.unreadMessages += 1
      }
    }
  }
}

export const conversationConsumer = (manager: Manager) =>
  new ConversationConsumer(manager, "conversation", "conversationStore")
