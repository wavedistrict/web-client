import { when } from "mobx"
import { FetcherStoreConsumer } from "../../realtime/classes/FetcherStoreConsumer"
import { MessageModel } from "../models/MessageModel"
import { MessageData } from "../types/MessageData"
import { Manager } from "../../../common/state/types/Manager"

export class MessageConsumer extends FetcherStoreConsumer<
  MessageData,
  MessageModel
> {
  protected addListeners() {
    this.on("create", (message) => this.handleCreateMessage(message))
  }

  private handleCreateMessage = async (message: MessageModel) => {
    const { authStore, conversationStore, activityStore } = this.manager.stores
    if (!authStore.user) return

    const { data } = message
    const { user, ...safeData } = data

    const fetcher = conversationStore.fetchById(data.conversation.id)

    await when(() => fetcher.isFetched)

    const conversation = fetcher.model

    if (conversation) {
      conversation.data.lastMessage = {
        ...conversation.data.lastMessage,
        ...safeData,
      }

      authStore.user.conversations.add(conversation.data, "first")
      conversation.messages.add(message.data, "first")

      const shouldNotify =
        message.data.user.id !== authStore.user.data.id &&
        conversation.viewing === false

      if (shouldNotify) {
        activityStore.unreadMessages += 1
        conversation.data.unreadMessages += 1
      }
    }
  }
}

export const messageConsumer = (manager: Manager) =>
  new MessageConsumer(manager, "message", "messageStore")
