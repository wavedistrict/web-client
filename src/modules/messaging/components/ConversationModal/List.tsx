import { observer } from "mobx-react"
import React from "react"
import { useActivity } from "../../../../common/ui/navigation/hooks/useActivity"
import { UserModel } from "../../../user/models/User"
import { ConversationList } from "../ConversationList"
import { useStores } from "../../../../common/state/hooks/useStores"

export interface ConversationListProps {
  user: UserModel
}

export const List = observer(function List(props: ConversationListProps) {
  const { user } = props

  const activity = useActivity()
  const { conversationStore } = useStores()

  return (
    <ConversationList
      list={user.conversations}
      onClickRow={(conversation) => {
        conversationStore.selected = conversation.data.id
        activity.setCurrent(conversationStore.selected)
      }}
    />
  )
})
