import { observer } from "mobx-react"
import React, { useEffect } from "react"
import { getColor, getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { MessageForm } from "../MessageForm"
import { MessageList } from "./MessageList"
import { useStores } from "../../../../common/state/hooks/useStores"

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;

  > .messages {
    flex: 1;
    position: relative;
  }

  > .footer {
    border-top: solid 1px ${getColor("divider")};
    padding: ${getSpacing("4")}px;
  }
`

export const Conversation = observer(function Conversation() {
  const { conversationStore } = useStores()

  const conversation = conversationStore.currentConversation

  if (!conversation)
    throw new Error("Conversation cannot be undefined in message modal")

  useEffect(() => {
    conversation.viewing = true

    return () => {
      conversation.viewing = false
    }
  })

  return (
    <Container>
      <div className="messages">
        <MessageList key={conversation.data.id} conversation={conversation} />
      </div>
      <div className="footer">
        <MessageForm conversation={conversation} />
      </div>
    </Container>
  )
})
