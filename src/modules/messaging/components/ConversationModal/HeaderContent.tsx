import { observer } from "mobx-react"
import React from "react"
import {
  MoreButton,
  MoreButtonItem,
} from "../../../../common/ui/button/components/MoreButton"
import { useModal } from "../../../modal/hooks/useModal"
import { getFontSize, getFontWeight, getSpacing } from "../../../theme/helpers"
import { styled } from "../../../theme/themes"
import { OnlineStatusIndicator } from "../../../user/components/OnlineStatusIndicator"
import { useStores } from "../../../../common/state/hooks/useStores"

const Container = styled.div`
  margin-left: ${getSpacing("4")}px;
  padding-right: ${getSpacing("4")}px;

  display: flex;
  align-items: center;

  width: 100%;

  > .primaryInfo {
    flex: 1;
  }

  > .primaryInfo > .displayName {
    font-size: ${getFontSize("3")}px;
    font-weight: ${getFontWeight("bold")};
  }

  > .primaryInfo > .displayName > .status {
    display: inline-block;
    margin-left: ${getSpacing("3")}px;
  }

  > .actions {
  }
`

export const HeaderContent = observer(function HeaderContent() {
  const modal = useModal()
  const { conversationStore, routingStore } = useStores()

  const conversation = conversationStore.currentConversation
  if (!conversation) return null

  const { recipient } = conversation

  const actions: MoreButtonItem[] = [
    {
      icon: "personFilled",
      label: "View profile",
      onClick: () => {
        routingStore.push(recipient.clientLink)
        modal.dismiss()
      },
    },
    {
      icon: "failed",
      label: "Close",
      onClick: modal.dismiss,
    },
  ]

  return (
    <Container>
      <div className="primaryInfo">
        <div className="displayName">
          <span>{recipient.data.displayName}</span>
          <span className="status">
            <OnlineStatusIndicator user={recipient} />
          </span>
        </div>
      </div>
      <div className="actions">
        <MoreButton outlined compact kind="secondary" items={actions} />
      </div>
    </Container>
  )
})
