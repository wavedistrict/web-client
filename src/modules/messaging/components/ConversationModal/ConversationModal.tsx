import { observer } from "mobx-react"
import React from "react"
import { ActivityTabs } from "../../../../common/ui/navigation/components/ActivityTabs/ActivityTabs"
import { Navigation } from "../../../../common/ui/navigation/components/ActivityTabs/Navigation"
import { Pane } from "../../../../common/ui/navigation/components/ActivityTabs/Pane"
import { NAVIGATION_HEADER_HEIGHT } from "../../../../common/ui/navigation/constants"
import { ModalCloseButton } from "../../../modal/components/ModalCloseButton"
import { PrimaryModal } from "../../../modal/components/PrimaryModal"
import { UserModel } from "../../../user/models/User"
import { Conversation } from "./Conversation"
import { HeaderContent } from "./HeaderContent"
import { List } from "./List"
import { useStores } from "../../../../common/state/hooks/useStores"

export interface ConversationModalProps {
  selected: number
  user: UserModel
}

export const ConversationModal = observer(function ConversationModal(
  props: ConversationModalProps,
) {
  const { user } = props
  const { conversationStore } = useStores()

  return (
    <PrimaryModal.Base>
      <ActivityTabs
        initial={conversationStore.selected}
        initiallyOpen={!!conversationStore.selected}
      >
        <Navigation>
          <Navigation.Header title="Messages">
            <ModalCloseButton size={NAVIGATION_HEADER_HEIGHT} />
          </Navigation.Header>
          <Navigation.Body>
            <List user={user} />
          </Navigation.Body>
        </Navigation>
        <Pane>
          <Pane.Header>
            <HeaderContent />
          </Pane.Header>
          <Pane.Body<number>>{() => <Conversation />}</Pane.Body>
        </Pane>
      </ActivityTabs>
    </PrimaryModal.Base>
  )
})
