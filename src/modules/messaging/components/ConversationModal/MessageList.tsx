import { observer } from "mobx-react"
import React, { useEffect } from "react"
import { FetcherListCursor } from "../../../../common/ui/list/components/FetcherListCursor"
import { FetcherListTail } from "../../../../common/ui/list/components/FetcherListTail"
import { StableScrollContainer } from "../../../../common/ui/scroll/components/StableScrollContainer"
import { getGroupedMessages } from "../../helpers/getGroupedMessages"
import { ConversationModel } from "../../models/ConversationModel"
import { MessageListItem } from "../MessageListItem"

export interface MessageList {
  conversation: ConversationModel
}

export const MessageList = observer(function MessageList(props: MessageList) {
  const { conversation } = props
  const { messages } = conversation

  useEffect(() => {
    conversation.clearUnread()

    return () => {
      conversation.clearUnread()
    }
  }, [conversation])

  const renderList = () => {
    if (messages.models.length === 0) return []

    const grouped = getGroupedMessages([...messages.models].reverse())

    return grouped.map((messages) => (
      <MessageListItem key={messages[0].data.id} messages={messages} />
    ))
  }

  return (
    <FetcherListCursor padding={256} tail="top" list={messages}>
      {({ onScroll }) => (
        <>
          <StableScrollContainer onScroll={onScroll}>
            {renderList()}
          </StableScrollContainer>
          <FetcherListTail absolute="top" list={messages} />
        </>
      )}
    </FetcherListCursor>
  )
})
