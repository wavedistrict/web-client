import React from "react"
import { HeaderListPopover } from "../../core/components/HeaderListPopover"
import { RenderPopoverContentProps } from "../../popover/types/PopoverData"
import { spawnConversationModal } from "../actions/spawnConversationModal"
import { ConversationList } from "./ConversationList"
import { useStores } from "../../../common/state/hooks/useStores"
import { useManager } from "../../../common/state/hooks/useManager"

export function MessagePopover(props: RenderPopoverContentProps) {
  const { authStore, conversationStore } = useStores()
  const manager = useManager()

  const { user } = authStore
  if (!user) return null

  return (
    <HeaderListPopover title="Messages">
      <ConversationList
        list={user.conversations}
        onClickRow={(item) => {
          conversationStore.selected = item.data.id
          spawnConversationModal(manager, item.data.id, user)
          props.close()
        }}
      />
    </HeaderListPopover>
  )
}
