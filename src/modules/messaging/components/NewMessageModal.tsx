import React from "react"
import { FormField } from "../../../common/ui/form/components/FormField"
import { renderControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { FormModal } from "../../modal/components/FormModal"
import { useModal } from "../../modal/hooks/useModal"
import { UserModel } from "../../user/models/User"
import { MessageForm } from "../helpers/createMessageForm"

interface NewMessageModalProps {
  form: MessageForm
  user: UserModel
}

export function NewMessageModal(props: NewMessageModalProps) {
  const { form, user } = props
  const { fields } = form

  const { dismiss } = useModal()

  async function onSubmit() {
    await form.submit(`${user.apiPath}/messages`, "post")
    dismiss()
  }

  return (
    <FormModal title="New message" onSubmit={onSubmit} form={form}>
      <div className="FieldGrid">
        <div className="cell -full">
          <FormField
            label="Body"
            controller={fields.body}
            renderInput={renderControlledTextInput({
              autoFocus: true,
              placeholder: "Type your message here",
              multiline: true,
            })}
          />
        </div>
      </div>
    </FormModal>
  )
}
