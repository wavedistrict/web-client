import { observer } from "mobx-react"
import { size } from "polished"
import React from "react"
import { Markdown } from "../../../common/ui/markdown/components/Markdown"
import { HumanizedDate } from "../../core/components/HumanizedDate"
import {
  getColor,
  getFontColor,
  getFontSize,
  getFontWeight,
  getSpacing,
} from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { styled } from "../../theme/themes"
import { UserAvatar } from "../../user/components/UserAvatar"
import { MessageModel } from "../models/MessageModel"

interface MessageListItemProps {
  messages: MessageModel[]
}

const Container = styled.article`
  padding: ${getSpacing("4")}px 0px;
  margin: 0px ${getSpacing("4")}px;

  display: flex;

  > .avatar {
    ${size(42)};
    ${imageContainer("100%")};
    flex-shrink: 0;
  }

  & + & {
    border-top: solid 1px ${getColor("divider")};
  }
`

const Content = styled.div`
  margin-left: ${getSpacing("4")}px;
  flex: 1;

  > .header {
    display: flex;
    justify-content: space-between;
  }

  > .header > .displayName {
    font-size: ${getFontSize("2")}px;
    font-weight: ${getFontWeight("bold")};
  }

  > .header > .timestamp {
    font-size: ${getFontSize("1")}px;
    font-style: italic;

    color: ${getFontColor("muted")};
  }

  > .body {
    margin-top: ${getSpacing("2")}px;
    font-size: 14px;

    word-break: break-word;
  }
`

export const MessageListItem = observer(function MessageListItem(
  props: MessageListItemProps,
) {
  const { messages } = props

  const message = messages[0]
  const { user, data } = message

  const renderMessages = () => {
    return messages.map((message) => (
      <div key={message.data.id} className="line">
        <Markdown text={message.data.body} />
      </div>
    ))
  }

  return (
    <Container>
      <div className="avatar">
        <UserAvatar id="message" user={user} />
      </div>
      <Content>
        <div className="header">
          <h1 className="displayName">{user.data.displayName}</h1>
          <div className="timestamp">
            <HumanizedDate children={data.createdAt} />
          </div>
        </div>
        <div className="body">{renderMessages()}</div>
      </Content>
    </Container>
  )
})
