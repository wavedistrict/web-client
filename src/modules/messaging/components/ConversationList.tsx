import React from "react"
import { FetcherList } from "../../../common/state/classes/FetcherList"
import { createVirtualizedListComponent } from "../../../common/ui/list/helpers/createVirtualizedListComponent"
import { renderContainerList } from "../../../common/ui/list/helpers/renderContainerList"
import { wrapRowInLi } from "../../../common/ui/list/helpers/wrapRowInLi"
import { ListComponentProps } from "../../../common/ui/list/types/ListComponentProps"
import { ConversationModel } from "../models/ConversationModel"
import {
  ConversationListItem,
  CONVERSATION_LIST_ITEM_HEIGHT,
} from "./ConversationListItem"

interface ConversationListProps {
  list: FetcherList<ConversationModel>
  onClickRow?: (item: ConversationModel) => void
}

export class ConversationList extends React.Component<ConversationListProps> {
  private List: React.ComponentType<ListComponentProps<ConversationModel>>

  constructor(props: ConversationListProps) {
    super(props)

    const { onClickRow = () => {} } = props

    this.List = createVirtualizedListComponent<ConversationModel>({
      rowHeight: CONVERSATION_LIST_ITEM_HEIGHT,
      renderItem: wrapRowInLi((item) => (
        <ConversationListItem
          conversation={item}
          onClick={() => onClickRow(item)}
        />
      )),
    })
  }

  public render() {
    const { list } = this.props

    return renderContainerList({
      list,
      render: (props) => <this.List {...props} />,
    })
  }
}
