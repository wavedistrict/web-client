import styled from "@emotion/styled"
import React from "react"
import { FormContext } from "../../../common/ui/form/contexts/FormContext"
import { ControlledTextInput } from "../../../common/ui/input/components/ControlledTextInput"
import { ConversationModel } from "../models/ConversationModel"

export interface MessageFormProps {
  conversation: ConversationModel
}

const Container = styled.div`
  width: 100%;
`

export function MessageForm(props: MessageFormProps) {
  const { conversation } = props
  const { form } = conversation

  const submit = async () => {
    const path = `/conversations/${conversation.data.id}/messages`

    await conversation.form.submit(path, "post")
    conversation.form.reset()
  }

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === "Enter") {
      event.preventDefault()
      submit()
    }
  }

  return (
    <FormContext.Provider value={{ form }}>
      <Container>
        <div className="input" onKeyDown={handleKeyDown}>
          <ControlledTextInput
            placeholder="Type your message here"
            autoFocus={true}
            controller={conversation.form.fields.body}
            multiline
          />
        </div>
      </Container>
    </FormContext.Provider>
  )
}
