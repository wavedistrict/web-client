import { observer } from "mobx-react"
import { ellipsis, size } from "polished"
import React from "react"
import { preloadList } from "../../../common/state/helpers/preloadList"
import { HumanizedDate } from "../../core/components/HumanizedDate"
import {
  getColor,
  getDuration,
  getFontColor,
  getFontSize,
  getFontWeight,
  getShadow,
  getSpacing,
  getTransparency,
} from "../../theme/helpers"
import { imageContainer } from "../../theme/mixins/imageContainer"
import { styled } from "../../theme/themes"
import { OnlineStatusIndicator } from "../../user/components/OnlineStatusIndicator"
import { UserAvatar } from "../../user/components/UserAvatar"
import { ConversationModel } from "../models/ConversationModel"

export const CONVERSATION_LIST_ITEM_HEIGHT = 102

export interface ConversationListItemProps {
  conversation: ConversationModel
  isActive?: boolean
  onClick?: () => void
}

const Container = styled.div`
  position: relative;

  padding: ${getSpacing("4")}px;
  display: flex;

  transition: ${getDuration("normal")} ease background;

  &:hover {
    background: ${getTransparency("weakNegative")};
    cursor: pointer;
  }
`

const Avatar = styled.div`
  ${size(48)}

  flex-shrink: 0;
  position: relative;

  > .image {
    ${imageContainer("100%")}
    ${size("100%")}
  }

  > .status {
    position: absolute;
    bottom: 0px;
    right: 0px;

    background: ${getColor("primary")};
    border-radius: 100%;

    padding: ${getSpacing("1")}px;
  }
`

const Content = styled.div`
  flex: 1;

  margin-left: ${getSpacing("4")}px;
  overflow: hidden;

  > .displayName {
    font-size: ${getFontSize("2")}px;
    font-weight: ${getFontWeight("bold")};
  }

  > .lastMessage {
    margin-top: ${getSpacing("2")}px;
    font-size: ${getFontSize("2")}px;

    ${ellipsis()}
  }

  > .timestamp {
    margin-top: ${getSpacing("2")}px;

    font-size: ${getFontSize("1")}px;
    font-style: italic;

    color: ${getFontColor("muted")};
  }
`

const UnreadIndicator = styled.div<{ hidden: boolean }>`
  display: flex;
  align-items: center;
  padding-right: ${getSpacing("4")}px;

  > .indicator {
    ${size(38)};
    display: flex;

    justify-content: center;
    align-items: center;

    background: ${getColor("primary")};
    color: ${getColor("accent")};
    box-shadow: ${getShadow("light")};

    border: solid 2px ${getColor("accent")};
    border-radius: 100%;

    font-size: ${getFontSize("1")}px;
    font-weight: ${getFontWeight("bold")};

    transition: ${getDuration("normal")} ease;
    transition-property: transform opacity;

    ${(props) =>
      props.hidden &&
      `
      transform: scale(0);
      opacity: 0;
    `}
  }
`

export const ConversationListItem = observer(function ConversationListItem(
  props: ConversationListItemProps,
) {
  const { conversation, onClick } = props
  const { recipient, data } = conversation
  const { lastMessage, unreadMessages } = data

  if (!recipient) {
    throw new Error("Recipient is undefined in ConversationListItem")
  }

  const handleOver = () => {
    preloadList(conversation.messages)
  }

  return (
    <Container onMouseOver={handleOver} onClick={onClick}>
      <Avatar>
        <div className="image">
          <UserAvatar id="conversationItem" user={recipient} />
        </div>
        <div className="status">
          <OnlineStatusIndicator user={recipient} />
        </div>
      </Avatar>
      <Content>
        <div className="displayName">{recipient.data.displayName}</div>
        <div className="lastMessage">{lastMessage.body}</div>
        <div className="timestamp">
          <HumanizedDate children={lastMessage.createdAt} />
        </div>
      </Content>
      <UnreadIndicator hidden={unreadMessages === 0}>
        <div className="indicator">
          {unreadMessages > 99 ? "99+" : unreadMessages}
        </div>
      </UnreadIndicator>
    </Container>
  )
})
