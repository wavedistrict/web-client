import { observer } from "mobx-react"
import React from "react"
import { HeaderButton } from "../../core/components/HeaderButton"
import { PopoverTrigger } from "../../popover/components/PopoverTrigger"
import { MessagePopover } from "./MessagePopover"
import { useStores } from "../../../common/state/hooks/useStores"

export const MessageButton = observer(function MessageButton() {
  const { activityStore } = useStores()

  const { unreadMessages } = activityStore
  const hasUnreadMessages = unreadMessages > 0

  const title = hasUnreadMessages
    ? `You have ${unreadMessages} unread messages`
    : "Messages"

  return (
    <PopoverTrigger placement="bottom-end">
      {(props) => (
        <HeaderButton
          title={title}
          ref={props.ref}
          icon="envelope"
          onClick={() => props.toggle()}
          active={props.isActive}
          hasAlert={hasUnreadMessages}
        />
      )}
      {(props) => <MessagePopover {...props} />}
    </PopoverTrigger>
  )
})
