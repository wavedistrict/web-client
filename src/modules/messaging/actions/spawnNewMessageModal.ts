import React from "react"
import { requireAuth } from "../../auth/helpers/requireAuth"
import { UserModel } from "../../user/models/User"
import { NewMessageModal } from "../components/NewMessageModal"
import { createMessageForm } from "../helpers/createMessageForm"

export const spawnNewMessageModal = requireAuth((manager, user: UserModel) => {
  const { modalStore } = manager.stores
  const form = createMessageForm()

  modalStore.spawn({
    key: `new-message-${user.data.id}`,
    render: () => React.createElement(NewMessageModal, { form, user }),
  })
})
