import React from "react"
import { UserModel } from "../../user/models/User"
import { ConversationModal } from "../components/ConversationModal/ConversationModal"
import { Manager } from "../../../common/state/types/Manager"

export const spawnConversationModal = (
  manager: Manager,
  selected: number,
  user: UserModel,
) => {
  const { modalStore } = manager.stores

  modalStore.spawn({
    key: "conversation-modal",
    render: () => React.createElement(ConversationModal, { selected, user }),
    dismissOnClickout: true,
  })
}
