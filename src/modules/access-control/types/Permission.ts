import { PERMISSIONS } from "../constants"

export type Permission = typeof PERMISSIONS[number]
