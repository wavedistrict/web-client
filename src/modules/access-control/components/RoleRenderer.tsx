import { adjustHue, size } from "polished"
import React from "react"
import { getValueFromMap } from "../../../common/lang/object/helpers/getValueFromMap"
import { IconType } from "../../../common/ui/icon"
import { Icon } from "../../../common/ui/icon/components/Icon"
import {
  getFontSize,
  getFontWeight,
  getShadow,
  getSpacing,
  getTransparency,
  getFontColor,
} from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { Role } from "../types/Role"

export interface RoleRendererProps {
  name: Role | string
}

const iconMap: Record<Role, IconType> = {
  Administrator: "shieldWithStar",
  Moderator: "shield",
  Developer: "developer",
  Amplitude: "amplitude",
}

const GLYPH_COLOR = "rgba(255, 255, 255, 0.9)"

const Container = styled.div<{ name: Role | string }>`
  display: inline-flex;

  box-shadow: ${getShadow("light")};
  background: ${({ theme, name }) => {
    const { roleColors, fontColors } = theme
    const color = getValueFromMap(roleColors, name, fontColors.normal)

    return `linear-gradient(155deg, ${color}, ${adjustHue(35, color)})`
  }};

  color: ${GLYPH_COLOR};

  border-radius: 100px;

  font-weight: ${getFontWeight("bold")};
  font-size: ${getFontSize("1")}px;

  padding: 5px 12px;

  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  align-items: center;
`

const RoleIcon = styled(Icon)`
  ${size(16)}

  display: inline-block;
  margin-right: ${getSpacing("2")}px;

  fill: ${GLYPH_COLOR};
`

export function RoleRenderer(props: RoleRendererProps) {
  const { name } = props

  return (
    <Container name={name}>
      <RoleIcon name={iconMap[name as Role]} />
      <span>{name}</span>
    </Container>
  )
}
