import { observer } from "mobx-react"
import React, { useEffect } from "react"
import { CSSTransition, TransitionGroup } from "react-transition-group"
import { TransitionProps } from "react-transition-group/Transition"
import { styled } from "../../theme/themes"
import { PopoverItem } from "./PopoverItem"
import { useStores } from "../../../common/state/hooks/useStores"

type TransitionElement = React.ReactElement<TransitionProps>

const Container = styled.div`
  position: relative;
  top: 0;
  z-index: 3;
`

export const PopoverOverlay = observer(function PopoverOverlay() {
  const { popoverStore } = useStores()
  const rootRef = React.useRef<HTMLDivElement>(null)

  useEffect(() => {
    window.addEventListener("click", handleClick)

    return () => {
      window.removeEventListener("click", handleClick)
    }
  })

  const handleClick = (event: MouseEvent) => {
    const root = rootRef.current
    const target = event.target

    if (!root || !(target instanceof Node)) return

    const clickWasInside = root.contains(target)
    const clickWasOnAnchor = popoverStore.popovers.some((popover) =>
      popover.anchor.contains(target),
    )

    if (!clickWasInside && !clickWasOnAnchor) popoverStore.removeAll()
  }

  return (
    <Container ref={rootRef}>
      <TransitionGroup component={React.Fragment}>
        {popoverStore.popovers.map<TransitionElement>((popover) => (
          <CSSTransition timeout={200} key={popover.id} classNames="popover">
            <PopoverItem popover={popover} />
          </CSSTransition>
        ))}
      </TransitionGroup>
    </Container>
  )
})
