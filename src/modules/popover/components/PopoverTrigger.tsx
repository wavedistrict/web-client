import { observer } from "mobx-react"
import React, { useCallback, useEffect, useRef, useState } from "react"
import { isFixed } from "../helpers/isFixed"
import { PopoverModel } from "../models/Popover"
import { PopoverOptions, PopoverRendererFn } from "../types/PopoverData"
import { useStores } from "../../../common/state/hooks/useStores"

export interface PopoverChildrenFunctionProps {
  open: () => void
  close: () => void
  toggle: () => void
  isActive: boolean
  ref: React.RefObject<any>
}

type OmittedProps = "anchor" | "fixed"

interface PopoverTriggerProps extends Omit<PopoverOptions, OmittedProps> {
  children: [
    (props: PopoverChildrenFunctionProps) => JSX.Element,
    PopoverRendererFn,
  ]
}

export const PopoverTrigger = observer(function PopoverTrigger(
  props: PopoverTriggerProps,
) {
  const anchorRef = useRef<HTMLElement>(null)
  const fixedRef = useRef(false)

  const { popoverStore } = useStores()
  const { children, placement, useAnchorWidth } = props

  const [popover, setPopover] = useState<PopoverModel | undefined>(undefined)
  const [renderChildren, renderPopoverContent] = children

  const remove = useCallback(
    (popover?: PopoverModel) => {
      if (!popover) return
      popoverStore.removePopover(popover.id)
    },
    [popoverStore],
  )

  useEffect(() => {
    const { current: anchor } = anchorRef
    popoverStore.on("close", handleClose)

    if (anchor) {
      fixedRef.current = isFixed(anchor)
    }

    return () => {
      remove(popover)
      popoverStore.off("close", handleClose)
    }
  }, [popover, popoverStore, remove])

  const add = () => {
    const { current: anchor } = anchorRef

    if (!anchor) return

    const data: PopoverOptions = {
      placement,
      useAnchorWidth,
      positionFixed: fixedRef.current,
    }

    const popover = popoverStore.addPopover(anchor, renderPopoverContent, data)
    setPopover(popover)
  }

  const toggle = () => {
    if (popover) {
      remove(popover)
    } else {
      add()
    }
  }

  const handleClose = () => {
    setPopover(undefined)
  }

  return (
    <>
      {renderChildren({
        isActive: !!popover,
        ref: anchorRef,
        close: remove,
        open: add,
        toggle,
      })}
    </>
  )
})
