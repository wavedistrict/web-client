import Popper from "popper.js"
import React, { RefObject, useCallback, useEffect, useRef } from "react"
import { getColor, getDuration, getSpacing } from "../../theme/helpers"
import { styled } from "../../theme/themes"
import { PopoverModel } from "../models/Popover"
import { useStores } from "../../../common/state/hooks/useStores"

interface UsePopperOptions {
  anchor: HTMLElement
  root: RefObject<HTMLElement>
  placement: Popper.PopperOptions["placement"]
  positionFixed?: boolean
  useAnchorWidth?: boolean
}

function usePopper(options: UsePopperOptions) {
  const { anchor, root, placement, positionFixed, useAnchorWidth } = options

  useEffect(() => {
    if (!root.current) return
    const currentRoot = root.current

    if (useAnchorWidth) currentRoot.style.width = `${anchor.offsetWidth}px`

    const popper = new Popper(anchor, currentRoot, {
      placement,
      positionFixed,
      modifiers: {
        preventOverflow: {
          padding: { left: 16, right: 16, top: 56, bottom: 56 },
          boundariesElement: "viewport",
        },
        offset: {
          // Offset that creates 8px between root and anchor, no matter the placement
          offset: "0, 8px",
        },
      },
    })

    return () => popper.destroy()
  }, [useAnchorWidth, anchor, placement, positionFixed, root])
}

interface PopoverItemProps {
  popover: PopoverModel
}

const Container = styled.div`
  max-height: 100vh;

  > .content {
    background-color: ${getColor("primary")};
    border-radius: 3px;
    border: solid 1px ${getColor("divider")};

    max-width: calc(100vw - (${getSpacing("4")}px * 2));
    overflow: hidden;
    position: relative;
    z-index: 0;

    transition: ${getDuration("normal")} ease;
    transition-property: transform, opacity;
    opacity: 0;
    transform: translateY(-4px);
  }

  &.popover-enter-active > .content {
    opacity: 1;
    transform: translateY(0);
  }

  &.popover-enter-done > .content {
    opacity: 1;
    transform: translateY(0);
  }

  &.popover-exit > .content {
    opacity: 1;
    transform: translateY(0);
  }

  &.popover-exit-active > .content {
    opacity: 0;
    transform: translateY(-4px);
  }
`

export function PopoverItem(props: PopoverItemProps) {
  const { popover } = props
  const { anchor, options, renderContent } = popover
  const { placement, positionFixed, useAnchorWidth } = options
  const { popoverStore } = useStores()

  const close = useCallback(() => popoverStore.removePopover(popover.id), [popover.id, popoverStore])

  const root = useRef<HTMLDivElement>(null)
  usePopper({ root, anchor, placement, positionFixed, useAnchorWidth })

  const renderContentProps = {
    close,
    anchor,
  }

  return (
    <Container className="PopoverItem" ref={root}>
      <div className="content">{renderContent(renderContentProps)}</div>
    </Container>
  )
}
