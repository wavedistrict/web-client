import React from "react"
import {
  getColor,
  getDuration,
  getTransparency,
  getSpacing,
} from "../../theme/helpers"
import css from "@emotion/css"
import { styled } from "../../theme/themes"
import { Button } from "../../../common/ui/button/components/Button"

export interface MenuItemProps extends React.HTMLAttributes<HTMLElement> {
  isActive?: boolean
}

export type PopoverMenuProps = React.HTMLAttributes<HTMLElement>

const Container = styled.div`
  min-width: 150px;

  display: flex;
  flex-direction: column;

  > hr {
    margin: 0;
    border: none;
    border-bottom: 1px solid ${getColor("divider")};
  }
`

const StaticItem = styled.div`
  display: block;
  padding: ${getSpacing("4")}px ${getSpacing("4")}px;
`

const ClickableItem = styled(Button)<{ active?: boolean }>`
  display: block;
  padding: ${getSpacing("4")}px ${getSpacing("4")}px;

  transition: ${getDuration("normal")} ease;
  transition-property: color background;
  outline: none;

  &:hover {
    background: ${getTransparency("weakPositive")};
  }

  &:active {
    background: ${getTransparency("strongNegative")};
  }

  ${(props) =>
    props.active &&
    css`
      color: ${props.theme.colors.accent};
    `}
`

export function PopoverMenu(props: PopoverMenuProps) {
  return <Container {...props} />
}

PopoverMenu.Item = function Item(props: MenuItemProps) {
  const { isActive, onClick, className, ...rest } = props
  const clickable = onClick !== undefined

  if (clickable)
    return (
      <ClickableItem
        className={className}
        active={isActive}
        onClick={onClick as any}
        {...rest}
      />
    )

  return <StaticItem className={className} {...rest} />
}
