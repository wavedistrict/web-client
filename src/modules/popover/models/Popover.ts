import { createUniqueKey } from "../../../common/state/helpers/createUniqueKey"
import { PopoverOptions, PopoverRendererFn } from "../types/PopoverData"

export class PopoverModel {
  public readonly id = createUniqueKey()
  public options: PopoverOptions

  public anchor: HTMLElement
  public renderContent: PopoverRendererFn

  constructor(
    anchor: HTMLElement,
    renderContent: PopoverRendererFn,
    options: PopoverOptions,
  ) {
    this.anchor = anchor
    this.renderContent = renderContent
    this.options = options
  }
}
