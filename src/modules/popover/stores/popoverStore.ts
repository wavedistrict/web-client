import { action, computed, observable } from "mobx"
import { Emitter } from "../../../common/state/classes/Emitter"
import { PopoverModel } from "../models/Popover"
import { PopoverOptions, PopoverRendererFn } from "../types/PopoverData"
import {
  InitializableStore,
  createStoreFactory,
} from "../../../common/state/classes/InitializableStore"

export interface PopoverStoreEvents {
  open: number
  close: number
}

export class PopoverStore extends InitializableStore {
  @observable public popoverMap = new Map<number, PopoverModel>()

  private emitter = new Emitter<PopoverStoreEvents>()
  public async init() {}

  @computed
  public get popovers(): PopoverModel[] {
    return [...this.popoverMap.values()]
  }

  @action
  public addPopover(
    anchor: HTMLElement,
    renderContent: PopoverRendererFn,
    options: PopoverOptions,
  ) {
    const popover = new PopoverModel(anchor, renderContent, options)

    this.removeAll()
    this.popoverMap.set(popover.id, popover)
    this.emit("open", popover.id)

    return popover
  }

  @action
  public removePopover(key: number) {
    const popover = this.popoverMap.get(key)
    if (!popover) return

    this.popoverMap.delete(key)
    this.emit("close", key)
  }

  public removeAll() {
    for (const popover of this.popovers) {
      this.removePopover(popover.id)
      this.emit("close", popover.id)
    }
  }

  @computed
  public get hasPopovers() {
    return this.popoverMap.size > 0
  }

  public get on() {
    return this.emitter.on
  }

  public get off() {
    return this.emitter.off
  }

  private get emit() {
    return this.emitter.emit
  }
}

export const popoverStore = createStoreFactory(PopoverStore)
