import { Placement } from "popper.js"
import { ReactNode } from "react"

export interface RenderPopoverContentProps {
  close: () => void
  anchor: HTMLElement
}

export type PopoverRendererFn = (
  props: RenderPopoverContentProps,
) => JSX.Element

export interface PopoverOptions {
  placement: Placement
  useAnchorWidth?: boolean
  positionFixed?: boolean
}
