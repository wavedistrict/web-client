export const isFixed = (element: HTMLElement): boolean => {
  if (element.offsetParent) {
    let childElement = element
    do {
      const position = window
        .getComputedStyle(childElement)
        .getPropertyValue("position")
        .toLowerCase()

      if (position === "fixed") {
        return true
      }

      childElement = childElement.offsetParent as HTMLElement
    } while (childElement)
  }

  return false
}
