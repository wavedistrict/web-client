// window.matchMedia doesn't exist in jsdom for some reason
const matchMedia = (media: string) => {
  return {
    media,
    matches: false,
    addListener() {},
    removeListener() {},
    onchange() {},
    addEventListener() {},
    removeEventListener() {},
    dispatchEvent: () => false,
  }
}

window.matchMedia = window.matchMedia || matchMedia

export {}
