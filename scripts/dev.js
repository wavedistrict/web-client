const serve = require("webpack-serve")
const config = require("./serve.config")
const stats = require("./stats.config")

serve({
  config,
  dev: { stats },
})
