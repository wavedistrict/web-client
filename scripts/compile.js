// @ts-check
const webpack = require("webpack")
const config = require("./webpack.config")

module.exports = function compile() {
  return new Promise((resolve, reject) => {
    webpack(config, (error, stats) => {
      if (error) {
        console.error(error.stack || error)

        // @ts-ignore
        if (error.details) {
          // @ts-ignore
          console.error(error.details)
        }

        return reject()
      }

      const info = stats.toJson()

      if (stats.hasErrors()) {
        console.error(info.errors)
      }

      if (stats.hasWarnings()) {
        console.warn(info.warnings)
      }

      resolve(stats)
    })
  })
}
