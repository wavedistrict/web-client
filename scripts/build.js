// @ts-check
const statsConfig = require("./stats.config")
const compile = require("./compile")

compile().then((stats) => {
  console.info(stats.toString(statsConfig))
})
