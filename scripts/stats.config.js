/** @type {import("webpack").Options.Stats} */
const stats = {
  modules: false,
  colors: true,
  assetsSort: "!size",
  children: false,
  hash: false,
  excludeAssets: /\.map$/,
}

module.exports = stats
