These are guidelines and other notes you should read and reference when contributing to client development. Feel free to make suggestions on how anything can be made clearer, or if you feel that anything is missing.

<!-- toc -->

- [Building](#building)
- [Editor Setup / Tooling](#editor-setup--tooling)
  - [TypeScript](#typescript)
  - [ESLint](#eslint)
    - [VSCode](#vscode)
  - [Prettier](#prettier)
  - [Yarn](#yarn)
  - [Testing](#testing)
- [Code Style](#code-style)
  - [Folder Structure](#folder-structure)
  - [React Components and Hooks](#react-components-and-hooks)
    - [Splitting up big components](#splitting-up-big-components)
  - [Styling](#styling)
    - [Emotion](#emotion)
    - [`styled` usage](#styled-usage)
    - [Theming](#theming)
  - [Comments](#comments)

<!-- tocstop -->

# Building

Before running the client, make sure you have a browser with web security (CORS) disabled: https://alfilatov.com/posts/run-chrome-without-cors/
Because of cookie restrictions you will not be able to send requests if not.

To build and run the client:

1. Make sure you have [Node.JS](https://nodejs.org/en/) installed, version 8.7.0 or higher.
1. Clone the repository to a folder of your choosing (e.g. `git clone https://gitlab.com/wavedistrict/web-client.git`)
1. `cd` into that folder
1. Install the project dependencies by running `npm install`. It might take a while.
1. Run the dev server: `npm run dev`

# Editor Setup / Tooling

## TypeScript

The project is written in [TypeScript][typescript], so make sure whatever editor you're using has proper support for it. We recommend [VSCode][vscode].

Before pushing commits, run the `typecheck` script to make sure there are no type errors. If an error can't be fixed for some reason, put a `// @ts-ignore` comment above the line with an explanation. Example:

```ts
// @ts-ignore: createThing accepts three arguments now instead of two, but the types aren't updated yet
const thing = createThing(1, 2, 3)
```

## ESLint

We use [ESLint](https://eslint.org) for linting, and pushed commits should be free of linter warnings and errors. To make things easier, for whichever editor you use, install the ESLint extension and enable formatting on save.

### VSCode

[ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

For linting with TS with auto fixing, use these settings:

```json
{
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    { "language": "typescript", "autoFix": true },
    { "language": "typescriptreact", "autoFix": true }
  ]
}
```

## Prettier

The project uses [Prettier][prettier] to format the codebase. Make sure your editor has proper support for it. Enable formatting on save for a more streamlined dev workflow.

## Yarn

We use [Yarn][yarn] instead of npm, for its speed and the reliability of the lockfile.

While it's possible to use npm, it's _strongly_ recommended that you use yarn instead, to ensure the exact same versions of projects are installed across systems. Here are a few example commands using yarn:

```sh
# install dependencies, should be done after every pull
yarn

# add a new dependency
yarn add xyz

# remove a dependency
yarn remove xyz

# run the dev server
yarn dev

# run the type checker
# make sure there are no type errors or linting errors before pushing
yarn typecheck
```

## Testing

We use [Jest](https://facebook.github.io/jest/en/) for testing. To make a test, make a `.test.ts(x)` file in the same place next to the file you're testing. For actually writing tests, [see Jest's "Getting Started" guide](https://facebook.github.io/jest/docs/en/getting-started.html) and the rest the docs on the Jest website.

We use [react-testing-library](https://github.com/kentcdodds/react-testing-library) to test React components, and the page should include instructions and examples on how to use it.

Run all of the tests with `yarn test`/`npm test`. Tests should be passing before pushing.

# Code Style

## Folder Structure

The project uses a feature-based folder structure, where files are grouped by _domain_ (auth, player, track, ...) instead of by _type_ (components, stores, ...).

- `src/modules` - Code specific to the app, separated by domain

- `src/common` - Code non-specific to the app, separated by domain

- `src/polyfills` - Folder for all of the polyfills needed to run the app

**Tip:** Use the `gencomponent` npm script to create a new component, which will generate the appropriate files and automatically update the appropriate index files.

## React Components and Hooks

We require any new components to be function components instead of class components. Instead of using `setState` and lifecycles in classes, use hooks.
[Read the guide on the react website for more info.](https://reactjs.org/hooks)

For current class components, keep them as is. Only rewrite them to function components with hooks if there's a compelling reason to do so.

If a class component has no state or lifecycle usage, feel free to turn it into a function component (since we would have been able to do that anyway).

### Splitting up big components

If a component starts to get really big, make a folder and split it up into smaller files within that folder. Example:

![splitting up big components](https://i.imgur.com/VG48NKE.png)

## Styling

### Emotion

The project uses [Emotion](https://emotion.sh) for styling and theming, and also makes use of various helpers from [polished](https://polished.js.org/docs/).

### `styled` usage

Styling should be done first and foremost using the `styled` helper from emotion. Make sure you import it from `src/modules/theme/themes.ts` instead of `@emotion/styled`, so that the props are typechecked.

The top-level styled component should be called `Container`. Use `styled` for all element styling.

We also allow using one top-level `Container` styled component, and targeting children using class names, to ease migration from Sass. Still, prefer using `styled` elements instead.

Example:

```tsx
function Button() {
  return (
    <Container role="a">
      <Label>{...}</Label>
    </Container>
  )
}

const Container = styled.a`
  background-color: palevioletred;
`

const Label = styled.span`
  color: papayawhip;
`
```

```tsx
// The previous example is preferred, but we allow this form for ease of migration away from Sass
function Button() {
  return (
    <Container role="a">
      <span className="label">{...}</span>
    </Container>
  )
}

const Container = styled.a`
  background-color: palevioletred;

  .label {
    color: papayawhip;
  }
`
```

### Theming

Prefer using values from the theme if available. Remember to add `px` for spacing and font size values.

You can view the available themes at `src/modules/theme/themes.ts`. We've also written up various helper functions to make it easier to use theme values. You can import various theme helpers from `src/modules/themes/helpers/index.ts`. In the case you need to do something more advanced, access the theme object directly.

Example:

```ts
const Container = styled.main`
  background: ${getColor("background")};
  padding: ${getSpacing("2")}px;
  color: ${({ theme }) => lighten(0.1, theme.fontColors.normal)};
`
```

## Comments

Write a comment whenever you write some code where the intent of it might not be clear to others. The comment shouldn't describe _what_, but _why_. If the comment doesn't give any more information than the code itself, it may as well not be there.

If the comment is relevant to public use, use a `/** documentation comment */`, which will show up when the symbol is hovered over.

If the comment is an implementation detail, use a `// normal comment`.

Bad:

```ts
/** Get the public path */
const getPublicPath = (file: string) => {
  // join the file with the public string
  return join("/public", file)
}
```

Good:

```ts
/**
 * Get the public path for a given file,
 * here so that we only need to change one place if the public path changes
 */
const getPublicPath = (file: string) => {
  // use `join` from the path module, so that the path slashes are resolved properly
  return join("/public", file)
}
```

[yarn]: http://yarnpkg.org
[tslint]: https://palantir.github.io/tslint/
[typescript]: http://www.typescriptlang.org/
[vscode]: https://code.visualstudio.com/
[prettier]: https://prettier.io
