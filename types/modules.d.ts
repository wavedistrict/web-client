declare module "*.scss" {
  const classes: { [key: string]: string }
  export default classes
}

declare module "s-ago" {
  const timeago: (time: Date) => string
  export = timeago
}

declare module "stackblur" {
  const stackblur: (
    pixels: Uint8ClampedArray,
    width: number,
    height: number,
    radius: number,
  ) => void
  export = stackblur
}

declare module "fork-ts-checker-webpack-plugin"
declare module "html-webpack-inline-source-plugin"
declare module "webpack-serve"
declare module "webpack-pwa-manifest"
declare module "offline-plugin"
declare module "webpack-visualizer-plugin"
declare module "react-media"
