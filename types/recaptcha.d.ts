interface Window {
  onCaptchaLoadCallback?: () => void
  grecaptcha?: Recaptcha
}

interface Recaptcha {
  render: (element: HTMLElement, config: RecaptchaConfig) => void
  reset: () => void
}

interface RecaptchaConfig {
  sitekey: string
  callback: (data: CaptchaData) => void
  theme: string
  "expired-callback": () => void
}

interface CaptchaData {
  // ???
}
