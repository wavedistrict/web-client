# WaveDistrict

WaveDistrict was an audio sharing platform with clean, modern design, and an open-source web client. The project has been discontinued due to lack of engagement, but the code for the frontend is archived here.

- [Discord Server][discord]
- [Trello][trello]

## Building

Before running the client, make sure you have a browser with web security (CORS) disabled: https://alfilatov.com/posts/run-chrome-without-cors/
Because of cookie restrictions you will not be able to send requests if not.

To build and run the client:

1. Make sure you have [Node.JS](https://nodejs.org/en/) installed, version 8.7.0 or higher.
1. Clone the repository to a folder of your choosing (e.g. `git clone https://gitlab.com/wavedistrict/web-client.git`)
1. `cd` into that folder
1. Install the project dependencies by running `npm install`. It might take a while.
1. Run the dev server: `npm run dev`

## Development

If you'd like to contribute to WaveDistrict development, read [CONTRIBUTING.md](./CONTRIBUTING.md) for contribution guidelines.

If you have any further questions, [feel free to join the WaveDistrict Discord server and ask around.][discord] We'll help you get started 👍

[discord]: https://discord.gg/GV8qJSR
[trello]: https://trello.com/b/An8Mhmgp/wavedistrict
[website]: https://wavedistrict.com
