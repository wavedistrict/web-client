// @ts-check

/** @type {jest.InitialOptions} */
const config = {
  testMatch: ["**/*.test.(js|ts)?(x)"],
  moduleFileExtensions: ["js", "json", "jsx", "node", "ts", "tsx"],
  transform: {
    "\\.(js|ts)x?$": "babel-jest",
  },
  moduleNameMapper: {
    "^modules/(.*)": "<rootDir>/src/modules/$1",
    "^common/(.*)": "<rootDir>/src/common/$1",
    "\\.scss$": "<rootDir>/test/scssStub.ts",
  },
  testEnvironmentOptions: {
    runScripts: "dangerously",
    resources: "usable",
  },
  // @ts-ignore
  setupFilesAfterEnv: ["<rootDir>/test/setupTests.ts"],
}

module.exports = config
